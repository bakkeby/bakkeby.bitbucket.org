<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1498726846448" ID="ID_1498726846448958" MODIFIED="1498726846448" TEXT="tapp" COLOR="#a72db7">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846450" ID="ID_1498726846450696" MODIFIED="1498726846450" TEXT="tapp_lib 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726846450" ID="ID_1498726846451002" MODIFIED="1498726846451" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846451" ID="ID_1498726846451744" MODIFIED="1498726846451" TEXT="text_utils.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/text_utils.tcl"/>
<node CREATED="1498726846613" ID="ID_1498726846613255" MODIFIED="1498726846613" TEXT="table_utils.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/table_utils.tcl"/>
<node CREATED="1498726846835" ID="ID_1498726846835496" MODIFIED="1498726846835" TEXT="opt_list.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/opt_list.tcl"/>
<node CREATED="1498726846849" ID="ID_1498726846849840" MODIFIED="1498726846849" TEXT="opt_array.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/opt_array.tcl"/>
<node CREATED="1498726846970" ID="ID_1498726846970523" MODIFIED="1498726846970" TEXT="log.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/log.tcl"/>
<node CREATED="1498726847094" ID="ID_1498726847094873" MODIFIED="1498726847094" TEXT="lang.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/lang.tcl"/>
<node CREATED="1498726847306" ID="ID_1498726847306445" MODIFIED="1498726847306" TEXT="file_utils.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/file_utils.tcl"/>
<node CREATED="1498726847815" ID="ID_1498726847815553" MODIFIED="1498726847815" TEXT="cfg.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/cfg.tcl"/>
<node CREATED="1498726848048" ID="ID_1498726848048421" MODIFIED="1498726848048" TEXT="args.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/args.tcl"/>
</node>
<node CREATED="1498726846451" ID="ID_1498726846451972" MODIFIED="1498726846451" TEXT="text_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846452" ID="ID_1498726846452670" MODIFIED="1498726846452" TEXT="This deals with more or less common string manipulation tasks.&#xA;&#xA;Most of the procedures in this namespace are intended for logging purposes." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846453" ID="ID_1498726846453323" MODIFIED="1498726846453" TEXT="text_utils::a_or_an" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846455" ID="ID_1498726846455499" MODIFIED="1498726846455" TEXT="Just a simple proc that returns a or an depending on whether&#xA;the next work starts with a vowel or is thought to be an acronym.&#xA;&#xA;This is far from a fool-proof solution and will give the wrong&#xA;article for certain words like &quot;honour&quot; for example.&#xA;&#xA;@param subject         The following word&#xA;@return                a or an in lower case" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846458" ID="ID_1498726846458096" MODIFIED="1498726846458" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846481" ID="ID_1498726846481632" MODIFIED="1498726846481">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>a_or_an <font color="#FF0000">{</font> subject <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">^([</font>aeiou<font color="#990000">]|</font>FHLMNRSX<font color="#990000">[</font>A<font color="#990000">-</font>Z<font color="#990000">]|</font>x<font color="#990000">[^</font>aei<font color="#990000">])</font><font color="#FF0000">}</font> <font color="#009900">$subject</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#FF0000">{</font>an<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#FF0000">{</font>a<font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846482" ID="ID_1498726846482438" MODIFIED="1498726846482" TEXT="text_utils::bash_format" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846483" ID="ID_1498726846483099" MODIFIED="1498726846483" TEXT="Proc to add text formatting and colors for bash output.&#xA;&#xA;Example usage:&#xA;  set str &quot;The old man had lost his marbles&quot;&#xA;  puts [text_utils::bash_colorise -red $str]&#xA;&#xA;@param input           The string to add colours or format to&#xA;@option -bold          formatting option&#xA;@option -bright        formatting option&#xA;@option -dim           formatting option&#xA;@option -italic        formatting option&#xA;@option -underline     formatting option&#xA;@option -underlined    formatting option&#xA;@option -blink         formatting option&#xA;@option -normal        formatting option&#xA;@option -invert        formatting option&#xA;@option -reverse       formatting option&#xA;@option -hidden        formatting option&#xA;@option -invisible     formatting option&#xA;@option -strikethrough formatting option&#xA;@option -black         foreground color&#xA;@option -red           foreground color&#xA;@option -green         foreground color&#xA;@option -yellow        foreground color&#xA;@option -blue          foreground color&#xA;@option -purple        foreground color&#xA;@option -cyan          foreground color&#xA;@option -white         foreground color&#xA;@option -bg_black      background color&#xA;@option -bg_red        background color&#xA;@option -bg_green      background color&#xA;@option -bg_yellow     background color&#xA;@option -bg_blue       background color&#xA;@option -bg_purple     background color&#xA;@option -bg_cyan       background color&#xA;@option -bg_white      background color&#xA;@return                The string wrapped with bash escaped formatting codes" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846483" ID="ID_1498726846483597" MODIFIED="1498726846483" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846493" ID="ID_1498726846493886" MODIFIED="1498726846493">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>bash_format <font color="#FF0000">{</font> option input <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> bash_format_map
        <b><font color="#0000FF">set</font></b> option <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$option</font> <font color="#990000">-]]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists bash_format_map<font color="#990000">(</font><font color="#009900">$option</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"Unknown option $option passed to text_utils::bash_format"</font>
        <font color="#FF0000">}</font>
        lassign <font color="#009900">$bash_format_map</font><font color="#990000">(</font><font color="#009900">$option</font><font color="#990000">)</font> <b><font color="#0000FF">format</font></b> endformat
        <b><font color="#0000FF">return</font></b> <font color="#990000">\</font><font color="#993399">033</font><font color="#990000">\[</font>$<font color="#FF0000">{</font><b><font color="#0000FF">format</font></b><font color="#FF0000">}</font>m<font color="#009900">$input</font><font color="#990000">\</font><font color="#993399">033</font><font color="#990000">\[</font>$<font color="#FF0000">{</font>endformat<font color="#FF0000">}</font>m
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846494" ID="ID_1498726846494502" MODIFIED="1498726846494" TEXT="text_utils::capitalize_numbered_names" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846494" ID="ID_1498726846494916" MODIFIED="1498726846494" TEXT="Proc to capitalise numbered names such as WW2, FF6, etc.&#xA;&#xA;Any word not starting with letters and ending with digits will remain&#xA;unchanged.&#xA;&#xA;@param input           The string input to capitalise&#xA;@return                The string input with numbered names captialised" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846495" ID="ID_1498726846495280" MODIFIED="1498726846495" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846505" ID="ID_1498726846505412" MODIFIED="1498726846505">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>capitalize_numbered_names <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> words <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$input</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> word <font color="#009900">$words</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">^[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">]+[</font><font color="#993399">0</font><font color="#990000">-</font><font color="#993399">9</font><font color="#990000">]+</font>$<font color="#FF0000">}</font> <font color="#009900">$word</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> word <font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#009900">$word</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$word</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$output</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846506" ID="ID_1498726846506062" MODIFIED="1498726846506" TEXT="text_utils::convert_clock_format_to_regexp" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846506" ID="ID_1498726846506641" MODIFIED="1498726846506" TEXT="This proc attempts to generate a regular expression that should match&#xA;date/time output produced with the given format.&#xA;&#xA;Example usage:&#xA;  text_utils::convert_clock_format_to_regexp &quot;%Y-%m-%d %H&quot;&#xA;&#xA;would return:&#xA;  ([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[01]) ([0-1][0-9]|2[0-3])&#xA;&#xA;Warning: Any locale-dependent format representations may not necessarily&#xA;produce the right regular expression for all locales.&#xA;&#xA;@param format          The clock format to convert to a regular expression&#xA;@return                The regular expression matching the given format" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846506" ID="ID_1498726846506957" MODIFIED="1498726846506" TEXT="@see" FOLDED="true">
<node CREATED="1498726846507" ID="ID_1498726846507169" MODIFIED="1498726846507" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/clock.htm#M26" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/clock.htm#M26">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846507" ID="ID_1498726846507501" MODIFIED="1498726846507" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846519" ID="ID_1498726846519148" MODIFIED="1498726846519">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>convert_clock_format_to_regexp <font color="#FF0000">{</font> <b><font color="#0000FF">format</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">variable</font></b> format_group_regular_expressions

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$format_group_regular_expressions</font> <font color="#009900">$format</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846519" ID="ID_1498726846519740" MODIFIED="1498726846519" TEXT="text_utils::edify" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846520" ID="ID_1498726846520312" MODIFIED="1498726846520" TEXT="A rather simplistic approach to converting a verb into the ed form by using&#xA;lazy evaluation.&#xA;&#xA;The proc uses some basic rules to determine whether the verb should get an&#xA;additional consonant before the &quot;ed&quot; or if the the last character in the verb&#xA;should be removed.&#xA;&#xA;This proc was introduced for logging purposes to make the output more&#xA;readable. For example say we have an action called &quot;move&quot; and we want to log&#xA;&quot;moved x to y&quot; instead of the more sterile &quot;action 'move' from x to y&#xA;complete&quot;.&#xA;&#xA;Exaple conversions:&#xA;    action  ==&gt; actioned&#xA;    move    ==&gt; moved&#xA;    play    ==&gt; played&#xA;    plan    ==&gt; planned&#xA;    execute ==&gt; executed&#xA;    stop    ==&gt; stopped&#xA;&#xA;This is obviously only intended for the English language.&#xA;&#xA;The approach here is generic and exceptions like &quot;swam&quot; or &quot;read&quot; are not&#xA;considered.&#xA;&#xA;@param verb            A given verb / string&#xA;@return                The same string with &quot;ed&quot; appended" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846520" ID="ID_1498726846520675" MODIFIED="1498726846520" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846533" ID="ID_1498726846533370" MODIFIED="1498726846533">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>edify <font color="#FF0000">{</font> verb <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> verb <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$verb</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> verb <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><b><font color="#0000FF">exec</font></b> execute<font color="#FF0000">}</font> <font color="#009900">$verb</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> ed <font color="#FF0000">{</font>ed<font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font>e$<font color="#FF0000">}</font> <font color="#009900">$verb</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> ed <font color="#FF0000">{</font>d<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">([</font>pn<font color="#990000">])</font>$<font color="#FF0000">}</font> <font color="#009900">$verb</font> match last<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> ed $<font color="#FF0000">{</font>last<font color="#FF0000">}{</font>ed<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> $<font color="#FF0000">{</font>verb<font color="#FF0000">}</font>$<font color="#FF0000">{</font>ed<font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846533" ID="ID_1498726846533949" MODIFIED="1498726846533" TEXT="text_utils::index_replace" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846534" ID="ID_1498726846534487" MODIFIED="1498726846534" TEXT="Replace every index in a given string with a given char.&#xA;&#xA;@param string          The base string&#xA;@param indexes         The indexes in the given string to replace with the&#xA;                       given character (as in string index)&#xA;@param chars           The character (or characters) to replace the given&#xA;                       indexes with, or alternatively a list of values to&#xA;                       be distributed across the indexes respectively. If&#xA;                       there are less values than indexes then the last&#xA;                       value will be repeated for the remaining indexes.&#xA;@return                The modified string" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846534" ID="ID_1498726846534866" MODIFIED="1498726846534" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846547" ID="ID_1498726846547535" MODIFIED="1498726846547">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>index_replace <font color="#FF0000">{</font> <b><font color="#0000FF">string</font></b> indexes chars <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        touch char
        
        <b><font color="#0000FF">set</font></b> length <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$indexes</font><font color="#990000">]</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$indexes</font> <font color="#009900">$i</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$chars</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> char <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$chars</font> <font color="#009900">$i</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> csize <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$char</font><font color="#990000">]</font> <font color="#990000">-</font> <font color="#993399">1</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">string</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> replace <font color="#009900">$string</font> <font color="#009900">$idx</font> <font color="#009900">$idx</font><font color="#990000">+</font><font color="#009900">$csize</font> <font color="#009900">$char</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$string</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846548" ID="ID_1498726846548109" MODIFIED="1498726846548" TEXT="text_utils::ingify" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846548" ID="ID_1498726846548669" MODIFIED="1498726846548" TEXT="A rather simplistic approach to converting a verb into the ing form by using&#xA;lazy evaluation.&#xA;&#xA;The proc uses some basic rules to determine whether the verb should get an&#xA;additional consonant before the ing or if the the last character in the verb&#xA;should be removed.&#xA;&#xA;This proc was primarily introduced for logging purposes to make the output&#xA;more readable. For example say we have an action called &quot;move&quot; and we want to&#xA;log &quot;moving x to y&quot; instead of the more sterile &quot;action 'move' from x to y&quot;.&#xA;&#xA;Exaple conversions:&#xA;    action  ==&gt; actioning&#xA;    move    ==&gt; moving&#xA;    play    ==&gt; playing&#xA;    swim    ==&gt; swimming&#xA;    plan    ==&gt; planning&#xA;    execute ==&gt; executing&#xA;    read    ==&gt; reading&#xA;&#xA;This is obviously only intended for the English language.&#xA;&#xA;The approach here is generic and will not work for all possible verbs.&#xA;&#xA;@param verb            A given verb / string&#xA;@return                The same string with &quot;ing&quot; appended" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846549" ID="ID_1498726846549026" MODIFIED="1498726846549" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846559" ID="ID_1498726846559941" MODIFIED="1498726846559">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>ingify <font color="#FF0000">{</font> verb <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> verb <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$verb</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> verb <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><b><font color="#0000FF">exec</font></b> execute<font color="#FF0000">}</font> <font color="#009900">$verb</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> ing <font color="#FF0000">{</font>ing<font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font>e$<font color="#FF0000">}</font> <font color="#009900">$verb</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">regsub</font></b> <font color="#FF0000">{</font>e$<font color="#FF0000">}</font> <font color="#009900">$verb</font> <font color="#009900">$ing</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">[^</font>ae<font color="#990000">]([</font>pnmt<font color="#990000">])</font>$<font color="#FF0000">}</font> <font color="#009900">$verb</font> match last<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> $<font color="#FF0000">{</font>verb<font color="#FF0000">}</font>$<font color="#FF0000">{</font>last<font color="#FF0000">}</font>$<font color="#FF0000">{</font>ing<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> $<font color="#FF0000">{</font>verb<font color="#FF0000">}</font>$<font color="#FF0000">{</font>ing<font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846560" ID="ID_1498726846560524" MODIFIED="1498726846560" TEXT="text_utils::pad" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846560" ID="ID_1498726846560965" MODIFIED="1498726846560" TEXT="Splits text into lines and adds padding on either the right or left side of&#xA;each line, then joins and returns the text.&#xA;&#xA;@param text            The text to add padding to&#xA;@param padding         What to pad the text with&#xA;@param side            What side of each line to add the padding to, either&#xA;                       left or right (defaults to left)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846561" ID="ID_1498726846561324" MODIFIED="1498726846561" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846571" ID="ID_1498726846571291" MODIFIED="1498726846571">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>pad <font color="#FF0000">{</font> text padding <font color="#FF0000">{</font> side left <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$side</font> <font color="#990000">==</font> <font color="#FF0000">{</font>left<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> text <font color="#009900">$padding</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">\</font>n <font color="#990000">\</font>n<font color="#009900">$padding</font><font color="#990000">]</font> <font color="#009900">$text</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> text <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">\</font>n <font color="#009900">$padding</font><font color="#990000">\</font>n<font color="#990000">]</font> <font color="#009900">$text</font><font color="#990000">]</font><font color="#009900">$padding</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$text</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846571" ID="ID_1498726846571869" MODIFIED="1498726846571" TEXT="text_utils::string_capitalize" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846572" ID="ID_1498726846572343" MODIFIED="1498726846572" TEXT="Proc to capitalise every word in a string.&#xA;&#xA;If you need to capitalise only the first word in a sentence then have a look&#xA;at &quot;string totitle&quot;.&#xA;&#xA;For example:&#xA;  set str &quot;The cunning black crow tricked the lazy dog&quot;&#xA;  text_utils::string_capitalize $str&#xA;&#xA;would return:&#xA;  The Cunning Black Crow Tricked The Lazy DogS&#xA;&#xA;@param input           The string to capitalise&#xA;@return                The capitalised string" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846572" ID="ID_1498726846572685" MODIFIED="1498726846572" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846582" ID="ID_1498726846582299" MODIFIED="1498726846582">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>string_capitalize <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> escaped <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#990000">\[</font> <font color="#990000">\\[</font> <font color="#990000">\]</font> <font color="#990000">\\]</font> <font color="#990000">\</font>$ <font color="#990000">\\</font>$ <font color="#990000">\\</font> <font color="#990000">\\\\</font><font color="#FF0000">}</font> <font color="#009900">$input</font><font color="#990000">]</font>
        <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#FF0000">{</font><font color="#990000">([</font>a<font color="#990000">-</font>zA<font color="#990000">-</font>Z<font color="#FF0000">']+)} $escaped {[string totitle &amp;]} result</font>
<font color="#FF0000">        return [subst $result]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846582" ID="ID_1498726846582866" MODIFIED="1498726846582" TEXT="text_utils::text_to_dict" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846583" ID="ID_1498726846583265" MODIFIED="1498726846583" TEXT="Converts a key-value pairs as text into a dict for lookup purposes.&#xA;&#xA;@param text            The text to parse&#xA;@param separator       The key-value separator, defaults to equals sign ( = )" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846583" ID="ID_1498726846583621" MODIFIED="1498726846583" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846594" ID="ID_1498726846594270" MODIFIED="1498726846594">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>text_to_dict <font color="#FF0000">{</font> text <font color="#FF0000">{</font>separator <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>keytolower <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> data <font color="#990000">[</font>dict create<font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> line <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$text</font> <font color="#990000">\</font>n<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> eq_idx <font color="#990000">[</font><b><font color="#0000FF">string</font></b> first <font color="#009900">$separator</font> <font color="#009900">$line</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$eq_idx</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># No key-value pairs found on this line</font></i>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> key <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$line</font> <font color="#993399">0</font> <font color="#009900">$eq_idx</font><font color="#990000">-</font><font color="#993399">1</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> val <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$line</font> <font color="#009900">$eq_idx</font><font color="#990000">+</font><font color="#993399">1</font> end<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$keytolower</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> key <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$key</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                dict <b><font color="#0000FF">set</font></b> data <font color="#009900">$key</font> <font color="#009900">$val</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$data</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846594" ID="ID_1498726846594885" MODIFIED="1498726846594" TEXT="text_utils::wrap_text" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846595" ID="ID_1498726846595409" MODIFIED="1498726846595" TEXT="Procedure to wrap text based on whole words.&#xA;&#xA;This can be used to optimise output to standard out by applying word wrap to&#xA;the output before printing. The advantage of this is that the output may&#xA;generally look better as the console wrapping will split words.&#xA;&#xA;Lines are separated by the newline character.&#xA;&#xA;@param text            The text to apply word wrapping to&#xA;@param text_width      The maximum width of the text&#xA;@param word_separator  The character or string that separates each word,&#xA;                       defaults to the space character&#xA;@return                The word wrapped string" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846595" ID="ID_1498726846595791" MODIFIED="1498726846595" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846606" ID="ID_1498726846606301" MODIFIED="1498726846606">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> text_utils<font color="#990000">::</font>wrap_text <font color="#FF0000">{</font>text text_width <font color="#FF0000">{</font>word_separator <font color="#FF0000">{</font> <font color="#FF0000">}}}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> line <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$text</font> <font color="#990000">\</font>n<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">&lt;=</font> <font color="#009900">$text_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$line</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">foreach</font></b> line_part <font color="#990000">[</font>_line_split <font color="#009900">$line</font> <font color="#009900">$text_width</font> <font color="#009900">$word_separator</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$line_part</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$output</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726846613" ID="ID_1498726846613462" MODIFIED="1498726846613" TEXT="table_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846613" ID="ID_1498726846613739" MODIFIED="1498726846613" TEXT="This package deals with common tcl two-dimensional list tables (lists within lists)." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846614" ID="ID_1498726846614144" MODIFIED="1498726846614" TEXT="table_utils::add_ascii_table_preset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846614" ID="ID_1498726846614472" MODIFIED="1498726846614" TEXT="Add application specific ASCII table presets for convenience.&#xA;&#xA;@param name            The name of the preset to set&#xA;@param args            The default parameters the preset should use" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846614" ID="ID_1498726846614745" MODIFIED="1498726846614" TEXT="@see" FOLDED="true">
<node CREATED="1498726846614" ID="ID_1498726846614935" MODIFIED="1498726846614" TEXT="table_utils::convert_to_ascii_table for available parameters" COLOR="#14666b" LINK="#ID_1498726846627979">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846615" ID="ID_1498726846615223" MODIFIED="1498726846615" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846627" ID="ID_1498726846627365" MODIFIED="1498726846627">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> table_utils<font color="#990000">::</font>add_ascii_table_preset <font color="#FF0000">{</font> name args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> ASCII_TABLE_PRESETS
        <b><font color="#0000FF">set</font></b> ASCII_TABLE_PRESETS<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#009900">$args</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846627" ID="ID_1498726846627979" MODIFIED="1498726846627" TEXT="table_utils::convert_to_ascii_table" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846629" ID="ID_1498726846629423" MODIFIED="1498726846629" TEXT="Create an ASCII table from a multidimensional list for display purposes.&#xA;&#xA;Example usage:&#xA;   table_utils::convert_to_ascii_table [list\ &#xA;           {Col1 Col2 Col3}\&#xA;           {Val1 Val2 Val3}\&#xA;           {Val4 Val5 Val6}\&#xA;   ]&#xA;&#xA;This will return:&#xA;   |------|------|------|&#xA;   | Col1 | Col2 | Col3 |&#xA;   |------|------|------|&#xA;   | Val1 | Val2 | Val3 |&#xA;   |------|------|------|&#xA;   | Val4 | Val5 | Val6 |&#xA;   |------|------|------|&#xA;&#xA;The below tries to explain what parameters control what aspect of the table&#xA;generation.  When specifying the characters to use if less than four&#xA;characters are specified in then the rest will default accordingly.&#xA;&#xA;     ‚----------------------------------‚--  --vb 1&#xA;    /                                  /&#xA;   /=====================\            /&#xA;   X   Table Title       Z           /&#xA;   /==========@==========@==========\        --hb 1&#xA;   «   Col1   Y   Col2   Y   Col3   »        --header 1&#xA;   X..........?..........?..........Z        --hlheader 1&#xA;   &lt;:--------:!:---------!---------:&gt;        --alignment_row 1&#xA;   (~~~~~~~~~~o~~~~~~~~~~o~~~~~~~~~~)        --hl 1&#xA;   &lt;   Val1   !   Val2   !   Val3   &gt;&#xA;   (~~~~~~~~~~o~~~~~~~~~~o~~~~~~~~~~)        --hl 1&#xA;   &lt;   Val4   !   Val5   !   Val6   &gt;&#xA;   \__________U__________U__________/        --hb 1&#xA;               \          \      \\\&#xA;                \          \       \&#xA;                 \          \       ‛------  --vpadding 3&#xA;                  ‛----------‛-------------  --vl 1&#xA;&#xA;The parameters needed to generate the above:&#xA;   --alignment_row 1&#xA;   --col_align {c l r}&#xA;   --colchars {! o @ U}&#xA;   --rowchars {~ o ( )}&#xA;   --borderchars {= _ &lt; &gt;}&#xA;   --headerrowchars {. ? X Z}&#xA;   --headercolchars {Y {} « »}&#xA;   --edgechars {/ \\ \\ /}&#xA;   --title {Table Title}&#xA;   --title_colspan 2&#xA;   --vpadding 3&#xA;&#xA;@param table                         The multidimensional list to generate&#xA;                                     the ASCII table from&#xA;@param args                          Optional formatting arguments&#xA;@option --header &lt;1/0&gt;               Indicates whether a header is present in&#xA;                                     the table or not, defaults to 1&#xA;@option --title &lt;string&gt;             Add an optional title at the top of the&#xA;                                     table&#xA;@option --title_colspan &lt;number&gt;     The number of columns that the title&#xA;                                     should span. Defaults to all columns.&#xA;@option --border &lt;1/0&gt;               Indicates whether ASCII border (vertical&#xA;                                     and horizontal) is enabled, defaults to&#xA;                                     enabled&#xA;@option --hb &lt;1/0&gt;                   Indicates whether horizontal border is&#xA;                                     enabled or not, defaults to border value&#xA;@option --vb &lt;1/0&gt;                   Indicates whether vertical border is&#xA;                                     enabled or not, defaults to border value&#xA;@option --hl &lt;1/0&gt;                   Indicates whether to include horizontal&#xA;                                     line separator or not, defaults to&#xA;                                     enabled&#xA;@option --hlheader &lt;1/0&gt;             Indicates whether to include a special&#xA;                                     horizontal line separator for the header&#xA;                                     or not, defaults to enabled&#xA;@option --alignment_row &lt;1/0&gt;        Indicates that a special horizontal&#xA;                                     alignment row should be inserted.&#xA;                                     Required for certain table formats such&#xA;                                     as the markdown syntax used on github&#xA;                                     and bitbucket.&#xA;@option --col_align &lt;list&gt;           List indicating how each column is&#xA;                                     aligned. Used by the special horizontal&#xA;                                     line separator if applicable.&#xA;                                     The values are limited to: Left,&#xA;                                     Right and Center (l, r, c and empty&#xA;                                     string)&#xA;@option --vl &lt;1/0&gt;                   Indicates whether to include vertical&#xA;                                     line separator or not, defaults to&#xA;                                     enabled&#xA;@option --borderchars &lt;list&gt;         Specifies the characters used for the&#xA;                                     border. The list is on the form: &quot;&lt;top&gt;&#xA;                                     &lt;bottom&gt; &lt;left&gt; &lt;right&gt;&quot;&#xA;@option --edgechars &lt;list&gt;           Specifies the edge characters where the&#xA;                                     borders meet. The list is on the form:&#xA;                                     &quot;&lt;top left&gt; &lt;top right&gt; &lt;bottom left&gt;&#xA;                                     &lt;bottom right&gt;&quot;.&#xA;@option --rowchars &lt;list&gt;            Specifies the row separator characters&#xA;                                     used. The list is on the form: &quot;&lt;row&#xA;                                     character&gt; &lt;column intersection&gt; &lt;left&#xA;                                     border&gt; &lt;right border&gt;&quot;. The border&#xA;                                     characters overrides the main border.&#xA;@option --colchars &lt;list&gt;            Specifies the column separator&#xA;                                     characters used. The list is on the&#xA;                                     form: &quot;&lt;column character&gt; &lt;row&#xA;                                     intersection&gt; &lt;top border&gt; &lt;bottom&#xA;                                     border&gt;&quot;. The border characters&#xA;                                     overrides the main border. The second&#xA;                                     value is the same as rowchars second&#xA;                                     value as they intersect. If both are&#xA;                                     set then the colchars value takes&#xA;                                     precedence.&#xA;@option --headerrowchars &lt;list&gt;      As --rowchars, but just applies to the&#xA;                                     header row&#xA;@option --headercolchars &lt;list&gt;      As --colchars, but just applies to the&#xA;                                     header row&#xA;@option --alignchars &lt;list&gt;          Specifies the alignment characters used.&#xA;                                     Defaults to {- :}&#xA;@option --vpadding &lt;1/0&gt;             Indicates whether to include padding on&#xA;                                     the right and left sides of the vertical&#xA;                                     ASCII lines (space character). Value&#xA;                                     represents the number of spaces used.&#xA;                                     Defaults to being enabled.&#xA;@option --hpadding &lt;1/0&gt;             Indicates whether to include padding on&#xA;                                     the top and bottom sides of the&#xA;                                     horizontal ASCII lines. Value represents&#xA;                                     the number of spaces (rows) used.&#xA;                                     Defaults to being disabled.&#xA;@return                              The data in text format with an ASCII&#xA;                                     border" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846630" ID="ID_1498726846630201" MODIFIED="1498726846630" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846670" ID="ID_1498726846670054" MODIFIED="1498726846670">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> table_utils<font color="#990000">::</font>convert_to_ascii_table <font color="#FF0000">{</font> data args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> ASCII_TABLE_PRESETS

        let <font color="#009900">$args</font> <font color="#FF0000">{</font> preset <font color="#FF0000">{}</font> <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$preset</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists ASCII_TABLE_PRESETS<font color="#990000">(</font><font color="#009900">$preset</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> args <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$ASCII_TABLE_PRESETS</font><font color="#990000">(</font><font color="#009900">$preset</font><font color="#990000">)</font> <font color="#009900">$args</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                header           <font color="#993399">1</font>
                border           <font color="#993399">1</font>
                hb               <font color="#009900">$border</font>
                vb               <font color="#009900">$border</font>
                title            <font color="#FF0000">{}</font>
                title_colspan    all
                hl               <font color="#993399">1</font>
                hlheader         <font color="#993399">1</font>
                alignment_row    <font color="#993399">0</font>
                col_align        <font color="#FF0000">{}</font>
                vl               <font color="#993399">1</font>
                vpadding         <font color="#993399">1</font>
                hpadding         <font color="#993399">0</font>
                rowchars         <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font>
                colchars         <font color="#FF0000">{</font><font color="#990000">|</font><font color="#FF0000">}</font>
                borderchars      <font color="#FF0000">{}</font>
                edgechars        <font color="#FF0000">{}</font>
                headerrowchars   <font color="#FF0000">{}</font>
                headercolchars   <font color="#FF0000">{}</font>
                alignchars       <font color="#FF0000">{</font><font color="#990000">-</font> <font color="#990000">:</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> hb <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$hb</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> vb <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$vb</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$rowchars</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> hl <font color="#993399">0</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$colchars</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> vl <font color="#993399">0</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Defaults</font></i>
        ldefault colchars       <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">1</font><font color="#990000">]</font>    <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault colchars       <font color="#993399">2</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault colchars       <font color="#993399">3</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">2</font><font color="#990000">]</font>
        ldefault borderchars    <font color="#993399">0</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault borderchars    <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault borderchars    <font color="#993399">2</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault borderchars    <font color="#993399">3</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault rowchars       <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">1</font><font color="#990000">]</font>    <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault rowchars       <font color="#993399">2</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">2</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">2</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault rowchars       <font color="#993399">3</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">2</font><font color="#990000">]</font>
        ldefault headerrowchars <font color="#993399">0</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault headerrowchars <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">1</font><font color="#990000">]</font>
        ldefault headerrowchars <font color="#993399">2</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">2</font><font color="#990000">]</font>
        ldefault headerrowchars <font color="#993399">3</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">3</font><font color="#990000">]</font>
        ldefault headercolchars <font color="#993399">0</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        ldefault headercolchars <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">1</font><font color="#990000">]</font>
        ldefault headercolchars <font color="#993399">2</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">2</font><font color="#990000">]</font>
        ldefault headercolchars <font color="#993399">3</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">3</font><font color="#990000">]</font>
        ldefault edgechars      <font color="#993399">3</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">3</font><font color="#990000">]</font>
        ldefault edgechars      <font color="#993399">2</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">2</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">2</font><font color="#990000">]</font>
        ldefault edgechars      <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">3</font><font color="#990000">]</font>
        ldefault edgechars      <font color="#993399">0</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">2</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">2</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$vb</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> <b><font color="#0000FF">list</font></b> <font color="#FF0000">{</font>borderchars headerrowchars headercolchars rowchars colchars<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lset</font></b> <font color="#009900">$list</font> <font color="#993399">2</font> <font color="#FF0000">{}</font>
                        <b><font color="#0000FF">lset</font></b> <font color="#009900">$list</font> <font color="#993399">3</font> <font color="#FF0000">{}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> edgechars <font color="#990000">[</font>lrepeat <font color="#993399">4</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> col_sizes <font color="#990000">[</font>table_utils<font color="#990000">::</font>get_column_sizes <font color="#009900">$data</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> max_cols <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$col_sizes</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># Default column alignment</font></i>
        <b><font color="#0000FF">while</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$col_align</font><font color="#990000">]</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> col_align <font color="#FF0000">{</font>l<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Adds a table title</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$title</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$title_colspan</font> <font color="#990000">==</font> <font color="#FF0000">{</font>all<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> title_colspan <font color="#009900">$max_cols</font>
                <font color="#FF0000">}</font>
                
                <i><font color="#9A1900"># Work out the required length for the title</font></i>
                <b><font color="#0000FF">set</font></b> required_length <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$title</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#009900">$vpadding</font> <font color="#990000">*</font> <font color="#993399">2</font> <font color="#990000">+</font> <font color="#009900">$vb</font> <font color="#990000">*</font> <font color="#993399">2</font> <font color="#990000">-</font> <font color="#993399">1</font><font color="#FF0000">}</font><font color="#990000">]</font>
                
                <i><font color="#9A1900"># Check whether the title will fit inside the given colspan</font></i>
                <b><font color="#0000FF">set</font></b> colspan_size <font color="#993399">0</font>
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$title_colspan</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> col_length <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#009900">$vl</font> <font color="#990000">+</font> <font color="#009900">$vpadding</font> <font color="#990000">*</font> <font color="#990000">(</font><font color="#009900">$vl</font> <font color="#990000">+</font> <font color="#993399">1</font><font color="#990000">)</font><font color="#FF0000">}</font><font color="#990000">]</font>
                        <b><font color="#0000FF">incr</font></b> colspan_size <font color="#009900">$col_length</font>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># If the title doesn't fit, expand the last column in the</font></i>
                <i><font color="#9A1900"># colspan so that it fits</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$colspan_size</font> <font color="#990000">&lt;</font> <font color="#009900">$required_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        decr i
                        <b><font color="#0000FF">set</font></b> new_size <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$col_length</font> <font color="#990000">+</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$title</font><font color="#990000">]</font> <font color="#990000">-</font> <font color="#009900">$colspan_size</font><font color="#FF0000">}</font><font color="#990000">]</font>
                        <b><font color="#0000FF">lset</font></b> col_sizes <font color="#009900">$i</font> <font color="#009900">$new_size</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> required_length <font color="#009900">$colspan_size</font>
                <font color="#FF0000">}</font>

                decr required_length
                <b><font color="#0000FF">set</font></b> title_length <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$required_length</font> <font color="#990000">-</font> <font color="#009900">$vpadding</font> <font color="#990000">*</font> <font color="#993399">2</font><font color="#FF0000">}</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>left_border<font color="#FF0000">}</font>$<font color="#FF0000">{</font>padding<font color="#FF0000">}</font>$<font color="#FF0000">{</font>data_0<font color="#FF0000">}</font>$<font color="#FF0000">{</font>padding<font color="#FF0000">}</font>$<font color="#FF0000">{</font>right_border<font color="#FF0000">}}</font>
                
                <i><font color="#9A1900"># Adds the border for the title, if applicable</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hb</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> left_border  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">0</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">1</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> padding <font color="#FF0000">{}</font>
                        <b><font color="#0000FF">set</font></b> data_0 <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$required_length</font><font color="#990000">]</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">2</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> data_0 <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#990000">%-</font>$<font color="#FF0000">{</font>title_length<font color="#FF0000">}</font>s <font color="#009900">$title</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Set up the generic format string that will later be substituted</font></i>
        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>left_border<font color="#FF0000">}}</font>
        <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>padding<font color="#FF0000">}}</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#990000">\</font>$<font color="#990000">\</font><font color="#FF0000">{</font>data_<font color="#009900">$i</font><font color="#990000">\</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">incr</font></b> i
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$vl</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>padding<font color="#FF0000">}}</font>
                                <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>separator<font color="#FF0000">}}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>padding<font color="#FF0000">}}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>padding<font color="#FF0000">}}</font>
        <b><font color="#0000FF">append</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>$<font color="#FF0000">{</font>right_border<font color="#FF0000">}}</font>

        <i><font color="#9A1900"># Adds top border</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hb</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">2</font><font color="#990000">]</font>

                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">1</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> skip_first_horizontal_line <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Adds header</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$header</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> header <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$data</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> data <font color="#990000">[</font><b><font color="#0000FF">lrange</font></b> <font color="#009900">$data</font> <font color="#993399">1</font> end<font color="#990000">]</font>
                
                <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headercolchars</font> <font color="#993399">2</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headercolchars</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#990000">%-[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]</font>s <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$header</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headercolchars</font> <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        
                <i><font color="#9A1900"># Adds header horizontal line separator</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hlheader</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">2</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">1</font><font color="#990000">]</font>

                        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$headerrowchars</font> <font color="#993399">3</font><font color="#990000">]</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> skip_first_horizontal_line <font color="#993399">1</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Adds special alignment row used by certain formats, such as the markdown syntax</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$alignment_row</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                touch header_cols
                <b><font color="#0000FF">foreach</font></b> col_length <font color="#009900">$col_sizes</font> align <font color="#009900">$col_align</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">incr</font></b> col_length <font color="#009900">$vpadding</font>
                        <b><font color="#0000FF">incr</font></b> col_length <font color="#009900">$vpadding</font>
                        <b><font color="#0000FF">set</font></b> dashes <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$alignchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$col_length</font><font color="#990000">]</font>
                        <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#990000">--</font> <font color="#009900">$align</font> <font color="#FF0000">{</font>
                        l<font color="#990000">*</font> <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> dashes <font color="#990000">[</font>index_replace <font color="#009900">$dashes</font> <font color="#993399">0</font>   <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$alignchars</font> <font color="#993399">1</font><font color="#990000">]]</font> <font color="#FF0000">}</font>
                        r<font color="#990000">*</font> <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> dashes <font color="#990000">[</font>index_replace <font color="#009900">$dashes</font> end <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$alignchars</font> <font color="#993399">1</font><font color="#990000">]]</font> <font color="#FF0000">}</font>
                        m<font color="#990000">*</font> <font color="#990000">-</font>
                        c<font color="#990000">*</font> <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> dashes <font color="#990000">[</font>index_replace <font color="#009900">$dashes</font> <font color="#FF0000">{</font><font color="#993399">0</font> end<font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$alignchars</font> <font color="#993399">1</font><font color="#990000">]]</font> <font color="#FF0000">}</font>
                        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> header_cols <font color="#009900">$dashes</font>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">2</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#FF0000">{}</font>
                <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>

                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#990000">%-[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]</font>s <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$header_cols</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> skip_first_horizontal_line <font color="#993399">0</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Prepares the horizontal line if relevant</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hl</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">2</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">1</font><font color="#990000">]</font>

                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$rowchars</font> <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> horizontal_line <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Prepeares the horizontal padding line if relevant</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hpadding</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> hpadding_line <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">2</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">3</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># Adds data rows to the table</font></i>
        <b><font color="#0000FF">foreach</font></b> row <font color="#009900">$data</font> <font color="#FF0000">{</font>
        
                <i><font color="#9A1900"># Adds horizontal line separator</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hl</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">!</font><font color="#009900">$skip_first_horizontal_line</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> h <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$h</font> <font color="#990000">&lt;</font> <font color="#009900">$hpadding</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> h<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$hpadding_line</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$horizontal_line</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> h <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$h</font> <font color="#990000">&lt;</font> <font color="#009900">$hpadding</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> h<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$hpadding_line</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> skip_first_horizontal_line <font color="#993399">0</font>
                
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#990000">--</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_align</font> <font color="#009900">$i</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                        m<font color="#990000">*</font> <font color="#990000">-</font>
                        c<font color="#990000">*</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> col_size <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]</font>
                                <b><font color="#0000FF">set</font></b> txt_size <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$row</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                                <b><font color="#0000FF">set</font></b> pad_size <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">(</font><font color="#009900">$col_size</font> <font color="#990000">-</font> <font color="#009900">$txt_size</font><font color="#990000">)</font> <font color="#990000">/</font> <font color="#993399">2</font><font color="#FF0000">}</font><font color="#990000">]</font>
                                decr col_size <font color="#009900">$pad_size</font>
                                <b><font color="#0000FF">set</font></b> align <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$pad_size</font><font color="#990000">]%-</font>$<font color="#FF0000">{</font>col_size<font color="#FF0000">}</font>s
                        <font color="#FF0000">}</font>
                        r<font color="#990000">*</font>      <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> align <font color="#990000">%[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]</font>s  <font color="#FF0000">}</font>
                        l<font color="#990000">*</font> <font color="#990000">-</font>
                        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> align <font color="#990000">%-[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]</font>s <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$align</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$row</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> h <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$h</font> <font color="#990000">&lt;</font> <font color="#009900">$hpadding</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> h<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$hpadding_line</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Adds bottom border</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$hb</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> left_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">2</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#009900">$vpadding</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> separator <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$colchars</font> <font color="#993399">3</font><font color="#990000">]</font>

                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> data_<font color="#009900">$i</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$borderchars</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$col_sizes</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> right_border <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$edgechars</font> <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$output</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846670" ID="ID_1498726846671008" MODIFIED="1498726846671" TEXT="table_utils::determine_number_of_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846671" ID="ID_1498726846671441" MODIFIED="1498726846671" TEXT="Determine the number of columns in a table.&#xA;&#xA;Typically a table contains an even number of columns, but in the case&#xA;where they are not this proc can be used to find out what is the maximum&#xA;number of columns in the given table.&#xA;&#xA;@param table           The data in list format&#xA;@return                The maximum number of columns in the table" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846671" ID="ID_1498726846671794" MODIFIED="1498726846671" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846682" ID="ID_1498726846682756" MODIFIED="1498726846682">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> table_utils<font color="#990000">::</font>determine_number_of_columns <font color="#FF0000">{</font> table <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> max_cols <font color="#993399">0</font>
        <b><font color="#0000FF">foreach</font></b> line <font color="#009900">$table</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> cols <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$line</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$cols</font> <font color="#990000">&gt;</font> <font color="#009900">$max_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> max_cols <font color="#009900">$cols</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$max_cols</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846683" ID="ID_1498726846683373" MODIFIED="1498726846683" TEXT="table_utils::get_column_sizes" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846683" ID="ID_1498726846683792" MODIFIED="1498726846683" TEXT="Procedure to get a list of max column sizes for a list table (list of lists).&#xA;&#xA;@param data            A list containing lists representing rows in a table&#xA;@return                A list of maximum column sizes" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846684" ID="ID_1498726846684145" MODIFIED="1498726846684" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846694" ID="ID_1498726846694866" MODIFIED="1498726846694">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> table_utils<font color="#990000">::</font>get_column_sizes <font color="#FF0000">{</font> data <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> cols <font color="#FF0000">{}</font>
        
        <i><font color="#9A1900"># determine max column lengths</font></i>
        <b><font color="#0000FF">foreach</font></b> row <font color="#009900">$data</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> col_count <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$row</font><font color="#990000">]</font>
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$cols</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$col_count</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> cols <font color="#FF0000">{</font><font color="#993399">0</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$col_count</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> col_length <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$row</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$cols</font> <font color="#009900">$i</font><font color="#990000">]</font> <font color="#990000">&lt;</font> <font color="#009900">$col_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lset</font></b> cols <font color="#009900">$i</font> <font color="#009900">$col_length</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$cols</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846695" ID="ID_1498726846695461" MODIFIED="1498726846695" TEXT="table_utils::identify_empty_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846695" ID="ID_1498726846695889" MODIFIED="1498726846695" TEXT="Process to identify columns that are empty in a tcl list table.&#xA;&#xA;@param table             The tcl list table to search through&#xA;@param ignore_first_row  Indicates whether to ignore the first row when&#xA;                         running the check. This is typically used when&#xA;                         the table contains a header.&#xA;@return                  The list indexes of the columns that were found to&#xA;                         be empty" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846696" ID="ID_1498726846696251" MODIFIED="1498726846696" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846707" ID="ID_1498726846707338" MODIFIED="1498726846707">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> table_utils<font color="#990000">::</font>identify_empty_columns <font color="#FF0000">{</font> table <font color="#FF0000">{</font> ignore_first_row <font color="#993399">0</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <i><font color="#9A1900"># Determining the maximum number of columns</font></i>
        <b><font color="#0000FF">set</font></b> num_cols <font color="#993399">0</font>
        <b><font color="#0000FF">foreach</font></b> row <font color="#009900">$table</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$row</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#009900">$num_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> num_cols <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$row</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$ignore_first_row</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> table <font color="#990000">[</font><b><font color="#0000FF">lreplace</font></b> <font color="#009900">$table</font> <font color="#993399">0</font> <font color="#993399">0</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        touch output
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$num_cols</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> content_found <font color="#993399">0</font>
                <b><font color="#0000FF">foreach</font></b> row <font color="#009900">$table</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$row</font> <font color="#009900">$i</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> content_found <font color="#993399">1</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$content_found</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$i</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846707" ID="ID_1498726846707934" MODIFIED="1498726846707" TEXT="table_utils::remove_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846708" ID="ID_1498726846708330" MODIFIED="1498726846708" TEXT="Process to remove columns from a given tcl list table.&#xA;&#xA;@param table           The tcl list table to remove columns from&#xA;@param indexes         The indexes of the columns to remove&#xA;@return                The table with the columns removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846708" ID="ID_1498726846708678" MODIFIED="1498726846708" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846718" ID="ID_1498726846718804" MODIFIED="1498726846718">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> table_utils<font color="#990000">::</font>remove_columns <font color="#FF0000">{</font> table indexes <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        touch output
        <b><font color="#0000FF">set</font></b> indexes <font color="#990000">[</font><b><font color="#0000FF">lsort</font></b> <font color="#990000">-</font>decreasing <font color="#990000">-</font>integer <font color="#009900">$indexes</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> row <font color="#009900">$table</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> col_idx <font color="#009900">$indexes</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> row <font color="#990000">[</font><b><font color="#0000FF">lreplace</font></b> <font color="#009900">$row</font> <font color="#009900">$col_idx</font> <font color="#009900">$col_idx</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$row</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726846835" ID="ID_1498726846835756" MODIFIED="1498726846835" TEXT="opt_list" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846836" ID="ID_1498726846836208" MODIFIED="1498726846836" TEXT="opt_list::preset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846836" ID="ID_1498726846836910" MODIFIED="1498726846836" TEXT="Option preset: list&#xA;&#xA;This will set / define the following proc options:&#xA;    &lt;proc&gt; init                   Initialises the list (setting it to empty)&#xA;    &lt;proc&gt; add &lt;value&gt;            Adds a value to the given list. If the opt&#xA;                                  proc is initialised with both the list&#xA;                                  preset and contains the flag &quot;-unique&quot; then&#xA;                                  the value will only be added if it does not&#xA;                                  already exist. Returns the list index where&#xA;                                  the value was added / exists.&#xA;    &lt;proc&gt; get &lt;index&gt;            Retrieves a value from the list with a&#xA;                                  given &quot;ID&quot; or index.&#xA;    &lt;proc&gt; remove &lt;index&gt;         Removes a value from the list with a given&#xA;                                  &quot;ID&quot; or index.  If this is a unique list&#xA;                                  then the removed value will be replaced by&#xA;                                  an empty string in order to preserve the&#xA;                                  index order. Otherwise deleting a value&#xA;                                  will shift the index order for all values&#xA;                                  following the deleted value.&#xA;    &lt;proc&gt; all &lt;indexes&gt;          Retrieves values from multiple indexes. The&#xA;                                  returned values are returned in the order&#xA;                                  of the given indexes.&#xA;    &lt;proc&gt; reset                  Clears / empties the list&#xA;    &lt;proc&gt; id &lt;value&gt; ?&lt;opt&gt;?     Retrieves the &quot;ID&quot; / index of the given&#xA;                                  value by applying lsearch. See lsearch for&#xA;                                  additional search options.&#xA;    &lt;proc&gt; load &lt;values&gt; ?&lt;opt&gt;?  Loads multiple entries into the list. If&#xA;                                  the option &quot;-unique&quot; is included as an&#xA;                                  an option then each value will only be&#xA;                                  added if it does not already exist.&#xA;    &lt;proc&gt; unload                 Returns the list data&#xA;&#xA;@param pname           The proc to add the options to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846837" ID="ID_1498726846837238" MODIFIED="1498726846837" TEXT="@see" FOLDED="true">
<node CREATED="1498726846837" ID="ID_1498726846837445" MODIFIED="1498726846837" TEXT="opt::proc" COLOR="#14666b" LINK="#ID_1498726846912513">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846837" ID="ID_1498726846837765" MODIFIED="1498726846837" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846848" ID="ID_1498726846848028" MODIFIED="1498726846848">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> opt_list<font color="#990000">::</font>preset <font color="#FF0000">{</font> pname args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> LIST <font color="#990000">[</font>opt<font color="#990000">::</font>get_namespace_var <font color="#009900">$pname</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> UNIQUE <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>param get <font color="#990000">-</font>unique<font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font><font color="#990000">]</font>
        
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726846850" ID="ID_1498726846850181" MODIFIED="1498726846850" TEXT="opt_array" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846850" ID="ID_1498726846850702" MODIFIED="1498726846850" TEXT="opt_array::preset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846851" ID="ID_1498726846851418" MODIFIED="1498726846851" TEXT="Option preset: array&#xA;&#xA;This will enable the following proc options:&#xA;    &lt;proc&gt; init &lt;location&gt;        Initialises the array and sets a given&#xA;                                  file location used for saving and loading&#xA;                                  the array data to/from file&#xA;    &lt;proc&gt; set &lt;key&gt; &lt;value&gt;      Sets a key in the array with a given value&#xA;    &lt;proc&gt; get &lt;key&gt;              Retrieves a value from the array with a&#xA;                                  given key&#xA;    &lt;proc&gt; remove &lt;key&gt;           Removes a key / value from the array. Can&#xA;                                  also be a string match pattern.&#xA;    &lt;proc&gt; reset                  Clears / empties the array&#xA;    &lt;proc&gt; dirty                  Returns 1 if the array has been altered&#xA;                                  since it was first initialised or loaded,&#xA;                                  returns 0 otherwise&#xA;    &lt;proc&gt; save                   Saves the content of the array as plain&#xA;                                  text to file defined by the location the&#xA;                                  proc was initialised with&#xA;    &lt;proc&gt; load                   Loads array data from the file location&#xA;                                  specified by the init option&#xA;    &lt;proc&gt; delete                 Deletes the file at the given location.&#xA;                                  This action is irreversible, use with care.&#xA;    &lt;proc&gt; keys                   Returns the available keys in the array,&#xA;                                  equivalent of &quot;array names&quot;&#xA;    &lt;proc&gt; location               Returns the file location where the array&#xA;                                  data is stored (if defined)&#xA;    &lt;proc&gt; exists &lt;key&gt;           Checks if a key exists in the array (as&#xA;                                  opposed to &quot;array exists&quot; which checks&#xA;                                  whether the array variable exists or not)&#xA;&#xA;@param pname           The proc to add the options to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846851" ID="ID_1498726846851757" MODIFIED="1498726846851" TEXT="@see" FOLDED="true">
<node CREATED="1498726846851" ID="ID_1498726846851964" MODIFIED="1498726846851" TEXT="opt::proc" COLOR="#14666b" LINK="#ID_1498726846912513">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846852" ID="ID_1498726846852281" MODIFIED="1498726846852" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846861" ID="ID_1498726846861905" MODIFIED="1498726846861">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> opt_array<font color="#990000">::</font>preset <font color="#FF0000">{</font> pname args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> PNAME <font color="#990000">[</font>opt<font color="#990000">::</font>get_namespace_var <font color="#009900">$pname</font><font color="#990000">]</font>

        array <b><font color="#0000FF">set</font></b> <font color="#009900">$PNAME</font> <font color="#FF0000">{}</font>
        
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726846970" ID="ID_1498726846970757" MODIFIED="1498726846970" TEXT="tapp_log" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846973" ID="ID_1498726846973016" MODIFIED="1498726846973" TEXT="Logging facility.&#xA;&#xA;The default log settings unless specified in config are INFO logging to&#xA;standard out.&#xA;&#xA;Main features include:&#xA;&#xA;  - Pre-init buffering which allows for log messages to be cached and then&#xA;    logged out when the log package initialises. This is useful for logging&#xA;    what happens during the boot sequence of the application prior to the&#xA;    package initialisation phase. For example it would allow the application&#xA;    to log out what log file it is attempting to write to. It also allows the&#xA;    developer to not worry about errors like &quot;logging not initialised yet!&quot;.&#xA;  - Use the same logging mechanism to log to file and standard out. The&#xA;    benefit is that standard out logging is useful for interactive scripts,&#xA;    but less so if running the script as a cron job for example.&#xA;    Configuration or script initialisation parameters can disable standard&#xA;    out logging and redirect the normal output to the log file.&#xA;  - Log rotation. While the tapp package is intended for short lived scripts&#xA;    there is support for automatic log rotation should this be needed.&#xA;&#xA;Note that most log configuration settings, such as forcing printing to&#xA;standard out and/or enabling debug logging can normally be overridden by&#xA;command line arguments to the application. Try passing in both ‑‑help and&#xA;‑‑more to the application for available parameters.&#xA;&#xA;The format of the log file output is currently not configurable, but may be&#xA;addressed if there is any general interest in being able to customise this&#xA;further.&#xA;&#xA;The general format of the log output looks something like this where&#xA;multi-line log messages are logged as individual lines:&#xA;&#xA;        &lt;timestamp&gt; &lt;level&gt; ?&lt;prefix&gt;? ?&lt;proc&gt;? &lt;sep&gt; &lt;msg_line1&gt;&#xA;        &lt;timestamp&gt; &lt;level&gt; ?&lt;prefix&gt;?                &lt;msg_line2&gt;&#xA;        ...&#xA;        &lt;timestamp&gt; &lt;level&gt; ?&lt;prefix&gt;?                &lt;msg_lineN&gt;" COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846973" ID="ID_1498726846973355" MODIFIED="1498726846973" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726846973" ID="ID_1498726846973559" MODIFIED="1498726846973" TEXT="LOG_FILE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846973" ID="ID_1498726846973832" MODIFIED="1498726846973" TEXT="The log file name, can be set to stdout or stderr if needed." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846974" ID="ID_1498726846974115" MODIFIED="1498726846974" TEXT="LOG_DIR" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846974" ID="ID_1498726846974381" MODIFIED="1498726846974" TEXT="The directory where log files are stored, if the directory does not already exist then it will be automatically created" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846974" ID="ID_1498726846974651" MODIFIED="1498726846974" TEXT="LOG_LEVEL" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846974" ID="ID_1498726846974927" MODIFIED="1498726846974" TEXT="The log level the application logs at. Available log levels are: TRACE, DEV, DEBUG, INFO, ERROR, FATAL, STDOUT (log to standard out instead of log file), STDERR and MUTE which disables all logging (unless the application actually logs at MUTE level, which would be uncommon)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846975" ID="ID_1498726846975195" MODIFIED="1498726846975" TEXT="LOG_TIME_FORMAT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846975" ID="ID_1498726846975460" MODIFIED="1498726846975" TEXT="Allows the date format used for the log line time stamp to be specified. Defaults to &quot;%Y-%m-%d %H:%M:%S&quot;. See http://www.tcl.tk/man/tcl8.5/TclCmd/clock.htm for available formatting groups." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846975" ID="ID_1498726846975722" MODIFIED="1498726846975" TEXT="LOG_PREFIX" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846975" ID="ID_1498726846975995" MODIFIED="1498726846976" TEXT="Allows for a specific prefix to be added to the log line. This can be particularly useful when multiple scripts are logging to the same log file. An example of this would be a script that runs another script and we want the log output for both to be written to the log file of the main application." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846976" ID="ID_1498726846976262" MODIFIED="1498726846976" TEXT="LOG_PROC_PREFIX" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846976" ID="ID_1498726846976522" MODIFIED="1498726846976" TEXT="Adds the name of the proc doing the logging as a prefix on each log line." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846976" ID="ID_1498726846976784" MODIFIED="1498726846976" TEXT="LOG_SEPARATOR" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846977" ID="ID_1498726846977052" MODIFIED="1498726846977" TEXT="Defines the character(s) that separates the log prefix string from the log message. Defaults to double angle brackets (&gt;&gt;)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846977" ID="ID_1498726846977316" MODIFIED="1498726846977" TEXT="LOG_DISABLE_STD_STREAMS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846977" ID="ID_1498726846977587" MODIFIED="1498726846977" TEXT="Disables logging to standard streams. With this turned on any logging to STDERR or STDOUT levels will be redirected to the log file instead. This can be useful for scripts that run as a cron job." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846977" ID="ID_1498726846977851" MODIFIED="1498726846977" TEXT="LOG_AUTO_WRAP_STD_STREAMS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846978" ID="ID_1498726846978130" MODIFIED="1498726846978" TEXT="Ensures that the what is logged to standard output streams are wrapped based on whole words instead of the default wrapping provided by the terminal. Enabled by default. Try to disable if output formatting issues occur." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846978" ID="ID_1498726846978393" MODIFIED="1498726846978" TEXT="LOG_ROTATION" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846978" ID="ID_1498726846978676" MODIFIED="1498726846978" TEXT="Log rotation for long running applications. Available options: yearly, monthly, daily, hourly, minutely, secondly and none. The daily and hourly log rotation options are the only ones that are recommendable. The secondly log rotation is primarily only for test purposes. Defaults to &quot;none&quot;." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846978" ID="ID_1498726846978939" MODIFIED="1498726846978" TEXT="VERBOSE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846979" ID="ID_1498726846979202" MODIFIED="1498726846979" TEXT="Enables verbose output which is a special log level that prints to standard out, but only if verbose logging is enabled)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726846979" ID="ID_1498726846979450" MODIFIED="1498726846979" TEXT="@see" FOLDED="true">
<node CREATED="1498726846979" ID="ID_1498726846979635" MODIFIED="1498726846979" TEXT="text_utils::wrap_text for information on how the auto wrapping works" COLOR="#14666b" LINK="#ID_1498726846594885">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846980" ID="ID_1498726846980053" MODIFIED="1498726846980" TEXT="tapp_log::close_log" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846980" ID="ID_1498726846980332" MODIFIED="1498726846980" TEXT="This will close the log file." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846980" ID="ID_1498726846980617" MODIFIED="1498726846980" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846991" ID="ID_1498726846991120" MODIFIED="1498726846991">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>close_log <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> INIT
        <b><font color="#0000FF">variable</font></b> LOGGER

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists LOGGER<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#FF0000">{</font>stdout stderr<font color="#FF0000">}</font> <font color="#009900">$LOGGER</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">close</font></b> <font color="#009900">$LOGGER</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> LOGGER <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> INIT <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846991" ID="ID_1498726846991740" MODIFIED="1498726846991" TEXT="tapp_log::get_next_log_rotation_time" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846992" ID="ID_1498726846992229" MODIFIED="1498726846992" TEXT="Date/time utility to get the next whole hour or day relative to current time.&#xA;&#xA;For example if the time is now 14:32:19 then:&#xA;   - the next hour will be 15:00:00&#xA;   - the next minute will be 14:33:00&#xA;   - the next second will be 14:32:20&#xA;&#xA;@param interval     The next interval to retrieve, can be one of year, month,&#xA;                    day, hour, minute, second.&#xA;@param time         The base time, will default to current time if not provided&#xA;@return             The relative time in seconds" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846992" ID="ID_1498726846992612" MODIFIED="1498726846992" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847014" ID="ID_1498726847014437" MODIFIED="1498726847014">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>get_next_log_rotation_time <font color="#FF0000">{</font> interval <font color="#FF0000">{</font><b><font color="#0000FF">time</font></b> <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$time</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#990000">--</font> <font color="#009900">$interval</font> <font color="#FF0000">{</font>
        none <font color="#990000">-</font>
        n<font color="#990000">/</font>a      <font color="#FF0000">{</font> <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#990000">+</font> <font color="#993399">1000000000</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#FF0000">}</font>
        second <font color="#990000">-</font>
        secondly <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#990000">%</font>H<font color="#990000">:%</font>M<font color="#990000">:%</font>S<font color="#FF0000">}</font> <font color="#FF0000">}</font>
        minute <font color="#990000">-</font>
        minutely <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#990000">%</font>H<font color="#990000">:%</font>M<font color="#990000">:</font><font color="#993399">00</font><font color="#FF0000">}</font> <font color="#FF0000">}</font>
        hour <font color="#990000">-</font>
        hourly   <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#990000">%</font>H<font color="#990000">:</font><font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#FF0000">}</font> <font color="#FF0000">}</font>
        day <font color="#990000">-</font>
        daily    <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#FF0000">}</font> <font color="#FF0000">}</font>
        month <font color="#990000">-</font>
        monthly  <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-</font><font color="#993399">01</font> <font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#FF0000">}</font> <font color="#FF0000">}</font>
        year <font color="#990000">-</font>
        yearly   <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-</font><font color="#993399">01</font><font color="#990000">-</font><font color="#993399">01</font> <font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#990000">:</font><font color="#993399">00</font><font color="#FF0000">}</font> <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"bad option </font><font color="#CC33CC">\"</font><font color="#FF0000">$interval</font><font color="#CC33CC">\"</font><font color="#FF0000">: must be year, month, day, hour, minute, second or none"</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> next_time <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> add <font color="#009900">$time</font> <font color="#993399">1</font> <font color="#009900">$interval</font><font color="#990000">]</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> next_secs <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">scan</font></b> <font color="#009900">$next_time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#990000">%</font>H<font color="#990000">:%</font>M<font color="#990000">:%</font>S<font color="#FF0000">}</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$next_secs</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847015" ID="ID_1498726847015088" MODIFIED="1498726847015" TEXT="tapp_log::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847015" ID="ID_1498726847015535" MODIFIED="1498726847015" TEXT="Initialise the log package and open a log file for writing.&#xA;&#xA;@param options         Key value pairs of log options. Generally this should&#xA;                       not be necessary to pass in unless the application&#xA;                       specifically needs to override log configuration set&#xA;                       in config file." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847015" ID="ID_1498726847015903" MODIFIED="1498726847015" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847030" ID="ID_1498726847030761" MODIFIED="1498726847030">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>init <font color="#FF0000">{</font> <font color="#FF0000">{</font> options <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> INIT
        <b><font color="#0000FF">variable</font></b> LOGGER
        <b><font color="#0000FF">variable</font></b> LOG_ROTATION_TIME
        <b><font color="#0000FF">variable</font></b> BUFFER

        <i><font color="#9A1900"># Don't reinitialise if already done before</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$INIT</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Override default configuration with with optional arguments</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>cfg val<font color="#FF0000">}</font> <font color="#009900">$options</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> cfg <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#009900">$cfg</font><font color="#990000">]</font> <font color="#990000">-]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font>LOG_<font color="#990000">*</font><font color="#FF0000">}</font> <font color="#009900">$cfg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> cfg LOG_<font color="#009900">$cfg</font>
                <font color="#FF0000">}</font>
                cfg <b><font color="#0000FF">set</font></b> <font color="#009900">$cfg</font> <font color="#009900">$val</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Set default values unless overridden</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>cfg <b><font color="#0000FF">default</font></b><font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b>                LOG_LEVEL   INFO                LOG_FILE    stdout                LOG_PREFIX  <font color="#FF0000">{}</font>                LOG_DIR     <font color="#FF0000">{}</font>        <font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg exists <font color="#009900">$cfg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        cfg <b><font color="#0000FF">set</font></b> <font color="#009900">$cfg</font> <font color="#009900">$default</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Set up log levels and level integer values</font></i>
        <b><font color="#0000FF">set</font></b> levels <font color="#990000">[</font><b><font color="#0000FF">list</font></b> TRACE DEV DEBUG INFO WARN ERROR FATAL STDOUT MUTE<font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> index  <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$levels</font> <font color="#990000">[</font>cfg get LOG_LEVEL<font color="#990000">]]</font>
        <b><font color="#0000FF">foreach</font></b> level <font color="#990000">[</font><b><font color="#0000FF">lrange</font></b> <font color="#009900">$levels</font> <font color="#009900">$index</font> end<font color="#990000">]</font> <font color="#FF0000">{</font>
                cfg <b><font color="#0000FF">set</font></b> <font color="#009900">$level</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$levels</font> <font color="#009900">$level</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Rolling over the log file is done by a re-init.</font></i>
        <b><font color="#0000FF">set</font></b> log_dir  <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">[</font>cfg get LOG_DIR<font color="#990000">]]</font>
        <b><font color="#0000FF">set</font></b> log_file <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">[</font>cfg get LOG_FILE<font color="#990000">]]</font>

        <b><font color="#0000FF">set</font></b> LOG_ROTATION_TIME <font color="#990000">[</font>get_next_log_rotation_time <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#990000">[</font>cfg get LOG_ROTATION none<font color="#990000">]]]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#FF0000">{</font>stdout stderr<font color="#FF0000">}</font> <font color="#009900">$log_file</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> LOGGER <font color="#009900">$log_file</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># Attempt to create the log directory if it does not already exist.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$log_dir</font><font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$log_dir</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">file</font></b> mkdir <font color="#009900">$log_dir</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">set</font></b> LOGGER <font color="#990000">[</font><b><font color="#0000FF">open</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$log_dir</font> <font color="#009900">$log_file</font><font color="#990000">]</font> a<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> INIT <font color="#993399">1</font>

        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>log_level log_msg<font color="#FF0000">}</font> <font color="#009900">$BUFFER</font> <font color="#FF0000">{</font>
                log <font color="#009900">$log_level</font> <font color="#009900">$log_msg</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847031" ID="ID_1498726847031426" MODIFIED="1498726847031" TEXT="tapp_log::log" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847031" ID="ID_1498726847031855" MODIFIED="1498726847031" TEXT="Log a message at a given log level.&#xA;&#xA;@param log_level       The log level to log the message at&#xA;@param log_msg         The message that is to be logged. Variables will be&#xA;                       substituted prior to being logged out if the log level&#xA;                       is enabled." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847032" ID="ID_1498726847032213" MODIFIED="1498726847032" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847052" ID="ID_1498726847052412" MODIFIED="1498726847052">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>log <font color="#FF0000">{</font> log_level log_msg <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> INIT
        <b><font color="#0000FF">variable</font></b> LOGGER
        <b><font color="#0000FF">variable</font></b> LOG_ROTATION_TIME
        <b><font color="#0000FF">variable</font></b> BUFFER

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$INIT</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> BUFFER <font color="#009900">$log_level</font> <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <b><font color="#0000FF">subst</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$log_msg</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> BUFFER <font color="#009900">$log_level</font> <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <b><font color="#0000FF">subst</font></b> <font color="#990000">-</font>nocommands <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$log_msg</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg exists <font color="#009900">$log_level</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$log_level</font> <font color="#990000">==</font> <font color="#FF0000">{</font>VERBOSE<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> log_level STDOUT
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> log_msg <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <b><font color="#0000FF">subst</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$log_msg</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> log_msg <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <b><font color="#0000FF">subst</font></b> <font color="#990000">-</font>nocommands <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$log_msg</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> log_msg <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{{</font><font color="#990000">\</font>r<font color="#FF0000">}</font> <font color="#FF0000">{}}</font> <font color="#009900">$log_msg</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> log_msg <font color="#FF0000">"Error occurred while attempting to log $log_msg</font><font color="#CC33CC">\n</font><font color="#FF0000">Error was: $msg"</font>
                <b><font color="#0000FF">set</font></b> log_level ERROR
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> now <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$now</font> <font color="#990000">&gt;</font> <font color="#009900">$LOG_ROTATION_TIME</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                tapp_log<font color="#990000">::</font>close_log
                tapp_log<font color="#990000">::</font>init
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg enabled LOG_DISABLE_STANDARD_STREAMS <font color="#993399">0</font><font color="#990000">]</font>
                    <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#FF0000">{</font>STDOUT STDERR<font color="#FF0000">}</font> <font color="#009900">$log_level</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> out <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$log_level</font><font color="#990000">]</font>
                        
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled LOG_AUTO_WRAP_STD_STREAMS <font color="#993399">1</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">puts</font></b> <font color="#009900">$out</font> <font color="#990000">[</font>text_utils<font color="#990000">::</font>wrap_text <font color="#009900">$log_msg</font> <font color="#990000">[</font>cfg get CONSOLE_WIDTH <font color="#993399">80</font><font color="#990000">]]</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">puts</font></b> <font color="#009900">$out</font> <font color="#009900">$log_msg</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">flush</font></b> <font color="#009900">$out</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$now</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">[</font>cfg get LOG_TIME_FORMAT <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#990000">%</font>H<font color="#990000">:%</font>M<font color="#990000">:%</font>S<font color="#FF0000">}</font><font color="#990000">]]</font>
                        <b><font color="#0000FF">lappend</font></b> log_format <font color="#009900">$time</font>
                        <b><font color="#0000FF">lappend</font></b> log_format <font color="#009900">$log_level</font>

                        <b><font color="#0000FF">set</font></b> log_prefix <font color="#990000">[</font>cfg get LOG_PREFIX<font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$log_prefix</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> log_format <font color="#009900">$log_prefix</font>
                        <font color="#FF0000">}</font>
                
                        <b><font color="#0000FF">set</font></b> sep <font color="#990000">[</font>cfg get LOG_SEPARATOR <font color="#FF0000">{</font><font color="#990000">&gt;&gt;</font><font color="#FF0000">}</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled LOG_PROC_PREFIX<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">info</font></b> level<font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> sep <font color="#FF0000">"&gt; [lindex [info level -1] 0] $sep"</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> indent <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$sep</font><font color="#990000">]]</font>
                        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$log_format</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
                        <b><font color="#0000FF">foreach</font></b> line <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$log_msg</font> <font color="#990000">\</font>n<font color="#990000">]</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#009900">$format</font><font color="#990000">\</font> <font color="#009900">$sep</font><font color="#990000">\</font> <font color="#009900">$line</font>
                                <b><font color="#0000FF">set</font></b> sep <font color="#009900">$indent</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">flush</font></b> <font color="#009900">$LOGGER</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">error</font></b> writing <font color="#FF0000">"std(out|err)"</font><font color="#990000">:</font> broken pipe<font color="#FF0000">}</font> <font color="#009900">$msg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">puts</font></b> stderr <font color="#009900">$msg</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847053" ID="ID_1498726847053084" MODIFIED="1498726847053" TEXT="tapp_log::log_level_enabled" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847053" ID="ID_1498726847053716" MODIFIED="1498726847053" TEXT="Check if a given log level is enabled or not.&#xA;&#xA;@param log_level       The log level to check if it is enabled&#xA;@return                1 if the log level is enabled, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847054" ID="ID_1498726847055003" MODIFIED="1498726847055" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847065" ID="ID_1498726847065266" MODIFIED="1498726847065">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>log_level_enabled <font color="#FF0000">{</font> log_level <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>cfg exists <font color="#009900">$log_level</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847065" ID="ID_1498726847065894" MODIFIED="1498726847065" TEXT="tapp_log::log_vars" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847066" ID="ID_1498726847066468" MODIFIED="1498726847066" TEXT="Debugging utility to log the values of variables at a given point in a procedure.&#xA;&#xA;@param log             List of what to log, either &quot;vars&quot;, &quot;locals&quot; or both.&#xA;                       The former logs all globally visible variables while&#xA;                       the latter logs all local variables. Defaults to both.&#xA;@param log_level       The log level to log the variables at, defaults to&#xA;                       logging at DEBUG level.&#xA;@param ignore          List of variable names to ignore, used to skip logging&#xA;                       of variables that may contain sensitive information" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847066" ID="ID_1498726847066857" MODIFIED="1498726847066" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847080" ID="ID_1498726847080992" MODIFIED="1498726847081">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>log_vars <font color="#FF0000">{</font> <font color="#FF0000">{</font> log <font color="#FF0000">{</font>vars locals<font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> log_level DEBUG <font color="#FF0000">}</font> <font color="#FF0000">{</font> ignore <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> LOGGER

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>log_level_enabled <font color="#009900">$log_level</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">[</font>cfg get LOG_TIME_FORMAT <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">-%</font>m<font color="#990000">-%</font>d <font color="#990000">%</font>H<font color="#990000">:%</font>M<font color="#990000">:%</font>S<font color="#FF0000">}</font><font color="#990000">]]</font>

        <b><font color="#0000FF">set</font></b> prefix <font color="#FF0000">"$time $log_level LOG_VARS: "</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> level<font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">2</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix [lindex [info level -1] 0], called from [lindex [info level -2] 0]"</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> variables <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$log</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg</font> <font color="#990000">==</font> <font color="#FF0000">{</font>vars<font color="#FF0000">}</font> <font color="#990000">||</font> <font color="#009900">$arg</font> <font color="#990000">==</font> <font color="#FF0000">{</font>locals<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> variables <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$variables</font> <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <b><font color="#0000FF">info</font></b> <font color="#009900">$arg</font><font color="#990000">]]]</font>
                        <b><font color="#0000FF">foreach</font></b> var <font color="#009900">$ignore</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> index <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$variables</font> <font color="#009900">$var</font><font color="#990000">]</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$index</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">set</font></b> variables <font color="#990000">[</font><b><font color="#0000FF">lreplace</font></b> <font color="#009900">$variables</font> <font color="#009900">$index</font> <font color="#009900">$index</font><font color="#990000">]</font>
                                        <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix Not displaying variable: $var"</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix [llength $variables] arguments [join $variables {/}]"</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix Unrecognised argument to log_vars: $arg"</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> <b><font color="#0000FF">variable</font></b> <font color="#009900">$variables</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> array exists <font color="#009900">$variable</font><font color="#990000">]]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>name value<font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> array get <font color="#009900">$variable</font><font color="#990000">]]</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix $variable</font><font color="#CC33CC">\(</font><font color="#FF0000">$name</font><font color="#CC33CC">\)</font><font color="#FF0000">: $value"</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <b><font color="#0000FF">info</font></b> exists <font color="#009900">$variable</font><font color="#990000">]]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix $variable: [uplevel 1 [list set $variable]]"</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">puts</font></b> <font color="#009900">$LOGGER</font> <font color="#FF0000">"$prefix '$variable' is declared but doesn't exist."</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847081" ID="ID_1498726847081653" MODIFIED="1498726847081" TEXT="tapp_log::reset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847082" ID="ID_1498726847082090" MODIFIED="1498726847082" TEXT="This will close the log file and re-initialise the log package." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847082" ID="ID_1498726847082463" MODIFIED="1498726847082" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847092" ID="ID_1498726847092949" MODIFIED="1498726847092">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_log<font color="#990000">::</font>reset <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> INIT
        <b><font color="#0000FF">variable</font></b> BUFFER
        <b><font color="#0000FF">variable</font></b> LOG_ROTATION_TIME
        <b><font color="#0000FF">variable</font></b> LOGGER

        tapp_log<font color="#990000">::</font>close_log
        <b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>unset BUFFER<font color="#FF0000">}</font>
        <b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>unset LOGGER<font color="#FF0000">}</font>
        <b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>unset LOG_ROTATION_TIME<font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> BUFFER <font color="#FF0000">{}</font>
        <b><font color="#0000FF">set</font></b> INIT <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726847095" ID="ID_1498726847095155" MODIFIED="1498726847095" TEXT="tapp_lang" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726847095" ID="ID_1498726847095585" MODIFIED="1498726847095" TEXT="This package provides a series of language additions to Tcl.&#xA;&#xA;Most of these are for convenience while others are primarily for debugging&#xA;purposes. All language additions (procs) are pure Tcl implementations." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847096" ID="ID_1498726847096485" MODIFIED="1498726847096" TEXT="tapp_lang::assert" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847096" ID="ID_1498726847096910" MODIFIED="1498726847096" TEXT="Assert functionality for a Tcl application.&#xA;&#xA;If the assert fails a Tcl error will be thrown.&#xA;&#xA;@param expression      The expression to assert, for example $myVar == 1.&#xA;                       The expression is run through a normal Tcl expr&#xA;                       statement. If the expression evaluates to false then&#xA;                       a Tcl error is thrown." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847097" ID="ID_1498726847097226" MODIFIED="1498726847097" TEXT="@see" FOLDED="true">
<node CREATED="1498726847097" ID="ID_1498726847097433" MODIFIED="1498726847097" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/expr.htm" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/expr.htm">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847097" ID="ID_1498726847097752" MODIFIED="1498726847097" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847107" ID="ID_1498726847107699" MODIFIED="1498726847107">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>assert <font color="#FF0000">{</font> expression <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font>$<font color="#990000">::</font>tapp_lang<font color="#990000">::</font>ASSERT <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <b><font color="#0000FF">expr</font></b> <font color="#009900">$expression</font><font color="#990000">]]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"assertion failed: $expression ([uplevel 1 [list subst $expression]])"</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847108" ID="ID_1498726847108277" MODIFIED="1498726847108" TEXT="tapp_lang::clear" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847108" ID="ID_1498726847108696" MODIFIED="1498726847108" TEXT="Short hand notation for setting variables to empty strings, but leaving them&#xA;initialised.&#xA;&#xA;This is typically used to reset variables in a for loop back to being empty&#xA;strings.&#xA;&#xA;Synopsis:&#xA;   clear ?varName varName ...?&#xA;&#xA;@param varName         The variable name to set to be an empty string" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847109" ID="ID_1498726847109041" MODIFIED="1498726847109" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847118" ID="ID_1498726847118532" MODIFIED="1498726847118">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>clear args <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#009900">$arg</font> var
                <b><font color="#0000FF">set</font></b> var <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847119" ID="ID_1498726847119099" MODIFIED="1498726847119" TEXT="tapp_lang::decr" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847119" ID="ID_1498726847119588" MODIFIED="1498726847119" TEXT="This is essentially the opposite of the Tcl command &quot;incr&quot;.&#xA;&#xA;To decrement a value in Tcl the usual approach is to increment a value with a&#xA;negative value, for example:&#xA;&#xA;   incr i -1&#xA;   incr i -2&#xA;&#xA;This proc is a short hand notation for the above, example usage:&#xA;&#xA;   decr i&#xA;   decr i 2&#xA;&#xA;Synopsis:&#xA;   decr varName ?decrement?&#xA;&#xA;@param varName         The local variable to decrement&#xA;@param decrement       How much to decrement the value by, defaults to 1" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847119" ID="ID_1498726847119891" MODIFIED="1498726847119" TEXT="@see" FOLDED="true">
<node CREATED="1498726847120" ID="ID_1498726847120099" MODIFIED="1498726847120" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/incr.htm" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/incr.htm">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847120" ID="ID_1498726847120417" MODIFIED="1498726847120" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847132" ID="ID_1498726847132115" MODIFIED="1498726847132">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>decr args <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <b><font color="#0000FF">in</font></b> <font color="#FF0000">{</font><font color="#993399">1</font> <font color="#993399">2</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">{</font>wrong <i><font color="#9A1900"># args: should be "decr varName ?decrement?"}</font></i>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> increment <font color="#990000">-</font><font color="#993399">1</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> integer <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#993399">1</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> is integer <font color="#009900">$integer</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">error</font></b> <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#FF0000">{</font>expected integer but got <font color="#FF0000">"$integer"</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> increment <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$increment</font> <font color="#990000">*</font> <font color="#009900">$integer</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <b><font color="#0000FF">incr</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#009900">$increment</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847132" ID="ID_1498726847132712" MODIFIED="1498726847132" TEXT="tapp_lang::default" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847133" ID="ID_1498726847133139" MODIFIED="1498726847133" TEXT="Short hand notation for checking whether a variable exists.&#xA;&#xA;If it does not exist then set the variable to given default value.&#xA;&#xA;@param variable        The variable to check whether it exists&#xA;@param default         The value to set the variable to if it does not exist&#xA;@return                1 if the variable was set, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847133" ID="ID_1498726847133505" MODIFIED="1498726847133" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847143" ID="ID_1498726847143895" MODIFIED="1498726847143">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font><b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">variable</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$variable</font> variable_to_check
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists variable_to_check<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> variable_to_check <font color="#009900">$default</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847144" ID="ID_1498726847144518" MODIFIED="1498726847144" TEXT="tapp_lang::do" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847145" ID="ID_1498726847145070" MODIFIED="1498726847145" TEXT="This adds the rarely used do while functionality to Tcl.&#xA;&#xA;Synopsis:&#xA;   do command while test&#xA;&#xA;Example usage:&#xA;&#xA;   do {&#xA;           incr i&#xA;           lappend result $i&#xA;   } while {$i &lt; 3}&#xA;&#xA;@param command         The command to execute, i.e. the body of the do while&#xA;                       block&#xA;@param while           Expected to be plain text &quot;while&quot; for readability&#xA;                       purposes, value is not used&#xA;@param test            The while test / condition as with a normal while loop" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847145" ID="ID_1498726847145418" MODIFIED="1498726847145" TEXT="@see" FOLDED="true">
<node CREATED="1498726847145" ID="ID_1498726847145642" MODIFIED="1498726847145" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/while.htm" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/while.htm">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847146" ID="ID_1498726847146029" MODIFIED="1498726847146" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847160" ID="ID_1498726847160142" MODIFIED="1498726847160">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>do args <font color="#FF0000">{</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#993399">3</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">{</font>wrong <i><font color="#9A1900"># args: should be "do command while test"}</font></i>
        <font color="#FF0000">}</font>
        
        lassign <font color="#009900">$args</font> script <b><font color="#0000FF">while</font></b> test
        <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <b><font color="#0000FF">set</font></b> <font color="#990000">\</font>u0 <font color="#993399">1</font>
        <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <b><font color="#0000FF">while</font></b> <font color="#990000">\</font><font color="#FF0000">{</font><font color="#990000">\[</font><b><font color="#0000FF">set</font></b> <font color="#990000">\</font>u0<font color="#990000">\]</font> <font color="#990000">||</font> <font color="#009900">$test</font><font color="#990000">\</font><font color="#FF0000">}</font> <font color="#990000">\</font><font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> <font color="#990000">\</font>u0 <font color="#993399">0</font><font color="#990000">\;</font><b><font color="#0000FF">eval</font></b> <font color="#990000">\</font><font color="#FF0000">{</font><font color="#009900">$script</font><font color="#990000">\</font><font color="#FF0000">}</font><font color="#990000">\</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> unset <font color="#990000">\</font>u0
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847160" ID="ID_1498726847160738" MODIFIED="1498726847160" TEXT="tapp_lang::larrange" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847161" ID="ID_1498726847161355" MODIFIED="1498726847161" TEXT="Re-arranges list entries based on options and patterns.&#xA;&#xA;For example:&#xA;   larrange -prioritise [list a b c] &quot;c&quot;&#xA;&#xA;will prioritise the &quot;c&quot; element by moving it to the start of the list.&#xA;&#xA;Synopsis:&#xA;   larrange ?-option value ...? list pattern&#xA;&#xA;@option -prioritise    Prioritise elements matching the given pattern by&#xA;                       moving them to the start of the list&#xA;@option -deprioritise  Deprioritise elements matching the given pattern by&#xA;                       moving them to the end of the list&#xA;@option -glob          Use glob matching for:4 the pattern (default)&#xA;@option -regexp        Use regexp matching for the pattern&#xA;@option -exact         Pattern is a literal string that is compared for exact&#xA;                       equality against each list element&#xA;@option -list          The list to re-arrange&#xA;@option -pattern       The pattern that indicates which elements to arrange&#xA;@option -patterns      Declares a list of patterns / elements to arrange&#xA;@param list            The list to re-arrange, presumed to be the next to&#xA;                       last parameter unless -list option is specified&#xA;@param pattern         The pattern, presumed to be the last parameter unless&#xA;                       the -pattern option is specified&#xA;@return                The re-arranged list" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847161" ID="ID_1498726847161810" MODIFIED="1498726847161" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847176" ID="ID_1498726847176594" MODIFIED="1498726847176">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>larrange <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> match_method <font color="#990000">-</font><b><font color="#0000FF">glob</font></b>
        touch task <b><font color="#0000FF">list</font></b> patterns

        <i><font color="#9A1900"># Parse arguments</font></i>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#009900">$i</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#990000">--</font> <font color="#009900">$arg</font> <font color="#FF0000">{</font>
                <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>
                <font color="#990000">-</font>exact <font color="#990000">-</font>
                <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> match_method <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                <font color="#990000">-</font>prioritise <font color="#990000">-</font>
                <font color="#990000">-</font>deprioritise <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> task <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                <font color="#990000">-</font><b><font color="#0000FF">list</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#990000">[</font><b><font color="#0000FF">incr</font></b> i<font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <font color="#990000">-</font>pattern <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> patterns <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#990000">[</font><b><font color="#0000FF">incr</font></b> i<font color="#990000">]]]</font>
                <font color="#FF0000">}</font>
                <font color="#990000">-</font>patterns <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> patterns <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#990000">[</font><b><font color="#0000FF">incr</font></b> i<font color="#990000">]]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$list</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">list</font></b> <font color="#009900">$arg</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$pattern</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> pattern <font color="#009900">$arg</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                log WARN <font color="#FF0000">{</font>larrange<font color="#990000">:</font> Unknown argument <font color="#009900">$arg</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Validate that we have a list and a pattern</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$list</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">![</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$patterns</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">{</font>wrong <i><font color="#9A1900"># args: "should be larrange ?-option value ...? list pattern"}</font></i>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> patt <font color="#009900">$patterns</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> matches <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$match_method</font> <font color="#990000">-</font>inline <font color="#990000">-</font>all <font color="#009900">$list</font> <font color="#009900">$patt</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> rest    <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$match_method</font> <font color="#990000">-</font>inline <font color="#990000">-</font>all <font color="#990000">-</font>not <font color="#009900">$list</font> <font color="#009900">$patt</font><font color="#990000">]</font>
                <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#990000">--</font> <font color="#009900">$task</font> <font color="#FF0000">{</font>
                <font color="#990000">-</font>prioritise   <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$matches</font> <font color="#009900">$rest</font><font color="#990000">]</font> <font color="#FF0000">}</font>
                <font color="#990000">-</font>deprioritise <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$rest</font> <font color="#009900">$matches</font><font color="#990000">]</font> <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$list</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847177" ID="ID_1498726847177209" MODIFIED="1498726847177" TEXT="tapp_lang::ldefault" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847177" ID="ID_1498726847177687" MODIFIED="1498726847177" TEXT="Checks if a given index in a list is an empty string,&#xA;and if so it will default it to the first non-empty&#xA;value provided.&#xA;&#xA;@param list            The name of the list to check&#xA;@param index           The index in the list to use&#xA;@param args            Possible default parameters" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847178" ID="ID_1498726847178054" MODIFIED="1498726847178" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847190" ID="ID_1498726847190029" MODIFIED="1498726847190">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>ldefault <font color="#FF0000">{</font> <b><font color="#0000FF">list</font></b> index args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$list</font> _list

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$_list</font> <font color="#009900">$index</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">while</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$_list</font><font color="#990000">]</font> <font color="#990000">&lt;=</font> <font color="#009900">$index</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> _list <font color="#FF0000">{}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lset</font></b> _list <font color="#009900">$index</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>inline <font color="#990000">-</font>not <font color="#009900">$args</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847190" ID="ID_1498726847190688" MODIFIED="1498726847190" TEXT="tapp_lang::lremove" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847191" ID="ID_1498726847191438" MODIFIED="1498726847191" TEXT="Removes all list entries that matches any of the given elements.&#xA;&#xA;For example:&#xA;   lremove [list a {} b {} c] {}]&#xA;&#xA;will remove all empty values from the list and return {a b c}&#xA;&#xA;Synopsis:&#xA;   lremove list ?element element ...?&#xA;&#xA;@param list            The list to remove elements from&#xA;@param element         The element to remove from the list&#xA;@return                The list with the given elements removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847191" ID="ID_1498726847191815" MODIFIED="1498726847191" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847202" ID="ID_1498726847202269" MODIFIED="1498726847202">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>lremove <font color="#FF0000">{</font> <b><font color="#0000FF">list</font></b> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">foreach</font></b> element <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>inline <font color="#990000">-</font>all <font color="#990000">-</font>not <font color="#009900">$list</font> <font color="#009900">$element</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$list</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847202" ID="ID_1498726847202860" MODIFIED="1498726847202" TEXT="tapp_lang::lsplit" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847203" ID="ID_1498726847203360" MODIFIED="1498726847203" TEXT="Splits a list into several based on the amount of variables passed in.&#xA;&#xA;For example splitting key value pairs:&#xA;   lsplit [list key1 val1 key2 val2 key3 val3] keys vals&#xA;&#xA;will result in:&#xA;   set keys {key1 key2 key3}&#xA;   set vals {val1 val2 val3}&#xA;&#xA;If there are not enough values to fulfil a whole set across all variables&#xA;then the remaining values will be filled up with empty strings.&#xA;&#xA;Synopsis:&#xA;   lsplit list ?varname1? ?varname2? ... ?varnameN?&#xA;&#xA;@param list            The list of elements to split into several lists&#xA;@param args            Two or more variable names to store the new lists in" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847203" ID="ID_1498726847203714" MODIFIED="1498726847203" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847213" ID="ID_1498726847213600" MODIFIED="1498726847213">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>lsplit <font color="#FF0000">{</font> <b><font color="#0000FF">list</font></b> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">foreach</font></b> <font color="#009900">$args</font> <font color="#009900">$list</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$args</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> l_<font color="#009900">$arg</font> <font color="#990000">[</font><b><font color="#0000FF">set</font></b> <font color="#009900">$arg</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">"set $arg {[set l_$arg]}"</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847214" ID="ID_1498726847214204" MODIFIED="1498726847214" TEXT="tapp_lang::nvl" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847214" ID="ID_1498726847214653" MODIFIED="1498726847214" TEXT="Inspired by database systems the nvl (null value) will return the given&#xA;default value if the variable does not exist, otherwise the value of the&#xA;variable itself is returned.&#xA;&#xA;@param variable        The variable to check if it is defined or not&#xA;@param default         The value to return if the variable does not exist&#xA;@return                The value of the variable if it exists, returns the&#xA;                       provided default variable if it doesn't" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847215" ID="ID_1498726847215008" MODIFIED="1498726847215" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847225" ID="ID_1498726847225312" MODIFIED="1498726847225">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>nvl <font color="#FF0000">{</font> <b><font color="#0000FF">variable</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> var <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$variable</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">regsub</font></b> <font color="#FF0000">{</font><font color="#990000">,</font>$<font color="#FF0000">}</font>   <font color="#009900">$var</font> <font color="#FF0000">{}</font> var
        <b><font color="#0000FF">regsub</font></b> <font color="#FF0000">{</font><font color="#990000">^[</font>$<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#009900">$var</font> <font color="#FF0000">{}</font> var
        <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$var</font> variable_to_test

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists variable_to_test<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$variable_to_test</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> default_value <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#990000">[</font><b><font color="#0000FF">lrange</font></b> <font color="#009900">$variable</font> <font color="#993399">1</font> end<font color="#990000">]</font> <font color="#009900">$default</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$default_value</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">,</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$default_value</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847225" ID="ID_1498726847225900" MODIFIED="1498726847225" TEXT="tapp_lang::prepend" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847226" ID="ID_1498726847226448" MODIFIED="1498726847226" TEXT="This proc is the logical opposite of append.&#xA;&#xA;The variable given will be get the value(s) prepended to it.&#xA;&#xA;Example usage:&#xA;   prepend str &quot;_&quot;&#xA;&#xA;is equivalent to:&#xA;   set str &quot;_${str}&quot;&#xA;&#xA;Synopsis:&#xA;   prepend varName ?value value ...?&#xA;&#xA;@param varName         The name of the variable to prepend the given string&#xA;                       to&#xA;@param value           The string to prepend the variable with&#xA;@return                The prepended string" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847226" ID="ID_1498726847226751" MODIFIED="1498726847226" TEXT="@see" FOLDED="true">
<node CREATED="1498726847226" ID="ID_1498726847226956" MODIFIED="1498726847226" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/append.htm" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/append.htm">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847227" ID="ID_1498726847227276" MODIFIED="1498726847227" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847236" ID="ID_1498726847236934" MODIFIED="1498726847236">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>prepend <font color="#FF0000">{</font> varName args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$varName</font> s
        <b><font color="#0000FF">default</font></b> s <font color="#FF0000">{}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">set</font></b> s <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$args</font> <font color="#FF0000">{}</font><font color="#990000">]</font>$<font color="#FF0000">{</font>s<font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847237" ID="ID_1498726847237520" MODIFIED="1498726847237" TEXT="tapp_lang::stack_trace" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847237" ID="ID_1498726847237896" MODIFIED="1498726847237" TEXT="Log a stack trace at a required point in the execution of the script." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847238" ID="ID_1498726847238257" MODIFIED="1498726847238" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847248" ID="ID_1498726847248280" MODIFIED="1498726847248">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>stack_trace <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> r <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> level <font color="#993399">1</font>
        <b><font color="#0000FF">while</font></b> <font color="#FF0000">{</font> <font color="#009900">$r</font> <font color="#990000">!=</font> <font color="#993399">1</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> r <font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">info</font></b> level <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#990000">[</font><b><font color="#0000FF">info</font></b> level<font color="#990000">]</font> <font color="#990000">-</font> <font color="#009900">$level</font><font color="#990000">]</font><font color="#FF0000">}</font> e<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$r</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log DEBUG <font color="#FF0000">"Called by ${e}."</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">incr</font></b> level
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847248" ID="ID_1498726847248870" MODIFIED="1498726847248" TEXT="tapp_lang::this" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847249" ID="ID_1498726847249345" MODIFIED="1498726847249" TEXT="A reference from object orientation methodology where &quot;this&quot; refers to the&#xA;current application or script running.&#xA;&#xA;Note that symlinks are automatically resolved when looking up the real path&#xA;of the running script.&#xA;&#xA;This can be used to identify the script running and access resources that are&#xA;located relative to the script installation directory.&#xA;&#xA;@return                The real path of the script running" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847249" ID="ID_1498726847249637" MODIFIED="1498726847249" TEXT="@see" FOLDED="true">
<node CREATED="1498726847249" ID="ID_1498726847249839" MODIFIED="1498726847249" TEXT="file_utils::get_real_path" COLOR="#14666b" LINK="#ID_1498726847513582">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847250" ID="ID_1498726847250174" MODIFIED="1498726847250" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847259" ID="ID_1498726847259500" MODIFIED="1498726847259">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>this <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>file_utils<font color="#990000">::</font>get_real_path <font color="#990000">[</font><b><font color="#0000FF">info</font></b> script<font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847260" ID="ID_1498726847260080" MODIFIED="1498726847260" TEXT="tapp_lang::this_dir" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847260" ID="ID_1498726847260492" MODIFIED="1498726847260" TEXT="Returns the directory of the current script running.&#xA;&#xA;@return                The real path to the directory of the script running" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847260" ID="ID_1498726847260780" MODIFIED="1498726847260" TEXT="@see" FOLDED="true">
<node CREATED="1498726847260" ID="ID_1498726847260977" MODIFIED="1498726847260" TEXT="tapp_lang::this" COLOR="#14666b" LINK="#ID_1498726847248870">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847261" ID="ID_1498726847261288" MODIFIED="1498726847261" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847270" ID="ID_1498726847270944" MODIFIED="1498726847270">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>this_dir <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dir <font color="#990000">[</font>this<font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847271" ID="ID_1498726847271522" MODIFIED="1498726847271" TEXT="tapp_lang::this_script" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847271" ID="ID_1498726847271959" MODIFIED="1498726847271" TEXT="Returns the name of the script running.&#xA;&#xA;For example let's say we are running the following script:&#xA;   /path/to/examine.tcl&#xA;&#xA;calling the &quot;this_script&quot; procedure would return:&#xA;   examine&#xA;&#xA;@return                The name of the script running without file extension" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847272" ID="ID_1498726847272299" MODIFIED="1498726847272" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847281" ID="ID_1498726847281734" MODIFIED="1498726847281">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>this_script <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#990000">[</font><b><font color="#0000FF">file</font></b> rootname <font color="#990000">[</font>this<font color="#990000">]]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847282" ID="ID_1498726847282394" MODIFIED="1498726847282" TEXT="tapp_lang::touch" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847282" ID="ID_1498726847282938" MODIFIED="1498726847282" TEXT="Short hand notation for initialising variables to empty strings.&#xA;&#xA;Inspired by the unix tool with the same name for updating timestamps and/or&#xA;creating new empty files.&#xA;&#xA;Note that it does not set or change the variable if the variable already&#xA;exists. Primarily used to ensure that variables exist.&#xA;&#xA;Example usage:&#xA;   touch someVar anotherVar&#xA;&#xA;is equivalent of:&#xA;   set someVar {}&#xA;   set anotherVar {}&#xA;&#xA;Synopsis:&#xA;   touch ?varName varName ...?&#xA;&#xA;@param varName         The variable to initialise if it is currently&#xA;                       undefined" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847283" ID="ID_1498726847283239" MODIFIED="1498726847283" TEXT="@see" FOLDED="true">
<node CREATED="1498726847283" ID="ID_1498726847283440" MODIFIED="1498726847283" TEXT="Also see tapp_lang::default" COLOR="#14666b" LINK="#ID_1498726847132712">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847283" ID="ID_1498726847283759" MODIFIED="1498726847283" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847293" ID="ID_1498726847293541" MODIFIED="1498726847293">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>touch args <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$arg</font> var
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists var<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> var <font color="#FF0000">{}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847294" ID="ID_1498726847294160" MODIFIED="1498726847294" TEXT="tapp_lang::vputs" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847294" ID="ID_1498726847294692" MODIFIED="1498726847294" TEXT="Short hand notation for printing out the value of a set of variables.&#xA;&#xA;This is primarily a development / debug tool that allows for a one-time&#xA;inspection of variable values by printing to standard out.&#xA;&#xA;For example:&#xA;   vputs someVar anotherVar&#xA;&#xA;is equivalent of:&#xA;   puts &quot;someVar = $someVar&quot;&#xA;   puts &quot;anotherVar = $anotherVar&quot;&#xA;&#xA;While this may sometimes be useful in order to get to the bottom of an issue&#xA;the general recommendation is to set up relevant tests instead.&#xA;&#xA;Synopsis:&#xA;   vputs ?varName varName ...?&#xA;&#xA;@param varName         The name of the variable to print to standard out" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847295" ID="ID_1498726847295035" MODIFIED="1498726847295" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847304" ID="ID_1498726847304599" MODIFIED="1498726847304">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> tapp_lang<font color="#990000">::</font>vputs args <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#009900">$arg</font> var
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists var<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">puts</font></b> <font color="#FF0000">"$arg = $var"</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">puts</font></b> <font color="#FF0000">"$arg does not exist"</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726847306" ID="ID_1498726847306707" MODIFIED="1498726847306" TEXT="file_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726847308" ID="ID_1498726847308075" MODIFIED="1498726847308" TEXT="File utilities package that deals with common file operations.&#xA;&#xA;If dry-run is enabled then no harmful file operations will take place." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847308" ID="ID_1498726847308376" MODIFIED="1498726847308" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726847308" ID="ID_1498726847308580" MODIFIED="1498726847308" TEXT="FILE_DATE_PREFIX_FORMAT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847308" ID="ID_1498726847308855" MODIFIED="1498726847308" TEXT="The file date prefix format, defaults to &quot;%Y.%m.%d.&quot;" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847309" ID="ID_1498726847309126" MODIFIED="1498726847309" TEXT="FILE_TIME_BY_DATE_PREFIX" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847309" ID="ID_1498726847309394" MODIFIED="1498726847309" TEXT="Whether to enable determining of time/date based on a prefix in the file name, defaults to disabled" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847309" ID="ID_1498726847309661" MODIFIED="1498726847309" TEXT="FILE_TIME_BY_EXIF" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847309" ID="ID_1498726847309926" MODIFIED="1498726847309" TEXT="Whether to enable determining of time/date based on EXIF information or not, defaults to disabled" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847310" ID="ID_1498726847310206" MODIFIED="1498726847310" TEXT="NEF_EXIF_CMD" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847310" ID="ID_1498726847310466" MODIFIED="1498726847310" TEXT="Determines what command to use in order to extract EXIF information from .NEF files. Defaults to &quot;exiftool&quot;." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847311" ID="ID_1498726847311161" MODIFIED="1498726847311" TEXT="FILE_TIME_MIDNIGHT_ADJUSTMENT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847311" ID="ID_1498726847311430" MODIFIED="1498726847311" TEXT="If the file was created after midnight, but before 5am, then use previous day instead. Only useful if we are looking for the date only. The primary use case here is for sorting photos where one might want pictures taken after midnight to still be placed in the previous day's folder, like for photos taken on New Year's Eve for example. Defaults to disabled." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847311" ID="ID_1498726847311692" MODIFIED="1498726847311" TEXT="CHECKSUM_CMD" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847311" ID="ID_1498726847311957" MODIFIED="1498726847311" TEXT="The command to use to get the checksum of a file. Defaults to: md5sum &quot;%file%&quot;. The %file% placeholder is substituted with the path to the file when the command is being executed." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726847310" ID="ID_1498726847310715" MODIFIED="1498726847310" TEXT="@see" FOLDED="true">
<node CREATED="1498726847310" ID="ID_1498726847310899" MODIFIED="1498726847310" TEXT="http://www.sno.phy.queensu.ca/~phil/exiftool/" COLOR="#14666b" LINK="http://www.sno.phy.queensu.ca/~phil/exiftool/">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847312" ID="ID_1498726847312604" MODIFIED="1498726847312" TEXT="file_utils::analyse" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847313" ID="ID_1498726847313150" MODIFIED="1498726847313" TEXT="Analyse a file using file lstat and add additional info to the array data.&#xA;&#xA;For the main elements in the returned array see the documentation for&#xA;&quot;file stat&quot;, typical values include the file ino ID, mtime, size, etc.&#xA;&#xA;Note that if the file does not exist then the file stat info will not be&#xA;available.&#xA;&#xA;For the added elements see the list below.&#xA;&#xA;Example usage:&#xA;   array set DATA [file_utils::analyse Backup.tgz]&#xA;   if {$DATA(digest) == {BROKEN_LINK}} {&#xA;           ...&#xA;   }&#xA;&#xA;@element file          The path of the file as passed in to the analyse proc&#xA;@element path          The absolute path to the file&#xA;@element name          The file name&#xA;@element ext           The file extension&#xA;@element exists        Whether the file exists or not&#xA;@element type          The file type, will be &quot;null&quot; if the file does not&#xA;                       exist&#xA;@element digest        A short summary of what sort of file we are actually&#xA;                       dealing with, will contain one of the following&#xA;                       values: DIRECTORY, FILE, NON_EXISTING_FILE,&#xA;                       BROKEN_LINK, SYMLINK, UNDETERMINED&#xA;@param file            The file to analyse&#xA;@return                The file info array in list form" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847313" ID="ID_1498726847313465" MODIFIED="1498726847313" TEXT="@see" FOLDED="true">
<node CREATED="1498726847313" ID="ID_1498726847313660" MODIFIED="1498726847313" TEXT="&quot;file lstat&quot; under http://www.tcl.tk/man/tcl8.5/TclCmd/file.htm#M21" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/file.htm#M21">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847313" ID="ID_1498726847313956" MODIFIED="1498726847313" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847324" ID="ID_1498726847324926" MODIFIED="1498726847324">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>analyse <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font>   <font color="#009900">$file</font>
        <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>path<font color="#990000">)</font>   <font color="#990000">[</font>get_absolute_path <font color="#009900">$FILE</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)]</font>
        <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>name<font color="#990000">)</font>   <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$FILE</font><font color="#990000">(</font>path<font color="#990000">)]</font>
        <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>exists<font color="#990000">)</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$FILE</font><font color="#990000">(</font>path<font color="#990000">)]</font>
        <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>ext<font color="#990000">)</font>    <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#990000">[</font><b><font color="#0000FF">file</font></b> extension <font color="#009900">$FILE</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)]]</font>
        <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>type<font color="#990000">)</font>   null

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$FILE</font><font color="#990000">(</font>exists<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">file</font></b> lstat <font color="#009900">$FILE</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font> FILE
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#990000">--</font> <font color="#FF0000">"$FILE(exists)|$FILE(type)"</font> <font color="#FF0000">{</font>
        <font color="#993399">1</font><font color="#990000">|</font>directory <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>digest<font color="#990000">)</font> DIRECTORY <font color="#FF0000">}</font>
        <font color="#993399">1</font><font color="#990000">|</font><b><font color="#0000FF">file</font></b>      <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>digest<font color="#990000">)</font> FILE <font color="#FF0000">}</font>
        <font color="#993399">0</font><font color="#990000">|</font>null      <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>digest<font color="#990000">)</font> NON_EXISTING_FILE <font color="#FF0000">}</font>
        <font color="#993399">0</font><font color="#990000">|</font>link      <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>digest<font color="#990000">)</font> BROKEN_LINK <font color="#FF0000">}</font>
        <font color="#993399">1</font><font color="#990000">|</font>link      <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>digest<font color="#990000">)</font> SYMLINK <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b>     <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> FILE<font color="#990000">(</font>digest<font color="#990000">)</font> UNDETERMINED <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>array get FILE<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847325" ID="ID_1498726847325522" MODIFIED="1498726847325" TEXT="file_utils::append_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847325" ID="ID_1498726847325901" MODIFIED="1498726847325" TEXT="Convenience proc to append content to a given file.&#xA;&#xA;@param file            The file to append to&#xA;@param content         The content/text to append" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847326" ID="ID_1498726847326285" MODIFIED="1498726847326" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847335" ID="ID_1498726847335578" MODIFIED="1498726847335">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>append_file <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> content <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>write_file <font color="#009900">$file</font> <font color="#009900">$content</font> <font color="#FF0000">"a"</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847336" ID="ID_1498726847336137" MODIFIED="1498726847336" TEXT="file_utils::compare" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847336" ID="ID_1498726847336993" MODIFIED="1498726847336" TEXT="This will compare two files based on the output of file_utils::analyse and may be&#xA;used to find out whether the files are the same or whether they are different in&#xA;one way or another.&#xA;&#xA;Typical usage:&#xA;&#xA;  - find out if the files are the same (via symlink hardlink or copy)&#xA;  - find out if one file is newer than the other&#xA;  - find out if the two files are completely different but have the same name&#xA;  - find duplicates&#xA;      &#xA;Possible return values:&#xA;&#xA;   foreach return_code [file_utils::compare &lt;file1&gt; &lt;file2&gt;] {&#xA;   	switch -- $return_code {&#xA;   	NEITHER_EXISTS -&#xA;   	ONLY_LEFT_EXISTS -&#xA;   	ONLY_RIGHT_EXISTS {&#xA;   		# One or both of the files doesn't exist&#xA;   	}&#xA;   	BOTH_EXISTS {&#xA;   		# Generally expected return code&#xA;   	}&#xA;   	LEFT_&lt;FILE_TYPE&gt;_RIGHT_&lt;FILE_TYPE&gt; -&#xA;   	DIFFERENT_TYPE {&#xA;   		# The files compared are of completely different file types&#xA;   		# Generated codes on the form LEFT_&lt;FILE_TYPE&gt;_RIGHT_&lt;FILE_TYPE&gt;&#xA;   		# may also be returned e.g. LEFT_DIRECTORY_RIGHT_FILE&#xA;   	}&#xA;   	SAME_TYPE {&#xA;   		# Generally expected return code&#xA;   	}&#xA;   	SAME_FILE {&#xA;   		# Both files are referring to the same path&#xA;   	}&#xA;   	BOTH_SYMLINKS_TO_SAME_FILE -&#xA;   	RIGHT_SYMLINK_TO_LEFT -&#xA;   	LEFT_SYMLINK_TO_RIGHT {&#xA;   		# One or both of the files are symlinks&#xA;   	}&#xA;   	HARDLINK {&#xA;   		# Both files are hardlinked to the same file&#xA;   	}&#xA;   	DUPLICATE {&#xA;   		# The two files are duplicates&#xA;   	}&#xA;   	LEFT_NEWER_THAN_RIGHT -&#xA;   	RIGHT_NEWER_THAN_LEFT -&#xA;   	SAME_MTIME {&#xA;   		# One of the files are newer than the other or have the same&#xA;   		# modification time&#xA;   	}&#xA;   	LEFT_LARGER_THAN_RIGHT -&#xA;   	RIGHT_LARGER_THAN_LEFT -&#xA;   	SAME_SIZE {&#xA;   		# One of the files are larger than the other or have the same&#xA;   		# size&#xA;   	}&#xA;   	DIFFERENT_FILES {&#xA;   		# Generally expected return code&#xA;   	}&#xA;   	MAD_SCIENTIST {&#xA;   		# This should never happen&#xA;   	}&#xA;   	default {&#xA;   	}&#xA;   	}&#xA;   }&#xA;&#xA;@param file1           The first file&#xA;@param file2           The file to compare to the first file&#xA;@return                A list of relevant return codes" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847337" ID="ID_1498726847337367" MODIFIED="1498726847337" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847353" ID="ID_1498726847353391" MODIFIED="1498726847353">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>compare <font color="#FF0000">{</font> file1 file2 <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> return_codes <font color="#FF0000">{}</font>

        array <b><font color="#0000FF">set</font></b> LEFT  <font color="#990000">[</font>analyse <font color="#009900">$file1</font><font color="#990000">]</font>
        array <b><font color="#0000FF">set</font></b> RIGHT <font color="#990000">[</font>analyse <font color="#009900">$file2</font><font color="#990000">]</font>

        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#FF0000">"$LEFT(exists)$RIGHT(exists)"</font> <font color="#FF0000">{</font>
        <font color="#993399">00</font> <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes NEITHER_EXISTS <font color="#FF0000">}</font>
        <font color="#993399">10</font> <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes ONLY_LEFT_EXISTS <font color="#FF0000">}</font>
        <font color="#993399">01</font> <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes ONLY_RIGHT_EXISTS <font color="#FF0000">}</font>
        <font color="#993399">11</font> <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes BOTH_EXISTS <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes MAD_SCIENTIST
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$LEFT</font><font color="#990000">(</font>type<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>type<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes SAME_TYPE
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes DIFFERENT_TYPE
                <b><font color="#0000FF">lappend</font></b> return_codes LEFT_<font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#009900">$LEFT</font><font color="#990000">(</font>type<font color="#990000">)]</font>_RIGHT_<font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#009900">$RIGHT</font><font color="#990000">(</font>type<font color="#990000">)]</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If both file paths refers to the same file it is the same file.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$LEFT</font><font color="#990000">(</font>path<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>path<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes SAME_FILE
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># The rest of the return codes only apply to existing files</font></i>
        <i><font color="#9A1900"># so return if this is not the case</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$LEFT</font><font color="#990000">(</font>exists<font color="#990000">)</font> <font color="#990000">||</font> <font color="#990000">!</font><font color="#009900">$RIGHT</font><font color="#990000">(</font>exists<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$return_codes</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If both file arguments refer to the same file but path differs.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font>
                   <font color="#009900">$LEFT</font><font color="#990000">(</font>path<font color="#990000">)</font> <font color="#990000">!=</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>path<font color="#990000">)</font>
                <font color="#990000">&amp;&amp;</font> <font color="#009900">$LEFT</font><font color="#990000">(</font>dev<font color="#990000">)</font>  <font color="#990000">==</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>dev<font color="#990000">)</font>
                <font color="#990000">&amp;&amp;</font> <font color="#009900">$LEFT</font><font color="#990000">(</font>ino<font color="#990000">)</font>  <font color="#990000">==</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>ino<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#FF0000">"$LEFT(type)|$RIGHT(type)"</font> <font color="#FF0000">{</font>
                <font color="#FF0000">"link|link"</font>      <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes BOTH_SYMLINKS_TO_SAME_FILE <font color="#FF0000">}</font>
                <font color="#FF0000">"directory|link"</font> <font color="#990000">-</font>
                <font color="#FF0000">"file|link"</font>      <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes RIGHT_SYMLINK_TO_LEFT <font color="#FF0000">}</font>
                <font color="#FF0000">"link|directory"</font> <font color="#990000">-</font>
                <font color="#FF0000">"link|file"</font>      <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes LEFT_SYMLINK_TO_RIGHT <font color="#FF0000">}</font>
                <font color="#FF0000">"directory|directory"</font> <font color="#990000">-</font>
                <font color="#FF0000">"file|file"</font>      <font color="#FF0000">{</font> <b><font color="#0000FF">lappend</font></b> return_codes HARDLINK <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font>The improbable but faulty theory relies on slack data<font color="#990000">.</font> File types are <font color="#009900">$file1_type</font> and <font color="#009900">$file2_type</font><font color="#990000">.</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> return_codes MAD_SCIENTIST
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$LEFT</font><font color="#990000">(</font>mtime<font color="#990000">)</font> <font color="#990000">&gt;</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>mtime<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes LEFT_NEWER_THAN_RIGHT
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$LEFT</font><font color="#990000">(</font>mtime<font color="#990000">)</font> <font color="#990000">&lt;</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>mtime<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes RIGHT_NEWER_THAN_LEFT
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes SAME_MTIME
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$LEFT</font><font color="#990000">(</font>size<font color="#990000">)</font> <font color="#990000">&gt;</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>size<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes LEFT_LARGER_THAN_RIGHT
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$LEFT</font><font color="#990000">(</font>size<font color="#990000">)</font> <font color="#990000">&lt;</font> <font color="#009900">$RIGHT</font><font color="#990000">(</font>size<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes RIGHT_LARGER_THAN_LEFT
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> return_codes SAME_SIZE
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$return_codes</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847354" ID="ID_1498726847354066" MODIFIED="1498726847354" TEXT="file_utils::decode_human_readable_size" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847354" ID="ID_1498726847354512" MODIFIED="1498726847354" TEXT="Procedure to convert a size in human readable format into bytes.&#xA;&#xA;Example usage:&#xA;   file_utils::decode_human_readable_size 2.1M&#xA;&#xA;this would return &quot;2202010&quot;.&#xA;&#xA;@param size            The size in human readable form, e.g. 952K, 1.5M, 7G&#xA;@return                The size in bytes" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847354" ID="ID_1498726847354867" MODIFIED="1498726847354" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847368" ID="ID_1498726847368894" MODIFIED="1498726847368">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>decode_human_readable_size <font color="#FF0000">{</font> size <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> HUMAN_READABLE_SIZE_MAP

        <b><font color="#0000FF">set</font></b> size <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{{</font> <font color="#FF0000">}</font> <font color="#FF0000">{}}</font> <font color="#009900">$size</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">([</font><font color="#993399">0</font><font color="#990000">-</font><font color="#993399">9</font><font color="#990000">.]+)([</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">]*?)(</font>B<font color="#990000">|</font>b<font color="#990000">)?</font>$<font color="#FF0000">}</font> <font color="#009900">$size</font> <font color="#FF0000">{}</font> num denotion bitorbyte<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#FF0000">{</font><font color="#990000">([^</font><font color="#993399">0</font><font color="#990000">-</font><font color="#993399">9</font><font color="#990000">]+)</font><font color="#FF0000">}</font> <font color="#009900">$size</font> <font color="#FF0000">{}</font> size<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> denominator <font color="#993399">1</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$bitorbyte</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$denotion</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{</font>b<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#990000">*</font>byte<font color="#990000">*</font> <font color="#009900">$denotion</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> bitorbyte b
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> d <font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$denotion</font> <font color="#993399">0</font><font color="#990000">]]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists HUMAN_READABLE_SIZE_MAP<font color="#990000">(</font><font color="#009900">$d</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> denominator <font color="#009900">$HUMAN_READABLE_SIZE_MAP</font><font color="#990000">(</font><font color="#009900">$d</font><font color="#990000">)</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$bitorbyte</font> <font color="#990000">==</font> <font color="#FF0000">{</font>b<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> denominator <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$denominator</font> <font color="#990000">/</font> <font color="#993399">8.00</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#990000">%.</font>f <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$num</font> <font color="#990000">*</font> <font color="#009900">$denominator</font><font color="#FF0000">}</font><font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847369" ID="ID_1498726847369494" MODIFIED="1498726847369" TEXT="file_utils::delete_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847369" ID="ID_1498726847369950" MODIFIED="1498726847369" TEXT="Delete a given file.&#xA;&#xA;Warning: Files will be permanently deleted, moving files to a designated&#xA;&quot;trash&quot; directory is recommended unless you are absolutely sure of what you&#xA;are doing.&#xA;&#xA;@param file            The file to permanently delete&#xA;@return                1 if the file was deleted, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847370" ID="ID_1498726847370432" MODIFIED="1498726847370" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847381" ID="ID_1498726847381020" MODIFIED="1498726847381">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>delete_file <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font> <font color="#990000">-</font> Not deleting non<font color="#990000">-</font>existing <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> Pretended to delete <font color="#009900">$file</font> <font color="#990000">(</font>dry run<font color="#990000">)</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b> delete <font color="#990000">-</font>force <font color="#990000">--</font> <font color="#009900">$file</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font> <font color="#990000">-</font> Failed to delete <font color="#009900">$file</font> due to <font color="#009900">$msg</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
        <font color="#FF0000">}</font>

        log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> Deleted <font color="#009900">$file</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847381" ID="ID_1498726847381608" MODIFIED="1498726847381" TEXT="file_utils::determine_dest" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847382" ID="ID_1498726847382089" MODIFIED="1498726847382" TEXT="Determines the destination directory based on a given prefix.&#xA;&#xA;@param prefix          A given prefix, f.ex. &quot;2013-11-23&quot;&#xA;@param destination     The root directory, f.ex. /path/to/&#xA;@return                The destination directory, i.e. /path/to/2013-11-23&#xA;                       if the directory does not already exist, otherwise&#xA;                       the existing directory is returned, e.g.&#xA;                       /path/to/2013-11-23.Photos" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847382" ID="ID_1498726847382457" MODIFIED="1498726847382" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847392" ID="ID_1498726847392768" MODIFIED="1498726847392">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>determine_dest <font color="#FF0000">{</font> prefix destination <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> dest_prefix <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$destination</font> <font color="#009900">$prefix</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> dest <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>path <font color="#009900">$dest_prefix</font> <font color="#990000">*]</font> <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> dest <font color="#009900">$dest_prefix</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$dest</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847393" ID="ID_1498726847393352" MODIFIED="1498726847393" TEXT="file_utils::determine_file_time" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847393" ID="ID_1498726847393890" MODIFIED="1498726847393" TEXT="This proc determines the time/date of a given file according to a set of&#xA;rules depending on configuration.&#xA;&#xA;This is primarily intended for applications that deals with images and&#xA;sorting.&#xA;&#xA;If the given file does not exist then an error is thrown.&#xA;&#xA;The priority order is as follows:&#xA;&#xA;   - if the file contains a date prefix then that is used&#xA;   - otherwise then EXIF data is used if available&#xA;   - finally the file modification time is used if the above fails&#xA;&#xA;See the FILE_TIME_* config items for details.&#xA;&#xA;@param file            The file to determine the time for&#xA;@param format          The output format to return the time in, if not&#xA;                       provided the time will be returned in seconds since&#xA;                       epoch&#xA;@return                The determined time in seconds or the given format" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847394" ID="ID_1498726847394324" MODIFIED="1498726847394" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847406" ID="ID_1498726847406245" MODIFIED="1498726847406">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>determine_file_time <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">format</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"Trying to determine time for an nonexistent file: $file"</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> mtime <font color="#009900">$file</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> prefix_found <font color="#993399">0</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled FILE_TIME_BY_DATE_PREFIX <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> prefix_found <font color="#990000">[</font>get_file_date_prefix <font color="#009900">$file</font> prefix_date<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$prefix_found</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">scan</font></b> <font color="#009900">$prefix_date</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">[</font>cfg get FILE_DATE_PREFIX_FORMAT <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">.%</font>m<font color="#990000">.%</font>d<font color="#990000">.</font><font color="#FF0000">}</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># File date prefix takes precedence over image time</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$prefix_found</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled FILE_TIME_BY_EXIF <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        lassign <font color="#990000">[</font>get_exif_date <font color="#009900">$file</font><font color="#990000">]</font> exif_found exif_date
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$exif_found</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#009900">$exif_date</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled FILE_TIME_MIDNIGHT_ADJUSTMENT <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># If the file was created after midnight but before 5am</font></i>
                        <i><font color="#9A1900"># then use previous day instead. Only useful if we are</font></i>
                        <i><font color="#9A1900"># looking for the date only.</font></i>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">"%H"</font><font color="#990000">]</font> <font color="#990000">&lt;</font> <font color="#993399">5</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">incr</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">-</font><font color="#993399">86400</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$format</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#009900">$format</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$time</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847406" ID="ID_1498726847406854" MODIFIED="1498726847406" TEXT="file_utils::ensure_exists" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847407" ID="ID_1498726847407268" MODIFIED="1498726847407" TEXT="Ensure that a destination directory exists.&#xA;&#xA;If the directory does not already exist then it will be created. If a dry-run&#xA;is performed then no directories will be created.&#xA;&#xA;@param dest            The directory to ensure exists" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847407" ID="ID_1498726847407606" MODIFIED="1498726847407" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847417" ID="ID_1498726847417592" MODIFIED="1498726847417">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>ensure_exists <font color="#FF0000">{</font> dest <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dest</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$dest</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font>Pretending to create directory <font color="#009900">$dest</font> <font color="#990000">(</font>dry<font color="#990000">-</font>run<font color="#990000">)</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font>Creating directory <font color="#009900">$dest</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">file</font></b> mkdir <font color="#009900">$dest</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847418" ID="ID_1498726847418204" MODIFIED="1498726847418" TEXT="file_utils::expand_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847418" ID="ID_1498726847418652" MODIFIED="1498726847418" TEXT="Tests a file matching pattern such as file*.txt and returns a list of files&#xA;that matches.&#xA;&#xA;If no files matches then an empty string is returned.&#xA;&#xA;@param pattern         The file pattern to check&#xA;@return                A list of files that matched the pattern, or an empty&#xA;                       list if no files matched." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847418" ID="ID_1498726847418999" MODIFIED="1498726847419" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847428" ID="ID_1498726847428362" MODIFIED="1498726847428">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>expand_file <font color="#FF0000">{</font> pattern <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>nocomplain <font color="#009900">$pattern</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847428" ID="ID_1498726847428937" MODIFIED="1498726847428" TEXT="file_utils::file_size" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847429" ID="ID_1498726847429435" MODIFIED="1498726847429" TEXT="Procedure to calculate the total size for a given set of files or&#xA;directories.&#xA;&#xA;Warning: This proc can be very resource intensive, use with care.&#xA;&#xA;If the unix utility &quot;du&quot; for reporting disk usage is available then that will&#xA;be used to determine directory sizes, otherwise directories will be processed&#xA;recursively.&#xA;&#xA;Synopsis:&#xA;   file_utils::file_size ?file file ...?&#xA;&#xA;@param file            The file or directory to get the total file size for&#xA;@return                The total size of the given files" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847429" ID="ID_1498726847429802" MODIFIED="1498726847429" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847441" ID="ID_1498726847441462" MODIFIED="1498726847441">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>file_size args <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> AUTO_EXECOK_DU

        <b><font color="#0000FF">set</font></b> size <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> index <font color="#993399">0</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">file</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#009900">$i</font><font color="#990000">]</font>

                <i><font color="#9A1900"># ignore directories . and ..</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file</font><font color="#990000">]</font> <b><font color="#0000FF">in</font></b> <font color="#FF0000">{</font><font color="#990000">.</font> <font color="#990000">..</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> isfile <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">incr</font></b> size <font color="#990000">[</font><b><font color="#0000FF">file</font></b> size <font color="#009900">$file</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">incr</font></b> size <font color="#990000">[</font><b><font color="#0000FF">string</font></b> bytelength <font color="#009900">$file</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$AUTO_EXECOK_DU</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">incr</font></b> size <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">exec</font></b> du <font color="#990000">-</font>bs <font color="#009900">$file</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <i><font color="#9A1900"># Warning: this may be slow</font></i>
                                <b><font color="#0000FF">set</font></b> normal_files <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>nocomplain <font color="#990000">-</font>directory <font color="#009900">$file</font> <font color="#990000">-</font>types <font color="#FF0000">{</font>r<font color="#FF0000">}</font> <font color="#990000">*]</font>
                                <b><font color="#0000FF">set</font></b> hidden_files <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>nocomplain <font color="#990000">-</font>directory <font color="#009900">$file</font> <font color="#990000">-</font>types <font color="#FF0000">{</font>r hidden<font color="#FF0000">}</font> <font color="#990000">*]</font>
                                <b><font color="#0000FF">lappend</font></b> args <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$normal_files</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$hidden_files</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$size</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847442" ID="ID_1498726847442099" MODIFIED="1498726847442" TEXT="file_utils::file_type" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847442" ID="ID_1498726847442525" MODIFIED="1498726847442" TEXT="Returns the file type of the given file.&#xA;&#xA;@param file            The file to retrieve the file type for&#xA;@return                The file type of the given file, or &quot;text&quot; if the file&#xA;                       does not exist" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847442" ID="ID_1498726847442871" MODIFIED="1498726847442" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847452" ID="ID_1498726847452572" MODIFIED="1498726847452">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>file_type <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> file_type <font color="#FF0000">{</font>text<font color="#FF0000">}</font>
        <i><font color="#9A1900"># Due to poor implementation of the Tcl "file type" which will throw an error</font></i>
        <i><font color="#9A1900"># if a file does not exist, combined with "file exists" which returns false</font></i>
        <i><font color="#9A1900"># for a symlink that refers to a non-existing file, we have to do a catch all</font></i>
        <i><font color="#9A1900"># as we need to get the file type of "link" although the file referred to may</font></i>
        <i><font color="#9A1900"># not exist.</font></i>
        <b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font><b><font color="#0000FF">file</font></b> type <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$file_type</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847453" ID="ID_1498726847453154" MODIFIED="1498726847453" TEXT="file_utils::file_writable" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847453" ID="ID_1498726847453613" MODIFIED="1498726847453" TEXT="Convenience proc to check whether a file is writable.&#xA;&#xA;If the file does not exist the proc will check whether the directory of the&#xA;given file is writable.&#xA;&#xA;@param file            The file to check if it can be written to&#xA;@return                1 if the file exists and is writable or if the file&#xA;                       does not exist and the parent directory is writable.&#xA;                       Otherwise 0 is returned." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847453" ID="ID_1498726847453962" MODIFIED="1498726847453" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847463" ID="ID_1498726847463567" MODIFIED="1498726847463">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>file_writable <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">file</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> writable <font color="#009900">$file</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847464" ID="ID_1498726847464179" MODIFIED="1498726847464" TEXT="file_utils::get_absolute_path" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847464" ID="ID_1498726847464609" MODIFIED="1498726847464" TEXT="Convenience proc as a reminder that it is possible to use file normalize for&#xA;this purpose.&#xA;&#xA;@param file            The file to get the absolute path for&#xA;@return                The absolute path (output from file normalize)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847464" ID="ID_1498726847464954" MODIFIED="1498726847464" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847474" ID="ID_1498726847474354" MODIFIED="1498726847474">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>get_absolute_path <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> normalize <font color="#009900">$file</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847474" ID="ID_1498726847474937" MODIFIED="1498726847474" TEXT="file_utils::get_checksum" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847475" ID="ID_1498726847475402" MODIFIED="1498726847475" TEXT="Procedure to get the file checksum / hash.&#xA;&#xA;The checksum command is configurable, see the CHECKSUM_CMD configuration&#xA;item. Throws an error if the checksum could not be made.&#xA;&#xA;@param file            The file to retrieve the checksum for&#xA;@return                The calculated checksum, or empty string if anything&#xA;                       has gone astray." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847475" ID="ID_1498726847475753" MODIFIED="1498726847475" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847486" ID="ID_1498726847486896" MODIFIED="1498726847486">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>get_checksum <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> replace_list <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">%</font><b><font color="#0000FF">file</font></b><font color="#990000">%</font> <font color="#009900">$file</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> cmd <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$replace_list</font> <font color="#990000">[</font>cfg get CHECKSUM_CMD <font color="#FF0000">{</font>md5sum <font color="#FF0000">"%file%"</font><font color="#FF0000">}</font><font color="#990000">]]</font>
        <b><font color="#0000FF">set</font></b> ignore_dry_run <font color="#993399">1</font>
        
        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>exec_utils<font color="#990000">::</font>execute <font color="#009900">$cmd</font> <font color="#990000">-</font>ignore_dry_run<font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">([</font><font color="#993399">0</font><font color="#990000">-</font>9a<font color="#990000">-</font>fA<font color="#990000">-</font>F<font color="#990000">]</font><font color="#FF0000">{</font><font color="#993399">31</font><font color="#FF0000">}</font><font color="#990000">[</font><font color="#993399">0</font><font color="#990000">-</font>9a<font color="#990000">-</font>fA<font color="#990000">-</font>F<font color="#990000">]+?)</font><font color="#FF0000">}</font> <font color="#009900">$output</font> checksum<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$checksum</font>
        <font color="#FF0000">}</font>
        
        log ERROR <font color="#FF0000">{</font>Checksum not found<font color="#990000">,</font> output was<font color="#990000">:</font> <font color="#009900">$output</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">error</font></b> <font color="#FF0000">"file_utils::get_checksum: Checksum not found, output was: $output"</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847487" ID="ID_1498726847487507" MODIFIED="1498726847487" TEXT="file_utils::get_exif_date" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847487" ID="ID_1498726847488005" MODIFIED="1498726847488" TEXT="Attempt to get the EXIF date from a given file.&#xA;&#xA;This proc depends on the jpeg package from tcllib for retrieving the EXIF&#xA;information from JPEG files&#xA;&#xA;@param file            The file to retrieve the EXIF information from&#xA;@return                A list on the form &lt;found&gt; &lt;time&gt; where the former&#xA;                       indicates whether the EXIF information was found and&#xA;                       the latter is the time found (will be empty string if&#xA;                       no EXIF information was found)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847488" ID="ID_1498726847488370" MODIFIED="1498726847488" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847500" ID="ID_1498726847500690" MODIFIED="1498726847500">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>get_exif_date <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">package</font></b> require jpeg <font color="#993399">0.3</font><font color="#990000">.</font><font color="#993399">5</font> <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Package jpeg <font color="#993399">0.3</font><font color="#990000">.</font><font color="#993399">5</font> is not present<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"Trying to read exif for an nonexistent file: $file"</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> exif_found <font color="#993399">0</font>
        touch <b><font color="#0000FF">time</font></b>
        <b><font color="#0000FF">set</font></b> extension <font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#990000">[</font><b><font color="#0000FF">file</font></b> extension <font color="#009900">$file</font><font color="#990000">]]</font>

        <i><font color="#9A1900"># The EXIF information (if present)</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">package</font></b> present jpeg <font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[::</font>jpeg<font color="#990000">::</font>isJPEG <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                array <b><font color="#0000FF">set</font></b> EXIF <font color="#990000">[::</font>jpeg<font color="#990000">::</font>getExif <font color="#009900">$file</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> exifkey <font color="#FF0000">{</font>DateTimeOriginal DateTimeDigitized DateTime<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists EXIF<font color="#990000">(</font><font color="#009900">$exifkey</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">scan</font></b> <font color="#009900">$EXIF</font><font color="#990000">(</font><font color="#009900">$exifkey</font><font color="#990000">)</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">"%Y:%m:%d %H:%M:%S"</font><font color="#990000">]</font>
                                <b><font color="#0000FF">set</font></b> exif_found <font color="#993399">1</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$extension</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">.</font>NEF<font color="#FF0000">}</font> <font color="#990000">&amp;&amp;</font> $<font color="#990000">::</font>tcl_platform<font color="#990000">(</font>platform<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>unix<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                array <b><font color="#0000FF">set</font></b> EXIF <font color="#990000">[</font>_get_nef_exif <font color="#009900">$file</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> exifkey <font color="#FF0000">{{</font>Date<font color="#990000">/</font>Time Original<font color="#FF0000">}</font> <font color="#FF0000">{</font>Create Date<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists EXIF<font color="#990000">(</font><font color="#009900">$exifkey</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">scan</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$EXIF</font><font color="#990000">(</font><font color="#009900">$exifkey</font><font color="#990000">)</font> <font color="#993399">0</font> <font color="#993399">18</font><font color="#990000">]</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">"%Y:%m:%d %H:%M:%S"</font><font color="#990000">]</font>
                                <b><font color="#0000FF">set</font></b> exif_found <font color="#993399">1</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$exif_found</font> <font color="#009900">$time</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847501" ID="ID_1498726847501322" MODIFIED="1498726847501" TEXT="file_utils::get_file_date_prefix" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847501" ID="ID_1498726847501789" MODIFIED="1498726847501" TEXT="Attempts to retrieve a date prefix from a file.&#xA;&#xA;The date prefix can be specified with the FILE_DATE_PREFIX_FORMAT&#xA;configuration item.&#xA;&#xA;@param file            The file to retrieve the date prefix from&#xA;@param match           The variable name to store the prefix in if found&#xA;@return                1 if the file contained a prefix, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847502" ID="ID_1498726847502203" MODIFIED="1498726847502" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847512" ID="ID_1498726847512988" MODIFIED="1498726847512">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>get_file_date_prefix <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> match <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$match</font> prefix_date
        <b><font color="#0000FF">set</font></b> regex <font color="#990000">[</font>text_utils<font color="#990000">::</font>convert_clock_format_to_regexp <font color="#990000">[</font>cfg get FILE_DATE_PREFIX_FORMAT <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">.%</font>m<font color="#990000">.%</font>d<font color="#990000">.</font><font color="#FF0000">}</font><font color="#990000">]]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">"^($regex)"</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file</font><font color="#990000">]</font> <font color="#990000">-</font> prefix_date<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847513" ID="ID_1498726847513582" MODIFIED="1498726847513" TEXT="file_utils::get_real_path" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847514" ID="ID_1498726847514071" MODIFIED="1498726847514" TEXT="Get the real path to the file, i.e. with any links resolved.&#xA;&#xA;This simulates by recursion the unix command line tool readlink -f &lt;file&gt;.&#xA;&#xA;@param file            The file to get the real path for&#xA;@return                The real path of the file (like absolute path, but&#xA;                       with symlinks resolved)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847514" ID="ID_1498726847514428" MODIFIED="1498726847514" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847534" ID="ID_1498726847534291" MODIFIED="1498726847534">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>get_real_path <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        array <b><font color="#0000FF">set</font></b> FILE <font color="#990000">[</font>analyse <font color="#009900">$file</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$FILE</font><font color="#990000">(</font>digest<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>SYMLINK<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> linked_file <font color="#990000">[</font><b><font color="#0000FF">file</font></b> readlink <font color="#009900">$file</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> pathtype <font color="#009900">$linked_file</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{</font>relative<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> linked_file <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$file</font><font color="#990000">]</font> <font color="#009900">$linked_file</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>get_real_path <font color="#009900">$linked_file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$FILE</font><font color="#990000">(</font>path<font color="#990000">)</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847535" ID="ID_1498726847535263" MODIFIED="1498726847535" TEXT="file_utils::human_readable_size" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847535" ID="ID_1498726847535770" MODIFIED="1498726847535" TEXT="Convert bytes into a human readable size.&#xA;&#xA;For example:&#xA;   file_utils::human_readable_size 2202010&#xA;&#xA;would return &quot;2.1M&quot;&#xA;&#xA;@param bytes           Size in bytes to convert into human readable form&#xA;@return                The size in human readable form,  e.g. 841K, 1.2M, 4G" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847536" ID="ID_1498726847536147" MODIFIED="1498726847536" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847551" ID="ID_1498726847551948" MODIFIED="1498726847551">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>human_readable_size <font color="#FF0000">{</font> bytes <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> HUMAN_READABLE_SIZE_MAP
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> is integer <font color="#009900">$bytes</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#009900">$bytes</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"</font><font color="#CC33CC">\"</font><font color="#FF0000">$bytes</font><font color="#CC33CC">\"</font><font color="#FF0000"> is not a number"</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> denotion B<font color="#990000">;</font> <i><font color="#9A1900"># Byte</font></i>
        <b><font color="#0000FF">set</font></b> denominator <font color="#993399">1</font>
        <b><font color="#0000FF">foreach</font></b> den <font color="#FF0000">{</font>B K M G T P E Z Y<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$bytes</font> <font color="#990000">&lt;</font> <font color="#009900">$HUMAN_READABLE_SIZE_MAP</font><font color="#990000">(</font><font color="#009900">$den</font><font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">break</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> denotion <font color="#009900">$den</font>
                <b><font color="#0000FF">set</font></b> denominator <font color="#009900">$HUMAN_READABLE_SIZE_MAP</font><font color="#990000">(</font><font color="#009900">$den</font><font color="#990000">).</font><font color="#993399">00</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> num <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$bytes</font> <font color="#990000">/</font> <font color="#009900">$denominator</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$num</font> <font color="#990000">&lt;</font> <font color="#993399">10</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> num <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#990000">%.</font>1f <font color="#009900">$num</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> num <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#990000">%.</font>0f <font color="#009900">$num</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$num$denotion</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847552" ID="ID_1498726847552558" MODIFIED="1498726847552" TEXT="file_utils::is_empty" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847553" ID="ID_1498726847553013" MODIFIED="1498726847553" TEXT="Checks whether a file or directory is empty.&#xA;&#xA;@param file            The file to check if empty&#xA;@return                1 if the file does not exist, if it is a normal file&#xA;                       and it's size is zero, if the file is a directory that&#xA;                       contains no files. In all other circumstances 0 will&#xA;                       be returned." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847553" ID="ID_1498726847553366" MODIFIED="1498726847553" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847564" ID="ID_1498726847564478" MODIFIED="1498726847564">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>is_empty <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font><b><font color="#0000FF">file</font></b> type <font color="#009900">$file</font><font color="#990000">]</font> <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Failure to retrieve <b><font color="#0000FF">file</font></b> type assumes <b><font color="#0000FF">file</font></b> does not exist thus empty<font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> command _is_empty_$<font color="#FF0000">{</font>file_type<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> is_empty <font color="#990000">[</font>_is_empty_$<font color="#FF0000">{</font>file_type<font color="#FF0000">}</font> <font color="#009900">$file</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> is <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#993399">0</font> <font color="#FF0000">{</font>is not<font color="#FF0000">}</font> <font color="#993399">1</font> is<font color="#FF0000">}</font> <font color="#009900">$is_empty</font><font color="#990000">]</font>
                log DEBUG <font color="#FF0000">{</font>The $<font color="#FF0000">{</font>file_type<font color="#FF0000">}</font> <font color="#009900">$file</font> <font color="#009900">$is</font> empty<font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$is_empty</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847565" ID="ID_1498726847565092" MODIFIED="1498726847565" TEXT="file_utils::load_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847565" ID="ID_1498726847565565" MODIFIED="1498726847565" TEXT="Load an array from file.&#xA;&#xA;@param arr             The name of the variable to load the array data into&#xA;@param data_file       The file to load the array data from&#xA;@return                The array data in list form" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847565" ID="ID_1498726847565928" MODIFIED="1498726847565" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847576" ID="ID_1498726847576778" MODIFIED="1498726847576">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>load_array <font color="#FF0000">{</font> arr data_file <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">set</font></b> array_data <font color="#FF0000">{}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$data_file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> array_data <font color="#990000">[</font>read_file <font color="#009900">$data_file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arr</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$arr</font> ARRAY
                array unset ARRAY
                array <b><font color="#0000FF">set</font></b> ARRAY <font color="#009900">$array_data</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$array_data</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847577" ID="ID_1498726847577374" MODIFIED="1498726847577" TEXT="file_utils::move" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847577" ID="ID_1498726847577896" MODIFIED="1498726847577" TEXT="This proc handles common move operations.&#xA;&#xA;The proc handles the following:&#xA;&#xA;   - not attempting to move the file to the same destination / folder&#xA;   - not moving the file if another file with same name already exists at&#xA;     destination&#xA;   - not making any changes if performing a dry-run&#xA;   - unified logging&#xA;&#xA;@param file            The file to move&#xA;@param dest            The destination file or directory to move the file to&#xA;@return                1 if the file was moved, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847578" ID="ID_1498726847578292" MODIFIED="1498726847578" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847591" ID="ID_1498726847591698" MODIFIED="1498726847591">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>move <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> dest <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <i><font color="#9A1900"># Verify that we are not attempting to move the file to the same location</font></i>
        <b><font color="#0000FF">set</font></b> file_name <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file</font><font color="#990000">]</font>

        <b><font color="#0000FF">file</font></b> stat <font color="#009900">$file</font> FILE
        <b><font color="#0000FF">set</font></b> DEST<font color="#990000">(</font>ino<font color="#990000">)</font> <font color="#990000">-</font><font color="#993399">1</font>
        <b><font color="#0000FF">set</font></b> DEST<font color="#990000">(</font>dev<font color="#990000">)</font> <font color="#990000">-</font><font color="#993399">1</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg disabled DRY_RUN<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$dest</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$dest</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$dest</font> <font color="#009900">$file_name</font><font color="#990000">]]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">file</font></b> stat <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$dest</font> <font color="#009900">$file_name</font><font color="#990000">]</font> DEST
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">file</font></b> stat <font color="#009900">$dest</font> DEST
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If this is the same device and we have the same inode ID then we are referring to the same file.</font></i>
        <i><font color="#9A1900"># Note: Two different files can have the same inode ID on separate drives / media.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$FILE</font><font color="#990000">(</font>dev<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$DEST</font><font color="#990000">(</font>dev<font color="#990000">)</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$FILE</font><font color="#990000">(</font>ino<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$DEST</font><font color="#990000">(</font>ino<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Not moving the <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font> to same destination<font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$DEST</font><font color="#990000">(</font>ino<font color="#990000">)</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Another <b><font color="#0000FF">file</font></b> already exists at destination not moving <b><font color="#0000FF">file</font></b><font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log INFO <font color="#FF0000">{</font>Pretending to move the <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font> to <font color="#009900">$dest</font> <font color="#990000">(</font>dry<font color="#990000">-</font>run<font color="#990000">)</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        log INFO <font color="#FF0000">{</font>Moving the <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font> to <font color="#009900">$dest</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">rename</font></b> <font color="#009900">$file</font> <font color="#009900">$dest</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                log ERROR <font color="#FF0000">{</font>An <b><font color="#0000FF">error</font></b> occurred <b><font color="#0000FF">while</font></b> attempting to move the <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font> to <font color="#009900">$dest</font><font color="#990000">\</font>nError was<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847592" ID="ID_1498726847592335" MODIFIED="1498726847592" TEXT="file_utils::purge_empty_directories" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847592" ID="ID_1498726847592764" MODIFIED="1498726847592" TEXT="This proc will go through a list of directories and remove any that are empty.&#xA;&#xA;@param directories     List of directories to purge" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847593" ID="ID_1498726847593100" MODIFIED="1498726847593" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847603" ID="ID_1498726847603756" MODIFIED="1498726847603">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>purge_empty_directories <font color="#FF0000">{</font> directories <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">foreach</font></b> dir <font color="#009900">$directories</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$dir</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># If directory use glob to check whether it contains any files.</font></i>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>nocomplain <font color="#990000">-</font>directory <font color="#009900">$dir</font> <font color="#990000">*]]</font> <font color="#990000">==</font> <font color="#993399">0</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        log INFO <font color="#FF0000">{</font>Pretending to remove directory <font color="#009900">$dir</font><font color="#FF0000">}</font>
                                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b> delete <font color="#009900">$dir</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                log ERROR <font color="#FF0000">{</font>An <b><font color="#0000FF">error</font></b> occurred attempting to remove directory <font color="#009900">$dir</font><font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                                log INFO <font color="#FF0000">{</font>Removed empty directory <font color="#009900">$dir</font><font color="#FF0000">}</font>
                                        <font color="#FF0000">}</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847604" ID="ID_1498726847604363" MODIFIED="1498726847604" TEXT="file_utils::read_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847604" ID="ID_1498726847604841" MODIFIED="1498726847604" TEXT="Convenience proc to read and return the content of a given file.&#xA;&#xA;Error will be thrown if the file does not exist or the user has no permission&#xA;to read the file.&#xA;&#xA;@param file            The file to read&#xA;@return                The file content as plain text" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847605" ID="ID_1498726847605183" MODIFIED="1498726847605" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847615" ID="ID_1498726847615421" MODIFIED="1498726847615">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>read_file <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font>Cannot <b><font color="#0000FF">read</font></b> non<font color="#990000">-</font>existing <b><font color="#0000FF">file</font></b> <font color="#990000">(</font><font color="#009900">$file</font><font color="#990000">)</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> readable <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font>No permission to <b><font color="#0000FF">read</font></b> <b><font color="#0000FF">file</font></b> <font color="#990000">(</font><font color="#009900">$file</font><font color="#990000">)</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> fp <font color="#990000">[</font><b><font color="#0000FF">open</font></b> <font color="#009900">$file</font> r<font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> file_data <font color="#990000">[</font><b><font color="#0000FF">read</font></b> <font color="#009900">$fp</font><font color="#990000">]</font>
        <b><font color="#0000FF">close</font></b> <font color="#009900">$fp</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$file_data</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847616" ID="ID_1498726847616034" MODIFIED="1498726847616" TEXT="file_utils::recurse_directory" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847616" ID="ID_1498726847616562" MODIFIED="1498726847616" TEXT="Recurses a directory and returns two lists:&#xA;&#xA;   - a list of all subfolders&#xA;   - a list of all files across all subfolders&#xA;&#xA;@param directory       The directory to recurse&#xA;@param filters         Optional filters to only return particular files.&#xA;                       Can be a list of glob patterns like {*.cfg *.tcl},&#xA;                       defaults to selecting all files.&#xA;@return                A list containing two lists, one containing all&#xA;                       subfolders and one containing all files" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847616" ID="ID_1498726847616915" MODIFIED="1498726847616" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847633" ID="ID_1498726847633897" MODIFIED="1498726847633">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>recurse_directory <font color="#FF0000">{</font> directory <font color="#FF0000">{</font>filters <font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$directory</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> files <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>nocomplain <font color="#990000">-</font>types <font color="#FF0000">{</font>f r<font color="#FF0000">}</font> <font color="#990000">-</font>path <font color="#FF0000">"$directory/"</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$filters</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> dirs  <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>nocomplain <font color="#990000">-</font>types <font color="#FF0000">{</font>d r<font color="#FF0000">}</font> <font color="#990000">-</font>path <font color="#FF0000">"$directory/"</font> <font color="#990000">*]</font>
        <b><font color="#0000FF">set</font></b> subdirs <font color="#FF0000">{}</font>

        <b><font color="#0000FF">foreach</font></b> dir <font color="#009900">$dirs</font> <font color="#FF0000">{</font>
                lassign <font color="#990000">[</font>file_utils<font color="#990000">::</font>recurse_directory <font color="#009900">$dir</font><font color="#990000">]</font> subd subf
                <b><font color="#0000FF">set</font></b> files   <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$files</font>   <font color="#009900">$subf</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> subdirs <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$subdirs</font> <font color="#009900">$subd</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> dirs <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$dirs</font> <font color="#009900">$subdirs</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$dirs</font> <font color="#009900">$files</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847634" ID="ID_1498726847634642" MODIFIED="1498726847634" TEXT="file_utils::save_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847635" ID="ID_1498726847635151" MODIFIED="1498726847635" TEXT="Save an array to file.&#xA;&#xA;@param arr             The name of the variable to retrieve the array data&#xA;                       from&#xA;@param data_file       The file to save the array data to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847635" ID="ID_1498726847635505" MODIFIED="1498726847635" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847646" ID="ID_1498726847646215" MODIFIED="1498726847646">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>save_array <font color="#FF0000">{</font> arr data_file <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arr</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$arr</font> ARRAY
                <b><font color="#0000FF">set</font></b> content <font color="#990000">[</font>array get ARRAY<font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> content <font color="#009900">$arr</font>
        <font color="#FF0000">}</font>

        write_file <font color="#009900">$data_file</font> <font color="#009900">$content</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847646" ID="ID_1498726847646831" MODIFIED="1498726847646" TEXT="file_utils::write_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847647" ID="ID_1498726847647385" MODIFIED="1498726847647" TEXT="Convenience proc to write data to a given file.&#xA;&#xA;Note that if the file already exists then the content will be overwritten.&#xA;&#xA;@param file            The file to append to&#xA;@param content         The content/text to append&#xA;@param mode            The mode to write to the file with, defaults to &quot;w&quot;&#xA;                       which is to (over)write the file. An alternative is&#xA;                       to pass in &quot;a&quot; in order to append to the file." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847647" ID="ID_1498726847647718" MODIFIED="1498726847647" TEXT="@see" FOLDED="true">
<node CREATED="1498726847647" ID="ID_1498726847647927" MODIFIED="1498726847647" TEXT="Also see file_utils::append_file" COLOR="#14666b" LINK="#ID_1498726847325522">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847648" ID="ID_1498726847648253" MODIFIED="1498726847648" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847659" ID="ID_1498726847659872" MODIFIED="1498726847659">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> file_utils<font color="#990000">::</font>write_file <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> content <font color="#FF0000">{</font>mode <font color="#FF0000">"w"</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

         assert <font color="#FF0000">{</font><font color="#009900">$mode</font> <b><font color="#0000FF">in</font></b> <font color="#FF0000">{</font>w a<font color="#FF0000">}}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>file_writable <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font>No permission to write to <b><font color="#0000FF">file</font></b> <font color="#990000">(</font><font color="#009900">$file</font><font color="#990000">)</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font>Pretending to write to <b><font color="#0000FF">file</font></b> <font color="#990000">(</font><font color="#009900">$file</font><font color="#990000">)</font> <font color="#990000">(</font>dry<font color="#990000">-</font>run<font color="#990000">)</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> fp <font color="#990000">[</font><b><font color="#0000FF">open</font></b> <font color="#009900">$file</font> <font color="#009900">$mode</font><font color="#990000">]</font>
                <b><font color="#0000FF">puts</font></b> <font color="#009900">$fp</font> <font color="#009900">$content</font>
                <b><font color="#0000FF">flush</font></b> <font color="#009900">$fp</font>
                <b><font color="#0000FF">close</font></b> <font color="#009900">$fp</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726847815" ID="ID_1498726847815812" MODIFIED="1498726847815" TEXT="cfg_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726847816" ID="ID_1498726847816876" MODIFIED="1498726847816" TEXT="Configuration facility.&#xA;&#xA;The idea with this configuration package is to use the same naming convention&#xA;as the array proc uses, i.e. we have cfg get, cfg set, cfg names etc.&#xA;&#xA;A rather specific feature that is worth noting is that it is possible to have&#xA;a configuration file automatically generated if it doesn't already exist when&#xA;the application starts.&#xA;If the configuration file has the same name as the application namespace, and&#xA;the namespace contains a proc named &quot;write_default_config&quot;, then that proc&#xA;will be called (assuming that it will write the default configuration to&#xA;disk)." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847817" ID="ID_1498726847817169" MODIFIED="1498726847817" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726847817" ID="ID_1498726847817368" MODIFIED="1498726847817" TEXT="MASKED_CFG" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847817" ID="ID_1498726847817652" MODIFIED="1498726847817" TEXT="List of specific configuration items that should not be listed in full when doing cfg print. Defaults to HELP, ARGS and MOREARGS as these tend to contain a lot of data and is not particularly useful for debugging purposes." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847817" ID="ID_1498726847817925" MODIFIED="1498726847817" TEXT="CFG_MULTI_LINE_SEPARATOR" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726847818" ID="ID_1498726847818242" MODIFIED="1498726847818" TEXT="Configuration items can span multiple lines if they have a trailing backslash at the end of each line. In most cases it is just fine to join each line adding up to one long string. Typically this will be a list of values to set a given configuration item to. In some special cases, however, the lines in themselves might be of relevance and the application may need to parse each line separately. This is where this configuration item comes in. By default this will be set to a space character meaning that each line is joined by a space. Setting the config to a pipe character or a null value (\u0) for example would allow the application to split the config value based on this character in order to process each line as a list." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726847818" ID="ID_1498726847818674" MODIFIED="1498726847818" TEXT="cfg all" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847819" ID="ID_1498726847819017" MODIFIED="1498726847819" TEXT="Returns all configuration values in key value form like one would expect from&#xA;an &quot;array get&quot; call.&#xA;&#xA;Synopsis:&#xA;   cfg all&#xA;&#xA;@return                All configuration items and values in list form" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847819" ID="ID_1498726847819325" MODIFIED="1498726847819" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847829" ID="ID_1498726847829037" MODIFIED="1498726847829">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg all <font color="#990000">--</font>params <font color="#FF0000">{}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>array get CFG<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847829" ID="ID_1498726847829623" MODIFIED="1498726847829" TEXT="cfg append" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847830" ID="ID_1498726847830076" MODIFIED="1498726847830" TEXT="Appends a value to a given configuration item.&#xA;&#xA;Synopsis:&#xA;   cfg append &lt;config&gt; &lt;value&gt;&#xA;&#xA;@param config          Name of the configuration item to modify&#xA;@param value           The value to append to the configuration item" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847830" ID="ID_1498726847830436" MODIFIED="1498726847830" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847840" ID="ID_1498726847840183" MODIFIED="1498726847840">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg <b><font color="#0000FF">append</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> name value <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">append</font></b> CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#009900">$value</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847840" ID="ID_1498726847840779" MODIFIED="1498726847840" TEXT="cfg disable" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847841" ID="ID_1498726847841198" MODIFIED="1498726847841" TEXT="Disable functionality by setting a given configuration item to 0.&#xA;&#xA;Synopsis:&#xA;   cfg disable &lt;config&gt;&#xA;&#xA;@param config          Name of the configuration item to disable" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847841" ID="ID_1498726847841548" MODIFIED="1498726847841" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847851" ID="ID_1498726847851311" MODIFIED="1498726847851">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg disable <font color="#990000">--</font>params <font color="#FF0000">{</font> name <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">set</font></b> CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847851" ID="ID_1498726847851892" MODIFIED="1498726847851" TEXT="cfg disabled" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847852" ID="ID_1498726847852436" MODIFIED="1498726847852" TEXT="Indicates whether a particular functionality (config item) is disabled.&#xA;&#xA;Synopsis:&#xA;   cfg disabled &lt;config&gt; ?&lt;default&gt;?&#xA;&#xA;@param config          Name of the configuration item to check&#xA;@param default         The default value that should be used if the&#xA;                       configuration item is not set. It should be clarified,&#xA;                       however, that the default value here is in respect to&#xA;                       the value of the configuration item and not whether it&#xA;                       is disabled or not. E.g. passing in &quot;1&quot; as the default&#xA;                       will mean that the configuration item is enabled by&#xA;                       default meaning it is not disabled so 0 would be&#xA;                       returned. Typically no default option is provided for&#xA;                       this option as if a configuration item is not set then&#xA;                       it will be interpreted as not being enabled.&#xA;@return                1 if the configuration is disabled, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847852" ID="ID_1498726847852816" MODIFIED="1498726847852" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847862" ID="ID_1498726847862768" MODIFIED="1498726847862">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg disabled <font color="#990000">--</font>params <font color="#FF0000">{</font> name <font color="#FF0000">{</font> <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg enabled <font color="#009900">$name</font> <font color="#009900">$default</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847863" ID="ID_1498726847863389" MODIFIED="1498726847863" TEXT="cfg enable" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847863" ID="ID_1498726847863815" MODIFIED="1498726847863" TEXT="Enable functionality by setting a given configuration item to 1.&#xA;&#xA;Synopsis:&#xA;   cfg enable &lt;config&gt;&#xA;&#xA;@param config          Name of the configuration item to enable" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847864" ID="ID_1498726847864167" MODIFIED="1498726847864" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847874" ID="ID_1498726847874386" MODIFIED="1498726847874">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg enable <font color="#990000">--</font>params <font color="#FF0000">{</font> name <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">set</font></b> CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847874" ID="ID_1498726847874978" MODIFIED="1498726847874" TEXT="cfg enabled" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847875" ID="ID_1498726847875469" MODIFIED="1498726847875" TEXT="Indicates whether a particular functionality (config item) is enabled.&#xA;&#xA;Synopsis:&#xA;   cfg enabled &lt;config&gt; ?&lt;default&gt;?&#xA;&#xA;@param config          Name of the configuration item to check&#xA;@param default         The default value that should be used if the&#xA;                       configuration item is not set.&#xA;@return                1 if the configuration is enabled, interpreted by the&#xA;                       first character of the value being one of &quot;1&quot;, &quot;Y&quot;, &#xA;                       &quot;T&quot;, or &quot;A&quot; which should cover cases like &quot;1&quot;, &quot;Yes&quot;,&#xA;                       &quot;True&quot;, &quot;Active&quot;" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847875" ID="ID_1498726847875840" MODIFIED="1498726847875" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847886" ID="ID_1498726847886373" MODIFIED="1498726847886">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg enabled <font color="#990000">--</font>params <font color="#FF0000">{</font> name <font color="#FF0000">{</font> <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> cfg <font color="#009900">$CFG</font><font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> cfg <font color="#009900">$default</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>nocase <font color="#FF0000">{</font>Y T <font color="#993399">1</font> A<font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$cfg</font> <font color="#993399">0</font><font color="#990000">]]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847886" ID="ID_1498726847886991" MODIFIED="1498726847886" TEXT="cfg exists" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847887" ID="ID_1498726847887452" MODIFIED="1498726847887" TEXT="Checks whether a particular configuration item exists (is set).&#xA;&#xA;Synopsis:&#xA;   cfg exists &lt;config&gt;&#xA;&#xA;@param config          The name of the configuration item to check if it&#xA;                       exists or not&#xA;@return                1 if the configuration item is set, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847887" ID="ID_1498726847887906" MODIFIED="1498726847887" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847899" ID="ID_1498726847899557" MODIFIED="1498726847899">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg exists <font color="#990000">--</font>params <font color="#FF0000">{</font> name <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847900" ID="ID_1498726847900175" MODIFIED="1498726847900" TEXT="cfg file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847900" ID="ID_1498726847900836" MODIFIED="1498726847900" TEXT="Loads a configuration file.&#xA;&#xA;Specifies a configuration file to load configuration from.&#xA;&#xA;Lines starting with a hash (#) are being treated as comments and will be&#xA;ignored.&#xA;&#xA;Lines starting with !include specifies other configuration files to be&#xA;loaded. This can be used to store the common settings in a base configuration&#xA;file and have environment specific files that only specifies configuration&#xA;relevant for the given environment. The !include statements are processed as&#xA;soon as they are found and loaded configuration files may themselves contain&#xA;further !include statements.&#xA;&#xA;Infinite loops are automatically detected and resolved. Configuration items&#xA;set after a file has been loaded will override those set in the included&#xA;file(s).&#xA;&#xA;Multi-line configuration items are supported and will need a trailng&#xA;backslash.&#xA;&#xA;When a file is being loaded any configuration items previously set in the&#xA;file is available through a Tcl variable with the same name. This does not&#xA;work cross file loads, i.e. the configuration items set in an included file&#xA;will not be available as a variable. Consider using configuration subsitution&#xA;placeholders instead (e.g. %CONFIG_ITEM%).&#xA;&#xA;@param file_name       The name of the file to load" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847901" ID="ID_1498726847901175" MODIFIED="1498726847901" TEXT="@see" FOLDED="true">
<node CREATED="1498726847901" ID="ID_1498726847901391" MODIFIED="1498726847901" TEXT="cfg subst for more information on substitution variables" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847901" ID="ID_1498726847901729" MODIFIED="1498726847901" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847918" ID="ID_1498726847918917" MODIFIED="1498726847918">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg <b><font color="#0000FF">file</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> file_name <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>SOURCED_CFG_FILES
        
        <b><font color="#0000FF">set</font></b> file_name <font color="#990000">[</font><b><font color="#0000FF">file</font></b> normalize <font color="#009900">$file_name</font><font color="#990000">]</font>
        log DEBUG <font color="#FF0000">{</font>Loading config <b><font color="#0000FF">file</font></b> <font color="#009900">$file_name</font><font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file_name</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log DEBUG <font color="#FF0000">{</font>Config <b><font color="#0000FF">file</font></b> <font color="#009900">$file_name</font> does not exist<font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> cfg_gen_proc <font color="#990000">::[</font><b><font color="#0000FF">file</font></b> rootname <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file_name</font><font color="#990000">]]::</font>write_default_config
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> commands <font color="#009900">$cfg_gen_proc</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b>
                <font color="#FF0000">}</font>
                log STDOUT <font color="#FF0000">{</font>Warning<font color="#990000">:</font> The configuration <b><font color="#0000FF">file</font></b> <font color="#009900">$file_name</font> does not exist<font color="#FF0000">}</font>
                log STDOUT <font color="#FF0000">{</font>Generating <b><font color="#0000FF">default</font></b> configuration <b><font color="#0000FF">file</font></b> <font color="#009900">$file_name</font><font color="#990000">\</font>n<font color="#FF0000">}</font>
                <font color="#009900">$cfg_gen_proc</font> <font color="#009900">$file_name</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file_name</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARNING <font color="#FF0000">{</font>Configuration <b><font color="#0000FF">file</font></b> still does not exist<font color="#990000">,</font> ignoring<font color="#990000">.</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">return</font></b>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Mechanism to prevent the same config file from being loaded more</font></i>
        <i><font color="#9A1900"># than once (preventing infinite loops).</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> $<font color="#990000">::</font>cfg_utils<font color="#990000">::</font>SOURCED_CFG_FILES <font color="#009900">$file_name</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">lappend</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>SOURCED_CFG_FILES <font color="#009900">$file_name</font>

        <b><font color="#0000FF">set</font></b> file_data <font color="#990000">[</font>file_utils<font color="#990000">::</font>read_file <font color="#009900">$file_name</font><font color="#990000">]</font>

        <i><font color="#9A1900"># Process config file</font></i>
        <b><font color="#0000FF">set</font></b> data <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$file_data</font> <font color="#FF0000">"</font><font color="#CC33CC">\n</font><font color="#FF0000">"</font><font color="#990000">]</font>
        touch cfg_name cfg_value
        <b><font color="#0000FF">set</font></b> multi_line <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> multi_line_separator <font color="#990000">[</font>cfg get CFG_MULTI_LINE_SEPARATOR <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> line <font color="#009900">$data</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">set</font></b> line <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trim <font color="#009900">$line</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><i><font color="#9A1900">#*} $line]} {</font></i>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># Lines starting with !include indicate other</font></i>
                <i><font color="#9A1900"># configuration files that should also be sourced.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><font color="#990000">!</font>include <font color="#990000">*</font><font color="#FF0000">}</font> <font color="#009900">$line</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> incl_name <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$line</font> <font color="#993399">9</font> end<font color="#990000">]</font>
                        
                        <i><font color="#9A1900"># If included file has a relative path, then determine the</font></i>
                        <i><font color="#9A1900"># absolute path based on the current config file.</font></i>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> path <font color="#009900">$incl_name</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{</font>relative<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> incl_name <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dir <font color="#009900">$file_name</font><font color="#990000">]</font> <font color="#009900">$incl_name</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                        cfg <b><font color="#0000FF">file</font></b> <font color="#009900">$incl_name</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font> 

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$multi_line</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">append</font></b> cfg_value <font color="#009900">$line</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">^\</font>s<font color="#990000">*([</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z0<font color="#990000">-</font>9_<font color="#990000">-]+)\</font>s<font color="#990000">*=\</font>s<font color="#990000">*(.*)\</font>s<font color="#990000">*</font>$<font color="#FF0000">}</font> <font color="#009900">$line</font> <font color="#990000">--&gt;</font> cfg_name cfg_value<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># Removes the trailing \ of the line (for multi-line configs)</font></i>
                <b><font color="#0000FF">set</font></b> multi_line <font color="#990000">[</font><b><font color="#0000FF">regsub</font></b> <font color="#FF0000">{</font><font color="#990000">\\</font>$<font color="#FF0000">}</font> <font color="#009900">$cfg_value</font> <font color="#009900">$multi_line_separator</font> cfg_value<font color="#990000">]</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$multi_line</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> cfg_value <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$cfg_value</font><font color="#990000">]</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <font color="#990000">::</font>tapp_log<font color="#990000">::</font>log DEBUG <font color="#FF0000">{</font>Unable to substitute <font color="#009900">$cfg_name</font> configuration value due to<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        cfg <b><font color="#0000FF">set</font></b> <font color="#009900">$cfg_name</font> <font color="#009900">$cfg_value</font>
                        <b><font color="#0000FF">set</font></b> <font color="#009900">$cfg_name</font> <font color="#009900">$cfg_value</font>
                        clear cfg_name cfg_value
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Exception, if the last config ends with a trailing backslash then it is treated</font></i>
        <i><font color="#9A1900"># as a multi-line, but we don't have any more lines to process. Catch this here.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$multi_line</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> cfg_value <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$cfg_value</font><font color="#990000">]</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font>tapp_log<font color="#990000">::</font>log DEBUG <font color="#FF0000">{</font>Unable to substitute <font color="#009900">$cfg_name</font> configuration value due to<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                cfg <b><font color="#0000FF">set</font></b> <font color="#009900">$cfg_name</font> <font color="#009900">$cfg_value</font>
                <b><font color="#0000FF">set</font></b> <font color="#009900">$cfg_name</font> <font color="#009900">$cfg_value</font>
                clear cfg_name cfg_value
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847919" ID="ID_1498726847919589" MODIFIED="1498726847919" TEXT="cfg get" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847920" ID="ID_1498726847920053" MODIFIED="1498726847920" TEXT="Retrieves the value of a given configuration item.&#xA;&#xA;Synopsis:&#xA;   cfg get &lt;config&gt; ?&lt;default&gt;?&#xA;&#xA;@param config          Name of the configuration item to retrieve&#xA;@param default         The default value to return if the config item is not&#xA;                       set&#xA;@return                The value of the configuration item if set, otherwise&#xA;                       the passed in default value is returned" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847920" ID="ID_1498726847920414" MODIFIED="1498726847920" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847931" ID="ID_1498726847931185" MODIFIED="1498726847931">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg get <font color="#990000">--</font>params <font color="#FF0000">{</font> name <font color="#FF0000">{</font> <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$CFG</font><font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$default</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847931" ID="ID_1498726847931861" MODIFIED="1498726847931" TEXT="cfg lappend" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847932" ID="ID_1498726847932386" MODIFIED="1498726847932" TEXT="Like the list lappend proc this will list append a value to a given&#xA;configuration item.&#xA;&#xA;Synopsis:&#xA;   cfg lappend &lt;config&gt; &lt;value&gt;&#xA;&#xA;@param config          Name of the configuration item to add to&#xA;@param value           The value to list append the configuration item to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847932" ID="ID_1498726847932811" MODIFIED="1498726847932" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847942" ID="ID_1498726847942996" MODIFIED="1498726847943">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg <b><font color="#0000FF">lappend</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> name args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">lappend</font></b> CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847943" ID="ID_1498726847943604" MODIFIED="1498726847943" TEXT="cfg load" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847944" ID="ID_1498726847944073" MODIFIED="1498726847944" TEXT="Loads a set of configuration items and their values. Any existing&#xA;configuration items that is present in the data being loaded will be&#xA;overwritten without warning.&#xA;&#xA;Synopsis:&#xA;   cfg load ?&lt;data&gt;?&#xA;&#xA;@param data            A list of key value pairs of configuration items and&#xA;                       their respective values to load" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847944" ID="ID_1498726847944427" MODIFIED="1498726847944" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847954" ID="ID_1498726847954070" MODIFIED="1498726847954">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg <b><font color="#0000FF">load</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> cfg_data <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        array <b><font color="#0000FF">set</font></b> CFG <font color="#009900">$cfg_data</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847954" ID="ID_1498726847954695" MODIFIED="1498726847954" TEXT="cfg names" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847955" ID="ID_1498726847955179" MODIFIED="1498726847955" TEXT="Returns a list of configuration names matching a given pattern.&#xA;&#xA;Synopsis:&#xA;   cfg names ?&lt;pattern&gt;?&#xA;&#xA;@param pattern         A glob pattern to match the configuration names to&#xA;                       list. Defaults to *, i.e. all configuration items.&#xA;@return                All configuration items that matches the given glob&#xA;                       pattern" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847955" ID="ID_1498726847955544" MODIFIED="1498726847955" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847965" ID="ID_1498726847965254" MODIFIED="1498726847965">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg names <font color="#990000">--</font>params <font color="#FF0000">{</font> <font color="#FF0000">{</font> pattern <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>array names <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG <font color="#009900">$pattern</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847965" ID="ID_1498726847965864" MODIFIED="1498726847965" TEXT="cfg print" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847966" ID="ID_1498726847966437" MODIFIED="1498726847966" TEXT="Collate configuration items for print purposes.&#xA;&#xA;This is mainly used for debugging purposes. The proc will generate a string&#xA;output that lists all configuration items and their values. This in turn can&#xA;be logged or printed to standard out.&#xA;&#xA;Synopsis:&#xA;   cfg print ?&lt;pattern&gt;?&#xA;&#xA;@param pattern         An optional glob pattern that can be used to only&#xA;                       only include specific configuration items in the&#xA;                       returned list. This can also be a list of glob&#xA;                       patterns if necessary.&#xA;@return                A string on the form &quot;&lt;CONFIG&gt; = &lt;VALUE&gt;&quot; where each&#xA;                       configuration item is separated by a newline." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847966" ID="ID_1498726847966805" MODIFIED="1498726847966" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847978" ID="ID_1498726847978145" MODIFIED="1498726847978">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg print <font color="#990000">--</font>params <font color="#FF0000">{</font> <font color="#FF0000">{</font> pattern <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        touch output
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$pattern</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> pattern <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> cfg_names <font color="#990000">[</font>cfg names <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$pattern</font> <font color="#993399">0</font><font color="#990000">]]</font>
        <b><font color="#0000FF">foreach</font></b> filter <font color="#990000">[</font><b><font color="#0000FF">lrange</font></b> <font color="#009900">$pattern</font> <font color="#993399">1</font> end<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> cfg_names <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#990000">[</font>cfg names <font color="#009900">$filter</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> name <font color="#990000">[</font><b><font color="#0000FF">lsort</font></b> <font color="#990000">-</font>unique <font color="#009900">$cfg_names</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">[</font>cfg get MASKED_CFG <font color="#FF0000">{</font>HELP ARGS MOREARGS<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#009900">$name</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">append</font></b> output <font color="#FF0000">"$name = ***</font><font color="#CC33CC">\n</font><font color="#FF0000">"</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">append</font></b> output <font color="#FF0000">"$name = [cfg get $name]</font><font color="#CC33CC">\n</font><font color="#FF0000">"</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimright <font color="#009900">$output</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847978" ID="ID_1498726847978776" MODIFIED="1498726847978" TEXT="cfg reset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847979" ID="ID_1498726847979225" MODIFIED="1498726847979" TEXT="This will clear all configuration items currently set.&#xA;&#xA;This is primarily intended for test purposes where it may be necessary to&#xA;start a test without any pre-existing configuration items set.&#xA;&#xA;Synopsis:&#xA;   cfg reset" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847979" ID="ID_1498726847979568" MODIFIED="1498726847979" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847989" ID="ID_1498726847989441" MODIFIED="1498726847989">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg reset <font color="#990000">--</font>params <font color="#FF0000">{}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>SOURCED_CFG_FILES
        
        <b><font color="#0000FF">set</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>SOURCED_CFG_FILES <font color="#FF0000">{}</font>
        array unset CFG
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847990" ID="ID_1498726847990055" MODIFIED="1498726847990" TEXT="cfg set" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847990" ID="ID_1498726847990511" MODIFIED="1498726847990" TEXT="Set the value of a given configuration item.&#xA;&#xA;Synopsis:&#xA;   cfg set &lt;config&gt; &lt;value&gt;&#xA;&#xA;@param config          Name of the configuration item to set&#xA;@param value           The value to set the configuration item to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847990" ID="ID_1498726847990867" MODIFIED="1498726847990" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848000" ID="ID_1498726848000604" MODIFIED="1498726848000">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg <b><font color="#0000FF">set</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> name value <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">set</font></b> CFG<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#009900">$value</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848001" ID="ID_1498726848001211" MODIFIED="1498726848001" TEXT="cfg subst" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848001" ID="ID_1498726848001907" MODIFIED="1498726848001" TEXT="Substitute configuration placeholders with their relative configuration&#xA;item values.&#xA;&#xA;Configuration placeholders are uppercase alphanumeric strings wrapped with&#xA;the % sign. Underscores are allowed. For example say we have the following&#xA;string: &quot;Tea ready in %TEA_DELAY% minutes&quot;. The %TEA_DELAY% placeholder here&#xA;will be replaced by &quot;5&quot; if the configuration item TEA_DELAY is set to 5.&#xA;&#xA;If the configuration item TEA_DELAY is not set, then the placeholder is left&#xA;intact.&#xA;&#xA;If the input is in itself a configuration item then the value of that will&#xA;be substituted.&#xA;&#xA;Note that substitution happens recursively, i.e. the configuration item that&#xA;is substituting a placeholder may itself contain configuration placeholders.&#xA;This also means that infinite loops are possible.&#xA;&#xA;Synopsis:&#xA;   cfg subst &lt;text&gt;&#xA;   cfg subst &lt;config&gt; ?&lt;default&gt;?&#xA;&#xA;@param text            The input string that contains configuration&#xA;                       placeholders that need to be substituted&#xA;@param config          The name of a configuration item to apply the&#xA;                       substitution to&#xA;@param default         Optional default value to return if the config item&#xA;                       does not exist. If erroneously used in combination with&#xA;                       substituting plain text (as opposed to a config item)&#xA;                       then the default will simply override the text.&#xA;@return                The value of the input string (or config item) with&#xA;                       configuration placeholders substituted" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848002" ID="ID_1498726848002330" MODIFIED="1498726848002" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848014" ID="ID_1498726848014640" MODIFIED="1498726848014">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg <b><font color="#0000FF">subst</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> input <font color="#FF0000">{</font> <b><font color="#0000FF">default</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        touch replace_list
        <b><font color="#0000FF">set</font></b> start <font color="#993399">0</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg exists <font color="#009900">$input</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> input <font color="#990000">[</font>cfg get <font color="#009900">$input</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$default</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> input <font color="#009900">$default</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">while</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">-</font>indices <font color="#990000">-</font>start <font color="#009900">$start</font> <font color="#FF0000">{</font><font color="#990000">(%[</font>A<font color="#990000">-</font>Z0<font color="#990000">-</font>9_<font color="#990000">][</font>A<font color="#990000">-</font>Z0<font color="#990000">-</font>9_<font color="#990000">]+%)</font><font color="#FF0000">}</font> <font color="#009900">$input</font> indices<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                lassign <font color="#009900">$indices</font> from to
                <b><font color="#0000FF">set</font></b> subst_name  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$input</font> <font color="#009900">$from</font> <font color="#009900">$to</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> cfg_name    <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trim <font color="#009900">$subst_name</font> <font color="#990000">%]</font>
                <b><font color="#0000FF">set</font></b> subst_value <font color="#990000">[</font>cfg get <font color="#009900">$cfg_name</font> <font color="#009900">$subst_name</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$subst_value</font> <font color="#990000">!=</font> <font color="#009900">$subst_name</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">%[</font>A<font color="#990000">-</font>Z0<font color="#990000">-</font>9_<font color="#990000">][</font>A<font color="#990000">-</font>Z0<font color="#990000">-</font>9_<font color="#990000">]+%</font><font color="#FF0000">}</font> <font color="#009900">$subst_value</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> subst_value <font color="#990000">[</font>cfg <b><font color="#0000FF">subst</font></b> <font color="#009900">$subst_value</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#009900">$subst_name</font> <font color="#009900">$subst_value</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> start <font color="#009900">$to</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$replace_list</font> <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848015" ID="ID_1498726848015266" MODIFIED="1498726848015" TEXT="cfg unload" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848015" ID="ID_1498726848015769" MODIFIED="1498726848015" TEXT="Returns all configuration values of a given pattern in key value form like&#xA;one would expect from an &quot;array get&quot; call.&#xA;&#xA;Synopsis:&#xA;   cfg unload ?&lt;pattern&gt;?&#xA;&#xA;@param pattern         A glob pattern representing the configuration items to&#xA;                       include in the unload. Defaults to *, i.e. all&#xA;                       configuration items.&#xA;@return                All configuration items matching the glob pattern and&#xA;                       their values in list form" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848016" ID="ID_1498726848016130" MODIFIED="1498726848016" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848031" ID="ID_1498726848031044" MODIFIED="1498726848031">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg unload <font color="#990000">--</font>params <font color="#FF0000">{</font> <font color="#FF0000">{</font> pattern <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>array get CFG <font color="#009900">$pattern</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848031" ID="ID_1498726848031632" MODIFIED="1498726848031" TEXT="cfg unset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848032" ID="ID_1498726848032098" MODIFIED="1498726848032" TEXT="Unset a particular configuration item.&#xA;&#xA;Synopsis:&#xA;   cfg unset ?&lt;pattern&gt;?&#xA;&#xA;@param pattern         The name of the configuration item or a glob pattern&#xA;                       representing the configuration items to unset.&#xA;                       Defaults to *, i.e. all configuration items." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848032" ID="ID_1498726848032450" MODIFIED="1498726848032" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848046" ID="ID_1498726848046448" MODIFIED="1498726848046">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>cfg unset <font color="#990000">--</font>params <font color="#FF0000">{</font> <font color="#FF0000">{</font> pattern <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$pattern</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>SOURCED_CFG_FILES <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>

        array unset <font color="#990000">::</font>cfg_utils<font color="#990000">::</font>CFG <font color="#009900">$pattern</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726848048" ID="ID_1498726848048726" MODIFIED="1498726848048" TEXT="arg_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726848049" ID="ID_1498726848049310" MODIFIED="1498726848049" TEXT="The application argument utility. Provides functionality for both proc and&#xA;application parameter handling." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848049" ID="ID_1498726848049643" MODIFIED="1498726848049" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726848049" ID="ID_1498726848049863" MODIFIED="1498726848049" TEXT="AUTO_LOG_INIT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848050" ID="ID_1498726848050202" MODIFIED="1498726848050" TEXT="Logs are automatically initialised after parsing the default TApp parameters. Defaults to enabled. Disable via config if the application at hand handles additional log configuration." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726848051" ID="ID_1498726848051223" MODIFIED="1498726848051" TEXT="arg_utils::help" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848051" ID="ID_1498726848051704" MODIFIED="1498726848051" TEXT="Prints out the help info to standard out." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848052" ID="ID_1498726848052048" MODIFIED="1498726848052" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726848052" ID="ID_1498726848052264" MODIFIED="1498726848052" TEXT="HELP" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848052" ID="ID_1498726848052570" MODIFIED="1498726848052" TEXT="Contains the help info text. If the %args% placeholder is present then this will be replaced with formatted arguments taken from the ARGS config item." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848052" ID="ID_1498726848052896" MODIFIED="1498726848052" TEXT="ARGS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848053" ID="ID_1498726848053204" MODIFIED="1498726848053" TEXT="The application arguments that can be passed in, this will override the description for any existing TApp parameters provided the short and long parameter names match. Should be a list on the form: {‑s ‑‑long} {description} {‑l ‑‑list} {description}" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726848053" ID="ID_1498726848053545" MODIFIED="1498726848053" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848064" ID="ID_1498726848064360" MODIFIED="1498726848064">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> arg_utils<font color="#990000">::</font>help <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> ARGS

        <b><font color="#0000FF">set</font></b> charmap <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">%</font>args<font color="#990000">%</font> <font color="#990000">[</font>param <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#990000">[</font>cfg get ARGS<font color="#990000">]</font> <font color="#990000">[</font>_get_help_args<font color="#990000">]]]]</font>
        log STDOUT <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$charmap</font> <font color="#990000">[</font>cfg get HELP<font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848064" ID="ID_1498726848064961" MODIFIED="1498726848064" TEXT="arg_utils::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848065" ID="ID_1498726848065382" MODIFIED="1498726848065" TEXT="Initialises the arg_utils by setting up the application parameters.&#xA;&#xA;This is called automatically when the tapp library is being loaded." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848065" ID="ID_1498726848065765" MODIFIED="1498726848065" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848080" ID="ID_1498726848080162" MODIFIED="1498726848080">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> arg_utils<font color="#990000">::</font>init args <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> ARGS
        
        param register <font color="#990000">-</font>d <font color="#990000">--</font>dry<font color="#990000">-</font>run                <font color="#990000">--</font>config DRY_RUN                <font color="#990000">--</font>disporder <font color="#993399">10</font>                <font color="#990000">--</font>priority <font color="#993399">10</font>                <font color="#990000">--</font>morearg                <font color="#990000">--</font>comment <font color="#FF0000">{</font>run through the script<font color="#990000">,</font> but don<font color="#FF0000">'t change anything}</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        param register -v --verbose                --config VERBOSE                --disporder 30                --morearg                --comment {enable verbose output}</font>

<font color="#FF0000">        param register {} --more                --priority 10                --disporder 35                --body {</font>
<font color="#FF0000">                        set more_level [expr {[string length $argument] - 4}]</font>
<font color="#FF0000">                        cfg set MORE_LEVEL $more_level</font>
<font color="#FF0000">                        log DEBUG {Enabled more output (more level $more_level)}</font>
<font color="#FF0000">                }                --morearg                --comment {show more output, further dashes (e.g. ---more) show additional output (for applications where applicable)}</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        param register {} --stdout                --config LOG_FILE                --config_value {stdout}                --disporder 55                --morearg                --log {Setting log file to standard out}                --comment {set log file to standard out}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --stderr                --config LOG_FILE                --config_value {stderr}                --disporder 55                --morearg                --log {Setting log file to standard err}                --comment {set log file to standard err}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--&lt;log level&gt;}                --disporder 90                --morearg                --comment {enable logging at a specific log level:</font><font color="#CC33CC">\n</font><font color="#FF0000">trace, dev, debug, info, warn, error, fatal, mute}</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        foreach level {trace dev debug info warn error fatal mute} {</font>
<font color="#FF0000">                param register {} --$level                        --priority 10                        --unlisted                        --config LOG_LEVEL                        --config_value [string toupper $level]                        --log "Enabled $level logging"</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        param register -h --help                --body { arg_utils::help }                --disporder 20                --comment {display this help, try including --more for additional options}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register -c --config                --body {</font>
<font color="#FF0000">                        cfg file $value</font>
<font color="#FF0000">                        log DEBUG {Using $value as configuration file}</font>
<font color="#FF0000">                }                --max_args 1                --min_args 1                --validation {file}                --disporder 60                --priority 10                --morearg                --comment {specify the configuration file to load}</font>

<font color="#FF0000">        param register {} {--cset &lt;cfg&gt;=1}                --max_args 1                --body {</font>
<font color="#FF0000">                        lassign [split $values {=}] cfg_name cfg_value</font>
<font color="#FF0000">                        catch {</font>
<font color="#FF0000">                                set cfg_value [subst $cfg_value]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        log DEBUG {Setting config item $cfg_name to $cfg_value}</font>
<font color="#FF0000">                        cfg set $cfg_name $cfg_value</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {override a specific configuration item}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--cappend &lt;cfg&gt;=val}                --max_args 1                --body {</font>
<font color="#FF0000">                        lassign [split $values {=}] cfg_name cfg_value</font>
<font color="#FF0000">                        catch {</font>
<font color="#FF0000">                                set cfg_value [subst $cfg_value]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        log DEBUG {Appending </font><font color="#CC33CC">\"</font><font color="#FF0000">$cfg_value</font><font color="#CC33CC">\"</font><font color="#FF0000"> to config item $cfg_name}</font>
<font color="#FF0000">                        cfg append $cfg_name $cfg_value</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {append a value to a specific configuration item}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--clappend &lt;cfg&gt;=val}                --max_args 1                --body {</font>
<font color="#FF0000">                        lassign [split $values {=}] cfg_name cfg_value</font>
<font color="#FF0000">                        catch {</font>
<font color="#FF0000">                                set cfg_value [subst $cfg_value]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        log DEBUG {Appending </font><font color="#CC33CC">\"</font><font color="#FF0000">$cfg_value</font><font color="#CC33CC">\"</font><font color="#FF0000"> to config item $cfg_name}</font>
<font color="#FF0000">                        cfg lappend $cfg_name {*}$cfg_value</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {list append a value to a specific configuration item}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--log-file=&lt;file&gt;}                --config LOG_FILE                --max_args 1                --disporder 90                --morearg                --comment {override file to log to}</font>

<font color="#FF0000">        param register {} {--log-dir=&lt;dir&gt;}                --config LOG_DIR                --max_args 1                --disporder 90                --morearg                --comment {override directory to log to}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--log-prefix=&lt;text&gt;}                --config LOG_PREFIX                --max_args 1                --disporder 90                --morearg                --comment {specify a log prefix, for cross-script calls}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --log-proc                --config LOG_PROC_PREFIX                --disporder 90                --morearg                --comment {also output the Tcl procedure doing the logging}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --log_enable_stdout                --config LOG_DISABLE_STD_STREAMS                --config_value 0                --disporder 90                --morearg                --comment {enable logging to standard out (refers to stdout log statements)}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --log_disable_stdout                --config LOG_DISABLE_STD_STREAMS                --config_value 1                --disporder 90                --morearg                --comment {disable logging to standard out (refers to stdout log statements)}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--show-config ?&lt;cfg&gt;?}                --body {</font>
<font color="#FF0000">                        if {$values == {}} {</font>
<font color="#FF0000">                                set values {*}</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        foreach filter $values {</font>
<font color="#FF0000">                                log STDOUT [cfg print $filter]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --max_args *                --comment {dump configuration items currently set; accepts multiple glob patterns                           as arguments which can be used to pull out individual config items,                           defaults to listing all configuration items}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--trace_var=&lt;var&gt;}                --body {</font>
<font color="#FF0000">                        trace add variable $value array tapp_lang::_watch</font>
<font color="#FF0000">                        trace add variable $value write tapp_lang::_watch</font>
<font color="#FF0000">                        trace add variable $value read  tapp_lang::_watch</font>
<font color="#FF0000">                        trace add variable $value unset tapp_lang::_watch</font>
<font color="#FF0000">                        log DEBUG {Enabled tracing of variable: $value}</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {enable tracing of when a given variable is set and read}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--trace_cmd=&lt;cmd&gt;}                --body {</font>
<font color="#FF0000">                        trace add execution $value enter tapp_lang::_watch_command_enter</font>
<font color="#FF0000">                        trace add execution $value leave tapp_lang::_watch_command_leave</font>
<font color="#FF0000">                        log DEBUG {Enabled tracing of command: $value}</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {enable tracing of when a given procedure is entered and left}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --assert                --body {</font>
<font color="#FF0000">                        set ::tapp::ASSERT 1</font>
<font color="#FF0000">                        log DEV {Enabled assert statements}</font>
<font color="#FF0000">                }                --disporder 65                --morearg                --comment [subst {enable assert statements [expr {[info exists ::tapp::ASSERT] &amp;&amp; $::tapp::ASSERT} ? </font><font color="#CC33CC">\{</font><font color="#FF0000">(default)</font><font color="#CC33CC">\}</font><font color="#FF0000"> : </font><font color="#CC33CC">\{\}</font><font color="#FF0000">]}]</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --no_assert                --body {</font>
<font color="#FF0000">                        set ::tapp::ASSERT 0</font>
<font color="#FF0000">                        log DEV {Disabled assert statements}</font>
<font color="#FF0000">                }                --disporder 65                --morearg                --comment [subst {disable assert statements [expr {[info exists ::tapp::ASSERT] &amp;&amp; !$::tapp::ASSERT} ? </font><font color="#CC33CC">\{</font><font color="#FF0000">(default)</font><font color="#CC33CC">\}</font><font color="#FF0000"> : </font><font color="#CC33CC">\{\}</font><font color="#FF0000">]}]</font>

<font color="#FF0000">        param register {} {--dest=&lt;dir&gt;}                --config DESTINATION                --max_args 1                --validation {directory}                --log {Destination set to $value}                --disporder 68                --morearg                --comment {set destination for scripts where this is relevant}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848080" ID="ID_1498726848080867" MODIFIED="1498726848080" TEXT="arg_utils::let" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848081" ID="ID_1498726848081684" MODIFIED="1498726848081" TEXT="Shorthand proc to get a series of parameter values from a list of arguments.&#xA;&#xA;Each parameter will be looked up in the list of arguments.&#xA;  - if the parameter is not found then the default value will be used&#xA;  - if the parameter is found then any following values will be used&#xA;  - if the parameter is found, but isn't followed by any explicit values then&#xA;    it will default to 1 (as in true / enabled)&#xA;&#xA;The value used is stored in a variable with the same name as the parameter&#xA;(less any argument prefix) in the calling proc.&#xA;&#xA;The parameters passed in have to have the argument prefix. The amount of&#xA;prefixes denote whether it is a flag taking no values (e.g. -nocase) or a&#xA;parameter with a value (e.g. --file ex.txt).&#xA;&#xA;If parameters matching regular expressions is needed then have a look at&#xA;param get.&#xA;&#xA;Synopsis:&#xA;   let $args [list param default param default ... param default] ?arg_prefix?&#xA;&#xA;Example:&#xA;   set args {--color red -enabled --list {d e f} --ignored yes}&#xA;   let $args {&#xA;           color   {blue}&#xA;           list    {a b c}&#xA;           enabled {0}&#xA;           repeat  {no}&#xA;   }&#xA;&#xA;Will result in the following variables being set:&#xA;   set color   {red}&#xA;   set list    {d e f}&#xA;   set enabled {1}&#xA;   set repeat  {no}&#xA;&#xA;@param arguments       The list of arguments to search through&#xA;@param arg_list        A list on the form &lt;parameter&gt; &lt;default&gt;, can contain&#xA;                       multiple parameters&#xA;@param remaining_args  Name of the variable to store the remaining args in&#xA;@param arg_prefix      The parameter prefix, defaults to hyphen (-)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848082" ID="ID_1498726848082144" MODIFIED="1498726848082" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848095" ID="ID_1498726848095768" MODIFIED="1498726848095">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> arg_utils<font color="#990000">::</font>let <font color="#FF0000">{</font> arguments arg_list <font color="#FF0000">{</font>remaining_args <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        touch plist vlist rlist
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>param <b><font color="#0000FF">default</font></b><font color="#FF0000">}</font> <font color="#009900">$arg_list</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> plist <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> vlist <font color="#009900">$default</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">llength</font></b> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$llength</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$arg</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#009900">$arg_prefix</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> rlist <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> val <font color="#993399">1</font>
                <b><font color="#0000FF">set</font></b> prm <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$arg</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font>exact <font color="#009900">$plist</font> <font color="#009900">$prm</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$idx</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> rlist <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$arg</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#009900">$arg_prefix</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> next <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">+</font><font color="#993399">1</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$i</font><font color="#990000">+</font><font color="#993399">1</font> <font color="#990000">&gt;=</font> <font color="#009900">$llength</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+\[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">\]</font> <font color="#009900">$next</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                log WARNING <font color="#FF0000">{</font>let<font color="#990000">:</font> No value passed <b><font color="#0000FF">in</font></b> <b><font color="#0000FF">for</font></b> parameter <font color="#009900">$arg</font><font color="#990000">,</font> defaulting to <font color="#009900">$val</font><font color="#990000">.</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> val <font color="#009900">$next</font>
                                <b><font color="#0000FF">incr</font></b> i
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">lset</font></b> vlist <font color="#009900">$idx</font> <font color="#009900">$val</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> param <font color="#009900">$plist</font> value <font color="#009900">$vlist</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">"set $param {$value}"</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$remaining_args</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">"set $remaining_args {[join $rlist { }]}"</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848096" ID="ID_1498726848096426" MODIFIED="1498726848096" TEXT="param format" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848097" ID="ID_1498726848097191" MODIFIED="1498726848097" TEXT="This procedure is responsible for formatting and aligning the passed in&#xA;arguments for display purposes.&#xA;&#xA;@param args               The arguments to format for display purposes&#xA;@return                   The arguments formatted as text (string containing&#xA;                          newlines)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848097" ID="ID_1498726848097612" MODIFIED="1498726848097" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726848097" ID="ID_1498726848097867" MODIFIED="1498726848097" TEXT="CONSOLE_WIDTH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848098" ID="ID_1498726848098229" MODIFIED="1498726848098" TEXT="Determines the width of the console, note that this is automatically set by the tapp package on load if the core linux utility &quot;tty&quot; is available. Defaults to 80." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848098" ID="ID_1498726848098563" MODIFIED="1498726848098" TEXT="HELP_ARG_WIDTH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848098" ID="ID_1498726848098863" MODIFIED="1498726848098" TEXT="The width of the argument column, which determines where the argument comment column starts, defaults to 27." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848099" ID="ID_1498726848099174" MODIFIED="1498726848099" TEXT="HELP_ARG_INDENT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848099" ID="ID_1498726848099481" MODIFIED="1498726848099" TEXT="Determines where the argument column starts, i.e. how much the arguments are indented. Defaults to 2." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848099" ID="ID_1498726848099809" MODIFIED="1498726848099" TEXT="HELP_ARG_PADDING" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848100" ID="ID_1498726848100149" MODIFIED="1498726848100" TEXT="Determines the padding needed when there are only one argument. The formatting utility displays up to two aliases for the same parameter, for example the help can be triggered by passing in either ‑h or ‑‑help. This would be listed as ‑h, ‑‑help in the output. The padding here is for when the ‑h parameter does not exist, in which case we need 4 spaces to align the ‑‑help parameter with other parameters. Defaults to 4. This is a config that one should generally not need to change." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848100" ID="ID_1498726848100486" MODIFIED="1498726848100" TEXT="EXIT_ON_ARG_FAILURE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848100" ID="ID_1498726848100817" MODIFIED="1498726848100" TEXT="By default TApp will exit when argument validation fails, i.e. an illegal parameter is passed in or a parameter expecting a file argument receives none. When exiting an error message will be written to standard err. This can be disabled by setting this configuration item to 0. The main risk of disabling this functionality is that it may not be clear to the end user that an argument passed in was not actually applied." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726848101" ID="ID_1498726848101183" MODIFIED="1498726848101" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848120" ID="ID_1498726848120526" MODIFIED="1498726848120">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param <b><font color="#0000FF">format</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        touch args_to_print padding processed_args

        <b><font color="#0000FF">set</font></b> line_width  <font color="#990000">[</font>cfg get CONSOLE_WIDTH <font color="#993399">80</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> arg_width   <font color="#990000">[</font>cfg get HELP_ARG_WIDTH <font color="#993399">27</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> arg_indent  <font color="#990000">[</font>cfg get HELP_ARG_INDENT <font color="#993399">2</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> arg_padding <font color="#990000">[</font>cfg get HELP_ARG_PADDING <font color="#993399">4</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> arguments <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>

        <i><font color="#9A1900"># Check whether we only have single numbered arguments, if not</font></i>
        <i><font color="#9A1900"># then we want to pad the first argument with spaces so that single</font></i>
        <i><font color="#9A1900"># numbered arguments line up with double numbered arguments</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>arglist desc<font color="#FF0000">}</font> <font color="#009900">$arguments</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arglist</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$arg_padding</font><font color="#990000">]</font>
                        <b><font color="#0000FF">break</font></b>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Loop through all arguments first to find out if we need to auto</font></i>
        <i><font color="#9A1900"># increase the arg width.</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>arglist desc<font color="#FF0000">}</font> <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <i><font color="#9A1900"># Only show up to two of the arguments</font></i>
                <b><font color="#0000FF">set</font></b> arg1 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> arg2 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">1</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">set</font></b> joined_arg <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$arg2</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">?</font> <font color="#FF0000">"$arg1, $arg2"</font> <font color="#990000">:</font> <font color="#FF0000">"$padding$arg1"</font><font color="#FF0000">}</font><font color="#990000">]</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$joined_arg</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#009900">$arg_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> arg_width <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$joined_arg</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#993399">1</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> desc_width  <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$line_width</font> <font color="#990000">-</font> <font color="#009900">$arg_width</font> <font color="#990000">-</font> <font color="#009900">$arg_indent</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> desc_indent <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$arg_width</font> <font color="#990000">+</font> <font color="#009900">$arg_indent</font><font color="#FF0000">}</font><font color="#990000">]]</font>
        <b><font color="#0000FF">set</font></b> format_str  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$arg_indent</font><font color="#990000">]%-</font>$<font color="#FF0000">{</font>arg_width<font color="#FF0000">}</font>s<font color="#990000">%</font>s

        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>arglist desc<font color="#FF0000">}</font> <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <i><font color="#9A1900"># Only show up to two of the arguments</font></i>
                <b><font color="#0000FF">set</font></b> arg1 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> arg2 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">1</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg2</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> joined_arg <font color="#FF0000">"$arg1, $arg2"</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><font color="#990000">--*</font><font color="#FF0000">}</font> <font color="#009900">$arg1</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$arg1</font><font color="#990000">]</font> <font color="#990000">&lt;=</font> <font color="#009900">$arg_padding</font> <font color="#990000">-</font> <font color="#993399">2</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> joined_arg <font color="#009900">$arg1</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> joined_arg <font color="#FF0000">"$padding$arg1"</font>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># We only want to process each argument once, for example if</font></i>
                <i><font color="#9A1900"># defined in both application help and in the more help section.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>exact <font color="#009900">$processed_args</font> <font color="#009900">$joined_arg</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> processed_args <font color="#009900">$joined_arg</font>

                clear line
                
                <b><font color="#0000FF">set</font></b> arg_printed <font color="#993399">0</font>

                <b><font color="#0000FF">foreach</font></b> desc_line <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#990000">\</font>n <font color="#990000">\</font>x00 <font color="#990000">\\</font>n <font color="#990000">\</font>x00<font color="#FF0000">}</font> <font color="#009900">$desc</font><font color="#990000">]</font> <font color="#990000">\</font>x00<font color="#990000">]</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$desc_line</font><font color="#990000">]</font> <font color="#990000">&lt;=</font> <font color="#009900">$desc_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg_printed</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> args_to_print $<font color="#FF0000">{</font>desc_indent<font color="#FF0000">}</font><font color="#009900">$desc_line</font>
                                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> args_to_print <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$format_str</font> <font color="#009900">$joined_arg</font> <font color="#009900">$desc_line</font><font color="#990000">]</font>
                                        <b><font color="#0000FF">set</font></b> arg_printed <font color="#993399">1</font>
                                <font color="#FF0000">}</font>
                                <b><font color="#0000FF">continue</font></b>
                        <font color="#FF0000">}</font>

                        <b><font color="#0000FF">foreach</font></b> word <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$desc_line</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$word</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#993399">1</font> <font color="#990000">&lt;=</font> <font color="#009900">$desc_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">append</font></b> line <font color="#FF0000">{</font> <font color="#FF0000">}</font> 
                                        <font color="#FF0000">}</font>
                                        <b><font color="#0000FF">append</font></b> line <font color="#009900">$word</font>
                                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg_printed</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">lappend</font></b> args_to_print $<font color="#FF0000">{</font>desc_indent<font color="#FF0000">}</font><font color="#009900">$line</font>
                                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">lappend</font></b> args_to_print <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$format_str</font> <font color="#009900">$joined_arg</font> <font color="#009900">$line</font><font color="#990000">]</font>
                                                <b><font color="#0000FF">set</font></b> arg_printed <font color="#993399">1</font>
                                        <font color="#FF0000">}</font>
                                        <b><font color="#0000FF">set</font></b> line <font color="#009900">$word</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>

                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> args_to_print $<font color="#FF0000">{</font>desc_indent<font color="#FF0000">}</font><font color="#009900">$line</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If we have additional args to display, add a trailing newline</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args_to_print</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> args_to_print <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$args_to_print</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848121" ID="ID_1498726848121259" MODIFIED="1498726848121" TEXT="param get" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848122" ID="ID_1498726848122195" MODIFIED="1498726848122" TEXT="Procedure to retrieve the value of a given parameter from a list of arguments.&#xA;&#xA;The number of argument prefixes (hyphen) determines the role of the argument&#xA;passed in (either flags or parameters).&#xA;&#xA;A single prefix indicates an optional flag (e.g. -flag) and will default to 1&#xA;if passed in. If a flag is followed by a value then this will not be taken&#xA;into account as flags don't take values.&#xA;&#xA;Two prefixes indicates a parameter and parameters are followed by an explicit&#xA;value (e.g. --file ex.txt). If the following argument looks like another&#xA;parameter (e.g. starting with the prefix) then it will default to 1 and a&#xA;warning will be logged.&#xA;&#xA;If more than one of the same parameter or flag is passed in then the last&#xA;argument takes precedence and a warning will be logged.&#xA;&#xA;Parameters and flags are case insensitive.&#xA;&#xA;Synopsis:&#xA;   param get &lt;parameter&gt; ?&lt;default&gt;? ?&lt;list&gt;?&#xA;&#xA;For example:&#xA;   set args [list -i 3 --file ex1.txt ex2.txt --sep {|} --num 1 --num 2}]&#xA;   set file [param get --file]&#xA;   set sep  [param get --sep {:}]&#xA;   set i    [param get -i]&#xA;   set num  [param get --num]&#xA;   set dflt [param get --default {99}]&#xA;&#xA;will result in:&#xA;   set file {ex1.txt}; # because parameters takes maximum 1 value (use list)&#xA;   set sep  {|};       # because parameter was found in args list&#xA;   set i    {1};       # because flags (single hyphen) enable functionality&#xA;   set num  {2};       # because the last parameter takes precedence&#xA;   set dflt {99};      # because the parameter was not found in args list&#xA;&#xA;@param param           The parameter to search for (can be a regular expression)&#xA;@param default         The default value if not found&#xA;@param arguments       The list of arguments to search through, defaults to&#xA;                       doing upvar to get hold of the proc's &quot;args&quot; variable&#xA;                       if not defined.&#xA;@param arg_prefix      The parameter prefix, defaults to hyphen (-)&#xA;@return                Empty string if the argument value was not found,&#xA;                       the value if a single value was found or &quot;1&quot; if the&#xA;                       argument was found to exist, but with no values." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848122" ID="ID_1498726848122792" MODIFIED="1498726848122" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848136" ID="ID_1498726848136540" MODIFIED="1498726848136">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param get <font color="#990000">--</font>params <font color="#FF0000">{</font> param <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>arguments <font color="#FF0000">"</font><font color="#CC33CC">\0</font><font color="#FF0000">"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arguments</font> <font color="#990000">==</font> <font color="#FF0000">"</font><font color="#CC33CC">\0</font><font color="#FF0000">"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                unset arguments
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> args arguments
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists arguments<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">error</font></b> <font color="#FF0000">{</font>Either pass the arguments <b><font color="#0000FF">list</font></b> <b><font color="#0000FF">in</font></b> or make sure the <b><font color="#0000FF">variable</font></b> is named <font color="#FF0000">"args"</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> length <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> param <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font>all <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$arguments</font> <font color="#990000">^</font>$<font color="#FF0000">{</font>arg_prefix<font color="#FF0000">}</font><font color="#990000">+</font><font color="#009900">$param</font><font color="#990000">\</font>$<font color="#990000">]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$idx</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$default</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$idx</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARNING <font color="#FF0000">{</font>param get<font color="#990000">:</font> Parameter <font color="#009900">$param</font> passed <b><font color="#0000FF">in</font></b> more than once<font color="#990000">.</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$idx</font> end<font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$idx</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> num <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$arg</font><font color="#990000">]</font> <font color="#990000">-</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$arg</font> <font color="#009900">$arg_prefix</font><font color="#990000">]]</font><font color="#FF0000">}</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> value <font color="#993399">1</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$num</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">incr</font></b> idx
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$idx</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$idx</font> <font color="#990000">&gt;=</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+\[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">\]</font> <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARNING <font color="#FF0000">{</font>param get<font color="#990000">:</font> No value passed <b><font color="#0000FF">in</font></b> <b><font color="#0000FF">for</font></b> parameter <font color="#009900">$param</font><font color="#990000">,</font> defaulting to <font color="#993399">1</font><font color="#990000">.</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> value <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$value</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848137" ID="ID_1498726848137184" MODIFIED="1498726848137" TEXT="param join" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848137" ID="ID_1498726848137750" MODIFIED="1498726848137" TEXT="Join arguments that are separated by a space.&#xA;&#xA;Most parameter flags are just single values such as --debug, --stdout or&#xA;--help.&#xA;&#xA;Some parameters take a value and these would need to be joined in the&#xA;parameter list before the parsing takes place. An example of this would be&#xA;the --cset CONFIG_ITEM=my_value parameter that allows for individual config&#xA;items to be overridden from the command line.&#xA;&#xA;@param arguments       The full list of command line parameters&#xA;@param args_to_join    List of parameters that take a value and needs to be&#xA;                       joined (with the value) prior to the parameter parsing&#xA;                       process taking place. Note that the items in this list&#xA;                       can be regular expressions.&#xA;@return                The list of command line parameters with the given&#xA;                       parameters joined if they exist. The order of the&#xA;                       argument list is preserved." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848138" ID="ID_1498726848138254" MODIFIED="1498726848138" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848149" ID="ID_1498726848149757" MODIFIED="1498726848149">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param <b><font color="#0000FF">join</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> arguments <font color="#FF0000">{</font> args_to_join <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        touch joined_args
        <b><font color="#0000FF">set</font></b> args_length <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$args_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> <b><font color="#0000FF">regexp</font></b> <font color="#009900">$args_to_join</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$regexp</font> <font color="#009900">$arg</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">incr</font></b> i
                                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$arg</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> joined_args <font color="#009900">$arg</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$joined_args</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848150" ID="ID_1498726848150405" MODIFIED="1498726848150" TEXT="param parse" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848151" ID="ID_1498726848151043" MODIFIED="1498726848151" TEXT="Parse the application arguments and take action based on registered&#xA;parameters.&#xA;&#xA;Main responsibilities:&#xA;    - expand file arguments&#xA;    - identify application parameters&#xA;    - prioritise parameters that need to run prior to others&#xA;    - perform the action that should be taken when the parameter is passed in&#xA;&#xA;Synopsis:&#xA;   param parse &lt;arguments&gt; ?&lt;remaining_arguments_variable&gt;?&#xA;&#xA;Example usage:&#xA;   param parse $argv remaining_args&#xA;&#xA;or alternatively:&#xA;   set remaining_args [param parse $argv]&#xA;&#xA;@param arguments      &#xA;@param remaining_args_var  The variable in which to store the remaining&#xA;                           arguments (i.e. the ones that does not seem&#xA;                           to be registered application parameters)&#xA;@param arg_prefix          The parameter prefix, should always be a hyphen&#xA;                           (-) character&#xA;@return                    The remaining application parameters that were&#xA;                           not identified (same as stored in the&#xA;                           remaining_args_var if provided)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848151" ID="ID_1498726848151457" MODIFIED="1498726848151" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848162" ID="ID_1498726848162793" MODIFIED="1498726848162">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param parse <font color="#990000">--</font>params <font color="#FF0000">{</font> arguments <font color="#FF0000">{</font> remaining_args_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$remaining_args_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$remaining_args_var</font> remaining_args
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> expanded_args <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_expand_fileargs <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> args_to_parse <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_identify_params <font color="#009900">$expanded_args</font> remaining_args <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> args_to_parse <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_prioritise_args <font color="#009900">$args_to_parse</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> parse_result  <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_evaluate_params <font color="#009900">$args_to_parse</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled AUTO_LOG_INIT <font color="#993399">1</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                tapp_log<font color="#990000">::</font>init
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$remaining_args</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848163" ID="ID_1498726848163392" MODIFIED="1498726848163" TEXT="param register" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848164" ID="ID_1498726848164447" MODIFIED="1498726848164" TEXT="Register an application parameter.&#xA;&#xA;Synopsis:&#xA;   param register &lt;short param&gt; &lt;long param&gt; ?options?&#xA;&#xA;Example:&#xA;   param register -d --dry-run --config DRY_RUN&#xA;&#xA;The above enables the config DRY_RUN when -d or --dry-run is passed in to the&#xA;application.&#xA;&#xA;@param short_param         The short hand parameter, should be a single&#xA;                           character&#xA;@param long_param          The long version of the parameter&#xA;@param args                Additional options&#xA;@option --config           Specifies a config item to modify when the&#xA;                           parameter is passed in to the application&#xA;@option --config_value     The value to set the config item to when&#xA;                           the parameter is passed in, defaults to 1&#xA;                           (enabled)&#xA;@option --override         If registering a parameter that already exists&#xA;                           (like -d for example) then an error will be&#xA;                           thrown. The --override flag allows for the&#xA;                           application to override the existing parameter.&#xA;@option --comment          The comment to display alongside the parameter&#xA;                           when listing the help options&#xA;@option --body             Specifies Tcl code that should be evaluated when&#xA;                           the parameter is passed in (allows for more&#xA;                           control). Existing variables: $values (or $value)&#xA;                           containing the parameter argument (if any),&#xA;                           $argument containing the original argument. The&#xA;                           Tcl body will be evaluated in the namespace where&#xA;                           the parameter was registered from. Do not use the&#xA;                           &quot;return&quot; command within the body as this will&#xA;                           cause parameter evaluation to stop abruptly&#xA;                           resulting in undefined behaviour.&#xA;@option --log              The message to log when the parameter has been&#xA;                           evaluated. If not set and the parameter controls&#xA;                           a config item then a default message is logged.&#xA;@option --max_args         Sets the number of expected  arguments following&#xA;                           the parameter, defaults to 0 (no arguments). This&#xA;                           is primarily used when deducing whether&#xA;                           unparameterised arguments (like files for example)&#xA;                           belong as an argument to a parameter or not.&#xA;@option --min_args         Sets the minimum number of arguments to following&#xA;                           the parameter. If the parameter is passed in, but&#xA;                           fails to supply enough arguments then an error is&#xA;                           thrown.&#xA;@option --validation       Apply validation to the parameter arguments. This&#xA;                           can be used to check that a given input value is&#xA;                           an integer, a file or directory for example.&#xA;                           See arg_utils::param validate for available options.&#xA;@option --priority         Certain parameters need to be run prior to others&#xA;                           and this allows for some control over the order in&#xA;                           which they are processed. Defaults to 50.&#xA;@option --disporder        Allows for some control over the order in which&#xA;                           the parameters are displayed when listing options&#xA;                           on the help output. Defaults to 20.&#xA;@option --more_level       Controls what parameters are displayed when the&#xA;                           help section is listed. Defaults to 0, meaning&#xA;                           the parameter info is displayed when the --help&#xA;                           argument is passed to the application. If --help&#xA;                           and --more is passed then parameters with a&#xA;                           more level of 2 and below are displayed. If&#xA;                           ---more is passed in the parameters with a&#xA;                           more_level of 3 and below are displayed, etc.&#xA;@option --unlisted         Never list the parameter in the help info, this is&#xA;                           an alias for --more_level -1&#xA;@option --morearg          Only list the parameter when --more is passed in,&#xA;                           this is an alias for --more_level 1" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848164" ID="ID_1498726848164999" MODIFIED="1498726848165" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848180" ID="ID_1498726848180807" MODIFIED="1498726848180">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param register <font color="#990000">--</font>params <font color="#FF0000">{</font> short_param long_param args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>arg_utils<font color="#990000">::</font>ARGS
        
        <i><font color="#9A1900"># proc args</font></i>
        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                override      <font color="#993399">0</font>
                unlisted      <font color="#FF0000">{}</font>
                morearg       <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># split param if it happens to be one of these two forms:</font></i>
        <i><font color="#9A1900">#    --clappend &lt;cfg&gt;=val</font></i>
        <i><font color="#9A1900">#    --log-file=&lt;file&gt;</font></i>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$short_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$short</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$long_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$long</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$short</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$long</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> opts <font color="#990000">[</font>dict create<font color="#990000">]</font>
        
        <i><font color="#9A1900"># param configuration</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> value<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                comment       <font color="#FF0000">{</font> <font color="#FF0000">}</font>
                max_args      <font color="#993399">0</font>
                min_args      <font color="#993399">0</font>
                priority      <font color="#993399">50</font>
                disporder     <font color="#993399">20</font>
                body          <font color="#FF0000">{}</font>
                validation    none
                config        <font color="#FF0000">{}</font>
                config_value  <font color="#993399">1</font>
                log           <font color="#FF0000">{}</font>
                more_level    <font color="#993399">0</font>
        <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts <font color="#009900">$default</font> <font color="#990000">[</font>param get <font color="#009900">$default</font> <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        dict <b><font color="#0000FF">set</font></b> opts <b><font color="#0000FF">namespace</font></b> <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">{</font><b><font color="#0000FF">namespace</font></b> current<font color="#FF0000">}</font><font color="#990000">]</font>
        
        dict <b><font color="#0000FF">set</font></b> opts param_long  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$long_param</font>  <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
        dict <b><font color="#0000FF">set</font></b> opts param_short <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$short_param</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># This is in particular for the --help output to decide</font></i>
        <i><font color="#9A1900"># whether to display a parameter or not</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$unlisted</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#990000">-</font><font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$morearg</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$short</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> short_match <font color="#990000">[</font>array names ARGS <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#009900">$short</font><font color="#990000">,*]</font> <font color="#FF0000">{</font>
                        lassign <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$short_match</font> <font color="#990000">,]</font> x_short x_long
                
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$override</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> msg <font color="#FF0000">"Trying to register parameter $short_param, $long_param, but it already exists as -$x_short, --$x_long. If the intention is to override this parameter then also supply --override."</font>
                                log ERROR <font color="#FF0000">{</font><font color="#009900">$msg</font><font color="#FF0000">}</font>
                                <b><font color="#0000FF">error</font></b> <font color="#009900">$msg</font>
                        <font color="#FF0000">}</font>
                        
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$x_long</font> <font color="#990000">==</font> <font color="#009900">$long</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                log DEBUG <font color="#FF0000">{</font>Overriding app parameter <font color="#990000">-</font><font color="#009900">$short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                log DEBUG <font color="#FF0000">{</font>Overriding short app parameter <font color="#990000">-</font><font color="#009900">$x_short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$x_long</font> with <font color="#990000">-</font><font color="#009900">$short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
                                <b><font color="#0000FF">set</font></b> ARGS<font color="#990000">(,</font><font color="#009900">$x_long</font><font color="#990000">)</font> <font color="#009900">$ARGS</font><font color="#990000">(</font><font color="#009900">$short_match</font><font color="#990000">)</font>
                        <font color="#FF0000">}</font>
                        unset ARGS<font color="#990000">(</font><font color="#009900">$short_match</font><font color="#990000">)</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> long_match <font color="#990000">[</font>array names ARGS <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#990000">*,</font><font color="#009900">$long</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$override</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> msg <font color="#FF0000">"Trying to register parameter $long_param, but it already exists. If the intention is to override this parameter then also supply --override."</font>
                        log ERROR <font color="#FF0000">{</font><font color="#009900">$msg</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">error</font></b> <font color="#009900">$msg</font>
                <font color="#FF0000">}</font>
                log DEBUG <font color="#FF0000">{</font>Overriding long app parameter <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
                
                unset ARGS<font color="#990000">(</font><font color="#009900">$long_match</font><font color="#990000">)</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$short</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$long</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> disp <font color="#FF0000">"-$short, --$long"</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$short</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> disp <font color="#990000">--</font><font color="#009900">$long</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> disp <font color="#990000">-</font><font color="#009900">$short</font>
        <font color="#FF0000">}</font>
        log DEV <font color="#FF0000">{</font>Registering app param <font color="#009900">$disp</font><font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> ARGS<font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)</font> <font color="#009900">$opts</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848181" ID="ID_1498726848181473" MODIFIED="1498726848181" TEXT="param separate" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848181" ID="ID_1498726848182005" MODIFIED="1498726848182" TEXT="Split file arguments based on the file type.&#xA;&#xA;The names of the variables to store the result in are passed in to the proc,&#xA;the procedure does not return any values.&#xA;&#xA;Example usage:&#xA;   param separate $argv params files directories&#xA;   process_parameters $params&#xA;   process_files $files&#xA;   process_directories $directories&#xA;&#xA;@param arguments       List of arguments passed to the script&#xA;@param app_var         The name of the variable to store application&#xA;                       arguments as (this will be all non-file arguments)&#xA;@param files_var       The variable name to store the file arguments as&#xA;@param dirs_var        The variable name to store the directory arguments as" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848182" ID="ID_1498726848182375" MODIFIED="1498726848182" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848194" ID="ID_1498726848194054" MODIFIED="1498726848194">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param separate <font color="#990000">--</font>params <font color="#FF0000">{</font> arguments <font color="#FF0000">{</font> app_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> files_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> dirs_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$app_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$app_var</font> application_args
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$files_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$files_var</font> files_to_process
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dirs_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$dirs_var</font> dirs_to_process
        <font color="#FF0000">}</font>

        touch application_args files_to_process dirs_to_process

        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> application_args <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># If "." set file to current working directory</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font> <font color="#009900">$arg</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">.</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">file</font></b> <font color="#990000">[</font><b><font color="#0000FF">pwd</font></b><font color="#990000">]</font>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># Process based on file type</font></i>
                <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font><b><font color="#0000FF">file</font></b> type <font color="#009900">$arg</font><font color="#990000">]</font>
                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$file_type</font> <font color="#FF0000">{</font>
                fifo <font color="#990000">-</font>
                link <font color="#990000">-</font>
                characterSpecial <font color="#990000">-</font>
                blockSpecial <font color="#990000">-</font>
                <b><font color="#0000FF">socket</font></b> <font color="#990000">-</font>
                <b><font color="#0000FF">file</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> files_to_process <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                directory <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> dirs_to_process <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        log INFO <font color="#FF0000">{</font>Strangely enough the <b><font color="#0000FF">file</font></b> <font color="#009900">$arg</font> exists but is not a recognisable <b><font color="#0000FF">file</font></b> type <font color="#990000">(</font><font color="#009900">$file_type</font><font color="#990000">).</font> Omitting<font color="#990000">.</font> <font color="#990000">(</font><font color="#009900">$arg</font><font color="#990000">)</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848194" ID="ID_1498726848194657" MODIFIED="1498726848194" TEXT="param split" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848195" ID="ID_1498726848195141" MODIFIED="1498726848195" TEXT="Procedure to split parameters based on a given prefix.&#xA;&#xA;For example:&#xA;   --type d -i --file ex1.txt ex2.txt --num 1 2 3&#xA;&#xA;would be split up into a list on the form {param} {value...} {param} {value...} ...&#xA;&#xA;I.e.:&#xA;   {--type} {d} {-i} {} {--file} {ex1.txt ex2.txt} {--num} {1 2 3}&#xA;&#xA;@param arguments        List of arguments to split&#xA;@option -strip_prefix   Option to remove the prefix from the params in the returned list&#xA;@option --arg_prefix    Specifies the prefix used, defaults to hyphen (-)&#xA;@return                 A list on the form {param} {value...} {param} {value...} ..." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848195" ID="ID_1498726848195505" MODIFIED="1498726848195" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848205" ID="ID_1498726848205891" MODIFIED="1498726848205">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param <b><font color="#0000FF">split</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                strip_prefix <font color="#993399">0</font>
                arg_prefix   <font color="#990000">-</font>
                <b><font color="#0000FF">default</font></b>      <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font> arguments
        
        touch output 
        <b><font color="#0000FF">foreach</font></b> arg <font color="#990000">[</font>lreverse <font color="#009900">$arguments</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+</font> <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> value <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists value<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>lreverse <font color="#009900">$value</font><font color="#990000">]</font>
                        unset value
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$default</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$strip_prefix</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$arg</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$arg</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>lreverse <font color="#009900">$output</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848206" ID="ID_1498726848206503" MODIFIED="1498726848206" TEXT="param update" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848206" ID="ID_1498726848206998" MODIFIED="1498726848207" TEXT="This allows for an application to update a registered parameter. A common&#xA;use case is to alter the description of what a given parameter does.&#xA;&#xA;For example the default description for the --dry-run option may not be&#xA;suitable for all applications.&#xA;&#xA;@param short_param         The short hand parameter, should be a single&#xA;                           character&#xA;@param long_param          The long version of the parameter&#xA;@param args                Additional options to specify what needs to&#xA;                           be changed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848207" ID="ID_1498726848207303" MODIFIED="1498726848207" TEXT="@see" FOLDED="true">
<node CREATED="1498726848207" ID="ID_1498726848207504" MODIFIED="1498726848207" TEXT="param register for a list of available options" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848207" ID="ID_1498726848207817" MODIFIED="1498726848207" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848220" ID="ID_1498726848220713" MODIFIED="1498726848220">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param update <font color="#990000">--</font>params <font color="#FF0000">{</font> short_param long_param args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>arg_utils<font color="#990000">::</font>ARGS
        
        <i><font color="#9A1900"># proc args</font></i>
        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                unlisted      <font color="#FF0000">{}</font>
                morearg       <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># split param if it happens to be one of these two forms:</font></i>
        <i><font color="#9A1900">#    --clappend &lt;cfg&gt;=val</font></i>
        <i><font color="#9A1900">#    --log-file=&lt;file&gt;</font></i>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$short_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$short</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$long_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$long</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$short</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$long</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists ARGS<font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"Parameter $short_param, $long_param is not registered"</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> opts <font color="#009900">$ARGS</font><font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)</font>
        
        <i><font color="#9A1900"># param configuration</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> value<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                comment       <font color="#990000">\</font><font color="#993399">0</font>
                max_args      <font color="#990000">\</font><font color="#993399">0</font>
                min_args      <font color="#990000">\</font><font color="#993399">0</font>
                priority      <font color="#990000">\</font><font color="#993399">0</font>
                disporder     <font color="#990000">\</font><font color="#993399">0</font>
                body          <font color="#990000">\</font><font color="#993399">0</font>
                validation    <font color="#990000">\</font><font color="#993399">0</font>
                config        <font color="#990000">\</font><font color="#993399">0</font>
                config_value  <font color="#990000">\</font><font color="#993399">0</font>
                log           <font color="#990000">\</font><font color="#993399">0</font>
                more_level    <font color="#990000">\</font><font color="#993399">0</font>
        <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> p_value <font color="#990000">[</font>param get <font color="#009900">$default</font> <font color="#009900">$value</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$p_value</font> <font color="#990000">!=</font> <font color="#990000">\</font><font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        dict <b><font color="#0000FF">set</font></b> opts <font color="#009900">$default</font> <font color="#009900">$p_value</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># This is in particular for the --help output to decide</font></i>
        <i><font color="#9A1900"># whether to display a parameter or not</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$unlisted</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#990000">-</font><font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$morearg</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        
        dict <b><font color="#0000FF">set</font></b> opts param_long  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$long_param</font>  <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
        dict <b><font color="#0000FF">set</font></b> opts param_short <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$short_param</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>

        log DEBUG <font color="#FF0000">{</font>Updating app param <font color="#990000">-</font><font color="#009900">$short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> ARGS<font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)</font> <font color="#009900">$opts</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848221" ID="ID_1498726848221336" MODIFIED="1498726848221" TEXT="param validate" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848221" ID="ID_1498726848221910" MODIFIED="1498726848221" TEXT="Validate an argument value.&#xA;&#xA;Possible validation checks:&#xA;   - any &quot;string is&quot; class   e.g. &quot;integer&quot;, empty strings fail validation&#xA;   - file                    input must be a regular file&#xA;   - directory               input must be a directory (or not exist), the&#xA;                             application using the parameter is responsible&#xA;                             for creating the directory if it does not&#xA;                             already exist&#xA;   - directory_exists        input must be an existing directory&#xA;   - anyfile                 input file must exists (i.e. file or directory)&#xA;   - nofile                  input must not be a file&#xA;&#xA;List of &quot;string is&quot; classes supported:&#xA;   alnum, alpha, ascii, control, boolean, digit,&#xA;   double, entier, false, graph, integer, list,&#xA;   lower, print, punct, space, true, upper,&#xA;   wideinteger, wordchar, xdigit&#xA;&#xA;@param value               The argument value to validate&#xA;@param validation_check    The validation check to apply&#xA;@return                    1 if the value passed the validation check&#xA;                           and 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848222" ID="ID_1498726848222354" MODIFIED="1498726848222" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848235" ID="ID_1498726848235137" MODIFIED="1498726848235">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param validate <font color="#990000">--</font>params <font color="#FF0000">{</font> value validation_check <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$validation_check</font> <font color="#FF0000">{</font>
        <font color="#FF0000">{}</font> <font color="#990000">-</font> 
        <font color="#FF0000">{</font>none<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>alpha<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>alnum<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>ascii<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>graph<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>upper<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>lower<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>print<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>wordchar<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>control<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>punct<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>space<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>bool<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>boolean<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>true<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>false<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>digit<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>int<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>integer<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>wideint<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>wideinteger<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>double<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>xdigit<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> is <font color="#009900">$validation_check</font> <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> isfile <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>directory<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># If the directory does not exist then it should be fine, the</font></i>
                <i><font color="#9A1900"># application using the argument is responsible for creating</font></i>
                <i><font color="#9A1900"># the directory if it doesn't exist.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$value</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>directory_exists<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>anyfile<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>nofile<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$value</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Unknown validation check <font color="#009900">$validation_check</font> <b><font color="#0000FF">for</font></b> input <font color="#FF0000">'$value'</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1498726848235" ID="ID_1498726848235636" MODIFIED="1498726848235" TEXT="$ns" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726848236" ID="ID_1498726848236628" MODIFIED="1498726848236" TEXT="arg_utils::help" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848237" ID="ID_1498726848237033" MODIFIED="1498726848237" TEXT="Prints out the help info to standard out." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848237" ID="ID_1498726848237312" MODIFIED="1498726848237" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726848237" ID="ID_1498726848237508" MODIFIED="1498726848237" TEXT="HELP" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848237" ID="ID_1498726848237784" MODIFIED="1498726848237" TEXT="Contains the help info text. If the %args% placeholder is present then this will be replaced with formatted arguments taken from the ARGS config item." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848238" ID="ID_1498726848238112" MODIFIED="1498726848238" TEXT="ARGS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848238" ID="ID_1498726848238383" MODIFIED="1498726848238" TEXT="The application arguments that can be passed in, this will override the description for any existing TApp parameters provided the short and long parameter names match. Should be a list on the form: {‑s ‑‑long} {description} {‑l ‑‑list} {description}" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726848238" ID="ID_1498726848238688" MODIFIED="1498726848238" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848248" ID="ID_1498726848248453" MODIFIED="1498726848248">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> arg_utils<font color="#990000">::</font>help <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> ARGS

        <b><font color="#0000FF">set</font></b> charmap <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">%</font>args<font color="#990000">%</font> <font color="#990000">[</font>param <b><font color="#0000FF">format</font></b> <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#990000">[</font>cfg get ARGS<font color="#990000">]</font> <font color="#990000">[</font>_get_help_args<font color="#990000">]]]]</font>
        log STDOUT <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$charmap</font> <font color="#990000">[</font>cfg get HELP<font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848249" ID="ID_1498726848249051" MODIFIED="1498726848249" TEXT="arg_utils::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848249" ID="ID_1498726848249433" MODIFIED="1498726848249" TEXT="Initialises the arg_utils by setting up the application parameters.&#xA;&#xA;This is called automatically when the tapp library is being loaded." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848249" ID="ID_1498726848249783" MODIFIED="1498726848249" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848264" ID="ID_1498726848264338" MODIFIED="1498726848264">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> arg_utils<font color="#990000">::</font>init args <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> ARGS
        
        param register <font color="#990000">-</font>d <font color="#990000">--</font>dry<font color="#990000">-</font>run                <font color="#990000">--</font>config DRY_RUN                <font color="#990000">--</font>disporder <font color="#993399">10</font>                <font color="#990000">--</font>priority <font color="#993399">10</font>                <font color="#990000">--</font>morearg                <font color="#990000">--</font>comment <font color="#FF0000">{</font>run through the script<font color="#990000">,</font> but don<font color="#FF0000">'t change anything}</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        param register -v --verbose                --config VERBOSE                --disporder 30                --morearg                --comment {enable verbose output}</font>

<font color="#FF0000">        param register {} --more                --priority 10                --disporder 35                --body {</font>
<font color="#FF0000">                        set more_level [expr {[string length $argument] - 4}]</font>
<font color="#FF0000">                        cfg set MORE_LEVEL $more_level</font>
<font color="#FF0000">                        log DEBUG {Enabled more output (more level $more_level)}</font>
<font color="#FF0000">                }                --morearg                --comment {show more output, further dashes (e.g. ---more) show additional output (for applications where applicable)}</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        param register {} --stdout                --config LOG_FILE                --config_value {stdout}                --disporder 55                --morearg                --log {Setting log file to standard out}                --comment {set log file to standard out}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --stderr                --config LOG_FILE                --config_value {stderr}                --disporder 55                --morearg                --log {Setting log file to standard err}                --comment {set log file to standard err}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--&lt;log level&gt;}                --disporder 90                --morearg                --comment {enable logging at a specific log level:</font><font color="#CC33CC">\n</font><font color="#FF0000">trace, dev, debug, info, warn, error, fatal, mute}</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        foreach level {trace dev debug info warn error fatal mute} {</font>
<font color="#FF0000">                param register {} --$level                        --priority 10                        --unlisted                        --config LOG_LEVEL                        --config_value [string toupper $level]                        --log "Enabled $level logging"</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        param register -h --help                --body { arg_utils::help }                --disporder 20                --comment {display this help, try including --more for additional options}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register -c --config                --body {</font>
<font color="#FF0000">                        cfg file $value</font>
<font color="#FF0000">                        log DEBUG {Using $value as configuration file}</font>
<font color="#FF0000">                }                --max_args 1                --min_args 1                --validation {file}                --disporder 60                --priority 10                --morearg                --comment {specify the configuration file to load}</font>

<font color="#FF0000">        param register {} {--cset &lt;cfg&gt;=1}                --max_args 1                --body {</font>
<font color="#FF0000">                        lassign [split $values {=}] cfg_name cfg_value</font>
<font color="#FF0000">                        catch {</font>
<font color="#FF0000">                                set cfg_value [subst $cfg_value]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        log DEBUG {Setting config item $cfg_name to $cfg_value}</font>
<font color="#FF0000">                        cfg set $cfg_name $cfg_value</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {override a specific configuration item}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--cappend &lt;cfg&gt;=val}                --max_args 1                --body {</font>
<font color="#FF0000">                        lassign [split $values {=}] cfg_name cfg_value</font>
<font color="#FF0000">                        catch {</font>
<font color="#FF0000">                                set cfg_value [subst $cfg_value]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        log DEBUG {Appending </font><font color="#CC33CC">\"</font><font color="#FF0000">$cfg_value</font><font color="#CC33CC">\"</font><font color="#FF0000"> to config item $cfg_name}</font>
<font color="#FF0000">                        cfg append $cfg_name $cfg_value</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {append a value to a specific configuration item}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--clappend &lt;cfg&gt;=val}                --max_args 1                --body {</font>
<font color="#FF0000">                        lassign [split $values {=}] cfg_name cfg_value</font>
<font color="#FF0000">                        catch {</font>
<font color="#FF0000">                                set cfg_value [subst $cfg_value]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        log DEBUG {Appending </font><font color="#CC33CC">\"</font><font color="#FF0000">$cfg_value</font><font color="#CC33CC">\"</font><font color="#FF0000"> to config item $cfg_name}</font>
<font color="#FF0000">                        cfg lappend $cfg_name {*}$cfg_value</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {list append a value to a specific configuration item}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--log-file=&lt;file&gt;}                --config LOG_FILE                --max_args 1                --disporder 90                --morearg                --comment {override file to log to}</font>

<font color="#FF0000">        param register {} {--log-dir=&lt;dir&gt;}                --config LOG_DIR                --max_args 1                --disporder 90                --morearg                --comment {override directory to log to}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--log-prefix=&lt;text&gt;}                --config LOG_PREFIX                --max_args 1                --disporder 90                --morearg                --comment {specify a log prefix, for cross-script calls}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --log-proc                --config LOG_PROC_PREFIX                --disporder 90                --morearg                --comment {also output the Tcl procedure doing the logging}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --log_enable_stdout                --config LOG_DISABLE_STD_STREAMS                --config_value 0                --disporder 90                --morearg                --comment {enable logging to standard out (refers to stdout log statements)}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --log_disable_stdout                --config LOG_DISABLE_STD_STREAMS                --config_value 1                --disporder 90                --morearg                --comment {disable logging to standard out (refers to stdout log statements)}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--show-config ?&lt;cfg&gt;?}                --body {</font>
<font color="#FF0000">                        if {$values == {}} {</font>
<font color="#FF0000">                                set values {*}</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                        foreach filter $values {</font>
<font color="#FF0000">                                log STDOUT [cfg print $filter]</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --max_args *                --comment {dump configuration items currently set; accepts multiple glob patterns                           as arguments which can be used to pull out individual config items,                           defaults to listing all configuration items}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--trace_var=&lt;var&gt;}                --body {</font>
<font color="#FF0000">                        trace add variable $value array tapp_lang::_watch</font>
<font color="#FF0000">                        trace add variable $value write tapp_lang::_watch</font>
<font color="#FF0000">                        trace add variable $value read  tapp_lang::_watch</font>
<font color="#FF0000">                        trace add variable $value unset tapp_lang::_watch</font>
<font color="#FF0000">                        log DEBUG {Enabled tracing of variable: $value}</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {enable tracing of when a given variable is set and read}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} {--trace_cmd=&lt;cmd&gt;}                --body {</font>
<font color="#FF0000">                        trace add execution $value enter tapp_lang::_watch_command_enter</font>
<font color="#FF0000">                        trace add execution $value leave tapp_lang::_watch_command_leave</font>
<font color="#FF0000">                        log DEBUG {Enabled tracing of command: $value}</font>
<font color="#FF0000">                }                --disporder 60                --morearg                --comment {enable tracing of when a given procedure is entered and left}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --assert                --body {</font>
<font color="#FF0000">                        set ::tapp::ASSERT 1</font>
<font color="#FF0000">                        log DEV {Enabled assert statements}</font>
<font color="#FF0000">                }                --disporder 65                --morearg                --comment [subst {enable assert statements [expr {[info exists ::tapp::ASSERT] &amp;&amp; $::tapp::ASSERT} ? </font><font color="#CC33CC">\{</font><font color="#FF0000">(default)</font><font color="#CC33CC">\}</font><font color="#FF0000"> : </font><font color="#CC33CC">\{\}</font><font color="#FF0000">]}]</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        param register {} --no_assert                --body {</font>
<font color="#FF0000">                        set ::tapp::ASSERT 0</font>
<font color="#FF0000">                        log DEV {Disabled assert statements}</font>
<font color="#FF0000">                }                --disporder 65                --morearg                --comment [subst {disable assert statements [expr {[info exists ::tapp::ASSERT] &amp;&amp; !$::tapp::ASSERT} ? </font><font color="#CC33CC">\{</font><font color="#FF0000">(default)</font><font color="#CC33CC">\}</font><font color="#FF0000"> : </font><font color="#CC33CC">\{\}</font><font color="#FF0000">]}]</font>

<font color="#FF0000">        param register {} {--dest=&lt;dir&gt;}                --config DESTINATION                --max_args 1                --validation {directory}                --log {Destination set to $value}                --disporder 68                --morearg                --comment {set destination for scripts where this is relevant}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848265" ID="ID_1498726848265038" MODIFIED="1498726848265" TEXT="arg_utils::let" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848265" ID="ID_1498726848265720" MODIFIED="1498726848265" TEXT="Shorthand proc to get a series of parameter values from a list of arguments.&#xA;&#xA;Each parameter will be looked up in the list of arguments.&#xA;  - if the parameter is not found then the default value will be used&#xA;  - if the parameter is found then any following values will be used&#xA;  - if the parameter is found, but isn't followed by any explicit values then&#xA;    it will default to 1 (as in true / enabled)&#xA;&#xA;The value used is stored in a variable with the same name as the parameter&#xA;(less any argument prefix) in the calling proc.&#xA;&#xA;The parameters passed in have to have the argument prefix. The amount of&#xA;prefixes denote whether it is a flag taking no values (e.g. -nocase) or a&#xA;parameter with a value (e.g. --file ex.txt).&#xA;&#xA;If parameters matching regular expressions is needed then have a look at&#xA;param get.&#xA;&#xA;Synopsis:&#xA;   let $args [list param default param default ... param default] ?arg_prefix?&#xA;&#xA;Example:&#xA;   set args {--color red -enabled --list {d e f} --ignored yes}&#xA;   let $args {&#xA;           color   {blue}&#xA;           list    {a b c}&#xA;           enabled {0}&#xA;           repeat  {no}&#xA;   }&#xA;&#xA;Will result in the following variables being set:&#xA;   set color   {red}&#xA;   set list    {d e f}&#xA;   set enabled {1}&#xA;   set repeat  {no}&#xA;&#xA;@param arguments       The list of arguments to search through&#xA;@param arg_list        A list on the form &lt;parameter&gt; &lt;default&gt;, can contain&#xA;                       multiple parameters&#xA;@param remaining_args  Name of the variable to store the remaining args in&#xA;@param arg_prefix      The parameter prefix, defaults to hyphen (-)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848266" ID="ID_1498726848266236" MODIFIED="1498726848266" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848280" ID="ID_1498726848280866" MODIFIED="1498726848280">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> arg_utils<font color="#990000">::</font>let <font color="#FF0000">{</font> arguments arg_list <font color="#FF0000">{</font>remaining_args <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        touch plist vlist rlist
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>param <b><font color="#0000FF">default</font></b><font color="#FF0000">}</font> <font color="#009900">$arg_list</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> plist <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> vlist <font color="#009900">$default</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">llength</font></b> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$llength</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$arg</font> <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#009900">$arg_prefix</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> rlist <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> val <font color="#993399">1</font>
                <b><font color="#0000FF">set</font></b> prm <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$arg</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font>exact <font color="#009900">$plist</font> <font color="#009900">$prm</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$idx</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> rlist <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> index <font color="#009900">$arg</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#009900">$arg_prefix</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> next <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">+</font><font color="#993399">1</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$i</font><font color="#990000">+</font><font color="#993399">1</font> <font color="#990000">&gt;=</font> <font color="#009900">$llength</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+\[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">\]</font> <font color="#009900">$next</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                log WARNING <font color="#FF0000">{</font>let<font color="#990000">:</font> No value passed <b><font color="#0000FF">in</font></b> <b><font color="#0000FF">for</font></b> parameter <font color="#009900">$arg</font><font color="#990000">,</font> defaulting to <font color="#009900">$val</font><font color="#990000">.</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> val <font color="#009900">$next</font>
                                <b><font color="#0000FF">incr</font></b> i
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">lset</font></b> vlist <font color="#009900">$idx</font> <font color="#009900">$val</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> param <font color="#009900">$plist</font> value <font color="#009900">$vlist</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">"set $param {$value}"</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$remaining_args</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">"set $remaining_args {[join $rlist { }]}"</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848281" ID="ID_1498726848281576" MODIFIED="1498726848281" TEXT="param format" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848282" ID="ID_1498726848282440" MODIFIED="1498726848282" TEXT="This procedure is responsible for formatting and aligning the passed in&#xA;arguments for display purposes.&#xA;&#xA;@param args               The arguments to format for display purposes&#xA;@return                   The arguments formatted as text (string containing&#xA;                          newlines)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848282" ID="ID_1498726848282871" MODIFIED="1498726848282" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726848283" ID="ID_1498726848283159" MODIFIED="1498726848283" TEXT="CONSOLE_WIDTH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848283" ID="ID_1498726848283551" MODIFIED="1498726848283" TEXT="Determines the width of the console, note that this is automatically set by the tapp package on load if the core linux utility &quot;tty&quot; is available. Defaults to 80." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848283" ID="ID_1498726848283982" MODIFIED="1498726848283" TEXT="HELP_ARG_WIDTH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848284" ID="ID_1498726848284307" MODIFIED="1498726848284" TEXT="The width of the argument column, which determines where the argument comment column starts, defaults to 27." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848284" ID="ID_1498726848284605" MODIFIED="1498726848284" TEXT="HELP_ARG_INDENT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848284" ID="ID_1498726848284881" MODIFIED="1498726848284" TEXT="Determines where the argument column starts, i.e. how much the arguments are indented. Defaults to 2." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848285" ID="ID_1498726848285155" MODIFIED="1498726848285" TEXT="HELP_ARG_PADDING" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848285" ID="ID_1498726848285441" MODIFIED="1498726848285" TEXT="Determines the padding needed when there are only one argument. The formatting utility displays up to two aliases for the same parameter, for example the help can be triggered by passing in either ‑h or ‑‑help. This would be listed as ‑h, ‑‑help in the output. The padding here is for when the ‑h parameter does not exist, in which case we need 4 spaces to align the ‑‑help parameter with other parameters. Defaults to 4. This is a config that one should generally not need to change." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848285" ID="ID_1498726848285730" MODIFIED="1498726848285" TEXT="EXIT_ON_ARG_FAILURE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726848286" ID="ID_1498726848286038" MODIFIED="1498726848286" TEXT="By default TApp will exit when argument validation fails, i.e. an illegal parameter is passed in or a parameter expecting a file argument receives none. When exiting an error message will be written to standard err. This can be disabled by setting this configuration item to 0. The main risk of disabling this functionality is that it may not be clear to the end user that an argument passed in was not actually applied." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726848286" ID="ID_1498726848286356" MODIFIED="1498726848286" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848303" ID="ID_1498726848303319" MODIFIED="1498726848303">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param <b><font color="#0000FF">format</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        touch args_to_print padding processed_args

        <b><font color="#0000FF">set</font></b> line_width  <font color="#990000">[</font>cfg get CONSOLE_WIDTH <font color="#993399">80</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> arg_width   <font color="#990000">[</font>cfg get HELP_ARG_WIDTH <font color="#993399">27</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> arg_indent  <font color="#990000">[</font>cfg get HELP_ARG_INDENT <font color="#993399">2</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> arg_padding <font color="#990000">[</font>cfg get HELP_ARG_PADDING <font color="#993399">4</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> arguments <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>

        <i><font color="#9A1900"># Check whether we only have single numbered arguments, if not</font></i>
        <i><font color="#9A1900"># then we want to pad the first argument with spaces so that single</font></i>
        <i><font color="#9A1900"># numbered arguments line up with double numbered arguments</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>arglist desc<font color="#FF0000">}</font> <font color="#009900">$arguments</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arglist</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> padding <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$arg_padding</font><font color="#990000">]</font>
                        <b><font color="#0000FF">break</font></b>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Loop through all arguments first to find out if we need to auto</font></i>
        <i><font color="#9A1900"># increase the arg width.</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>arglist desc<font color="#FF0000">}</font> <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <i><font color="#9A1900"># Only show up to two of the arguments</font></i>
                <b><font color="#0000FF">set</font></b> arg1 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> arg2 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">1</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">set</font></b> joined_arg <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$arg2</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">?</font> <font color="#FF0000">"$arg1, $arg2"</font> <font color="#990000">:</font> <font color="#FF0000">"$padding$arg1"</font><font color="#FF0000">}</font><font color="#990000">]</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$joined_arg</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#009900">$arg_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> arg_width <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$joined_arg</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#993399">1</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> desc_width  <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$line_width</font> <font color="#990000">-</font> <font color="#009900">$arg_width</font> <font color="#990000">-</font> <font color="#009900">$arg_indent</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> desc_indent <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$arg_width</font> <font color="#990000">+</font> <font color="#009900">$arg_indent</font><font color="#FF0000">}</font><font color="#990000">]]</font>
        <b><font color="#0000FF">set</font></b> format_str  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$arg_indent</font><font color="#990000">]%-</font>$<font color="#FF0000">{</font>arg_width<font color="#FF0000">}</font>s<font color="#990000">%</font>s

        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>arglist desc<font color="#FF0000">}</font> <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <i><font color="#9A1900"># Only show up to two of the arguments</font></i>
                <b><font color="#0000FF">set</font></b> arg1 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> arg2 <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arglist</font> <font color="#993399">1</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg2</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> joined_arg <font color="#FF0000">"$arg1, $arg2"</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><font color="#990000">--*</font><font color="#FF0000">}</font> <font color="#009900">$arg1</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$arg1</font><font color="#990000">]</font> <font color="#990000">&lt;=</font> <font color="#009900">$arg_padding</font> <font color="#990000">-</font> <font color="#993399">2</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> joined_arg <font color="#009900">$arg1</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> joined_arg <font color="#FF0000">"$padding$arg1"</font>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># We only want to process each argument once, for example if</font></i>
                <i><font color="#9A1900"># defined in both application help and in the more help section.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>exact <font color="#009900">$processed_args</font> <font color="#009900">$joined_arg</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> processed_args <font color="#009900">$joined_arg</font>

                clear line
                
                <b><font color="#0000FF">set</font></b> arg_printed <font color="#993399">0</font>

                <b><font color="#0000FF">foreach</font></b> desc_line <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#990000">\</font>n <font color="#990000">\</font>x00 <font color="#990000">\\</font>n <font color="#990000">\</font>x00<font color="#FF0000">}</font> <font color="#009900">$desc</font><font color="#990000">]</font> <font color="#990000">\</font>x00<font color="#990000">]</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$desc_line</font><font color="#990000">]</font> <font color="#990000">&lt;=</font> <font color="#009900">$desc_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg_printed</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> args_to_print $<font color="#FF0000">{</font>desc_indent<font color="#FF0000">}</font><font color="#009900">$desc_line</font>
                                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> args_to_print <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$format_str</font> <font color="#009900">$joined_arg</font> <font color="#009900">$desc_line</font><font color="#990000">]</font>
                                        <b><font color="#0000FF">set</font></b> arg_printed <font color="#993399">1</font>
                                <font color="#FF0000">}</font>
                                <b><font color="#0000FF">continue</font></b>
                        <font color="#FF0000">}</font>

                        <b><font color="#0000FF">foreach</font></b> word <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$desc_line</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$word</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#993399">1</font> <font color="#990000">&lt;=</font> <font color="#009900">$desc_width</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">append</font></b> line <font color="#FF0000">{</font> <font color="#FF0000">}</font> 
                                        <font color="#FF0000">}</font>
                                        <b><font color="#0000FF">append</font></b> line <font color="#009900">$word</font>
                                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arg_printed</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">lappend</font></b> args_to_print $<font color="#FF0000">{</font>desc_indent<font color="#FF0000">}</font><font color="#009900">$line</font>
                                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">lappend</font></b> args_to_print <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$format_str</font> <font color="#009900">$joined_arg</font> <font color="#009900">$line</font><font color="#990000">]</font>
                                                <b><font color="#0000FF">set</font></b> arg_printed <font color="#993399">1</font>
                                        <font color="#FF0000">}</font>
                                        <b><font color="#0000FF">set</font></b> line <font color="#009900">$word</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>

                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$line</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> args_to_print $<font color="#FF0000">{</font>desc_indent<font color="#FF0000">}</font><font color="#009900">$line</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If we have additional args to display, add a trailing newline</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args_to_print</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> args_to_print <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$args_to_print</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848304" ID="ID_1498726848304039" MODIFIED="1498726848304" TEXT="param get" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848304" ID="ID_1498726848304734" MODIFIED="1498726848304" TEXT="Procedure to retrieve the value of a given parameter from a list of arguments.&#xA;&#xA;The number of argument prefixes (hyphen) determines the role of the argument&#xA;passed in (either flags or parameters).&#xA;&#xA;A single prefix indicates an optional flag (e.g. -flag) and will default to 1&#xA;if passed in. If a flag is followed by a value then this will not be taken&#xA;into account as flags don't take values.&#xA;&#xA;Two prefixes indicates a parameter and parameters are followed by an explicit&#xA;value (e.g. --file ex.txt). If the following argument looks like another&#xA;parameter (e.g. starting with the prefix) then it will default to 1 and a&#xA;warning will be logged.&#xA;&#xA;If more than one of the same parameter or flag is passed in then the last&#xA;argument takes precedence and a warning will be logged.&#xA;&#xA;Parameters and flags are case insensitive.&#xA;&#xA;Synopsis:&#xA;   param get &lt;parameter&gt; ?&lt;default&gt;? ?&lt;list&gt;?&#xA;&#xA;For example:&#xA;   set args [list -i 3 --file ex1.txt ex2.txt --sep {|} --num 1 --num 2}]&#xA;   set file [param get --file]&#xA;   set sep  [param get --sep {:}]&#xA;   set i    [param get -i]&#xA;   set num  [param get --num]&#xA;   set dflt [param get --default {99}]&#xA;&#xA;will result in:&#xA;   set file {ex1.txt}; # because parameters takes maximum 1 value (use list)&#xA;   set sep  {|};       # because parameter was found in args list&#xA;   set i    {1};       # because flags (single hyphen) enable functionality&#xA;   set num  {2};       # because the last parameter takes precedence&#xA;   set dflt {99};      # because the parameter was not found in args list&#xA;&#xA;@param param           The parameter to search for (can be a regular expression)&#xA;@param default         The default value if not found&#xA;@param arguments       The list of arguments to search through, defaults to&#xA;                       doing upvar to get hold of the proc's &quot;args&quot; variable&#xA;                       if not defined.&#xA;@param arg_prefix      The parameter prefix, defaults to hyphen (-)&#xA;@return                Empty string if the argument value was not found,&#xA;                       the value if a single value was found or &quot;1&quot; if the&#xA;                       argument was found to exist, but with no values." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848305" ID="ID_1498726848305163" MODIFIED="1498726848305" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848318" ID="ID_1498726848318563" MODIFIED="1498726848318">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param get <font color="#990000">--</font>params <font color="#FF0000">{</font> param <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>arguments <font color="#FF0000">"</font><font color="#CC33CC">\0</font><font color="#FF0000">"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$arguments</font> <font color="#990000">==</font> <font color="#FF0000">"</font><font color="#CC33CC">\0</font><font color="#FF0000">"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                unset arguments
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> args arguments
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists arguments<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">error</font></b> <font color="#FF0000">{</font>Either pass the arguments <b><font color="#0000FF">list</font></b> <b><font color="#0000FF">in</font></b> or make sure the <b><font color="#0000FF">variable</font></b> is named <font color="#FF0000">"args"</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> length <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> param <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font>all <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$arguments</font> <font color="#990000">^</font>$<font color="#FF0000">{</font>arg_prefix<font color="#FF0000">}</font><font color="#990000">+</font><font color="#009900">$param</font><font color="#990000">\</font>$<font color="#990000">]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$idx</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$default</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$idx</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARNING <font color="#FF0000">{</font>param get<font color="#990000">:</font> Parameter <font color="#009900">$param</font> passed <b><font color="#0000FF">in</font></b> more than once<font color="#990000">.</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$idx</font> end<font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$idx</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> num <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$arg</font><font color="#990000">]</font> <font color="#990000">-</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$arg</font> <font color="#009900">$arg_prefix</font><font color="#990000">]]</font><font color="#FF0000">}</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> value <font color="#993399">1</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$num</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">incr</font></b> idx
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$idx</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$idx</font> <font color="#990000">&gt;=</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+\[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">\]</font> <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARNING <font color="#FF0000">{</font>param get<font color="#990000">:</font> No value passed <b><font color="#0000FF">in</font></b> <b><font color="#0000FF">for</font></b> parameter <font color="#009900">$param</font><font color="#990000">,</font> defaulting to <font color="#993399">1</font><font color="#990000">.</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> value <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$value</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848319" ID="ID_1498726848319215" MODIFIED="1498726848319" TEXT="param join" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848319" ID="ID_1498726848319744" MODIFIED="1498726848319" TEXT="Join arguments that are separated by a space.&#xA;&#xA;Most parameter flags are just single values such as --debug, --stdout or&#xA;--help.&#xA;&#xA;Some parameters take a value and these would need to be joined in the&#xA;parameter list before the parsing takes place. An example of this would be&#xA;the --cset CONFIG_ITEM=my_value parameter that allows for individual config&#xA;items to be overridden from the command line.&#xA;&#xA;@param arguments       The full list of command line parameters&#xA;@param args_to_join    List of parameters that take a value and needs to be&#xA;                       joined (with the value) prior to the parameter parsing&#xA;                       process taking place. Note that the items in this list&#xA;                       can be regular expressions.&#xA;@return                The list of command line parameters with the given&#xA;                       parameters joined if they exist. The order of the&#xA;                       argument list is preserved." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848320" ID="ID_1498726848320113" MODIFIED="1498726848320" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848330" ID="ID_1498726848330940" MODIFIED="1498726848330">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param <b><font color="#0000FF">join</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> arguments <font color="#FF0000">{</font> args_to_join <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        touch joined_args
        <b><font color="#0000FF">set</font></b> args_length <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#009900">$args_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> <b><font color="#0000FF">regexp</font></b> <font color="#009900">$args_to_join</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$regexp</font> <font color="#009900">$arg</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">incr</font></b> i
                                <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$arg</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$i</font><font color="#990000">]]</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> joined_args <font color="#009900">$arg</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$joined_args</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848331" ID="ID_1498726848331579" MODIFIED="1498726848331" TEXT="param parse" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848332" ID="ID_1498726848332171" MODIFIED="1498726848332" TEXT="Parse the application arguments and take action based on registered&#xA;parameters.&#xA;&#xA;Main responsibilities:&#xA;    - expand file arguments&#xA;    - identify application parameters&#xA;    - prioritise parameters that need to run prior to others&#xA;    - perform the action that should be taken when the parameter is passed in&#xA;&#xA;Synopsis:&#xA;   param parse &lt;arguments&gt; ?&lt;remaining_arguments_variable&gt;?&#xA;&#xA;Example usage:&#xA;   param parse $argv remaining_args&#xA;&#xA;or alternatively:&#xA;   set remaining_args [param parse $argv]&#xA;&#xA;@param arguments      &#xA;@param remaining_args_var  The variable in which to store the remaining&#xA;                           arguments (i.e. the ones that does not seem&#xA;                           to be registered application parameters)&#xA;@param arg_prefix          The parameter prefix, should always be a hyphen&#xA;                           (-) character&#xA;@return                    The remaining application parameters that were&#xA;                           not identified (same as stored in the&#xA;                           remaining_args_var if provided)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848332" ID="ID_1498726848332542" MODIFIED="1498726848332" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848343" ID="ID_1498726848343077" MODIFIED="1498726848343">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param parse <font color="#990000">--</font>params <font color="#FF0000">{</font> arguments <font color="#FF0000">{</font> remaining_args_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$remaining_args_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$remaining_args_var</font> remaining_args
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> expanded_args <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_expand_fileargs <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> args_to_parse <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_identify_params <font color="#009900">$expanded_args</font> remaining_args <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> args_to_parse <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_prioritise_args <font color="#009900">$args_to_parse</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> parse_result  <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_evaluate_params <font color="#009900">$args_to_parse</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled AUTO_LOG_INIT <font color="#993399">1</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                tapp_log<font color="#990000">::</font>init
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$remaining_args</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848343" ID="ID_1498726848343692" MODIFIED="1498726848343" TEXT="param register" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848344" ID="ID_1498726848344741" MODIFIED="1498726848344" TEXT="Register an application parameter.&#xA;&#xA;Synopsis:&#xA;   param register &lt;short param&gt; &lt;long param&gt; ?options?&#xA;&#xA;Example:&#xA;   param register -d --dry-run --config DRY_RUN&#xA;&#xA;The above enables the config DRY_RUN when -d or --dry-run is passed in to the&#xA;application.&#xA;&#xA;@param short_param         The short hand parameter, should be a single&#xA;                           character&#xA;@param long_param          The long version of the parameter&#xA;@param args                Additional options&#xA;@option --config           Specifies a config item to modify when the&#xA;                           parameter is passed in to the application&#xA;@option --config_value     The value to set the config item to when&#xA;                           the parameter is passed in, defaults to 1&#xA;                           (enabled)&#xA;@option --override         If registering a parameter that already exists&#xA;                           (like -d for example) then an error will be&#xA;                           thrown. The --override flag allows for the&#xA;                           application to override the existing parameter.&#xA;@option --comment          The comment to display alongside the parameter&#xA;                           when listing the help options&#xA;@option --body             Specifies Tcl code that should be evaluated when&#xA;                           the parameter is passed in (allows for more&#xA;                           control). Existing variables: $values (or $value)&#xA;                           containing the parameter argument (if any),&#xA;                           $argument containing the original argument. The&#xA;                           Tcl body will be evaluated in the namespace where&#xA;                           the parameter was registered from. Do not use the&#xA;                           &quot;return&quot; command within the body as this will&#xA;                           cause parameter evaluation to stop abruptly&#xA;                           resulting in undefined behaviour.&#xA;@option --log              The message to log when the parameter has been&#xA;                           evaluated. If not set and the parameter controls&#xA;                           a config item then a default message is logged.&#xA;@option --max_args         Sets the number of expected  arguments following&#xA;                           the parameter, defaults to 0 (no arguments). This&#xA;                           is primarily used when deducing whether&#xA;                           unparameterised arguments (like files for example)&#xA;                           belong as an argument to a parameter or not.&#xA;@option --min_args         Sets the minimum number of arguments to following&#xA;                           the parameter. If the parameter is passed in, but&#xA;                           fails to supply enough arguments then an error is&#xA;                           thrown.&#xA;@option --validation       Apply validation to the parameter arguments. This&#xA;                           can be used to check that a given input value is&#xA;                           an integer, a file or directory for example.&#xA;                           See arg_utils::param validate for available options.&#xA;@option --priority         Certain parameters need to be run prior to others&#xA;                           and this allows for some control over the order in&#xA;                           which they are processed. Defaults to 50.&#xA;@option --disporder        Allows for some control over the order in which&#xA;                           the parameters are displayed when listing options&#xA;                           on the help output. Defaults to 20.&#xA;@option --more_level       Controls what parameters are displayed when the&#xA;                           help section is listed. Defaults to 0, meaning&#xA;                           the parameter info is displayed when the --help&#xA;                           argument is passed to the application. If --help&#xA;                           and --more is passed then parameters with a&#xA;                           more level of 2 and below are displayed. If&#xA;                           ---more is passed in the parameters with a&#xA;                           more_level of 3 and below are displayed, etc.&#xA;@option --unlisted         Never list the parameter in the help info, this is&#xA;                           an alias for --more_level -1&#xA;@option --morearg          Only list the parameter when --more is passed in,&#xA;                           this is an alias for --more_level 1" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848345" ID="ID_1498726848345178" MODIFIED="1498726848345" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848360" ID="ID_1498726848360720" MODIFIED="1498726848360">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param register <font color="#990000">--</font>params <font color="#FF0000">{</font> short_param long_param args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>arg_utils<font color="#990000">::</font>ARGS
        
        <i><font color="#9A1900"># proc args</font></i>
        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                override      <font color="#993399">0</font>
                unlisted      <font color="#FF0000">{}</font>
                morearg       <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># split param if it happens to be one of these two forms:</font></i>
        <i><font color="#9A1900">#    --clappend &lt;cfg&gt;=val</font></i>
        <i><font color="#9A1900">#    --log-file=&lt;file&gt;</font></i>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$short_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$short</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$long_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$long</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$short</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$long</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> opts <font color="#990000">[</font>dict create<font color="#990000">]</font>
        
        <i><font color="#9A1900"># param configuration</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> value<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                comment       <font color="#FF0000">{</font> <font color="#FF0000">}</font>
                max_args      <font color="#993399">0</font>
                min_args      <font color="#993399">0</font>
                priority      <font color="#993399">50</font>
                disporder     <font color="#993399">20</font>
                body          <font color="#FF0000">{}</font>
                validation    none
                config        <font color="#FF0000">{}</font>
                config_value  <font color="#993399">1</font>
                log           <font color="#FF0000">{}</font>
                more_level    <font color="#993399">0</font>
        <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts <font color="#009900">$default</font> <font color="#990000">[</font>param get <font color="#009900">$default</font> <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        dict <b><font color="#0000FF">set</font></b> opts <b><font color="#0000FF">namespace</font></b> <font color="#990000">[</font><b><font color="#0000FF">uplevel</font></b> <font color="#993399">1</font> <font color="#FF0000">{</font><b><font color="#0000FF">namespace</font></b> current<font color="#FF0000">}</font><font color="#990000">]</font>
        
        dict <b><font color="#0000FF">set</font></b> opts param_long  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$long_param</font>  <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
        dict <b><font color="#0000FF">set</font></b> opts param_short <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$short_param</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># This is in particular for the --help output to decide</font></i>
        <i><font color="#9A1900"># whether to display a parameter or not</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$unlisted</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#990000">-</font><font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$morearg</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$short</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> short_match <font color="#990000">[</font>array names ARGS <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#009900">$short</font><font color="#990000">,*]</font> <font color="#FF0000">{</font>
                        lassign <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$short_match</font> <font color="#990000">,]</font> x_short x_long
                
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$override</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> msg <font color="#FF0000">"Trying to register parameter $short_param, $long_param, but it already exists as -$x_short, --$x_long. If the intention is to override this parameter then also supply --override."</font>
                                log ERROR <font color="#FF0000">{</font><font color="#009900">$msg</font><font color="#FF0000">}</font>
                                <b><font color="#0000FF">error</font></b> <font color="#009900">$msg</font>
                        <font color="#FF0000">}</font>
                        
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$x_long</font> <font color="#990000">==</font> <font color="#009900">$long</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                log DEBUG <font color="#FF0000">{</font>Overriding app parameter <font color="#990000">-</font><font color="#009900">$short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                log DEBUG <font color="#FF0000">{</font>Overriding short app parameter <font color="#990000">-</font><font color="#009900">$x_short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$x_long</font> with <font color="#990000">-</font><font color="#009900">$short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
                                <b><font color="#0000FF">set</font></b> ARGS<font color="#990000">(,</font><font color="#009900">$x_long</font><font color="#990000">)</font> <font color="#009900">$ARGS</font><font color="#990000">(</font><font color="#009900">$short_match</font><font color="#990000">)</font>
                        <font color="#FF0000">}</font>
                        unset ARGS<font color="#990000">(</font><font color="#009900">$short_match</font><font color="#990000">)</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> long_match <font color="#990000">[</font>array names ARGS <font color="#990000">-</font><b><font color="#0000FF">glob</font></b> <font color="#990000">*,</font><font color="#009900">$long</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$override</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> msg <font color="#FF0000">"Trying to register parameter $long_param, but it already exists. If the intention is to override this parameter then also supply --override."</font>
                        log ERROR <font color="#FF0000">{</font><font color="#009900">$msg</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">error</font></b> <font color="#009900">$msg</font>
                <font color="#FF0000">}</font>
                log DEBUG <font color="#FF0000">{</font>Overriding long app parameter <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
                
                unset ARGS<font color="#990000">(</font><font color="#009900">$long_match</font><font color="#990000">)</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$short</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$long</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> disp <font color="#FF0000">"-$short, --$long"</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$short</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> disp <font color="#990000">--</font><font color="#009900">$long</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> disp <font color="#990000">-</font><font color="#009900">$short</font>
        <font color="#FF0000">}</font>
        log DEV <font color="#FF0000">{</font>Registering app param <font color="#009900">$disp</font><font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> ARGS<font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)</font> <font color="#009900">$opts</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848361" ID="ID_1498726848361402" MODIFIED="1498726848361" TEXT="param separate" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848361" ID="ID_1498726848361938" MODIFIED="1498726848361" TEXT="Split file arguments based on the file type.&#xA;&#xA;The names of the variables to store the result in are passed in to the proc,&#xA;the procedure does not return any values.&#xA;&#xA;Example usage:&#xA;   param separate $argv params files directories&#xA;   process_parameters $params&#xA;   process_files $files&#xA;   process_directories $directories&#xA;&#xA;@param arguments       List of arguments passed to the script&#xA;@param app_var         The name of the variable to store application&#xA;                       arguments as (this will be all non-file arguments)&#xA;@param files_var       The variable name to store the file arguments as&#xA;@param dirs_var        The variable name to store the directory arguments as" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848362" ID="ID_1498726848362385" MODIFIED="1498726848362" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848374" ID="ID_1498726848374484" MODIFIED="1498726848374">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param separate <font color="#990000">--</font>params <font color="#FF0000">{</font> arguments <font color="#FF0000">{</font> app_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> files_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> dirs_var <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$app_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$app_var</font> application_args
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$files_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$files_var</font> files_to_process
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dirs_var</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">upvar</font></b> <font color="#993399">1</font> <font color="#009900">$dirs_var</font> dirs_to_process
        <font color="#FF0000">}</font>

        touch application_args files_to_process dirs_to_process

        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> application_args <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># If "." set file to current working directory</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font> <font color="#009900">$arg</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">.</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">file</font></b> <font color="#990000">[</font><b><font color="#0000FF">pwd</font></b><font color="#990000">]</font>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># Process based on file type</font></i>
                <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font><b><font color="#0000FF">file</font></b> type <font color="#009900">$arg</font><font color="#990000">]</font>
                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$file_type</font> <font color="#FF0000">{</font>
                fifo <font color="#990000">-</font>
                link <font color="#990000">-</font>
                characterSpecial <font color="#990000">-</font>
                blockSpecial <font color="#990000">-</font>
                <b><font color="#0000FF">socket</font></b> <font color="#990000">-</font>
                <b><font color="#0000FF">file</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> files_to_process <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                directory <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> dirs_to_process <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        log INFO <font color="#FF0000">{</font>Strangely enough the <b><font color="#0000FF">file</font></b> <font color="#009900">$arg</font> exists but is not a recognisable <b><font color="#0000FF">file</font></b> type <font color="#990000">(</font><font color="#009900">$file_type</font><font color="#990000">).</font> Omitting<font color="#990000">.</font> <font color="#990000">(</font><font color="#009900">$arg</font><font color="#990000">)</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848375" ID="ID_1498726848375121" MODIFIED="1498726848375" TEXT="param split" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848375" ID="ID_1498726848375615" MODIFIED="1498726848375" TEXT="Procedure to split parameters based on a given prefix.&#xA;&#xA;For example:&#xA;   --type d -i --file ex1.txt ex2.txt --num 1 2 3&#xA;&#xA;would be split up into a list on the form {param} {value...} {param} {value...} ...&#xA;&#xA;I.e.:&#xA;   {--type} {d} {-i} {} {--file} {ex1.txt ex2.txt} {--num} {1 2 3}&#xA;&#xA;@param arguments        List of arguments to split&#xA;@option -strip_prefix   Option to remove the prefix from the params in the returned list&#xA;@option --arg_prefix    Specifies the prefix used, defaults to hyphen (-)&#xA;@return                 A list on the form {param} {value...} {param} {value...} ..." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848375" ID="ID_1498726848375991" MODIFIED="1498726848375" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848386" ID="ID_1498726848386845" MODIFIED="1498726848386">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param <b><font color="#0000FF">split</font></b> <font color="#990000">--</font>params <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                strip_prefix <font color="#993399">0</font>
                arg_prefix   <font color="#990000">-</font>
                <b><font color="#0000FF">default</font></b>      <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font> arguments
        
        touch output 
        <b><font color="#0000FF">foreach</font></b> arg <font color="#990000">[</font>lreverse <font color="#009900">$arguments</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+</font> <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> value <font color="#009900">$arg</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists value<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>lreverse <font color="#009900">$value</font><font color="#990000">]</font>
                        unset value
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$default</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$strip_prefix</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$arg</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$arg</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>lreverse <font color="#009900">$output</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848387" ID="ID_1498726848387470" MODIFIED="1498726848387" TEXT="param update" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848387" ID="ID_1498726848387968" MODIFIED="1498726848387" TEXT="This allows for an application to update a registered parameter. A common&#xA;use case is to alter the description of what a given parameter does.&#xA;&#xA;For example the default description for the --dry-run option may not be&#xA;suitable for all applications.&#xA;&#xA;@param short_param         The short hand parameter, should be a single&#xA;                           character&#xA;@param long_param          The long version of the parameter&#xA;@param args                Additional options to specify what needs to&#xA;                           be changed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848388" ID="ID_1498726848388274" MODIFIED="1498726848388" TEXT="@see" FOLDED="true">
<node CREATED="1498726848388" ID="ID_1498726848388479" MODIFIED="1498726848388" TEXT="param register for a list of available options" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726848388" ID="ID_1498726848388801" MODIFIED="1498726848388" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848401" ID="ID_1498726848401784" MODIFIED="1498726848401">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param update <font color="#990000">--</font>params <font color="#FF0000">{</font> short_param long_param args <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>arg_utils<font color="#990000">::</font>ARGS
        
        <i><font color="#9A1900"># proc args</font></i>
        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                unlisted      <font color="#FF0000">{}</font>
                morearg       <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># split param if it happens to be one of these two forms:</font></i>
        <i><font color="#9A1900">#    --clappend &lt;cfg&gt;=val</font></i>
        <i><font color="#9A1900">#    --log-file=&lt;file&gt;</font></i>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$short_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$short</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$long_param</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$long</font> <font color="#FF0000">{</font><font color="#990000">=</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> short <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$short</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> long  <font color="#990000">[</font>arg_utils<font color="#990000">::</font>_clean_param <font color="#009900">$long</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists ARGS<font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"Parameter $short_param, $long_param is not registered"</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> opts <font color="#009900">$ARGS</font><font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)</font>
        
        <i><font color="#9A1900"># param configuration</font></i>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> value<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                comment       <font color="#990000">\</font><font color="#993399">0</font>
                max_args      <font color="#990000">\</font><font color="#993399">0</font>
                min_args      <font color="#990000">\</font><font color="#993399">0</font>
                priority      <font color="#990000">\</font><font color="#993399">0</font>
                disporder     <font color="#990000">\</font><font color="#993399">0</font>
                body          <font color="#990000">\</font><font color="#993399">0</font>
                validation    <font color="#990000">\</font><font color="#993399">0</font>
                config        <font color="#990000">\</font><font color="#993399">0</font>
                config_value  <font color="#990000">\</font><font color="#993399">0</font>
                log           <font color="#990000">\</font><font color="#993399">0</font>
                more_level    <font color="#990000">\</font><font color="#993399">0</font>
        <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> p_value <font color="#990000">[</font>param get <font color="#009900">$default</font> <font color="#009900">$value</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$p_value</font> <font color="#990000">!=</font> <font color="#990000">\</font><font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        dict <b><font color="#0000FF">set</font></b> opts <font color="#009900">$default</font> <font color="#009900">$p_value</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># This is in particular for the --help output to decide</font></i>
        <i><font color="#9A1900"># whether to display a parameter or not</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$unlisted</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#990000">-</font><font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$morearg</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                dict <b><font color="#0000FF">set</font></b> opts more_level <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        
        dict <b><font color="#0000FF">set</font></b> opts param_long  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$long_param</font>  <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
        dict <b><font color="#0000FF">set</font></b> opts param_short <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$short_param</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>

        log DEBUG <font color="#FF0000">{</font>Updating app param <font color="#990000">-</font><font color="#009900">$short</font><font color="#990000">,</font> <font color="#990000">--</font><font color="#009900">$long</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> ARGS<font color="#990000">(</font><font color="#009900">$short</font><font color="#990000">,</font><font color="#009900">$long</font><font color="#990000">)</font> <font color="#009900">$opts</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726848402" ID="ID_1498726848402455" MODIFIED="1498726848402" TEXT="param validate" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726848403" ID="ID_1498726848403044" MODIFIED="1498726848403" TEXT="Validate an argument value.&#xA;&#xA;Possible validation checks:&#xA;   - any &quot;string is&quot; class   e.g. &quot;integer&quot;, empty strings fail validation&#xA;   - file                    input must be a regular file&#xA;   - directory               input must be a directory (or not exist), the&#xA;                             application using the parameter is responsible&#xA;                             for creating the directory if it does not&#xA;                             already exist&#xA;   - directory_exists        input must be an existing directory&#xA;   - anyfile                 input file must exists (i.e. file or directory)&#xA;   - nofile                  input must not be a file&#xA;&#xA;List of &quot;string is&quot; classes supported:&#xA;   alnum, alpha, ascii, control, boolean, digit,&#xA;   double, entier, false, graph, integer, list,&#xA;   lower, print, punct, space, true, upper,&#xA;   wideinteger, wordchar, xdigit&#xA;&#xA;@param value               The argument value to validate&#xA;@param validation_check    The validation check to apply&#xA;@return                    1 if the value passed the validation check&#xA;                           and 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726848403" ID="ID_1498726848403411" MODIFIED="1498726848403" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726848416" ID="ID_1498726848416059" MODIFIED="1498726848416">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option param validate <font color="#990000">--</font>params <font color="#FF0000">{</font> value validation_check <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$validation_check</font> <font color="#FF0000">{</font>
        <font color="#FF0000">{}</font> <font color="#990000">-</font> 
        <font color="#FF0000">{</font>none<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>alpha<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>alnum<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>ascii<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>graph<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>upper<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>lower<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>print<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>wordchar<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>control<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>punct<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>space<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>bool<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>boolean<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>true<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>false<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>digit<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>int<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>integer<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>wideint<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>wideinteger<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>double<font color="#FF0000">}</font> <font color="#990000">-</font>
        <font color="#FF0000">{</font>xdigit<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> is <font color="#009900">$validation_check</font> <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> isfile <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>directory<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># If the directory does not exist then it should be fine, the</font></i>
                <i><font color="#9A1900"># application using the argument is responsible for creating</font></i>
                <i><font color="#9A1900"># the directory if it doesn't exist.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$value</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>directory_exists<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> isdirectory <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>anyfile<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$value</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">{</font>nofile<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$value</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Unknown validation check <font color="#009900">$validation_check</font> <b><font color="#0000FF">for</font></b> input <font color="#FF0000">'$value'</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726846607" ID="ID_1498726846607238" MODIFIED="1498726846607" TEXT="tapp 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726846607" ID="ID_1498726846607528" MODIFIED="1498726846607" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846607" ID="ID_1498726846607719" MODIFIED="1498726846607" TEXT="opt 1.0" LINK="#ID_1498726846862651"/>
<node CREATED="1498726846607" ID="ID_1498726846607847" MODIFIED="1498726846607" TEXT="tapp_lib 1.0" LINK="#ID_1498726846450696"/>
</node>
<node CREATED="1498726846608" ID="ID_1498726846608081" MODIFIED="1498726846608" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846608" ID="ID_1498726846608859" MODIFIED="1498726846608" TEXT="tapp.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/tapp.tcl"/>
</node>
<node CREATED="1498726846609" ID="ID_1498726846609075" MODIFIED="1498726846609" TEXT="tapp" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846609" ID="ID_1498726846609864" MODIFIED="1498726846609" TEXT="This is a wrapper package that is responsible for importing the various&#xA;tapp_lib packages into the global namespace.&#xA;&#xA;The package also tries to determine the console width by executing the core&#xA;linux utility &quot;tty&quot; if it is present. If successfully determined then the&#xA;configuration item CONSOLE_WIDTH is updated to reflect this.&#xA;&#xA;This package also provides a rather powerful trace logging functionality.&#xA;If the parameter --trace is passed in to the application then all procs&#xA;created after package tapp has been loaded will be replaced by a wrapper&#xA;function that logs out the parameters to the proc being called and what the&#xA;returned output is. Be warned that this will produce massive amounts of log&#xA;output and be very costly performance wise." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846610" ID="ID_1498726846610158" MODIFIED="1498726846610" TEXT="@see" FOLDED="true">
<node CREATED="1498726846610" ID="ID_1498726846610351" MODIFIED="1498726846610" TEXT="cfg_utils" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846610" ID="ID_1498726846610615" MODIFIED="1498726846610" TEXT="tapp_log" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846610" ID="ID_1498726846610879" MODIFIED="1498726846610" TEXT="file_utils" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846611" ID="ID_1498726846611141" MODIFIED="1498726846611" TEXT="text_utils" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846611" ID="ID_1498726846611431" MODIFIED="1498726846611" TEXT="tapp_lang" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846611" ID="ID_1498726846611707" MODIFIED="1498726846611" TEXT="arg_utils" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846611" ID="ID_1498726846611975" MODIFIED="1498726846611" TEXT="opt" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
</node>
<node CREATED="1498726846719" ID="ID_1498726846719544" MODIFIED="1498726846719" TEXT="resume 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726846719" ID="ID_1498726846719848" MODIFIED="1498726846719" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846720" ID="ID_1498726846720740" MODIFIED="1498726846720" TEXT="resume.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/resume.tcl"/>
</node>
<node CREATED="1498726846721" ID="ID_1498726846721661" MODIFIED="1498726846721" TEXT="resume" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846722" ID="ID_1498726846722539" MODIFIED="1498726846722" TEXT="This package is intended for applications processing a lot of files and adds&#xA;the possibility of keeping track of what files are being processed and which&#xA;are still outstanding.&#xA;&#xA;This allows for the application to continue processing the remainign files if&#xA;the process was abrupted half-way for whatever reason.&#xA;&#xA;This package relies on the array store utility for keeping track of remaining&#xA;files to process.&#xA;&#xA;Warning: Depending on the application's integration with the resume package&#xA;the ‑‑resume parameter has to be passed in to the application in order to&#xA;continue processing where the application left off if it was abrupted.&#xA;Note that this typically this has to be done already on the next run,&#xA;otherwise the risk is that the resume list will be cleared.&#xA;&#xA;Warning: The list of remaining files to process will be written to disk for&#xA;each file being processed which may generate a high I/O." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846722" ID="ID_1498726846722864" MODIFIED="1498726846722" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726846723" ID="ID_1498726846723081" MODIFIED="1498726846723" TEXT="RESUME_TEMP_DIR" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846723" ID="ID_1498726846723372" MODIFIED="1498726846723" TEXT="The temporary directory to store the list of remaining files to process" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846723" ID="ID_1498726846723660" MODIFIED="1498726846723" TEXT="RESUME_FILE_PROCESSING" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846723" ID="ID_1498726846723953" MODIFIED="1498726846723" TEXT="Indicates whether to resume processing files after the process has been abrupted, this is generally enabled by passing ‑‑resume or ‑‑continue parameter to the application" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726846724" ID="ID_1498726846724414" MODIFIED="1498726846724" TEXT="resume::add_files" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846724" ID="ID_1498726846724744" MODIFIED="1498726846724" TEXT="Add files to the list of files to process.&#xA;&#xA;@param files           The files to add to the resume list" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846725" ID="ID_1498726846725061" MODIFIED="1498726846725" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846737" ID="ID_1498726846737373" MODIFIED="1498726846737">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>add_files <font color="#FF0000">{</font> files <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$files</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> file_index <font color="#990000">[</font>resume get FILE_INDEX<font color="#990000">]</font>

        <b><font color="#0000FF">foreach</font></b> <b><font color="#0000FF">file</font></b> <font color="#009900">$files</font> <font color="#FF0000">{</font>
                resume <b><font color="#0000FF">set</font></b> <font color="#009900">$file_index</font> <font color="#009900">$file</font>
                <b><font color="#0000FF">incr</font></b> file_index
        <font color="#FF0000">}</font>

        resume <b><font color="#0000FF">set</font></b> FILE_INDEX <font color="#009900">$file_index</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846738" ID="ID_1498726846738218" MODIFIED="1498726846738" TEXT="resume::commit" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846738" ID="ID_1498726846738768" MODIFIED="1498726846738" TEXT="Commit or save any changes made to the resume data." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846739" ID="ID_1498726846739168" MODIFIED="1498726846739" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846749" ID="ID_1498726846749106" MODIFIED="1498726846749">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>commit <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        resume save
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846749" ID="ID_1498726846749709" MODIFIED="1498726846749" TEXT="resume::finish" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846750" ID="ID_1498726846750147" MODIFIED="1498726846750" TEXT="Mark the job as finished. This will delete the resume list and associated&#xA;data." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846750" ID="ID_1498726846750504" MODIFIED="1498726846750" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846759" ID="ID_1498726846759907" MODIFIED="1498726846759">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>finish <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        resume delete
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846760" ID="ID_1498726846760486" MODIFIED="1498726846760" TEXT="resume::get_data" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846760" ID="ID_1498726846760890" MODIFIED="1498726846760" TEXT="Retrieve custom data from the resume package.&#xA;&#xA;@param key            The name of the value to look up&#xA;@return               The value associated with the given key, or empty&#xA;                      string if the key does not exist" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846761" ID="ID_1498726846761246" MODIFIED="1498726846761" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846770" ID="ID_1498726846771007" MODIFIED="1498726846771">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>get_data <font color="#FF0000">{</font> key <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>resume get <font color="#009900">$key</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846771" ID="ID_1498726846771719" MODIFIED="1498726846771" TEXT="resume::has_more" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846772" ID="ID_1498726846772118" MODIFIED="1498726846772" TEXT="Check whether there are more files to process.&#xA;&#xA;@return                1 if there are more files to process, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846772" ID="ID_1498726846772465" MODIFIED="1498726846772" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846781" ID="ID_1498726846781798" MODIFIED="1498726846781">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>has_more <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>resume exists <font color="#990000">[</font>resume get CURRENT<font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846782" ID="ID_1498726846782451" MODIFIED="1498726846782" TEXT="resume::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846782" ID="ID_1498726846782830" MODIFIED="1498726846782" TEXT="Initialises the resume package." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846783" ID="ID_1498726846783171" MODIFIED="1498726846783" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846793" ID="ID_1498726846793877" MODIFIED="1498726846793">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>init <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">variable</font></b> PREVIOUS

        clear PREVIOUS

        <i><font color="#9A1900"># Get the location where we can store the temporary file(s)</font></i>
        <i><font color="#9A1900"># to keep track of files left to process.</font></i>
        <b><font color="#0000FF">set</font></b> temp_dir <font color="#990000">[</font>cfg get RESUME_TEMP_DIR $<font color="#990000">::</font>env<font color="#990000">(</font>HOME<font color="#990000">)]</font>
        <b><font color="#0000FF">set</font></b> temp_file <font color="#990000">.[</font>this_script<font color="#990000">]</font>_resume<font color="#990000">~</font>
        cfg <b><font color="#0000FF">set</font></b> ARRAY_STORE_LOCATION <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$temp_dir</font> <font color="#009900">$temp_file</font><font color="#990000">]</font>
        resume <b><font color="#0000FF">load</font></b>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg disabled RESUME_FILE_PROCESSING<font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font>cfg enabled RESET_ARRAY_STORE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                resume reset
                resume save
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>resume exists CURRENT<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                resume <b><font color="#0000FF">set</font></b> CURRENT <font color="#993399">0</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>resume exists FILE_INDEX<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                resume <b><font color="#0000FF">set</font></b> FILE_INDEX <font color="#993399">0</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>

cfg <b><font color="#0000FF">lappend</font></b> MOREARGS         <font color="#FF0000">{</font><font color="#990000">--</font>resume<font color="#FF0000">}</font>              <font color="#FF0000">{</font>resume <b><font color="#0000FF">file</font></b> operation <b><font color="#0000FF">if</font></b> process was abrupted <font color="#990000">(</font>provided the script has not been run since<font color="#990000">)</font><font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846794" ID="ID_1498726846794543" MODIFIED="1498726846794" TEXT="resume::next" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846795" ID="ID_1498726846795052" MODIFIED="1498726846795" TEXT="Get the next file to process.&#xA;&#xA;@return                The next file to process, or empty string if there are&#xA;                       no more files to process" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846795" ID="ID_1498726846795400" MODIFIED="1498726846795" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846805" ID="ID_1498726846805202" MODIFIED="1498726846805">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>next <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PREVIOUS
        
        resume remove <font color="#009900">$PREVIOUS</font>
        resume save

        <b><font color="#0000FF">set</font></b> index <font color="#990000">[</font>resume get CURRENT<font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> current_file <font color="#990000">[</font>resume get <font color="#009900">$index</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> PREVIOUS <font color="#009900">$index</font>
        resume <b><font color="#0000FF">set</font></b> CURRENT <font color="#990000">[</font><b><font color="#0000FF">incr</font></b> index<font color="#990000">]</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$current_file</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846805" ID="ID_1498726846805914" MODIFIED="1498726846805" TEXT="resume::parse_args" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846806" ID="ID_1498726846806480" MODIFIED="1498726846806" TEXT="Parse resume package specific arguments.&#xA;&#xA;The package specifically looks for the ‑‑resume and ‑‑continue parameters.&#xA;&#xA;@param arguments       The application parameters to parse&#xA;@return                The remaining arguments after parsing is complete&#xA;                       (i.e. any arguments determined to not be relevant to&#xA;                       the resume package)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846806" ID="ID_1498726846806856" MODIFIED="1498726846806" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846822" ID="ID_1498726846822230" MODIFIED="1498726846822">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>parse_args <font color="#FF0000">{</font> arguments <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        touch remaining_args

        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$arguments</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#990000">--</font> <font color="#009900">$arg</font> <font color="#FF0000">{</font>
                <font color="#FF0000">{</font><font color="#990000">--</font>resume<font color="#FF0000">}</font> <font color="#990000">-</font>
                <font color="#FF0000">{</font><font color="#990000">--</font><b><font color="#0000FF">continue</font></b><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        cfg <b><font color="#0000FF">set</font></b> RESUME_FILE_PROCESSING <font color="#993399">1</font>
                        log DEBUG <font color="#FF0000">{</font>Enabled resuming of <b><font color="#0000FF">file</font></b> processing<font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> remaining_args <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$remaining_args</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846822" ID="ID_1498726846822900" MODIFIED="1498726846822" TEXT="resume::set_data" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846823" ID="ID_1498726846823385" MODIFIED="1498726846823" TEXT="Add custom data as key value pairs to the resume package for later retrieval.&#xA;&#xA;This typically represents internal settings or array data that is required if&#xA;the application is to resume processing from a given point.&#xA;&#xA;@param key            The name of the value being set&#xA;@param value          The data to set the associated key to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846823" ID="ID_1498726846823774" MODIFIED="1498726846823" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846833" ID="ID_1498726846833766" MODIFIED="1498726846833">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> resume<font color="#990000">::</font>set_data <font color="#FF0000">{</font> key value <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>resume <b><font color="#0000FF">set</font></b> <font color="#009900">$key</font> <font color="#009900">$value</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726846862" ID="ID_1498726846862651" MODIFIED="1498726846862" TEXT="opt 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726846862" ID="ID_1498726846862948" MODIFIED="1498726846862" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846863" ID="ID_1498726846863773" MODIFIED="1498726846863" TEXT="opt.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/opt.tcl"/>
</node>
<node CREATED="1498726846863" ID="ID_1498726846863987" MODIFIED="1498726846863" TEXT="opt" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846864" ID="ID_1498726846864295" MODIFIED="1498726846864" TEXT="Create a proc that provides a variety of functions based on second class options." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846865" ID="ID_1498726846865009" MODIFIED="1498726846865" TEXT="opt::destroy" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846865" ID="ID_1498726846865338" MODIFIED="1498726846865" TEXT="If the generated proc is only for temporary use then it can be destroyed.&#xA;&#xA;Note that this proc will not unset the proc's namespace variable and that&#xA;this will need to be dealt with separately in order to prevent memory leaks.&#xA;&#xA;@param name            The name of the proc to remove" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846865" ID="ID_1498726846865653" MODIFIED="1498726846865" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846875" ID="ID_1498726846875796" MODIFIED="1498726846875">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><font color="#990000">::</font><b><font color="#0000FF">proc</font></b> opt<font color="#990000">::</font>destroy <font color="#FF0000">{</font>name<font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PROCS

        <b><font color="#0000FF">set</font></b> pname <font color="#990000">[</font>_get_process_name <font color="#009900">$name</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> commands <font color="#009900">$pname</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#009900">$pname</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">rename</font></b> <font color="#009900">$pname</font> <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> commands <font color="#009900">$name</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">"::$name"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">rename</font></b> <font color="#009900">$name</font> <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        
        array unset PROCS <font color="#009900">$pname</font><font color="#990000">,*</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846876" ID="ID_1498726846876408" MODIFIED="1498726846876" TEXT="opt::explain" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846876" ID="ID_1498726846876822" MODIFIED="1498726846876" TEXT="Returns a list of the proc options including the parameters and the body.&#xA;Intended for debug situations.&#xA;&#xA;@param name            The name of the proc to list options for&#xA;@return                The proc name along with the option parameter and body" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846877" ID="ID_1498726846877176" MODIFIED="1498726846877" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846887" ID="ID_1498726846887034" MODIFIED="1498726846887">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><font color="#990000">::</font><b><font color="#0000FF">proc</font></b> opt<font color="#990000">::</font>explain <font color="#FF0000">{</font>name<font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PROCS
        
        <b><font color="#0000FF">set</font></b> pname <font color="#990000">[</font>_get_process_name <font color="#009900">$name</font><font color="#990000">]</font>
        
        <b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> option <font color="#990000">[</font>_options <font color="#009900">$pname</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#FF0000">"</font><font color="#CC33CC">\t</font><font color="#FF0000">$pname $option $PROCS($pname,$option)"</font>
        
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$output</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846887" ID="ID_1498726846887640" MODIFIED="1498726846887" TEXT="opt::get_namespace_var" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846888" ID="ID_1498726846888101" MODIFIED="1498726846888" TEXT="For a given namespace proc returns the name of a namespace variable with the&#xA;same name (in uppercase).&#xA;&#xA;For example:&#xA;   opt::get_namespace_var {::ns::etc::myproc}&#xA;&#xA;will return:&#xA;   ::ns::etc::MYPROC&#xA;&#xA;@param pname           The proc name (including namespace)&#xA;@return                The equivalent namespace variable (in upper case)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846888" ID="ID_1498726846888458" MODIFIED="1498726846888" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846898" ID="ID_1498726846898343" MODIFIED="1498726846898">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><font color="#990000">::</font><b><font color="#0000FF">proc</font></b> opt<font color="#990000">::</font>get_namespace_var <font color="#FF0000">{</font> pname <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> plist <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$pname</font> <font color="#FF0000">{</font><font color="#990000">:</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> plist <font color="#990000">[</font><b><font color="#0000FF">lreplace</font></b> <font color="#009900">$plist</font> end end <font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$plist</font> end<font color="#990000">]]]</font>
        <b><font color="#0000FF">set</font></b> vname <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$plist</font> <font color="#FF0000">{</font><font color="#990000">:</font><font color="#FF0000">}</font><font color="#990000">]</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$vname</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846898" ID="ID_1498726846898943" MODIFIED="1498726846898" TEXT="opt::option" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846899" ID="ID_1498726846899632" MODIFIED="1498726846899" TEXT="Adds options to the given proc.&#xA;&#xA;Synopsis:&#xA;   opt::option &lt;proc&gt; &lt;option&gt; &lt;cmd&gt; ?&lt;option&gt; &lt;cmd&gt;?&#xA;&#xA;Example:&#xA;   set pname [opt::proc good]&#xA;   opt::option $pname morning [list puts {hello there}]&#xA;   good morning&#xA;&#xA;The above will create a proc good and specify an option &quot;morning&quot;. Passing&#xA;the &quot;morning&quot; option as an argument when calling the command &quot;good&quot; will&#xA;result in &quot;hello there&quot; to be printed to standard out. Alternatively the&#xA;proc parameter names and body can be passed in to define anonymous procs&#xA;(or functions) evaluated through the &quot;apply&quot; command.&#xA;&#xA;Take special notes regarding the use of a Tcl body:&#xA;   - this will be arbitrary Tcl code that will run one level&#xA;     below the calling code&#xA;   - if namespace variables are used then they need to be&#xA;     addressed in full, i.e. with the namespace prefix&#xA;&#xA;@param pname             The proc name to add the option(s) for&#xA;@param option            The name of the option to add, e.g. &quot;get&quot;&#xA;@option --proc cmd       The proc to execute when the option is passed in,&#xA;                         can be a list representing command followed by&#xA;                         default arguments (like passing in the name of the&#xA;                         namespace variable to use)&#xA;@option --params &lt;list&gt;  Anonymous proc parameters, defaults to &quot;args&quot;&#xA;@option --body &lt;code&gt;    Anonymous Tcl body to execute when option is called" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846899" ID="ID_1498726846899979" MODIFIED="1498726846899" TEXT="@see" FOLDED="true">
<node CREATED="1498726846900" ID="ID_1498726846900190" MODIFIED="1498726846900" TEXT="code for proc opt_list::preset for example usage" COLOR="#14666b" LINK="#ID_1498726846836208">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846900" ID="ID_1498726846900475" MODIFIED="1498726846900" TEXT="opt::get_namespace_var in order to get hold of the proc's namespace variable" COLOR="#14666b" LINK="#ID_1498726846887640">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846900" ID="ID_1498726846900751" MODIFIED="1498726846900" TEXT="apply" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846901" ID="ID_1498726846901070" MODIFIED="1498726846901" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846911" ID="ID_1498726846911910" MODIFIED="1498726846911">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><font color="#990000">::</font><b><font color="#0000FF">proc</font></b> opt<font color="#990000">::</font>option <font color="#FF0000">{</font>name option args<font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> PROCS
        
        <b><font color="#0000FF">set</font></b> pname <font color="#990000">[</font>_get_process_name <font color="#009900">$name</font><font color="#990000">]</font>

        <b><font color="#0000FF">foreach</font></b> param <font color="#FF0000">{</font><b><font color="#0000FF">proc</font></b> params body<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <font color="#009900">$param</font> <font color="#FF0000">{}</font>
                <b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$args</font> <font color="#990000">--</font><font color="#009900">$param</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$idx</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <font color="#009900">$param</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#009900">$idx</font><font color="#990000">+</font><font color="#993399">1</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$proc</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> PROCS<font color="#990000">(</font><font color="#009900">$pname</font><font color="#990000">,</font><font color="#009900">$option</font><font color="#990000">)</font> <font color="#FF0000">"args </font><font color="#CC33CC">\{\n\t\t</font><font color="#FF0000">uplevel 1 $proc </font><font color="#CC33CC">\$</font><font color="#FF0000">args</font><font color="#CC33CC">\n\t\}</font><font color="#FF0000">"</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> PROCS<font color="#990000">(</font><font color="#009900">$pname</font><font color="#990000">,</font><font color="#009900">$option</font><font color="#990000">)</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$params</font> <font color="#009900">$body</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846912" ID="ID_1498726846912513" MODIFIED="1498726846912" TEXT="opt::proc" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846913" ID="ID_1498726846913182" MODIFIED="1498726846913" TEXT="Create a single proc that can perform many actions depending on the options&#xA;provided.&#xA;&#xA;This is similar to the struct:: packages part of tcllib with the main&#xA;difference being that you can add additional options / procs to call.&#xA;&#xA;Default proc options:&#xA;    &lt;proc&gt; options                Returns a list of the available options for&#xA;                                  the proc&#xA;    &lt;proc&gt; destroy                Deletes the generated proc / command. Note&#xA;                                  that this will not unset any namespace&#xA;                                  variables used. If this is used in a&#xA;                                  context where procs are dynamically created&#xA;                                  and destroyed / deleted then it is&#xA;                                  recommended that the &quot;destroy&quot; option is&#xA;                                  overridden by a proc that takes care of&#xA;                                  any cleanup necessary.&#xA;&#xA;Synopsis:&#xA;   opt::proc &lt;proc&gt; ?options? ?options?&#xA;&#xA;@param name              The name of the new proc. The current namespace will&#xA;                         be used if not specified.&#xA;@option -preset &lt;name&gt;   Specifies which options that should be available when&#xA;                         the proc is created. Available presets are: list and&#xA;                         array&#xA;@return                  The name of the new proc including namespace" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846913" ID="ID_1498726846913514" MODIFIED="1498726846913" TEXT="@see" FOLDED="true">
<node CREATED="1498726846913" ID="ID_1498726846913718" MODIFIED="1498726846913" TEXT="opt::option for how to add custom options to the created proc" COLOR="#14666b" LINK="#ID_1498726846898943">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846914" ID="ID_1498726846914025" MODIFIED="1498726846914" TEXT="opt_array::preset" COLOR="#14666b" LINK="#ID_1498726846850702">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846914" ID="ID_1498726846914302" MODIFIED="1498726846914" TEXT="opt_list::preset" COLOR="#14666b" LINK="#ID_1498726846836208">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846914" ID="ID_1498726846914582" MODIFIED="1498726846914" TEXT="http://tcllib.sourceforge.net/doc/" COLOR="#14666b" LINK="http://tcllib.sourceforge.net/doc/">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846914" ID="ID_1498726846914898" MODIFIED="1498726846914" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846924" ID="ID_1498726846924885" MODIFIED="1498726846924">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> opt<font color="#990000">::</font><b><font color="#0000FF">proc</font></b> <font color="#FF0000">{</font>name args<font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">variable</font></b> PROCS
        
        <b><font color="#0000FF">set</font></b> pname <font color="#990000">[</font>_get_process_name <font color="#009900">$name</font><font color="#990000">]</font>
        
        <font color="#990000">::</font><b><font color="#0000FF">proc</font></b> <font color="#009900">$pname</font> <font color="#FF0000">{</font>option args<font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">\[</font>opt<font color="#990000">::</font>_parse <font color="#009900">$pname</font> <font color="#990000">\</font><font color="#009900">$option</font> <font color="#990000">\</font><font color="#FF0000">{</font><font color="#990000">*\</font><font color="#FF0000">}</font><font color="#990000">\</font><font color="#009900">$args</font><font color="#990000">\]</font>
        <font color="#FF0000">}</font><font color="#990000">]</font>
        
        _setup_opts <font color="#009900">$pname</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$pname</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726846925" ID="ID_1498726846925601" MODIFIED="1498726846925" TEXT="notifications 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726846925" ID="ID_1498726846925917" MODIFIED="1498726846925" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846926" ID="ID_1498726846926188" MODIFIED="1498726846926" TEXT="tapp_lib 1.0" LINK="#ID_1498726846450696"/>
</node>
<node CREATED="1498726846926" ID="ID_1498726846926437" MODIFIED="1498726846926" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846927" ID="ID_1498726846927301" MODIFIED="1498726846927" TEXT="notifications.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/notifications.tcl"/>
</node>
<node CREATED="1498726846927" ID="ID_1498726846927514" MODIFIED="1498726846927" TEXT="notifications" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846928" ID="ID_1498726846928509" MODIFIED="1498726846928" TEXT="This package is intended as a wrapper for common message logic, such as for&#xA;example desktop notifications.&#xA;&#xA;If dbus-tcl is installed then that will be the preferred method for sending&#xA;notifications, but will need to be enabled via configuration. If it is not&#xA;installed then the application &quot;notify-send&quot; will be used instead.&#xA;&#xA;For Windows Growl integration has been done, depends on growlnotify.exe&#xA;(command line script).&#xA;&#xA;Mac notifications has not yet been implemented, but contributions are most&#xA;welcome." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846928" ID="ID_1498726846928796" MODIFIED="1498726846928" TEXT="@cfg" FOLDED="true">
<node CREATED="1498726846929" ID="ID_1498726846929008" MODIFIED="1498726846929" TEXT="DBUS_NOTIFICATION" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846929" ID="ID_1498726846929294" MODIFIED="1498726846929" TEXT="Enable dbus notification, defaults to disabled" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846929" ID="ID_1498726846929578" MODIFIED="1498726846929" TEXT="ENV_DISPLAY_VAL" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846929" ID="ID_1498726846929863" MODIFIED="1498726846929" TEXT="On unix systems this config sets the DISPLAY environment variable if not already set (it does not override it). The main purpose of this is to enable desktop notifications for non-session processes such as cron jobs for example. Defaults to &quot;:0&quot; (display 0, i.e. main display)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846930" ID="ID_1498726846930159" MODIFIED="1498726846930" TEXT="NOTIFY_LENGTH_LIMIT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1498726846930" ID="ID_1498726846930432" MODIFIED="1498726846930" TEXT="Very long messages can cause various issues with the front end, such as display freezing / hanging and/or performance issues. Because of this a default limit of 2000 characters has been set on the message payload, but this can be overridden by changing this configuration item." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1498726846930" ID="ID_1498726846930691" MODIFIED="1498726846930" TEXT="@see" FOLDED="true">
<node CREATED="1498726846930" ID="ID_1498726846930879" MODIFIED="1498726846930" TEXT="http://dbus-tcl.sourceforge.net/" COLOR="#14666b" LINK="http://dbus-tcl.sourceforge.net/">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846931" ID="ID_1498726846931148" MODIFIED="1498726846931" TEXT="http://ss64.com/bash/notify-send.html" COLOR="#14666b" LINK="http://ss64.com/bash/notify-send.html">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846931" ID="ID_1498726846931408" MODIFIED="1498726846931" TEXT="http://www.growlforwindows.com/gfw/help/growlnotify.aspx" COLOR="#14666b" LINK="http://www.growlforwindows.com/gfw/help/growlnotify.aspx">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846932" ID="ID_1498726846932118" MODIFIED="1498726846932" TEXT="notifications::notify" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846932" ID="ID_1498726846932536" MODIFIED="1498726846932" TEXT="Send desktop notification message.&#xA;&#xA;@param icon            The icon representing the alert or the application&#xA;                       sending the notification&#xA;@param subject         Short description for the title of the notification&#xA;@param body            The notification message content, the following formats&#xA;                       should be supported:&#xA;                       &lt;b&gt;...&lt;/b&gt;                (bold),&#xA;                       &lt;i&gt;...&lt;/i&gt;                (italic),&#xA;                       &lt;u&gt;...&lt;/u&gt;                (underline),&#xA;                       &lt;a href=&quot;...&quot;&gt;...&lt;/a&gt;     (hyperlink),&#xA;                       &lt;img src=&quot;...&quot; alt=&quot;...&quot;/&gt;  (image)&#xA;                       &#xA;@param application     The application sending the message" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846932" ID="ID_1498726846932873" MODIFIED="1498726846932" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846943" ID="ID_1498726846943826" MODIFIED="1498726846943">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> notifications<font color="#990000">::</font>notify <font color="#FF0000">{</font> icon subject body application <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        _env_setup
        
        <b><font color="#0000FF">set</font></b> max_length <font color="#990000">[</font>cfg get NOTIFY_LENGTH_LIMIT <font color="#993399">2000</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> length <font color="#009900">$body</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#009900">$max_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> body <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$body</font> <font color="#993399">0</font> <font color="#009900">$max_length</font><font color="#990000">]...</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DBUS_NOTIFICATION <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">package</font></b> require dbus<font color="#990000">-</font>tcl <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                _notify_dbus <font color="#009900">$icon</font> <font color="#009900">$subject</font> <font color="#009900">$body</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>auto_execok <font color="#FF0000">{</font>notify<font color="#990000">-</font>send<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                _notify_send <font color="#009900">$icon</font> <font color="#009900">$subject</font> <font color="#009900">$body</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>auto_execok <font color="#FF0000">{</font>growlnotify<font color="#990000">.</font>exe<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                _notify_growl <font color="#009900">$icon</font> <font color="#009900">$subject</font> <font color="#009900">$body</font> <font color="#009900">$application</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                log DEBUG <font color="#FF0000">{</font>Notifications not enabled<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726846944" ID="ID_1498726846944521" MODIFIED="1498726846944" TEXT="net_utils 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726846944" ID="ID_1498726846944814" MODIFIED="1498726846944" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846945" ID="ID_1498726846945639" MODIFIED="1498726846945" TEXT="net_utils.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/net_utils.tcl"/>
</node>
<node CREATED="1498726846945" ID="ID_1498726846945852" MODIFIED="1498726846945" TEXT="net_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726846946" ID="ID_1498726846946289" MODIFIED="1498726846946" TEXT="This is pretty much fetched directly from http://wiki.tcl.tk/14144." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726846946" ID="ID_1498726846946563" MODIFIED="1498726846946" TEXT="@see" FOLDED="true">
<node CREATED="1498726846946" ID="ID_1498726846946758" MODIFIED="1498726846946" TEXT="http://wiki.tcl.tk/14144" COLOR="#14666b" LINK="http://wiki.tcl.tk/14144">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726846947" ID="ID_1498726846947178" MODIFIED="1498726846947" TEXT="net_utils::urldecode" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846947" ID="ID_1498726846947469" MODIFIED="1498726846947" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846957" ID="ID_1498726846957609" MODIFIED="1498726846957">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> net_utils<font color="#990000">::</font>urldecode <font color="#FF0000">{</font><b><font color="#0000FF">string</font></b><font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <i><font color="#9A1900"># rewrite "+" back to space</font></i>
        <i><font color="#9A1900"># protect \ from quoting another '\'</font></i>
        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">string</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">+</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#FF0000">"</font><font color="#CC33CC">\\</font><font color="#FF0000">"</font> <font color="#FF0000">"</font><font color="#CC33CC">\\\\</font><font color="#FF0000">"</font><font color="#990000">]</font> <font color="#009900">$string</font><font color="#990000">]</font>

        <i><font color="#9A1900"># prepare to process all %-escapes</font></i>
        <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#990000">--</font> <font color="#FF0000">{</font><font color="#990000">%([</font>A<font color="#990000">-</font>Fa<font color="#990000">-</font>f0<font color="#990000">-</font><font color="#993399">9</font><font color="#990000">][</font>A<font color="#990000">-</font>Fa<font color="#990000">-</font>f0<font color="#990000">-</font><font color="#993399">9</font><font color="#990000">])</font><font color="#FF0000">}</font> <font color="#009900">$string</font> <font color="#FF0000">{</font><font color="#990000">\\</font>u00<font color="#990000">\</font><font color="#993399">1</font><font color="#FF0000">}</font> <b><font color="#0000FF">string</font></b>

        <i><font color="#9A1900"># process \u unicode mapped chars</font></i>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#990000">-</font>novar <font color="#990000">-</font>nocommand <font color="#009900">$string</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726846958" ID="ID_1498726846958271" MODIFIED="1498726846958" TEXT="net_utils::urlencode" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726846958" ID="ID_1498726846958638" MODIFIED="1498726846958" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726846968" ID="ID_1498726846968874" MODIFIED="1498726846968">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> net_utils<font color="#990000">::</font>urlencode <font color="#FF0000">{</font><b><font color="#0000FF">string</font></b><font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">variable</font></b> map
        <b><font color="#0000FF">variable</font></b> alphanumeric

        <i><font color="#9A1900"># The spec says: "non-alphanumeric characters are replaced by '%HH'"</font></i>
        <i><font color="#9A1900"># 1 leave alphanumerics characters alone</font></i>
        <i><font color="#9A1900"># 2 Convert every other character to an array lookup</font></i>
        <i><font color="#9A1900"># 3 Escape constructs that are "special" to the tcl parser</font></i>
        <i><font color="#9A1900"># 4 "subst" the result, doing all the array substitutions</font></i>

        <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#990000">\[^</font><font color="#009900">$alphanumeric</font><font color="#990000">\]</font> <font color="#009900">$string</font> <font color="#FF0000">{</font><font color="#009900">$map</font><font color="#990000">(&amp;)</font><font color="#FF0000">}</font> <b><font color="#0000FF">string</font></b>
        <i><font color="#9A1900"># This quotes cases like $map([) or $map($) =&gt; $map(\[) ...</font></i>
        <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#FF0000">{</font><font color="#990000">[][</font><font color="#FF0000">{}</font><font color="#990000">)\\]\)</font><font color="#FF0000">}</font> <font color="#009900">$string</font> <font color="#FF0000">{</font><font color="#990000">\\&amp;</font><font color="#FF0000">}</font> <b><font color="#0000FF">string</font></b>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#990000">-</font>nocommand <font color="#009900">$string</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726847660" ID="ID_1498726847660612" MODIFIED="1498726847660" TEXT="exec_utils 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726847660" ID="ID_1498726847660921" MODIFIED="1498726847660" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847661" ID="ID_1498726847661116" MODIFIED="1498726847661" TEXT="tapp 1.0" LINK="#ID_1498726846607238"/>
</node>
<node CREATED="1498726847661" ID="ID_1498726847661363" MODIFIED="1498726847661" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847662" ID="ID_1498726847662272" MODIFIED="1498726847662" TEXT="exec_utils.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/exec_utils.tcl"/>
</node>
<node CREATED="1498726847662" ID="ID_1498726847662503" MODIFIED="1498726847662" TEXT="exec_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726847663" ID="ID_1498726847663207" MODIFIED="1498726847663" TEXT="This execution package is intended as a wrapper around the internal Tcl&#xA;exec proc addressing some common tasks:&#xA;&#xA;  - Avoid execution pretending that the command executed fine if dry run is&#xA;    enabled&#xA;  - Avoid square brackes in the execution string from being interpreted as&#xA;    Tcl commands to be evaluated&#xA;  - Checking if the command being executed exists (verified by auto_execok)&#xA;  - Unified logging" COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847663" ID="ID_1498726847663554" MODIFIED="1498726847663" TEXT="@see" FOLDED="true">
<node CREATED="1498726847663" ID="ID_1498726847663772" MODIFIED="1498726847663" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/exec.htm" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/exec.htm">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847664" ID="ID_1498726847664054" MODIFIED="1498726847664" TEXT="http://www.tcl.tk/man/tcl8.5/TclCmd/library.htm" COLOR="#14666b" LINK="http://www.tcl.tk/man/tcl8.5/TclCmd/library.htm">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847664" ID="ID_1498726847664502" MODIFIED="1498726847664" TEXT="exec_utils::execute" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847665" ID="ID_1498726847665062" MODIFIED="1498726847665" TEXT="Attempts to execute a given command.&#xA;&#xA;If an error occurs during the execution then this will be raised with the&#xA;caller unless the -nocomplain flag is passed in.&#xA;&#xA;If the --cmd is not passed in then any remaining arguments will be treated&#xA;as the command to execute.&#xA;&#xA;Synopsis:&#xA;   exec_utils::execute ?-option ...? command&#xA;&#xA;@param command            The command to execute&#xA;@option -ignore_dry_run   Option to ignore the dry-run flag of the&#xA;                          application and execute the command anyway.&#xA;@option -nocomplain       Don't complain on errors &#xA;@option -nolog            Option to not log the output of the execution&#xA;@option --log_level       Option to adjust the general log level used by this&#xA;                          proc, defaults to INFO&#xA;@option --default         The default value this proc should return when an&#xA;                          error occurs executing the command and --nocomplain&#xA;                          is used, defaults to empty string&#xA;@option --dry_run_output  A special default value that is returned when a&#xA;                          dry-run is performed, e.g. simulating a successful&#xA;                          run of the command&#xA;@option -ignorestderr     Stops the exec command from treating the output of&#xA;                          messages to the pipeline's standard error channel&#xA;                          as an error case&#xA;@option -stderrtostdin    Capture standard error as part of the standard input&#xA;@option -keepnewline      Retains a trailing newline in the pipeline's output.&#xA;                          Normally a trailing newline will be deleted.&#xA;@return                   The output of the command (if any)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847665" ID="ID_1498726847665500" MODIFIED="1498726847665" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847679" ID="ID_1498726847679172" MODIFIED="1498726847679">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> exec_utils<font color="#990000">::</font>execute <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                ignore_dry_run <font color="#993399">0</font>
                nocomplain     <font color="#993399">0</font>
                nolog          <font color="#993399">0</font>
                <b><font color="#0000FF">default</font></b>        <font color="#FF0000">{}</font>
                log_level      INFO
                dry_run_output <font color="#FF0000">{}</font>
                ignorestderr   <font color="#993399">0</font>
                stderrtostdin  <font color="#993399">0</font>
                keepnewline    <font color="#993399">0</font>
        <font color="#FF0000">}</font> exec_string
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$exec_string</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">{</font>wrong <i><font color="#9A1900"># args: should be "execute ?-option ...? command"}</font></i>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Prevent file names containing square brackets from being executed</font></i>
        <b><font color="#0000FF">set</font></b> exec_string <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font> <font color="#990000">[</font> <font color="#990000">\\[</font> <font color="#990000">]</font> <font color="#990000">\\]</font><font color="#FF0000">}</font> <font color="#009900">$exec_string</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> cmd <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$exec_string</font> <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Error occurred <b><font color="#0000FF">while</font></b> attempting to identify command executable<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>auto_execok <font color="#009900">$cmd</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>This script requires <font color="#009900">$cmd</font> to be installed<font color="#990000">.</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$ignore_dry_run</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log <font color="#009900">$log_level</font> <font color="#FF0000">{</font> <font color="#990000">-</font> Pretending to execute $<font color="#FF0000">{</font>exec_string<font color="#FF0000">}</font> <font color="#990000">(</font>dry run<font color="#990000">)</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$dry_run_output</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> exec_cmd <font color="#FF0000">{}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$ignorestderr</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> exec_cmd <font color="#990000">-</font>ignorestderr
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$keepnewline</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> exec_cmd <font color="#990000">-</font>keepnewline
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">lappend</font></b> exec_cmd <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$exec_string</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$stderrtostdin</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> exec_cmd <font color="#FF0000">{</font><font color="#993399">2</font><font color="#990000">&gt;</font>@<font color="#993399">1</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        log <font color="#009900">$log_level</font> <font color="#FF0000">{</font> <font color="#990000">-</font> Attempting to execute $<font color="#FF0000">{</font>exec_string<font color="#FF0000">}}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font><b><font color="#0000FF">exec</font></b> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$exec_cmd</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font>Error occured <b><font color="#0000FF">while</font></b> executing<font color="#990000">:\</font>n    <font color="#009900">$exec_string</font><font color="#990000">\</font>nError was<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$nocomplain</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">error</font></b> <font color="#009900">$msg</font> $<font color="#990000">::</font>errorInfo $<font color="#990000">::</font>errorCode
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$default</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$nolog</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log <font color="#009900">$log_level</font> <font color="#FF0000">{</font> <font color="#990000">-</font> Command returned<font color="#990000">:\</font>n<font color="#009900">$output</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726847680" ID="ID_1498726847680066" MODIFIED="1498726847680" TEXT="escape_utils 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726847680" ID="ID_1498726847680354" MODIFIED="1498726847680" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847681" ID="ID_1498726847681213" MODIFIED="1498726847681" TEXT="esc_utils.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/esc_utils.tcl"/>
</node>
<node CREATED="1498726847681" ID="ID_1498726847681426" MODIFIED="1498726847681" TEXT="escape" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726847681" ID="ID_1498726847681826" MODIFIED="1498726847681" TEXT="escape bash" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847682" ID="ID_1498726847682223" MODIFIED="1498726847682" TEXT="Escapes a string for bash output.&#xA;&#xA;Synopsis:&#xA;   escape bash &lt;input&gt;&#xA;&#xA;@param input           The input string to escape&#xA;@return                The input string escaped for bash output" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847682" ID="ID_1498726847682535" MODIFIED="1498726847682" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847692" ID="ID_1498726847692258" MODIFIED="1498726847692">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>escape<font color="#990000">::</font>escape bash <font color="#990000">--</font>params <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map $<font color="#990000">::</font>escape<font color="#990000">::</font>bash_escape_map <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847692" ID="ID_1498726847692849" MODIFIED="1498726847692" TEXT="escape entity" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847693" ID="ID_1498726847693240" MODIFIED="1498726847693" TEXT="Escapes a string for entity output.&#xA;&#xA;Synopsis:&#xA;   escape entity &lt;input&gt;&#xA;&#xA;@param input           The input string to escape&#xA;@return                The input string escaped for entity output" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847693" ID="ID_1498726847693583" MODIFIED="1498726847693" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847703" ID="ID_1498726847703033" MODIFIED="1498726847703">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>escape<font color="#990000">::</font>escape entity <font color="#990000">--</font>params <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map $<font color="#990000">::</font>escape<font color="#990000">::</font>entity_escape_map <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847703" ID="ID_1498726847703592" MODIFIED="1498726847703" TEXT="unescape bash" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847703" ID="ID_1498726847703984" MODIFIED="1498726847703" TEXT="Unescapes bash input.&#xA;&#xA;Synopsis:&#xA;   unescape bash &lt;input&gt;&#xA;&#xA;@param input           The bash string to unescape&#xA;@return                The input string unescaped" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847704" ID="ID_1498726847704325" MODIFIED="1498726847704" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847714" ID="ID_1498726847714783" MODIFIED="1498726847714">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>escape<font color="#990000">::</font>unescape bash <font color="#990000">--</font>params <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#990000">[</font>lreverse $<font color="#990000">::</font>escape<font color="#990000">::</font>bash_escape_map<font color="#990000">]</font> <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847715" ID="ID_1498726847715356" MODIFIED="1498726847715" TEXT="unescape entity" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847715" ID="ID_1498726847715764" MODIFIED="1498726847715" TEXT="Unescapes entity input.&#xA;&#xA;Synopsis:&#xA;   unescape entity &lt;input&gt;&#xA;&#xA;@param input           The entity string to unescape&#xA;@return                The input string unescaped" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847716" ID="ID_1498726847716119" MODIFIED="1498726847716" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847731" ID="ID_1498726847731127" MODIFIED="1498726847731">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>opt<font color="#990000">::</font>option <font color="#990000">::</font>escape<font color="#990000">::</font>unescape entity <font color="#990000">--</font>params <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#990000">--</font>body <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#990000">[</font>lreverse $<font color="#990000">::</font>escape<font color="#990000">::</font>entity_escape_map<font color="#990000">]</font> <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498726847731" ID="ID_1498726847731961" MODIFIED="1498726847731" TEXT="csv_utils 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1498726847732" ID="ID_1498726847732303" MODIFIED="1498726847732" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847732" ID="ID_1498726847732499" MODIFIED="1498726847732" TEXT="csv" LINK="#ID_1498726847731961"/>
</node>
<node CREATED="1498726847732" ID="ID_1498726847732748" MODIFIED="1498726847732" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847733" ID="ID_1498726847733639" MODIFIED="1498726847733" TEXT="csv.tcl" LINK="https://bitbucket.org/bakkeby/tapp/src/master/csv.tcl"/>
</node>
<node CREATED="1498726847733" ID="ID_1498726847733889" MODIFIED="1498726847733" TEXT="csv_utils" COLOR="#ff8300" FOLDED="true">
<node CREATED="1498726847734" ID="ID_1498726847734282" MODIFIED="1498726847734" TEXT="This package provides some additional CSV related utilities on top of the&#xA;tcllib csv package." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847734" ID="ID_1498726847734784" MODIFIED="1498726847734" TEXT="csv_utils::convert_to_ascii_table" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847735" ID="ID_1498726847735348" MODIFIED="1498726847735" TEXT="Create an ASCII table containing the CSV data.&#xA;&#xA;This will produce an ascii border around the CSV data for display purposes.&#xA;&#xA;Example usage:&#xA;   csv_utils::convert_to_ascii_table &quot;Col1,Col2,Col3\nVal1,Val2,Val3\nVal4,Val5,Val6&quot;&#xA;&#xA;This will return:&#xA;   +------+------+------+&#xA;   | Col1 | Col2 | Col3 |&#xA;   +------+------+------+&#xA;   | Val1 | Val2 | Val3 |&#xA;   +------+------+------+&#xA;   | Val4 | Val5 | Val6 |&#xA;   +------+------+------+&#xA;&#xA;@param csv_data                      The CSV data to convert&#xA;@option --separator &lt;character&gt;      Indicates the CSV separator (the&#xA;                                     character that separates columns),&#xA;                                     defaults to comma (,)&#xA;@option --delimiter &lt;character&gt;      Indicates the CSV delimiter (the&#xA;                                     character that wraps content), defaults&#xA;                                     to double quote&#xA;@return                              The CSV data in text format with an&#xA;                                     ASCII border" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847735" ID="ID_1498726847735694" MODIFIED="1498726847735" TEXT="@see" FOLDED="true">
<node CREATED="1498726847735" ID="ID_1498726847735910" MODIFIED="1498726847735" TEXT="table_utils::convert_to_ascii_table for additional formatting options" COLOR="#14666b" LINK="#ID_1498726846627979">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1498726847736" ID="ID_1498726847736233" MODIFIED="1498726847736" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847746" ID="ID_1498726847746743" MODIFIED="1498726847746">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>convert_to_ascii_table <font color="#FF0000">{</font> csv_data args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>table_utils<font color="#990000">::</font>convert_to_ascii_table <font color="#990000">[</font>csv_to_table <font color="#009900">$csv_data</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847747" ID="ID_1498726847747314" MODIFIED="1498726847747" TEXT="csv_utils::csv_to_table" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847747" ID="ID_1498726847747771" MODIFIED="1498726847747" TEXT="Convert a CSV table into a list table (list of lists).&#xA;&#xA;@param csv_data                      The CSV data to convert&#xA;@option --separator &lt;character&gt;      Indicates the CSV separator (the&#xA;                                     character that separates columns),&#xA;                                     defaults to comma (,)&#xA;@option --delimiter &lt;character&gt;      Indicates the CSV delimiter (the&#xA;                                     character that wraps content), defaults&#xA;                                     to double quote&#xA;@return                              The table in list form" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847748" ID="ID_1498726847748134" MODIFIED="1498726847748" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847757" ID="ID_1498726847757701" MODIFIED="1498726847757">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>csv_to_table <font color="#FF0000">{</font> csv_data args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                separator        <font color="#FF0000">{</font><font color="#990000">,</font><font color="#FF0000">}</font>
                delimiter        <font color="#990000">\</font><font color="#FF0000">"</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        touch table</font>
<font color="#FF0000">        foreach line [split $csv_data </font><font color="#CC33CC">\n</font><font color="#FF0000">] {</font>
<font color="#FF0000">                lappend table [csv::split $line $separator $delimiter]</font>
<font color="#FF0000">        }</font>

<font color="#FF0000">        return $table</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847758" ID="ID_1498726847758306" MODIFIED="1498726847758" TEXT="csv_utils::determine_number_of_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847758" ID="ID_1498726847758741" MODIFIED="1498726847758" TEXT="Determine the number of columns in a CSV data set.&#xA;&#xA;Typically a CSV file contains an even number of columns, but in the case&#xA;where they are not this proc can be used to find out what is the maximum&#xA;number of columns in the given CSV data.&#xA;&#xA;@param csv_data        The data in CSV format&#xA;@param separator       Character that separates columns, defaults to comma&#xA;@param delimiter       Character that wraps content, defaults to double quote&#xA;@return                The maximum number of columns in the CSV data" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847759" ID="ID_1498726847759102" MODIFIED="1498726847759" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847768" ID="ID_1498726847768512" MODIFIED="1498726847768">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>determine_number_of_columns <font color="#FF0000">{</font> csv_data <font color="#FF0000">{</font> separator <font color="#990000">,</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> delimiter <font color="#990000">\</font><font color="#FF0000">" } } {</font>
<font color="#FF0000">        return [table_utils::determine_number_of_columns [csv_to_table $csv_data {*}$args]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847769" ID="ID_1498726847769076" MODIFIED="1498726847769" TEXT="csv_utils::identify_empty_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847769" ID="ID_1498726847769507" MODIFIED="1498726847769" TEXT="Identify empty columns in a CSV data set.&#xA;&#xA;@param csv_data        The data in CSV format&#xA;@param ignore_header   Indicates that the CSV data contains a header and that&#xA;                       this should be ignored when determining whether a&#xA;                       column is empty or not&#xA;@param separator       Character that separates columns, defaults to comma&#xA;@param delimiter       Character that wraps content, defaults to double quote&#xA;@return                A list of indexes indicating empty columns" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847769" ID="ID_1498726847769880" MODIFIED="1498726847769" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847779" ID="ID_1498726847779455" MODIFIED="1498726847779">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>identify_empty_columns <font color="#FF0000">{</font> csv_data <font color="#FF0000">{</font> ignore_header <font color="#993399">0</font> <font color="#FF0000">}</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>table_utils<font color="#990000">::</font>identify_empty_columns <font color="#990000">[</font>csv_to_table <font color="#009900">$csv_data</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font> <font color="#009900">$ignore_header</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847780" ID="ID_1498726847780012" MODIFIED="1498726847780" TEXT="csv_utils::remove_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847780" ID="ID_1498726847780434" MODIFIED="1498726847780" TEXT="Remove a column from a CSV data set.&#xA;&#xA;@param csv_data        The data in CSV format&#xA;@param indexes         A list of indexes indicating which columns to remove&#xA;@param separator       Character that separates columns, defaults to comma&#xA;@param delimiter       Character that wraps content, defaults to double quote&#xA;@return                The CSV data set with the given columns removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847780" ID="ID_1498726847780808" MODIFIED="1498726847780" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847790" ID="ID_1498726847790605" MODIFIED="1498726847790">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>remove_columns <font color="#FF0000">{</font> csv_data indexes args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> table <font color="#990000">[</font>csv_to_table <font color="#009900">$csv_data</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> table <font color="#990000">[</font>table_utils<font color="#990000">::</font>remove_columns <font color="#009900">$table</font> <font color="#009900">$indexes</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>table_to_csv <font color="#009900">$table</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847791" ID="ID_1498726847791191" MODIFIED="1498726847791" TEXT="csv_utils::remove_empty_columns" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847791" ID="ID_1498726847791662" MODIFIED="1498726847791" TEXT="Remove empty columns from a CSV data set.&#xA;&#xA;If the data set contains some columns that are all empty then this proc can&#xA;be used to remove those.&#xA;&#xA;@param csv_data        The data in CSV format&#xA;@param ignore_header   Indicates that the CSV data contains a header and that&#xA;                       this should be ignored when determining whether a&#xA;                       column is empty or not&#xA;@param separator       Character that separates columns, defaults to comma&#xA;@param delimiter       Character that wraps content, defaults to double quote&#xA;@return                The CSV data with empty columns removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847792" ID="ID_1498726847792029" MODIFIED="1498726847792" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847802" ID="ID_1498726847802170" MODIFIED="1498726847802">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>remove_empty_columns <font color="#FF0000">{</font> csv_data <font color="#FF0000">{</font> ignore_header <font color="#993399">0</font> <font color="#FF0000">}</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> table <font color="#990000">[</font>csv_to_table <font color="#009900">$csv_data</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> idxes <font color="#990000">[</font>table_utils<font color="#990000">::</font>identify_empty_columns <font color="#009900">$table</font> <font color="#009900">$ignore_header</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> table <font color="#990000">[</font>table_utils<font color="#990000">::</font>remove_columns <font color="#009900">$table</font> <font color="#009900">$idxes</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>table_to_csv <font color="#009900">$table</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1498726847802" ID="ID_1498726847802776" MODIFIED="1498726847802" TEXT="csv_utils::table_to_csv" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1498726847803" ID="ID_1498726847803258" MODIFIED="1498726847803" TEXT="Convert a tcl list table into a CSV table.&#xA;&#xA;@param table                         The tcl list table to convert&#xA;@option --separator &lt;character&gt;      Indicates the CSV separator (the&#xA;                                     character that separates columns),&#xA;                                     defaults to comma (,)&#xA;@option --delimiter &lt;character&gt;      Indicates the CSV delimiter (the&#xA;                                     character that wraps content), defaults&#xA;                                     to double quote&#xA;@return                              The table in CSV form" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1498726847803" ID="ID_1498726847803634" MODIFIED="1498726847803" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1498726847813" ID="ID_1498726847813718" MODIFIED="1498726847813">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> csv_utils<font color="#990000">::</font>table_to_csv <font color="#FF0000">{</font> table args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        let <font color="#009900">$args</font> <font color="#FF0000">{</font>
                separator        <font color="#FF0000">{</font><font color="#990000">,</font><font color="#FF0000">}</font>
                delimiter        <font color="#990000">\</font><font color="#FF0000">"</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        </font>
<font color="#FF0000">        touch csv_data</font>
<font color="#FF0000">        foreach line $table {</font>
<font color="#FF0000">                lappend csv_data [csv::join $line $separator $delimiter]</font>
<font color="#FF0000">        }</font>

<font color="#FF0000">        return [join $csv_data </font><font color="#CC33CC">\n</font><font color="#FF0000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>

