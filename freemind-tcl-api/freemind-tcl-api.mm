<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1457012604811" ID="ID_1457012604811994" MODIFIED="1457012604812" TEXT="freemind-tcl-api" COLOR="#a72db7">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604813" ID="ID_1457012604813523" MODIFIED="1457012604813" TEXT="" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012604813" ID="ID_1457012604813818" MODIFIED="1457012604813" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604814" ID="ID_1457012604814596" MODIFIED="1457012604814" TEXT="pkgIndex.tcl" LINK="https://bitbucket.org/bakkeby/freemind-tcl-api/src/master/pkgIndex.tcl"/>
</node>
</node>
<node CREATED="1457012604815" ID="ID_1457012604815316" MODIFIED="1457012604815" TEXT="freemind_api 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012604815" ID="ID_1457012604815575" MODIFIED="1457012604815" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604815" ID="ID_1457012604815746" MODIFIED="1457012604815" TEXT="tdom"/>
</node>
<node CREATED="1457012604815" ID="ID_1457012604815967" MODIFIED="1457012604815" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604816" ID="ID_1457012604816677" MODIFIED="1457012604816" TEXT="node.tcl" LINK="https://bitbucket.org/bakkeby/freemind-tcl-api/src/master/node.tcl"/>
<node CREATED="1457012605146" ID="ID_1457012605146965" MODIFIED="1457012605146" TEXT="mm.tcl" LINK="https://bitbucket.org/bakkeby/freemind-tcl-api/src/master/mm.tcl"/>
</node>
<node CREATED="1457012604816" ID="ID_1457012604816879" MODIFIED="1457012604816" TEXT="node" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012604817" ID="ID_1457012604817967" MODIFIED="1457012604817" TEXT="node::add_attribute" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604818" ID="ID_1457012604818576" MODIFIED="1457012604818" TEXT="Add an attribute for a given node.&#xA;&#xA;An &quot;attribute&quot; here refers to custom key value pairs of information that can&#xA;be added to nodes in the FreeMind application.&#xA;&#xA;This should not be confused with DOM attributes.&#xA;&#xA;Synopsis:&#xA;   node::add_attribute node attribute value&#xA;&#xA;@param node            The node to add the attribute for&#xA;@param attribute       The name of the attribute to set&#xA;@param value           The value to set the attribute to" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604818" ID="ID_1457012604818907" MODIFIED="1457012604818" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604828" ID="ID_1457012604828767" MODIFIED="1457012604828">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>add_attribute <font color="#FF0000">{</font> node attribute value <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> dom <font color="#990000">[</font>mm<font color="#990000">::</font>get_dom<font color="#990000">]</font>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> attrib_node <font color="#990000">[</font><font color="#009900">$dom</font> createElement attribute<font color="#990000">]</font>
        <font color="#009900">$attrib_node</font> setAttribute NAME <font color="#009900">$attribute</font>
        <font color="#009900">$attrib_node</font> setAttribute VALUE <font color="#009900">$value</font>
        <font color="#009900">$node</font> appendChild <font color="#009900">$attrib_node</font>
        mm<font color="#990000">::</font>mark_dirty

<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604829" ID="ID_1457012604829597" MODIFIED="1457012604829" TEXT="node::add_subnode" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604829" ID="ID_1457012604829974" MODIFIED="1457012604829" TEXT="Add a node as a child to a parent node.&#xA;&#xA;@param node            The parent node&#xA;@param subnode         The node to add as a child node to the parent node" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604830" ID="ID_1457012604830339" MODIFIED="1457012604830" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604840" ID="ID_1457012604840507" MODIFIED="1457012604840">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>add_subnode <font color="#FF0000">{</font> node subnode <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <i><font color="#9A1900"># if subnode is already a sub node of node, then ignore</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">[</font><font color="#009900">$node</font> childNodes<font color="#990000">]</font> <font color="#009900">$subnode</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match dom<font color="#990000">*</font> <font color="#009900">$node</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">![</font><b><font color="#0000FF">string</font></b> match dom<font color="#990000">*</font> <font color="#009900">$subnode</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"wrong # args: should be </font><font color="#CC33CC">\"</font><font color="#FF0000">[lindex [info level [info level]] 0] node subnode</font><font color="#CC33CC">\"</font><font color="#FF0000">"</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># verify that node and subnode are dom nodes?</font></i>
        <font color="#009900">$node</font> appendChild <font color="#009900">$subnode</font>
        mm<font color="#990000">::</font>mark_dirty
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604841" ID="ID_1457012604841149" MODIFIED="1457012604841" TEXT="node::create" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604841" ID="ID_1457012604841689" MODIFIED="1457012604841" TEXT="Creates a new node an sets specific attributes based on the passed&#xA;in arguments.&#xA;&#xA;The node ID and timestamps will be auto generated unless overridden.&#xA;&#xA;Synopsis:&#xA;   node::create ?--param? ... ?&#xA;&#xA;@return                         The newly created node" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604842" ID="ID_1457012604842066" MODIFIED="1457012604842" TEXT="@see" FOLDED="true">
<node CREATED="1457012604842" ID="ID_1457012604842348" MODIFIED="1457012604842" TEXT="node::set for what parameters are supported" COLOR="#14666b" LINK="#ID_1457012605079022">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012604842" ID="ID_1457012604842740" MODIFIED="1457012604842" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604853" ID="ID_1457012604853927" MODIFIED="1457012604853">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>create args <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>mm<font color="#990000">::</font>get_args <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">--</font>text <font color="#990000">--</font>parent <font color="#990000">--</font>if_not_exists<font color="#FF0000">}</font> <font color="#FF0000">{{}</font> <font color="#FF0000">{}</font> <font color="#993399">0</font><font color="#FF0000">}</font><font color="#990000">]</font> text parent notexists
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$parent</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$notexists</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># There could potentially be more than one match, so just return</font></i>
                <i><font color="#9A1900"># the first one if that's the case</font></i>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> node <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font>mm<font color="#990000">::</font>node_path_search <font color="#009900">$text</font> <font color="#009900">$parent</font> <font color="#FF0000">{}</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$node</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b> <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> node <font color="#990000">[</font>_create_new_node<font color="#990000">]</font>
        node<font color="#990000">::</font><b><font color="#0000FF">set</font></b> <font color="#990000">--</font>node <font color="#009900">$node</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$node</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604854" ID="ID_1457012604854618" MODIFIED="1457012604854" TEXT="node::delete" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604855" ID="ID_1457012604855059" MODIFIED="1457012604855" TEXT="Deletes a node from the mind map.&#xA;&#xA;Note that this action will be irreversible.&#xA;&#xA;Synopsis:&#xA;   node:delete node ?node? ... ?&#xA;&#xA;@param node            One or more nodes to delete, if not provided then the&#xA;                       selected node will be deleted." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604855" ID="ID_1457012604855410" MODIFIED="1457012604855" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604868" ID="ID_1457012604868123" MODIFIED="1457012604868">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>delete <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font> <font color="#990000">||</font> <font color="#009900">$args</font> <font color="#990000">==</font> <font color="#FF0000">"</font><font color="#CC33CC">\u</font><font color="#FF0000">0"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> args <font color="#990000">[</font>node<font color="#990000">::</font>selected<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> nodes <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># This is essentially a redundant for-loop, but allows for</font></i>
                <i><font color="#9A1900"># use cases like:</font></i>
                <i><font color="#9A1900">#    node::delete $node1 $node2 $node3</font></i>
                <i><font color="#9A1900">#    node::delete [list $node1 $node2 $node3]</font></i>
                <i><font color="#9A1900">#    node::delete [node::remove_subnodes $node]</font></i>
                <i><font color="#9A1900">#</font></i>
                <b><font color="#0000FF">foreach</font></b> node <font color="#009900">$nodes</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match dom<font color="#990000">*</font> <font color="#009900">$node</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"wrong # args: should be </font><font color="#CC33CC">\"</font><font color="#FF0000">[lindex [info level [expr {[info level] - 1}]] 0] node</font><font color="#CC33CC">\"</font><font color="#FF0000">"</font>
                        <font color="#FF0000">}</font>

                        <font color="#009900">$node</font> delete
                        mm<font color="#990000">::</font>mark_dirty
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604868" ID="ID_1457012604868725" MODIFIED="1457012604868" TEXT="node::fold" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604869" ID="ID_1457012604869193" MODIFIED="1457012604869" TEXT="Folds the given node. If no node is passed in then the selected node will be&#xA;used.&#xA;&#xA;Synopsis:&#xA;   node::fold ?--node &lt;node&gt;?&#xA;   node::fold ?&lt;node&gt;?&#xA;&#xA;If the node is a leaf node then it will not be folded.&#xA;&#xA;@param node            The node to fold, if no node is  provided then the&#xA;                       selected node will be used&#xA;@param --node &lt;node&gt;   Same as node param above" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604869" ID="ID_1457012604869594" MODIFIED="1457012604869" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604881" ID="ID_1457012604881076" MODIFIED="1457012604881">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>fold <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                node<font color="#990000">::</font><b><font color="#0000FF">set</font></b> <font color="#990000">--</font>node <font color="#009900">$node</font> <font color="#990000">--</font>folded true
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604881" ID="ID_1457012604881710" MODIFIED="1457012604881" TEXT="node::get" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604882" ID="ID_1457012604882731" MODIFIED="1457012604882" TEXT="Retrieves specific attributes for a node based on the passed in arguments.&#xA;&#xA;In general if any of the request parameters return an empty string then that&#xA;indicates that the default within FreeMind will be used.&#xA;&#xA;Note that some fields may appear multiple times for the node, such as&#xA;attributes, hooks and icons. These values will be returned as a list within&#xA;the returned list.&#xA;&#xA;Synopsis:&#xA;   node::get --node &lt;node&gt; ?--param? ... ?&#xA;&#xA;Example usage:&#xA;   set res [node::get --node $node --created --id --text]&#xA;   lassign $res created id text&#xA;&#xA;@param --node &lt;node&gt;            The node containing the data&#xA;@param --select                 Mark the given node to be selected for&#xA;                                further updates&#xA;@param --bg_color               Retrieves the background color of the node&#xA;@param --color                  Retrieves foreground color of the node&#xA;@param --created                Retrieves the created timestamp&#xA;@param --modified               Retrieves the last modified timestamp&#xA;@param --folded                 Retrieves the folded attribute of the node,&#xA;                                returns true if the node is folded, false or&#xA;                                empty string if not&#xA;@param --id                     The ID of the node&#xA;@param --link                   Retrieve the link set for this node, if the&#xA;                                node is linked to another node then the ID of&#xA;                                the linked node will be returned&#xA;@param --position               Node position, i.e. left or right of centre&#xA;                                node. &#xA;@param --style                  Retrieve the style of the node&#xA;@param --text                   Retrieves the name/text of the node&#xA;@param --attribute &lt;key&gt; &lt;val&gt;  Retrieves custom node attributes in key value&#xA;                                pairs, also see node::get_attribute&#xA;@param --bold                   Returns true if node is bold, false or empty&#xA;                                string otherwise&#xA;@param --italic                 Returns true if node is in italic, false or&#xA;                                empty string otherwise&#xA;@param --font                   Retrieves the font of the node, returns empty&#xA;                                string if default (the default is SansSerif)&#xA;@param --size                   Retrieves the font size of the node, or empty&#xA;                                string if not set (the default is 12)&#xA;@param --edge_style             Retrieves the edge style of the node&#xA;@param --edge_color             Retrieves the edge color of the node&#xA;@param --edge_width             Retrieves the edge width &#xA;@param --hook                   Retrieves the hooks set for the node&#xA;@param --icon                   Retrieves the icons set for the node&#xA;@param --rich_content           Retrieve the rich content from the node, also&#xA;                                see node::get_rich_content&#xA;@param --parent                 Returns the parent node&#xA;@param --subnode &lt;node&gt; &lt;text&gt;  Retrieves a child node matching the given text&#xA;@param --subnodes &lt;node&gt; ...    Retrieves all child nodes from the current node&#xA;@return                         A list of results based on the parameters&#xA;                                passed in to the proc" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604883" ID="ID_1457012604883242" MODIFIED="1457012604883" TEXT="@see" FOLDED="true">
<node CREATED="1457012604883" ID="ID_1457012604883455" MODIFIED="1457012604883" TEXT="node::set for a better explanation of what all these parameters do" COLOR="#14666b" LINK="#ID_1457012605079022">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012604883" ID="ID_1457012604883781" MODIFIED="1457012604883" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604897" ID="ID_1457012604897581" MODIFIED="1457012604897">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>get <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PARAMS

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font><font color="#990000">]</font> node remaining_args
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> _split_args <font color="#990000">[</font>mm<font color="#990000">::</font>_split_args <font color="#009900">$remaining_args</font> <font color="#993399">1</font><font color="#990000">]</font>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>

        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font> param values <font color="#FF0000">}</font> <font color="#009900">$_split_args</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists PARAMS<font color="#990000">(--</font><font color="#009900">$param</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                lassign <font color="#009900">$PARAMS</font><font color="#990000">(--</font><font color="#009900">$param</font><font color="#990000">)</font> multi element attribute validator <b><font color="#0000FF">default</font></b>

                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$element</font> <font color="#FF0000">{</font>
                <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><font color="#009900">$node</font> getAttribute <font color="#009900">$attribute</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                attribute <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>node<font color="#990000">::</font>get_attribute <font color="#009900">$node</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$values</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                rich_content <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>node<font color="#990000">::</font>get_rich_content <font color="#009900">$node</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                select <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>select <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
                parent <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><font color="#009900">$node</font> parentNode<font color="#990000">]</font>
                <font color="#FF0000">}</font>
                subnode <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> subnodes <font color="#FF0000">{}</font>
                        <b><font color="#0000FF">foreach</font></b> value <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> subnodes <font color="#990000">[</font>node<font color="#990000">::</font>get_subnode <font color="#009900">$node</font> <font color="#009900">$value</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$subnodes</font>
                <font color="#FF0000">}</font>
                subnodes <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnode_vals <font color="#FF0000">{}</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$values</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> values <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">foreach</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> <font color="#009900">$element</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">foreach</font></b> value <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#009900">$value</font> <font color="#990000">[</font><font color="#009900">$subnode</font> getAttribute <font color="#009900">$attribute</font> <font color="#FF0000">{}</font><font color="#990000">]]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                <b><font color="#0000FF">lappend</font></b> subnode_vals <font color="#990000">[</font><font color="#009900">$subnode</font> getAttribute <font color="#009900">$attribute</font> <font color="#009900">$default</font><font color="#990000">]</font>
                                        <font color="#FF0000">}</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnode_vals</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> output <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$subnode_vals</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$subnode_vals</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$output</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$output</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604898" ID="ID_1457012604898545" MODIFIED="1457012604898" TEXT="node::get_attribute" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604899" ID="ID_1457012604899023" MODIFIED="1457012604899" TEXT="Retrieves an attribute from a given node.&#xA;&#xA;Synopsis:&#xA;   node::get_attribute node ?attribute? ?attribute? ...&#xA;&#xA;@param node            The node to get the attribute from&#xA;@param attribute       The name of the attribute to get, if not set then all&#xA;                       attributes will be returned. Multiple attribute names&#xA;                       may be retrieved at once." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604899" ID="ID_1457012604899337" MODIFIED="1457012604899" TEXT="@see" FOLDED="true">
<node CREATED="1457012604899" ID="ID_1457012604899542" MODIFIED="1457012604899" TEXT="add_attribute for further details" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012604899" ID="ID_1457012604899859" MODIFIED="1457012604899" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604910" ID="ID_1457012604910738" MODIFIED="1457012604910">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>get_attribute <font color="#FF0000">{</font> node args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> args <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> attrib_node <font color="#990000">[</font><font color="#009900">$node</font> selectNodes <font color="#FF0000">"attribute"</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> value <font color="#009900">$args</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> attribute <font color="#990000">[</font><font color="#009900">$attrib_node</font> getAttribute NAME<font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#009900">$value</font> <font color="#009900">$attribute</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$attribute</font> <font color="#990000">[</font><font color="#009900">$attrib_node</font> getAttribute VALUE<font color="#990000">]</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604911" ID="ID_1457012604911327" MODIFIED="1457012604911" TEXT="node::get_linked_node" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604911" ID="ID_1457012604911805" MODIFIED="1457012604911" TEXT="Retrieve the linked node, if any. Note that a node can have a link to either&#xA;an internal node, or an external URL.&#xA;&#xA;Synopsis:&#xA;   node::get_linked_node ?--node &lt;node&gt;?&#xA;&#xA;@param node            The node to retrieve the linked node from, if no node&#xA;                       is provided then the selected node will be used.&#xA;@param --node &lt;node&gt;   Same as node param above&#xA;@return                The linked node if found, empty string otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604912" ID="ID_1457012604912174" MODIFIED="1457012604912" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604923" ID="ID_1457012604923027" MODIFIED="1457012604923">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>get_linked_node <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <b><font color="#0000FF">set</font></b> linked_node <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> link <font color="#990000">[</font>node<font color="#990000">::</font>get <font color="#990000">--</font>node <font color="#009900">$node</font> <font color="#990000">--</font>link<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><i><font color="#9A1900">#*} $link]} {</font></i>
                        <b><font color="#0000FF">set</font></b> linked_node <font color="#990000">[</font>node_id_search <font color="#990000">[</font><b><font color="#0000FF">string</font></b> range <font color="#009900">$link</font> <font color="#993399">1</font> end<font color="#990000">]]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$linked_node</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604923" ID="ID_1457012604923765" MODIFIED="1457012604923" TEXT="node::get_rich_content" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604924" ID="ID_1457012604924397" MODIFIED="1457012604924" TEXT="Retrieves the rich content (i.e. HTML formatted content) of the node, if any.&#xA;&#xA;Synopsis:&#xA;   node::get_rich_content --node &lt;node&gt; ?--strip_html &lt;1|0&gt;?&#xA;&#xA;@param node            The node to retrieve rich content from, if no node is&#xA;                       provided then the selected node will be used&#xA;@param strip_html      Indicates whether to strip the HTML tags from the&#xA;                       content and just return the plain text. Defaults to&#xA;                       returning HTML (0).&#xA;@return                The rich content in HTML format or plain text if the&#xA;                       strip_html parameter has been passed in as 1." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604924" ID="ID_1457012604924870" MODIFIED="1457012604924" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604936" ID="ID_1457012604936784" MODIFIED="1457012604936">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>get_rich_content <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> strip_html <font color="#993399">0</font>
        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$remaining_args</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> split_args <font color="#990000">[</font>mm<font color="#990000">::</font>_split_args <font color="#009900">$remaining_args</font> <font color="#993399">1</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>param values<font color="#FF0000">}</font> <font color="#009900">$split_args</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$param</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$values</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$remaining_args</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> strip_html <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$remaining_args</font> <font color="#993399">0</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> rich_text_node <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> richcontent<font color="#990000">]</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnodes <font color="#990000">[</font><font color="#009900">$rich_text_node</font> childNodes<font color="#990000">]</font>
                <b><font color="#0000FF">for</font></b> <font color="#FF0000">{</font><font color="#990000">::</font><b><font color="#0000FF">set</font></b> i <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$i</font> <font color="#990000">&lt;</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnodes</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><b><font color="#0000FF">incr</font></b> i<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnode <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$subnodes</font> <font color="#009900">$i</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$strip_html</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> output <font color="#990000">[</font><font color="#009900">$subnode</font> nodeValue<font color="#990000">]</font>
                                <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#FF0000">{</font><font color="#990000">&lt;[^&gt;]+&gt;</font><font color="#FF0000">}</font> <font color="#009900">$output</font> <font color="#FF0000">{}</font> output
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> output <font color="#990000">[</font><font color="#009900">$subnode</font> asXML<font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trim <font color="#009900">$output</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604937" ID="ID_1457012604937463" MODIFIED="1457012604937" TEXT="node::get_subnode" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604937" ID="ID_1457012604937919" MODIFIED="1457012604937" TEXT="Retrieves all child nodes that matches the given plain text.&#xA;&#xA;Note that this will not work with nodes that contains rich content.&#xA;&#xA;@param node            The node to retrieve the child node from&#xA;@param text            The name/text of the child node to retrieve&#xA;@return                One or more child nodes if found" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604938" ID="ID_1457012604938299" MODIFIED="1457012604938" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604949" ID="ID_1457012604949504" MODIFIED="1457012604949">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>get_subnode <font color="#FF0000">{</font> node text <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <i><font color="#9A1900"># Text containting backslashes and single quotes cannot be used as</font></i>
        <i><font color="#9A1900"># part of the XPath search string, so using a starts-with solution</font></i>
        <i><font color="#9A1900"># instead.</font></i>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> search_string <font color="#009900">$text</font>
        <b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">^([^\\\</font><font color="#FF0000">']+)} $text --&gt; search_string</font>

<font color="#FF0000">        return [$node selectNodes "node</font><font color="#CC33CC">\[</font><font color="#FF0000">starts-with(@TEXT,'</font><font color="#009900">$search_string</font><font color="#FF0000">')</font><font color="#CC33CC">\]</font><font color="#FF0000">"]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604950" ID="ID_1457012604950085" MODIFIED="1457012604950" TEXT="node::get_subnodes" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604950" ID="ID_1457012604950559" MODIFIED="1457012604950" TEXT="Retrieves a child nodes of the given node.&#xA;&#xA;Synopsis:&#xA;  node::get_subnodes ?--node &lt;node&gt;?&#xA;&#xA;@param node            The node to retrieve the child nodes from&#xA;@return                All child nodes found, or empty string if the node has&#xA;                       no child nodes" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604950" ID="ID_1457012604950914" MODIFIED="1457012604950" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604961" ID="ID_1457012604961581" MODIFIED="1457012604961">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>get_subnodes <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><font color="#009900">$node</font> selectNodes <font color="#FF0000">{</font>node<font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604962" ID="ID_1457012604962173" MODIFIED="1457012604962" TEXT="node::has_subnodes" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604962" ID="ID_1457012604962723" MODIFIED="1457012604962" TEXT="Checks whether a node has child nodes or not.&#xA;&#xA;If the node has no child nodes then it will be a leaf node.&#xA;&#xA;Synopsis:&#xA;  node::get_subnodes ?--node &lt;node&gt;?&#xA;&#xA;@param node            The node to check whether it has child nodes or not&#xA;@return                1 if the node has child nodes, 0 otherwise" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604963" ID="ID_1457012604963091" MODIFIED="1457012604963" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604972" ID="ID_1457012604972876" MODIFIED="1457012604972">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>has_subnodes <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604973" ID="ID_1457012604973475" MODIFIED="1457012604973" TEXT="node::optimise_attribute_width" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604973" ID="ID_1457012604973974" MODIFIED="1457012604973" TEXT="Procedure to simulate the &quot;Optimal Width&quot; functionality when right clicking&#xA;attributes within FreeMind.&#xA;&#xA;The optimal width is determined by the graphical width a text takes up based&#xA;on the number of pixels needed to display it.&#xA;&#xA;This procedure tries to calculate and set this optimal width based on&#xA;gathered stats, but may not provide 100% accurate sizes.&#xA;&#xA;Synopsis:&#xA;   node::optimise_attribute_width --node &lt;node&gt;&#xA;&#xA;@param node            The node to optimise the attribute width for" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604974" ID="ID_1457012604974350" MODIFIED="1457012604974" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604985" ID="ID_1457012604985484" MODIFIED="1457012604985">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>optimise_attribute_width <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> max_name_length <font color="#993399">0</font>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> max_value_length <font color="#993399">0</font>
        <b><font color="#0000FF">foreach</font></b> attribute <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> attribute<font color="#990000">]</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> name_length  <font color="#990000">[</font>_calc_attribute_width <font color="#990000">[</font><font color="#009900">$attribute</font> getAttribute NAME<font color="#990000">]]</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> value_length <font color="#990000">[</font>_calc_attribute_width <font color="#990000">[</font><font color="#009900">$attribute</font> getAttribute VALUE<font color="#990000">]]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$name_length</font> <font color="#990000">&gt;</font> <font color="#009900">$max_name_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> max_name_length <font color="#009900">$name_length</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value_length</font> <font color="#990000">&gt;</font> <font color="#009900">$max_value_length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> max_value_length <font color="#009900">$value_length</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$max_name_length</font> <font color="#990000">&gt;</font> <font color="#993399">0</font> <font color="#990000">||</font> <font color="#009900">$max_value_length</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> attrib_layout_node <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> attribute_layout<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$attrib_layout_node</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> dom <font color="#990000">[</font>mm<font color="#990000">::</font>get_dom<font color="#990000">]</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> attrib_layout_node <font color="#990000">[</font><font color="#009900">$dom</font> createElement attribute_layout<font color="#990000">]</font>
                        node<font color="#990000">::</font>add_subnode <font color="#009900">$node</font> <font color="#009900">$attrib_layout_node</font>
                        <i><font color="#9A1900"># I was just thinking, should we have a flag to pass to _get_element</font></i>
                        <i><font color="#9A1900"># to automatically create the element if it doesn't already exist</font></i>
                <font color="#FF0000">}</font>
                <font color="#009900">$attrib_layout_node</font> setAttribute NAME_WIDTH <font color="#009900">$max_name_length</font> VALUE_WIDTH <font color="#009900">$max_value_length</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012604986" ID="ID_1457012604986099" MODIFIED="1457012604986" TEXT="node::remove" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012604987" ID="ID_1457012604987147" MODIFIED="1457012604987" TEXT="Removes specific attributes for a node based on the passed in arguments.&#xA;&#xA;Note that for most parameters a value can be passed in to indicate that the&#xA;attribute should only be removed if the value matches the given value.&#xA;&#xA;Synopsis:&#xA;   node::remove --node &lt;node&gt; ?--param? ... ?&#xA;&#xA;E.g.&#xA;   node::remove --node $node --color #aa33cc&#xA;&#xA;This would only remove the color attribute of the value if the currently set&#xA;color is in fact #aa33cc.&#xA;&#xA;@param --node &lt;node&gt;            The node being updated&#xA;@param --select                 Mark the given node to be selected for&#xA;                                further updates&#xA;@param --bg_color               Removes the background color of the node&#xA;@param --color                  Removes foreground color of the node&#xA;@param --created                Removes the created timestamp&#xA;@param --modified               Removes the last modified timestamp&#xA;@param --folded                 Removes the folded attribute of the node,&#xA;                                this essentially unfolds the node, also see&#xA;                                node::unfold&#xA;@param --id                     The ID of the node&#xA;@param --link                   Remove the link set for this node&#xA;@param --position               Node position, i.e. left or right of centre&#xA;                                node&#xA;@param --style                  Remove the style from the node&#xA;@param --text                   Removes the name/text of the node&#xA;@param --attribute &lt;key&gt; &lt;val&gt;  Removes custom node attributes in key value&#xA;                                pairs.  This can be passed multiple times to&#xA;                                remove more attributes from the node. If the&#xA;                                value is passed in as * then all attributes&#xA;                                with the given key are removed. If the key&#xA;                                is passed in as * then all attributes with&#xA;                                the given value will be removed. If no&#xA;                                parameters are passed in then all attributes&#xA;                                will be removed from the node.&#xA;@param --bold                   The node text should no longer be in bold&#xA;@param --italic                 The node text should no longer be in italic&#xA;@param --font                   Removes the font from the node&#xA;@param --size                   Removes the font size from the node&#xA;@param --edge_style             Removes the edge style from the node&#xA;@param --edge_color             Removes the edge color from the node&#xA;@param --edge_width             Removes the edge width &#xA;@param --hook                   Removes a hook from the node&#xA;@param --icon &lt;?name?&gt;          Removes an icon from the node. If the name of&#xA;                                the icon is provided then just the matching&#xA;                                one will be removed. If no name is passed in&#xA;                                then all icons will be removed.&#xA;@param --rich_content           Remove the rich content from the node&#xA;@param --subnode &lt;node&gt; &lt;text&gt;  Remove a child node matching the given text.&#xA;                                If there is any need to get the references to&#xA;                                the removed child nodes then using node::get&#xA;                                first or using the node::remove_subnode proc&#xA;                                directly would be recommended.&#xA;@param --subnodes &lt;node&gt; ...    Removes all child nodes of the current node&#xA;@return                         The same node that was passed in" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012604987" ID="ID_1457012604987637" MODIFIED="1457012604987" TEXT="@see" FOLDED="true">
<node CREATED="1457012604987" ID="ID_1457012604987854" MODIFIED="1457012604987" TEXT="node::set for a better explanation of what all these parameters do" COLOR="#14666b" LINK="#ID_1457012605079022">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012604988" ID="ID_1457012604988179" MODIFIED="1457012604988" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012604999" ID="ID_1457012604999776" MODIFIED="1457012604999">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>remove <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PARAMS

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font><font color="#990000">]</font> node remaining_args
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> _split_args <font color="#990000">[</font>mm<font color="#990000">::</font>_split_args <font color="#009900">$remaining_args</font> <font color="#993399">1</font><font color="#990000">]</font>

        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font> param values <font color="#FF0000">}</font> <font color="#009900">$_split_args</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists PARAMS<font color="#990000">(--</font><font color="#009900">$param</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                lassign <font color="#009900">$PARAMS</font><font color="#990000">(--</font><font color="#009900">$param</font><font color="#990000">)</font> multi element attribute validator <b><font color="#0000FF">default</font></b>

                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$param</font> <font color="#FF0000">{</font>
                attribute <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$values</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> values <font color="#FF0000">{</font><font color="#990000">*</font> <font color="#990000">*</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>key value<font color="#FF0000">}</font> <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                node<font color="#990000">::</font>remove_attribute <font color="#009900">$node</font> <font color="#009900">$key</font> <font color="#009900">$value</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                select <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>select <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
                subnode <font color="#FF0000">{</font>
                        <b><font color="#0000FF">foreach</font></b> value <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                node<font color="#990000">::</font>remove_subnode <font color="#009900">$node</font> <font color="#009900">$value</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                subnodes <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>remove_subnodes <font color="#990000">--</font>node <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
                rich_content <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>remove_rich_content <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$values</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> values <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">foreach</font></b> value <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                node<font color="#990000">::</font>_remove <font color="#009900">$node</font> <font color="#009900">$element</font> <font color="#009900">$attribute</font> <font color="#009900">$value</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$node</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605000" ID="ID_1457012605000403" MODIFIED="1457012605000" TEXT="node::remove_attribute" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605000" ID="ID_1457012605000926" MODIFIED="1457012605000" TEXT="Removes an attribute from a given node.&#xA;&#xA;Synopsis:&#xA;   node::remove_attribute node ?attribute? ?value?&#xA;&#xA;@param node            The node to remove the attribute from&#xA;@param attribute       The name of the attribute to set, if not set then all&#xA;                       attributes matching the given value will be removed.&#xA;                       If both the attribute and value are not set then all&#xA;                       attributes will be removed from the given node.&#xA;@param value           The attribute value to remove, if not set then all&#xA;                       values of the given attribute will be removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605001" ID="ID_1457012605001247" MODIFIED="1457012605001" TEXT="@see" FOLDED="true">
<node CREATED="1457012605001" ID="ID_1457012605001457" MODIFIED="1457012605001" TEXT="add_attribute for further details" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605001" ID="ID_1457012605001775" MODIFIED="1457012605001" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605013" ID="ID_1457012605013610" MODIFIED="1457012605013">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>remove_attribute <font color="#FF0000">{</font> node <font color="#FF0000">{</font> attribute <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> value <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> attrib_nodes <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> attribute<font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> param <font color="#FF0000">{</font>attribute value<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">"</font><font color="#CC33CC">\u</font><font color="#FF0000">0"</font><font color="#990000">]</font> <font color="#990000">[::</font><b><font color="#0000FF">set</font></b> <font color="#009900">$param</font><font color="#990000">]]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> <font color="#009900">$param</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> attrib_node <font color="#009900">$attrib_nodes</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">(![</font><font color="#009900">$attrib_node</font> hasAttribute NAME <font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#009900">$attribute</font> <font color="#990000">[</font><font color="#009900">$attrib_node</font> getAttribute NAME <font color="#990000">]])</font> <font color="#990000">&amp;&amp;</font>
                    <font color="#990000">(![</font><font color="#009900">$attrib_node</font> hasAttribute VALUE<font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#009900">$value</font>     <font color="#990000">[</font><font color="#009900">$attrib_node</font> getAttribute VALUE<font color="#990000">]])</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <font color="#009900">$node</font> removeChild <font color="#009900">$attrib_node</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        mm<font color="#990000">::</font>mark_dirty
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605014" ID="ID_1457012605014222" MODIFIED="1457012605014" TEXT="node::remove_rich_content" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605014" ID="ID_1457012605014719" MODIFIED="1457012605014" TEXT="Removes the rich content (i.e. HTML formatted content) from the node.&#xA;&#xA;Synopsis:&#xA;   node::remove_rich_content ?--node? &lt;domNode&gt;&#xA;&#xA;@param node            The node to remove rich content from, if no node is&#xA;                       provided then the selected node will be used&#xA;@param --node &lt;node&gt;   Same as node param above" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605015" ID="ID_1457012605015081" MODIFIED="1457012605015" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605024" ID="ID_1457012605024906" MODIFIED="1457012605024">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>remove_rich_content <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> richcontent<font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$subnode</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <font color="#009900">$node</font> removeChild <font color="#009900">$subnode</font>
                mm<font color="#990000">::</font>mark_dirty
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605025" ID="ID_1457012605025518" MODIFIED="1457012605025" TEXT="node::remove_subnode" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605026" ID="ID_1457012605026024" MODIFIED="1457012605026" TEXT="Removes the child node that matches the given plain text.&#xA;&#xA;@param node            The node to remove the child node(s) from&#xA;@param text            The name/text of the child node to remove, if more&#xA;                       than one node has the same name/text (unlikely) then&#xA;                       all matching nodes will be returned in a list&#xA;@param delete          Delete the detached node from memory, this will be&#xA;                       irreversible&#xA;@return                The child node(s) removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605026" ID="ID_1457012605026369" MODIFIED="1457012605026" TEXT="@see" FOLDED="true">
<node CREATED="1457012605026" ID="ID_1457012605026583" MODIFIED="1457012605026" TEXT="node::remove_subnodes for further details" COLOR="#14666b" LINK="#ID_1457012605037583">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605026" ID="ID_1457012605026905" MODIFIED="1457012605026" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605036" ID="ID_1457012605036985" MODIFIED="1457012605036">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>remove_subnode <font color="#FF0000">{</font> node text <font color="#FF0000">{</font>delete <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnodes <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>get_subnode <font color="#009900">$node</font> <font color="#009900">$text</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <font color="#009900">$node</font> removeChild <font color="#009900">$subnode</font>
                <b><font color="#0000FF">lappend</font></b> subnodes <font color="#009900">$subnode</font>
                mm<font color="#990000">::</font>mark_dirty
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$delete</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnodes</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                node<font color="#990000">::</font>delete <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$subnodes</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$subnodes</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605037" ID="ID_1457012605037583" MODIFIED="1457012605037" TEXT="node::remove_subnodes" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605038" ID="ID_1457012605038162" MODIFIED="1457012605038" TEXT="Removes all child nodes of the given node.&#xA;&#xA;Note that this will dissociate the nodes as children of the parent node, but&#xA;the nodes will not be explicitly deleted from memory. This means that the&#xA;returned nodes can be added (i.e. moved) to another parent node.&#xA;&#xA;The nodes will exist as DOM fragments in memory until the process ends unless&#xA;they are explicitly deleted. If the removed child nodes are no longer needed&#xA;then the delete parameter can be passed or the node::delete proc can be used.&#xA;&#xA;Synopsis:&#xA;   node::remove_subnodes ?--node &lt;node&gt;? ?--delete?&#xA;&#xA;@param --node &lt;node&gt;   The node to remove the child nodes from&#xA;@param --delete        Delete the node from memory, this will be irreversible&#xA;@return                The child nodes removed" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605038" ID="ID_1457012605038555" MODIFIED="1457012605038" TEXT="@see" FOLDED="true">
<node CREATED="1457012605038" ID="ID_1457012605038758" MODIFIED="1457012605038" TEXT="node::delete" COLOR="#14666b" LINK="#ID_1457012604854618">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605039" ID="ID_1457012605039050" MODIFIED="1457012605039" TEXT="node::remove_subnode for details on removing specific child nodes" COLOR="#14666b" LINK="#ID_1457012605025518">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605039" ID="ID_1457012605039370" MODIFIED="1457012605039" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605049" ID="ID_1457012605049803" MODIFIED="1457012605049">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>remove_subnodes <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> delete <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$remaining_args</font> <font color="#FF0000">{</font><font color="#990000">^-+</font>del<font color="#990000">(</font>ete<font color="#990000">)?</font>$<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font><font color="#990000">]</font>

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnodes <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <font color="#009900">$node</font> removeChild <font color="#009900">$subnode</font>
                <b><font color="#0000FF">lappend</font></b> subnodes <font color="#009900">$subnode</font>
                mm<font color="#990000">::</font>mark_dirty
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$delete</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnodes</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                node<font color="#990000">::</font>delete <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$subnodes</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$subnodes</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605050" ID="ID_1457012605050450" MODIFIED="1457012605050" TEXT="node::select" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605051" ID="ID_1457012605051132" MODIFIED="1457012605051" TEXT="Select a node for multiple updates.&#xA;&#xA;Synopsis:&#xA;   node::select --node &lt;node&gt;&#xA;   node::select &lt;node&gt;&#xA;&#xA;A selected node can be used with the following short-hand alternatives for&#xA;the get, set and remove procs:&#xA;   node::bg_color&#xA;   node::color&#xA;   node::created&#xA;   node::folded&#xA;   node::id&#xA;   node::link&#xA;   node::modified&#xA;   node::position&#xA;   node::style&#xA;   node::text&#xA;   node::attribute&#xA;   node::bold&#xA;   node::italic&#xA;   node::font&#xA;   node::size&#xA;   node::edge_color&#xA;   node::edge_width&#xA;   node::edge_style&#xA;   node::cloud&#xA;   node::hook&#xA;   node::icon&#xA;   node::rich_content&#xA;&#xA;Example usage:&#xA;   node::select $node&#xA;   set old_text [node::text]; # retrieves the text of the node&#xA;   node::text &quot;new text&quot;;     # sets the new text for the node&#xA;   node::text {};             # removes the text from the node&#xA;&#xA;@param node            The node being selected" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605051" ID="ID_1457012605051445" MODIFIED="1457012605051" TEXT="@see" FOLDED="true">
<node CREATED="1457012605051" ID="ID_1457012605051657" MODIFIED="1457012605051" TEXT="node::set for what each of these parameters do" COLOR="#14666b" LINK="#ID_1457012605079022">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605051" ID="ID_1457012605051974" MODIFIED="1457012605051" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605062" ID="ID_1457012605062076" MODIFIED="1457012605062">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>select <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> NODE

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> NODE<font color="#990000">(</font>selected_node<font color="#990000">)</font> <font color="#FF0000">{}</font>
        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> NODE<font color="#990000">(</font>selected_node<font color="#990000">)</font> <font color="#009900">$node</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605062" ID="ID_1457012605062755" MODIFIED="1457012605062" TEXT="node::selected" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605063" ID="ID_1457012605063248" MODIFIED="1457012605063" TEXT="Retrieves the currently selected node.&#xA;&#xA;@return                The selected node or empty string if none has been set" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605063" ID="ID_1457012605063587" MODIFIED="1457012605063" TEXT="@see" FOLDED="true">
<node CREATED="1457012605063" ID="ID_1457012605063813" MODIFIED="1457012605063" TEXT="node::select" COLOR="#14666b" LINK="#ID_1457012605050450">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605064" ID="ID_1457012605064154" MODIFIED="1457012605064" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605078" ID="ID_1457012605078422" MODIFIED="1457012605078">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>selected <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> NODE

        <b><font color="#0000FF">return</font></b> <font color="#009900">$NODE</font><font color="#990000">(</font>selected_node<font color="#990000">)</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605079" ID="ID_1457012605079022" MODIFIED="1457012605079" TEXT="node::set" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605080" ID="ID_1457012605080058" MODIFIED="1457012605080" TEXT="Sets specific attributes for a node based on the passed in arguments.&#xA;&#xA;Note that some parameters can be added multiple times, such as attribute,&#xA;hook and icon.&#xA;&#xA;Synopsis:&#xA;   node::set --node &lt;node&gt; ?--param? ... ?&#xA;&#xA;Example usage:&#xA;   node::set --node $node --bold --icon idea --color #aa33cc&#xA;&#xA;@param --node &lt;node&gt;            The node being updated, if no node is&#xA;                                provided then the selected one will be used.&#xA;@param --select                 Mark the given node to be selected for&#xA;                                further updates&#xA;@param --bg_color &lt;rgb&gt;         Specifies the background color of the node,&#xA;                                e.g. #ffff00&#xA;@param --color &lt;rgb&gt;            Specifies the foreground (i.e. font) color,&#xA;                                e.g. #aa33cc&#xA;@param --created &lt;ms&gt;           Sets the created timestamp, in milliseconds&#xA;                                since epoc&#xA;@param --modified &lt;ms&gt;          Sets when the node was last modified, in&#xA;                                milliseconds since epoc&#xA;@param --folded                 Fold the given node, also see node::fold and&#xA;                                node::unfold&#xA;@param --id &lt;ID&gt;                Specify the node ID&#xA;@param --link &lt;ID&gt;              Links the node to another node&#xA;@param --link &lt;URL&gt;             Sets an external link, note that a node can&#xA;                                only contain one link&#xA;@param --position &lt;pos&gt;         Node position, i.e. left or right of centre&#xA;                                node. Only relevant for direct children of&#xA;                                the root node.&#xA;@param --style &lt;style&gt;          Assign a style to the node, e.g. bubble or&#xA;                                fork.&#xA;@param --text &lt;text&gt;            Sets the (plain) text/name of the node&#xA;@param --attribute &lt;key&gt; &lt;val&gt;  Add custom node attributes in key value pairs&#xA;                                This can be passed multiple times to add more&#xA;                                attributes to the node&#xA;@param --bold                   Makes node text/name bold&#xA;@param --italic                 Makes node text/name italic&#xA;@param --font &lt;font&gt;            Specifies the font used for the node text&#xA;@param --size &lt;size&gt;            Specifies the font size of the node&#xA;@param --edge_style &lt;style&gt;     The edge here refers to the line between the&#xA;                                current node and it's parent node. If no edge&#xA;                                style is defined then the style is inherited&#xA;                                from the parent node. Possible styles include:&#xA;                                linear, bezier, sharp_linear and sharp_bezier&#xA;@param --edge_color &lt;rgb&gt;       This defines the RGB color of the edge line&#xA;@param --edge_width &lt;val&gt;       This defines the width of the edge line,&#xA;                                typical values include:&#xA;                                1, 2, 4, 8, &quot;parent&quot; and &quot;thin&quot; &#xA;@param --hook &lt;path&gt;            Adds a hook for a plugin to control the style&#xA;                                of the node. One example is the blinking node&#xA;                                hook.&#xA;@param --icon &lt;name&gt;            Sets an icon for the node, examples include:&#xA;                                &quot;yes&quot;, &quot;stop&quot; and &quot;idea&quot;. See FORMAT(icons) in&#xA;                                node.tcl for a more complete list.&#xA;@param --rich_content &lt;html&gt;    Glorify the node content with rich text, i.e.&#xA;                                plain HTML that is rendered allowing further&#xA;                                control of layout and look-and-feel.&#xA;@param --parent &lt;node&gt;          Set current node as child of the given parent&#xA;                                node&#xA;@param --if_not_exists          create the node if such a path does not already&#xA;                                exist, this is only relevant when creating&#xA;                                nodes and if the --parent node is also supplied&#xA;@param --subnode &lt;node&gt;         Add a node as a child to the current node&#xA;@param --subnodes &lt;node&gt; ...    Add multiple child nodes to the current node&#xA;@return                         The same node that was passed in" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605080" ID="ID_1457012605080579" MODIFIED="1457012605080" TEXT="@see" FOLDED="true">
<node CREATED="1457012605080" ID="ID_1457012605080791" MODIFIED="1457012605080" TEXT="node::select" COLOR="#14666b" LINK="#ID_1457012605050450">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605081" ID="ID_1457012605081120" MODIFIED="1457012605081" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605098" ID="ID_1457012605098693" MODIFIED="1457012605098">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font><b><font color="#0000FF">set</font></b> <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PARAMS

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font><font color="#990000">]</font> node remaining_args
        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> _split_args <font color="#990000">[</font>mm<font color="#990000">::</font>_split_args <font color="#009900">$remaining_args</font> <font color="#993399">1</font><font color="#990000">]</font>

        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font> param values <font color="#FF0000">}</font> <font color="#009900">$_split_args</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists PARAMS<font color="#990000">(--</font><font color="#009900">$param</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>

                lassign <font color="#009900">$PARAMS</font><font color="#990000">(--</font><font color="#009900">$param</font><font color="#990000">)</font> multi element attribute validator <b><font color="#0000FF">default</font></b>

                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$param</font> <font color="#FF0000">{</font>
                attribute <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$values</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> values <font color="#FF0000">{</font><font color="#990000">*</font> <font color="#990000">*</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>key value<font color="#FF0000">}</font> <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                node<font color="#990000">::</font>add_attribute <font color="#009900">$node</font> <font color="#009900">$key</font> <font color="#009900">$value</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                rich_content <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>set_rich_content <font color="#990000">--</font>node <font color="#009900">$node</font> <font color="#990000">--</font>content <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$values</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                select <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>select <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
                parent <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>add_subnode <font color="#009900">$values</font> <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
                subnode <font color="#990000">-</font>
                subnodes <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># Double foreach loop, redundant, but allows for these</font></i>
                        <i><font color="#9A1900"># two parameter forms:</font></i>
                        <i><font color="#9A1900">#    node::set --node $n --subnodes $sn1 $sn2 $sn3</font></i>
                        <i><font color="#9A1900">#    node::set --node $n --subnodes [list $sn1 $sn2 $sn3]</font></i>
                        <b><font color="#0000FF">foreach</font></b> value <font color="#009900">$values</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">foreach</font></b> val <font color="#009900">$value</font> <font color="#FF0000">{</font>
                                        node<font color="#990000">::</font>add_subnode <font color="#009900">$node</font> <font color="#009900">$val</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$values</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$default</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> values <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#009900">$default</font><font color="#990000">]]</font>
                        <font color="#FF0000">}</font>
                        node<font color="#990000">::</font>_set <font color="#009900">$node</font> <font color="#009900">$element</font> <font color="#009900">$multi</font> <font color="#009900">$attribute</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$values</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$node</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605099" ID="ID_1457012605099368" MODIFIED="1457012605099" TEXT="node::set_rich_content" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605099" ID="ID_1457012605099919" MODIFIED="1457012605099" TEXT="Sets the rich content (i.e. HTML formatted content) of the node.&#xA;&#xA;Synopsis:&#xA;   node::set_rich_content --node &lt;domNode&gt; --content &lt;html&gt;&#xA;   node::set_rich_content &lt;domNode&gt; &lt;html&gt;&#xA;&#xA;@param node              The node to set rich content for, if no node is&#xA;                         provided then the selected node will be used&#xA;@param --node &lt;node&gt;     Same as node param above&#xA;@param content           The rich content (i.e. HTML) to set&#xA;@param --content &lt;html&gt;  Same as content param above" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605100" ID="ID_1457012605100310" MODIFIED="1457012605100" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605111" ID="ID_1457012605111500" MODIFIED="1457012605111">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>set_rich_content <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$remaining_args</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> split_args <font color="#990000">[</font>mm<font color="#990000">::</font>_split_args <font color="#009900">$remaining_args</font> <font color="#993399">1</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>param values<font color="#FF0000">}</font> <font color="#009900">$split_args</font> <font color="#FF0000">{</font>
                        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$param</font><font color="#990000">]</font> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$values</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$remaining_args</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> content <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$remaining_args</font> <font color="#990000">\</font>n<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists content<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">error</font></b> <font color="#FF0000">"Usage: [lindex [info level [info level]] 0] --node &lt;domNode&gt; --content &lt;html&gt;"</font>
        <font color="#FF0000">}</font>

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>_get_element <font color="#009900">$node</font> richcontent<font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$subnode</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <font color="#990000">::</font><b><font color="#0000FF">set</font></b> subnode <font color="#990000">[[</font>mm<font color="#990000">::</font>get_dom<font color="#990000">]</font> createElement richcontent<font color="#990000">]</font>
                <font color="#009900">$subnode</font> setAttribute TYPE NODE
                node<font color="#990000">::</font>add_subnode <font color="#009900">$node</font> <font color="#009900">$subnode</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> childNode <font color="#990000">[</font><font color="#009900">$subnode</font> childNodes<font color="#990000">]</font> <font color="#FF0000">{</font>
                <font color="#009900">$childNode</font> delete
        <font color="#FF0000">}</font>

        <font color="#009900">$subnode</font> appendFromScript <font color="#FF0000">{</font>rich_content_appender <font color="#990000">-</font>disableOutputEscaping <font color="#009900">$content</font><font color="#FF0000">}</font>
        mm<font color="#990000">::</font>mark_dirty
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605112" ID="ID_1457012605112121" MODIFIED="1457012605112" TEXT="node::unfold" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605112" ID="ID_1457012605112620" MODIFIED="1457012605112" TEXT="Unfolds the given node. If no node is passed in then the selected node will&#xA;be used.&#xA;&#xA;Synopsis:&#xA;   node::unfold ?--node &lt;node&gt;?&#xA;   node::unfold ?&lt;node&gt;?&#xA;&#xA;@param node            The node to unfold, if no node is  provided then the&#xA;                       selected node will be used&#xA;@param --node &lt;node&gt;   Same as node param above" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605112" ID="ID_1457012605112977" MODIFIED="1457012605112" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605122" ID="ID_1457012605122621" MODIFIED="1457012605122">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>unfold <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>_get_node_arg <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font> <font color="#993399">0</font><font color="#990000">]</font> node remaining_args

        node<font color="#990000">::</font>remove <font color="#990000">--</font>node <font color="#009900">$node</font> <font color="#990000">--</font>folded
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605123" ID="ID_1457012605123217" MODIFIED="1457012605123" TEXT="node::unselect" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605123" ID="ID_1457012605123683" MODIFIED="1457012605123" TEXT="Unselect a node.&#xA;&#xA;This is primarily for test purposes." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605123" ID="ID_1457012605123976" MODIFIED="1457012605123" TEXT="@see" FOLDED="true">
<node CREATED="1457012605124" ID="ID_1457012605124177" MODIFIED="1457012605124" TEXT="node::select" COLOR="#14666b" LINK="#ID_1457012605050450">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605124" ID="ID_1457012605124492" MODIFIED="1457012605124" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605133" ID="ID_1457012605133782" MODIFIED="1457012605133">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>unselect <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> NODE

        <font color="#990000">::</font><b><font color="#0000FF">set</font></b> NODE<font color="#990000">(</font>selected_node<font color="#990000">)</font> <font color="#FF0000">{}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605134" ID="ID_1457012605134412" MODIFIED="1457012605134" TEXT="node::unset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605134" ID="ID_1457012605134884" MODIFIED="1457012605134" TEXT="Alias for node::remove.&#xA;&#xA;Synopsis:&#xA;   node::unset --node &lt;node&gt; ?--param? ... ?" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605135" ID="ID_1457012605135181" MODIFIED="1457012605135" TEXT="@see" FOLDED="true">
<node CREATED="1457012605135" ID="ID_1457012605135390" MODIFIED="1457012605135" TEXT="node::remove" COLOR="#14666b" LINK="#ID_1457012604986099">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605135" ID="ID_1457012605135708" MODIFIED="1457012605135" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605145" ID="ID_1457012605145086" MODIFIED="1457012605145">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> node<font color="#990000">::</font>unset <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>node<font color="#990000">::</font>remove <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$args</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1457012605147" ID="ID_1457012605147195" MODIFIED="1457012605147" TEXT="mm" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012605147" ID="ID_1457012605147733" MODIFIED="1457012605147" TEXT="mm::compare_node_by_text" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605148" ID="ID_1457012605148130" MODIFIED="1457012605148" TEXT="Comparison proc which is can be used with lsort -command.&#xA;&#xA;The proc will compare nodes based on the node text (or rich content if set).&#xA;The sorting is case insensitive and ascending.&#xA;&#xA;@param left            First element&#xA;@param right           Second element&#xA;@return                An integer less than, equal to, or greater than zero&#xA;                       if the first element is to be considered less than,&#xA;                       equal to, or greater than the second, respectively." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605148" ID="ID_1457012605148460" MODIFIED="1457012605148" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605159" ID="ID_1457012605159437" MODIFIED="1457012605159">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>compare_node_by_text <font color="#FF0000">{</font> left right <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> strip_html <font color="#993399">1</font>
        <b><font color="#0000FF">foreach</font></b> side <font color="#FF0000">{</font>left right<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> $<font color="#FF0000">{</font>side<font color="#FF0000">}</font>_text <font color="#990000">[</font>node<font color="#990000">::</font>get <font color="#990000">--</font>node <font color="#990000">[</font><b><font color="#0000FF">set</font></b> <font color="#009900">$side</font><font color="#990000">]</font> <font color="#990000">--</font>text<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">set</font></b> $<font color="#FF0000">{</font>side<font color="#FF0000">}</font>_text<font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> $<font color="#FF0000">{</font>side<font color="#FF0000">}</font>_text <font color="#990000">[</font>node<font color="#990000">::</font>get_rich_content <font color="#990000">[</font><b><font color="#0000FF">set</font></b> <font color="#009900">$side</font><font color="#990000">]</font> <font color="#009900">$strip_html</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> compare <font color="#990000">-</font>nocase <font color="#009900">$left_text</font> <font color="#009900">$right_text</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605160" ID="ID_1457012605160021" MODIFIED="1457012605160" TEXT="mm::create_empty_mindmap" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605160" ID="ID_1457012605160402" MODIFIED="1457012605160" TEXT="Creates an empty mind map with no root node.&#xA;&#xA;@return                The map element of the new empty mind map" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605160" ID="ID_1457012605160756" MODIFIED="1457012605160" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605170" ID="ID_1457012605170771" MODIFIED="1457012605170">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>create_empty_mindmap <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">set</font></b> dom <font color="#990000">[</font>dom createDocument map<font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> map <font color="#990000">[</font><font color="#009900">$dom</font> childNodes<font color="#990000">]</font>

        <font color="#009900">$map</font> setAttribute version <font color="#993399">0.9</font><font color="#990000">.</font><font color="#993399">0</font>
        <font color="#009900">$map</font> appendChild <font color="#990000">[</font><font color="#009900">$dom</font> createComment <font color="#FF0000">{</font> To view this <b><font color="#0000FF">file</font></b><font color="#990000">,</font> download free mind mapping software FreeMind from <b><font color="#0000FF">http</font></b><font color="#990000">://</font>freemind<font color="#990000">.</font>sourceforge<font color="#990000">.</font>net <font color="#FF0000">}</font><font color="#990000">]</font>

        node<font color="#990000">::</font>unselect

        <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font>dom<font color="#990000">)</font> <font color="#009900">$dom</font>
        <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font>dirty<font color="#990000">)</font> <font color="#993399">1</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$map</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605171" ID="ID_1457012605171367" MODIFIED="1457012605171" TEXT="mm::fold_all" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605171" ID="ID_1457012605171844" MODIFIED="1457012605171" TEXT="Folds all given nodes and child nodes recursively. If no node is specified then all&#xA;nodes in the mind map are folded.&#xA;&#xA;Leaf nodes will not be marked as folded. The reason for this is that they can not be&#xA;folded manually within FreeMind and if they are marked as folded in the XML then they&#xA;will actually appear as being folded within FreeMind, but cannot be unfolded as they&#xA;have no child nodes.&#xA;&#xA;Synopsis:&#xA;   mm::fold_all ?node? ?node? ... ?&#xA;&#xA;@param node            A node in the mind map whose child nodes should be folded" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605172" ID="ID_1457012605172198" MODIFIED="1457012605172" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605182" ID="ID_1457012605182783" MODIFIED="1457012605182">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>fold_all <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> args <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#990000">[</font>mm<font color="#990000">::</font>get_root_node<font color="#990000">]]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> node <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> subnodes <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnodes</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        mm<font color="#990000">::</font>fold_all <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$subnodes</font>
                        <i><font color="#9A1900"># Leaf node are never folded, hence we only fold</font></i>
                        <i><font color="#9A1900"># if the node has child nodes. If we do fold leaf</font></i>
                        <i><font color="#9A1900"># nodes then they will appear as expandable in</font></i>
                        <i><font color="#9A1900"># FreeMind, but naturally can't be expanded.</font></i>
                        node<font color="#990000">::</font>fold <font color="#009900">$node</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605183" ID="ID_1457012605183383" MODIFIED="1457012605183" TEXT="mm::get_arg" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605183" ID="ID_1457012605183928" MODIFIED="1457012605183" TEXT="Procedure to retrieve the value of a given parameter from a list&#xA;of arguments.&#xA;&#xA;Synopsis:&#xA;   mm::get_arg &lt;list&gt; &lt;parameter&gt; ?&lt;default&gt;? ?&lt;prefix&gt;?&#xA;&#xA;For example:&#xA;   set args [list --type d -i --file ex1.txt ex2.txt --num 1 2 3 --file ex3.txt]&#xA;   set file [mm::get_arg $args --file]&#xA;&#xA;will return:&#xA;   [list ex1.txt ex2.txt ex3.txt]&#xA;&#xA;@param arguments       The list of arguments to search through&#xA;@param param           The parameter to search for&#xA;@param arg_prefix      The parameter prefix, defaults to hyphen (-)&#xA;@param default         The default value if not found&#xA;@return                Empty string if the argument value was not found,&#xA;                       the value if a single value was found, a list of&#xA;                       values if multiple values were found, or &quot;1&quot; if the&#xA;                       argument was found to exist, but with no values." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605184" ID="ID_1457012605184313" MODIFIED="1457012605184" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605196" ID="ID_1457012605196622" MODIFIED="1457012605196">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>get_arg <font color="#FF0000">{</font> arguments param <font color="#FF0000">{</font><b><font color="#0000FF">default</font></b> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> value <font color="#FF0000">"</font><font color="#CC33CC">\u</font><font color="#FF0000">0"</font>
        <b><font color="#0000FF">set</font></b> length <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$arguments</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> param <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font>all <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$arguments</font> <font color="#990000">^</font>$<font color="#FF0000">{</font>arg_prefix<font color="#FF0000">}</font><font color="#990000">+</font><font color="#009900">$param</font><font color="#990000">\</font>$<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">while</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">incr</font></b> idx<font color="#990000">]</font> <font color="#990000">&lt;</font> <font color="#009900">$length</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> arg <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$arguments</font> <font color="#009900">$idx</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">^</font><font color="#009900">$arg_prefix</font><font color="#990000">+\[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">\]</font> <font color="#009900">$arg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">"</font><font color="#CC33CC">\u</font><font color="#FF0000">0"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> value <font color="#009900">$arg</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> value <font color="#009900">$arg</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">"</font><font color="#CC33CC">\u</font><font color="#FF0000">0"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> value <font color="#993399">1</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">"</font><font color="#CC33CC">\u</font><font color="#FF0000">0"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> value <font color="#009900">$default</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$value</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605197" ID="ID_1457012605197260" MODIFIED="1457012605197" TEXT="mm::get_args" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605197" ID="ID_1457012605197812" MODIFIED="1457012605197" TEXT="Procedure to retrieve the values of multiple from a list of arguments.&#xA;&#xA;Synopsis:&#xA;   mm::get_args &lt;list&gt; &lt;parameters&gt; &lt;defaults&gt;&#xA;&#xA;For example:&#xA;   set args [list --type d -i --file ex1.txt ex2.txt --num 1 2 3 --file ex3.txt]&#xA;   lassign [mm::get_arg $args {--file --type --perm} {default.txt f x}] file type perm&#xA;&#xA;@param arguments       The list of arguments to search through&#xA;@param params          The list of parameters to search for&#xA;@param defaults        The default value should the parameter not exist in&#xA;                       the list of arguments. Expects one default value per&#xA;                       given parameter, if less then will default to empty&#xA;                       string for the remaining parameters.&#xA;@param arg_prefix      The parameter prefix, defaults to hyphen (-)&#xA;@return                A list of values relative to the list of arguments" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605198" ID="ID_1457012605198151" MODIFIED="1457012605198" TEXT="@see" FOLDED="true">
<node CREATED="1457012605198" ID="ID_1457012605198441" MODIFIED="1457012605198" TEXT="mm::get_arg" COLOR="#14666b" LINK="#ID_1457012605183383">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605198" ID="ID_1457012605198779" MODIFIED="1457012605198" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605209" ID="ID_1457012605209422" MODIFIED="1457012605209">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>get_args <font color="#FF0000">{</font> arguments params <font color="#FF0000">{</font>defaults <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>arg_prefix <font color="#990000">-</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> output <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> param <font color="#009900">$params</font> <b><font color="#0000FF">default</font></b> <font color="#009900">$defaults</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>mm<font color="#990000">::</font>get_arg <font color="#009900">$arguments</font> <font color="#009900">$param</font> <font color="#009900">$default</font> <font color="#009900">$arg_prefix</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605210" ID="ID_1457012605210120" MODIFIED="1457012605210" TEXT="mm::get_dom" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605210" ID="ID_1457012605210686" MODIFIED="1457012605210" TEXT="Returns the DOM object of the mind map.&#xA;&#xA;If no DOM object exists then a new empty mind map is automatically created.&#xA;&#xA;@return                The DOM object of the mind map" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605211" ID="ID_1457012605211078" MODIFIED="1457012605211" TEXT="@see" FOLDED="true">
<node CREATED="1457012605211" ID="ID_1457012605211332" MODIFIED="1457012605211" TEXT="http://docs.activestate.com/activetcl/8.5/tdom/dom.html" COLOR="#14666b" LINK="http://docs.activestate.com/activetcl/8.5/tdom/dom.html">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605211" ID="ID_1457012605211782" MODIFIED="1457012605211" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605221" ID="ID_1457012605221587" MODIFIED="1457012605221">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>get_dom <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists MM<font color="#990000">(</font>dom<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                create_empty_mindmap
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$MM</font><font color="#990000">(</font>dom<font color="#990000">)</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605222" ID="ID_1457012605222171" MODIFIED="1457012605222" TEXT="mm::get_next_even_position" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605222" ID="ID_1457012605222645" MODIFIED="1457012605222" TEXT="Retrieves the next optimal position, i.e. left or right of the centre (root)&#xA;node if one wants an even distribution.&#xA;&#xA;@return                a node position (&quot;left&quot; or &quot;right&quot;)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605222" ID="ID_1457012605223003" MODIFIED="1457012605223" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605233" ID="ID_1457012605233454" MODIFIED="1457012605233">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>get_next_even_position <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> r_count <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> l_count <font color="#993399">0</font>
        <b><font color="#0000FF">foreach</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#990000">[</font>mm<font color="#990000">::</font>get_root_node<font color="#990000">]]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>node<font color="#990000">::</font>get <font color="#990000">--</font>node <font color="#009900">$subnode</font> <font color="#990000">--</font>position<font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{</font>right<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">incr</font></b> r_count
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">incr</font></b> l_count
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$l_count</font> <font color="#990000">&gt;</font> <font color="#009900">$r_count</font> <font color="#990000">?</font> <font color="#FF0000">{</font>right<font color="#FF0000">}</font> <font color="#990000">:</font> <font color="#FF0000">{</font>left<font color="#FF0000">}}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605234" ID="ID_1457012605234049" MODIFIED="1457012605234" TEXT="mm::get_root_node" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605234" ID="ID_1457012605234485" MODIFIED="1457012605234" TEXT="Retrieves the root node of the mind map.&#xA;&#xA;@return                The root node (central node in the mind map)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605234" ID="ID_1457012605234835" MODIFIED="1457012605234" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605244" ID="ID_1457012605244674" MODIFIED="1457012605244">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>get_root_node <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> dom <font color="#990000">[</font>get_dom<font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> root_node <font color="#990000">[</font><font color="#009900">$dom</font> selectNodes <font color="#FF0000">{</font>map<font color="#990000">/</font>node<font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$root_node</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> map_node <font color="#990000">[</font>_get_map_element<font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> root_node <font color="#990000">[</font>node<font color="#990000">::</font>create <font color="#990000">--</font>text root<font color="#990000">]</font>
                node<font color="#990000">::</font>add_subnode <font color="#009900">$map_node</font> <font color="#009900">$root_node</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$root_node</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605245" ID="ID_1457012605245269" MODIFIED="1457012605245" TEXT="mm::get_xml" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605245" ID="ID_1457012605245687" MODIFIED="1457012605245" TEXT="Retrieves the XML of the mind map.&#xA;&#xA;This XML is the content of what is stored in the FreeMind .mm files.&#xA;&#xA;@return                The XML of the mind map" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605246" ID="ID_1457012605246039" MODIFIED="1457012605246" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605255" ID="ID_1457012605255795" MODIFIED="1457012605255">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>get_xml <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists MM<font color="#990000">(</font>dom<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><font color="#009900">$MM</font><font color="#990000">(</font>dom<font color="#990000">)</font> asXML <font color="#990000">-</font>indent <font color="#993399">0</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605256" ID="ID_1457012605256379" MODIFIED="1457012605256" TEXT="mm::load" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605256" ID="ID_1457012605256846" MODIFIED="1457012605256" TEXT="Load a mind map based on a mind map file.&#xA;&#xA;Only one mind map can be loaded into the system at any one time.&#xA;&#xA;If the file does not exist then an empty mind map is created instead.&#xA;&#xA;@param file            The mind map file (.mm) to load into the system&#xA;@return                1 if the file was loaded, 0 if the given file does&#xA;                       not exist and a new empty mind map has been created" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605257" ID="ID_1457012605257205" MODIFIED="1457012605257" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605267" ID="ID_1457012605267300" MODIFIED="1457012605267">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font><b><font color="#0000FF">load</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">set</font></b> loaded <font color="#993399">0</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> xmlfd <font color="#990000">[</font>tDOM<font color="#990000">::</font>xmlOpenFile <font color="#009900">$file</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font>dom<font color="#990000">)</font> <font color="#990000">[</font>dom parse <font color="#990000">-</font>channel <font color="#009900">$xmlfd</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> loaded <font color="#993399">1</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                create_empty_mindmap
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font> <font color="#009900">$file</font>
        <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font>dirty<font color="#990000">)</font> <font color="#993399">0</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$loaded</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605267" ID="ID_1457012605267907" MODIFIED="1457012605267" TEXT="mm::mark_dirty" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605268" ID="ID_1457012605268378" MODIFIED="1457012605268" TEXT="Mark the mind map as dirty. This is used to indicate that changes&#xA;have been made to the mind map and need to be written to disk.&#xA;&#xA;This can be used to explicitly mark the mind map as dirty if the&#xA;DOM tree has been modified outside of the FreeMind API package." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605268" ID="ID_1457012605268686" MODIFIED="1457012605268" TEXT="@see" FOLDED="true">
<node CREATED="1457012605268" ID="ID_1457012605268887" MODIFIED="1457012605268" TEXT="mm::save" COLOR="#14666b" LINK="#ID_1457012605316941">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605269" ID="ID_1457012605269201" MODIFIED="1457012605269" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605278" ID="ID_1457012605278445" MODIFIED="1457012605278">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>mark_dirty <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font>dirty<font color="#990000">)</font> <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605279" ID="ID_1457012605279016" MODIFIED="1457012605279" TEXT="mm::node_id_search" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605279" ID="ID_1457012605279489" MODIFIED="1457012605279" TEXT="Searches the mind map for a node with a given ID.&#xA;&#xA;Each node in a mind map has a unique ID. This procedure can be used to&#xA;look up a given node if the ID is known.&#xA;&#xA;Synopsis:&#xA;   mm::node_id_search id&#xA;&#xA;@param id              The node ID to search the mind map for&#xA;@return                The node matching the given ID, or empty string&#xA;                       if not found." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605279" ID="ID_1457012605279842" MODIFIED="1457012605279" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605289" ID="ID_1457012605289473" MODIFIED="1457012605289">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>node_id_search <font color="#FF0000">{</font> id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">set</font></b> doc <font color="#990000">[</font><font color="#009900">$MM</font><font color="#990000">(</font>dom<font color="#990000">)</font> documentElement<font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><font color="#009900">$doc</font> selectNodes descendant<font color="#990000">::</font>node<font color="#990000">\[</font>@ID<font color="#990000">=</font><font color="#FF0000">'$id'</font><font color="#990000">\]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605290" ID="ID_1457012605290062" MODIFIED="1457012605290" TEXT="mm::node_path_create" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605290" ID="ID_1457012605290556" MODIFIED="1457012605290" TEXT="Similar to path search, but will create any nodes in the path&#xA;if they don't already exist.&#xA;&#xA;@param path            The path to the relevant node&#xA;@param root_node       &#xA;@param node_separator  Defines how the nodes in the path are separated,&#xA;                       defaults to &quot;&gt;&quot;.&#xA;@return                the last node in the path" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605290" ID="ID_1457012605290909" MODIFIED="1457012605290" TEXT="@see" FOLDED="true">
<node CREATED="1457012605291" ID="ID_1457012605291109" MODIFIED="1457012605291" TEXT="mm::node_path_search" COLOR="#14666b" LINK="#ID_1457012605303283">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605291" ID="ID_1457012605291427" MODIFIED="1457012605291" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605302" ID="ID_1457012605302658" MODIFIED="1457012605302">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>node_path_create <font color="#FF0000">{</font> path <font color="#FF0000">{</font>root_node <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>node_separator <font color="#FF0000">{</font><font color="#990000">&gt;</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>formatting <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> first <font color="#990000">&gt;</font> <font color="#009900">$path</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> split_path <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$path</font> <font color="#009900">$node_separator</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> split_path <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$path</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> parent <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$root_node</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">?</font> <font color="#990000">[</font>mm<font color="#990000">::</font>get_root_node<font color="#990000">]</font> <font color="#990000">:</font> <font color="#009900">$root_node</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> node_text <font color="#009900">$split_path</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>get_subnode <font color="#009900">$parent</font> <font color="#009900">$node_text</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$subnode</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> subnode <font color="#990000">[</font>node<font color="#990000">::</font>create <font color="#990000">--</font>text <font color="#009900">$node_text</font> <font color="#990000">--</font>parent <font color="#009900">$parent</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$formatting</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> parent <font color="#009900">$subnode</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$parent</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605303" ID="ID_1457012605303283" MODIFIED="1457012605303" TEXT="mm::node_path_search" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605303" ID="ID_1457012605303951" MODIFIED="1457012605303" TEXT="Searches the mind map for a node given a specific path.&#xA;&#xA;To look up the &quot;relevant node&quot; a path text search can be performed&#xA;if the ID of the node is not known.&#xA;&#xA;The path should include the text of all nodes in the path, including&#xA;the root node.&#xA;&#xA;Example mind map:&#xA;&#xA;  my mind map&#xA;      +----+path&#xA;             +----+to&#xA;                    +----+relevant node&#xA;&#xA;The path in this case would be &quot;my mind map&gt;path&gt;to&gt;relevant node&quot;.&#xA;&#xA;The node separator (defaults to &quot;&gt;&quot;) to be used can be specified.&#xA;&#xA;The path search functionality assumes that the node content is plain&#xA;text, i.e. does not contain rich content (html). If a &quot;path&quot; element&#xA;is an asterix then it will match all nodes. Note that a partial match&#xA;such as &quot;mynode&gt;all*&gt;nodes&quot; will not work, but &quot;mynode&gt;*&gt;nodes&quot; will.&#xA;&#xA;@param path            The path to the relevant node&#xA;@param root_node       The starting point for the path search. If undefined&#xA;                       or empty string then the full path to the node will&#xA;                       have to be provided.&#xA;@param node_separator  Defines how the nodes in the path are separated,&#xA;                       defaults to &quot;&gt;&quot;.&#xA;@return                The last node in the path, if found, otherwise&#xA;                       returns empty string. Can return multiple matches." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605304" ID="ID_1457012605304334" MODIFIED="1457012605304" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605316" ID="ID_1457012605316276" MODIFIED="1457012605316">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>node_path_search <font color="#FF0000">{</font> path <font color="#FF0000">{</font> root_node <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> node_separator <font color="#FF0000">{</font><font color="#990000">&gt;</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> first <font color="#009900">$node_separator</font> <font color="#009900">$path</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> split_path <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$path</font> <font color="#009900">$node_separator</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> split_path <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$path</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> text <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$split_path</font> <font color="#993399">0</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> subnodes <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$root_node</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">?</font> <font color="#990000">[</font>mm<font color="#990000">::</font>get_root_node<font color="#990000">]</font> <font color="#990000">:</font> <font color="#009900">$root_node</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> node_text <font color="#009900">$split_path</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> possible_match <font color="#FF0000">{}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$node_text</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">foreach</font></b> subnode <font color="#009900">$subnodes</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> possible_match <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$subnode</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">foreach</font></b> subnode <font color="#009900">$subnodes</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> match <font color="#990000">[</font>node<font color="#990000">::</font>get_subnode <font color="#009900">$subnode</font> <font color="#009900">$node_text</font><font color="#990000">]</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$match</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> possible_match <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$match</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> subnodes <font color="#009900">$possible_match</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$subnodes</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605316" ID="ID_1457012605316941" MODIFIED="1457012605316" TEXT="mm::save" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605317" ID="ID_1457012605317537" MODIFIED="1457012605317" TEXT="Saves a modified mind map to file.&#xA;&#xA;Only one mind map can be loaded into the system at any one time.&#xA;&#xA;Synopsis:&#xA;   mm::save ?--file &lt;file&gt;? ?--backup?&#xA;&#xA;@param --file &lt;file&gt;   Optional parameter that can be used to specify what&#xA;                       file to store the mind map to, if not provided the&#xA;                       file loaded will be used. If no file was loaded then&#xA;                       this proc will return silently with 0.&#xA;@param --backup        Optional parameter to indicate that the original file&#xA;                       should be backed up before changes are saved. The&#xA;                       backup file name will be appended with .bak.&lt;seconds&gt;&#xA;                       where the latter is the number of seconds since epoc.&#xA;                       This can be used to determine at what time and date&#xA;                       the backup was made.&#xA;@return                If the file was saved successfully then 1 is returned,&#xA;                       otherwise 0 is returned" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605317" ID="ID_1457012605317937" MODIFIED="1457012605317" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605329" ID="ID_1457012605329215" MODIFIED="1457012605329">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>save args <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> MM

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">set</font></b> idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">^-+</font><b><font color="#0000FF">file</font></b>$<font color="#FF0000">}</font><font color="#990000">]]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$args</font> <font color="#990000">[</font><b><font color="#0000FF">incr</font></b> idx<font color="#990000">]]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists MM<font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)]</font> <font color="#990000">||</font> <font color="#009900">$MM</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$MM</font><font color="#990000">(</font>dirty<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$args</font> <font color="#FF0000">{</font><font color="#990000">^-+</font>backup$<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">file</font></b> <b><font color="#0000FF">rename</font></b> <font color="#009900">$MM</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font> <font color="#009900">$MM</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">).</font>bak<font color="#990000">.[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> fp <font color="#990000">[</font><b><font color="#0000FF">open</font></b> <font color="#009900">$MM</font><font color="#990000">(</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font> w<font color="#990000">]</font>
        <b><font color="#0000FF">puts</font></b> <font color="#009900">$fp</font> <font color="#990000">[</font>get_xml<font color="#990000">]</font>
        <b><font color="#0000FF">close</font></b> <font color="#009900">$fp</font>
        <b><font color="#0000FF">set</font></b> MM<font color="#990000">(</font>dirty<font color="#990000">)</font> <font color="#993399">0</font>

        <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605329" ID="ID_1457012605329839" MODIFIED="1457012605329" TEXT="mm::sort_subnodes" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605330" ID="ID_1457012605330356" MODIFIED="1457012605330" TEXT="Sorts the child nodes of a node.&#xA;&#xA;This proc defaults to sort nodes based on text in ascending order. Optionally&#xA;the sorting comparision command can be specified with the second argument and&#xA;this defaults to use mm::compare_node_by_text for this purpose.&#xA;&#xA;@param node            The node whose child nodes to sort&#xA;@param sort_command    Specify the command to use for sorting the nodes" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605330" ID="ID_1457012605330672" MODIFIED="1457012605330" TEXT="@see" FOLDED="true">
<node CREATED="1457012605330" ID="ID_1457012605330888" MODIFIED="1457012605330" TEXT="mm::compare_node_by_text" COLOR="#14666b" LINK="#ID_1457012605147733">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012605331" ID="ID_1457012605331224" MODIFIED="1457012605331" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605341" ID="ID_1457012605341365" MODIFIED="1457012605341">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>sort_subnodes <font color="#FF0000">{</font> node <font color="#FF0000">{</font> sort_command mm<font color="#990000">::</font>compare_node_by_text <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> node_list <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> sort_list <font color="#990000">[</font><b><font color="#0000FF">lsort</font></b> <font color="#990000">-</font>command <font color="#009900">$sort_command</font> <font color="#009900">$node_list</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$node_list</font> <font color="#990000">!=</font> <font color="#009900">$sort_list</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> subnode <font color="#009900">$sort_list</font> <font color="#FF0000">{</font>
                        <font color="#009900">$node</font> removeChild <font color="#009900">$subnode</font>
                        <font color="#009900">$node</font> appendChild <font color="#009900">$subnode</font>
                <font color="#FF0000">}</font>
                mark_dirty
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605342" ID="ID_1457012605342021" MODIFIED="1457012605342" TEXT="mm::unfold_all" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605342" ID="ID_1457012605342521" MODIFIED="1457012605342" TEXT="Unfolds all given nodes and child nodes recursively. If no node is specified&#xA;then all nodes in the mind map are unfolded.&#xA;&#xA;Synopsis:&#xA;   mm::unfold_all ?node? ?node? ... ?&#xA;&#xA;@param node            A node in the mind map whose child nodes should be unfolded" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605342" ID="ID_1457012605342879" MODIFIED="1457012605342" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605353" ID="ID_1457012605353420" MODIFIED="1457012605353">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>unfold_all <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> args <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#990000">[</font>mm<font color="#990000">::</font>get_root_node<font color="#990000">]]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> node <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> subnodes <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnodes</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        mm<font color="#990000">::</font>unfold_all <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$subnodes</font>
                <font color="#FF0000">}</font>
                node<font color="#990000">::</font>unfold <font color="#009900">$node</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605354" ID="ID_1457012605354035" MODIFIED="1457012605354" TEXT="mm::unfold_leaf_nodes" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605354" ID="ID_1457012605354668" MODIFIED="1457012605354" TEXT="Unfolds all leaf nodes of a given node recursively. If no node&#xA;is specified then all leaf nodes in the mind map are unfolded.&#xA;&#xA;As it is possible to create folded nodes it is easy to end up&#xA;with leaf nodes that contains the folded attribute, which within&#xA;FreeMind makes the node look like it has subnodes.&#xA;&#xA;This is a convenience proc that removes the folded attribute from&#xA;all leaf nodes (i.e. nodes that does not have any child nodes).&#xA;&#xA;Synopsis: &#xA;   mm::unfold_leaf_nodes ?node? ?node? ... ?&#xA;&#xA;@param node            A node in the mind map whose leaf nodes should be unfolded" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605355" ID="ID_1457012605355037" MODIFIED="1457012605355" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605365" ID="ID_1457012605365334" MODIFIED="1457012605365">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>unfold_leaf_nodes <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> args <font color="#990000">[</font>mm<font color="#990000">::</font>get_root_node<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> node <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> subnodes <font color="#990000">[</font>node<font color="#990000">::</font>get_subnodes <font color="#009900">$node</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$subnodes</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>unfold <font color="#009900">$node</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        mm<font color="#990000">::</font>unfold_leaf_nodes <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$subnodes</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012605365" ID="ID_1457012605365995" MODIFIED="1457012605366" TEXT="mm::unfold_parents" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012605366" ID="ID_1457012605366505" MODIFIED="1457012605366" TEXT="Unfolds all parent nodes of a given node recursively.&#xA;&#xA;Synopsis:&#xA;  mm::unfold_parents ?node? ?node? ... ?&#xA;&#xA;@param node            The node who's parent nodes should be unfolded" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012605366" ID="ID_1457012605366861" MODIFIED="1457012605366" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012605377" ID="ID_1457012605377102" MODIFIED="1457012605377">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> mm<font color="#990000">::</font>unfold_parents <font color="#FF0000">{</font> args <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">foreach</font></b> node <font color="#009900">$args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> parent <font color="#990000">[</font><font color="#009900">$node</font> parentNode<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$parent</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><font color="#009900">$parent</font> nodeType<font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{</font>ELEMENT_NODE<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        node<font color="#990000">::</font>unfold <font color="#009900">$parent</font>
                        mm<font color="#990000">::</font>unfold_parents <font color="#009900">$parent</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>

