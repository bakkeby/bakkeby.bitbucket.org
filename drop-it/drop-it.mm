<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1457012571840" ID="ID_1457012571840273" MODIFIED="1457012571840" TEXT="drop-it" COLOR="#a72db7">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571841" ID="ID_1457012571841757" MODIFIED="1457012571841" TEXT="drop_it_type 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012571842" ID="ID_1457012571842046" MODIFIED="1457012571842" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571842" ID="ID_1457012571842228" MODIFIED="1457012571842" TEXT="tapp 1.0"/>
<node CREATED="1457012572355" ID="ID_1457012572355304" MODIFIED="1457012572355" TEXT="drop_it 1.0" LINK="#ID_1457012572108556"/>
</node>
<node CREATED="1457012571842" ID="ID_1457012571842476" MODIFIED="1457012571842" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571843" ID="ID_1457012571843188" MODIFIED="1457012571843" TEXT="type.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/type.tcl"/>
<node CREATED="1457012572356" ID="ID_1457012572356250" MODIFIED="1457012572356" TEXT="drop_it.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/drop_it.tcl"/>
</node>
<node CREATED="1457012571843" ID="ID_1457012571843392" MODIFIED="1457012571843" TEXT="type" COLOR="#ff8300"/>
<node CREATED="1457012572356" ID="ID_1457012572356470" MODIFIED="1457012572356" TEXT="drop_it" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012572363" ID="ID_1457012572363478" MODIFIED="1457012572363" TEXT="Drop-It configuration options.&#xA;&#xA;Most of these can be enabled from the command line by passing in flags to&#xA;the drop-it script/application. The rest may only be of interest if advanced&#xA;features are required.&#xA;&#xA;In general no configuration changes other than rules setup and perhaps log&#xA;directory are needed for normal usage." COLOR="#c14120">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572363" ID="ID_1457012572363778" MODIFIED="1457012572363" TEXT="@cfg" FOLDED="true">
<node CREATED="1457012572363" ID="ID_1457012572363976" MODIFIED="1457012572363" TEXT="DRY_RUN" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572364" ID="ID_1457012572364247" MODIFIED="1457012572364" TEXT="Do a dry-run, i.e. pretend to do the job, but don't change or move files around. This is enabled by passing ‑d / ‑‑dry‑run to drop-it." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572364" ID="ID_1457012572364515" MODIFIED="1457012572364" TEXT="RULES" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572364" ID="ID_1457012572364781" MODIFIED="1457012572364" TEXT="This configuration item defines all the drop-it rules." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572365" ID="ID_1457012572365044" MODIFIED="1457012572365" TEXT="ACTION_RULE_MATCH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572365" ID="ID_1457012572365312" MODIFIED="1457012572365" TEXT="Determines whether to enable rule actions. Defaults to being enabled, but can be disabled by passing ‑‑no_action to drop-it. This may be considered as an alternative to a dry‑run and can be useful in a few special cases, but in general the recommendation would be to do a dry‑run instead." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572365" ID="ID_1457012572365574" MODIFIED="1457012572365" TEXT="CASE_SENSITIVE_RENAME_ACTION" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572365" ID="ID_1457012572365832" MODIFIED="1457012572365" TEXT="This option enables case sensitive rename operations for files being mangled with with the ‑‑rename action." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572366" ID="ID_1457012572366098" MODIFIED="1457012572366" TEXT="CFG_FILE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572366" ID="ID_1457012572366370" MODIFIED="1457012572366" TEXT="This defines the name and location of the rules configuration file. Defaults to &quot;drop_it_rules.cfg&quot; in the drop-it script directory." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572366" ID="ID_1457012572366632" MODIFIED="1457012572366" TEXT="CHECKSUM_DROPIT_SUFFIX" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572366" ID="ID_1457012572366906" MODIFIED="1457012572366" TEXT="When moving files to the &quot;trash&quot; directory the file is appended with a drop-it suffix to 1) indicate that it has been moved there by drop-it and 2) avoid name clashes and overwriting files. If this config is enabled then a checksum (MD5 or otherwise) is used as the suffix. The benefit of this is that it to some degree prevents duplicate files taking up space in the &quot;trash&quot; directory. Defaults to 1 (enabled)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572367" ID="ID_1457012572367165" MODIFIED="1457012572367" TEXT="CHECKSUM_CMD" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572367" ID="ID_1457012572367425" MODIFIED="1457012572367" TEXT="This defines the command to execute in order to calculate the file checksum. Defaults to: md5sum &quot;%file%&quot;. The %file% placeholder is substituted with the path to the file when the command is being executed." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572367" ID="ID_1457012572367683" MODIFIED="1457012572367" TEXT="CONTAINS_EXEC_DEFAULT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572367" ID="ID_1457012572367963" MODIFIED="1457012572367" TEXT="This defines the executables to execute in relation to the ‑contains check. There are two placeholders: %file% which will be replaced by the file being processed and %contains% which will be replaced by the ‑contains expression for the rule. This configuration item defines the default command to execute if one has not been defined for the relevant file type (this will typically be a standard grep). This is made configurable for cross-platform purposes and to allow customised searches through files. Example configuration: grep -Ei &quot;%contains%&quot; &quot;%file%&quot;" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572368" ID="ID_1457012572368228" MODIFIED="1457012572368" TEXT="CONTAINS_EXEC_&lt;file_extension&gt;" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572368" ID="ID_1457012572368504" MODIFIED="1457012572368" TEXT="Custom command for file type specific contains checks. The extension is in uppercase and dots are replaced by underscores. For example we could match on a .tar.gz file based on zgrep by defining CONTAINS_EXEC_TAR_GZ = zgrep -Ei &quot;%contains% %file%, or we could check if the archive contains a file name matching the contains expression with: tar -ztf %file%  | grep -Ei &quot;%contains%&quot;. Other alternatives could be to use pdftotext to search through the content of PDF files, to use unoconv to search through .doc and many other document formats, or exiftool to match on image EXIF data." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572368" ID="ID_1457012572368771" MODIFIED="1457012572368" TEXT="CUSTOM_RULE_PARAM_OVERRIDE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572369" ID="ID_1457012572369040" MODIFIED="1457012572369" TEXT="Experimental functionality allowing override of rule definitions. This is primarily intended for plugins / extensions." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572369" ID="ID_1457012572369298" MODIFIED="1457012572369" TEXT="DEFAULT_CASE_SENSITIVE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572369" ID="ID_1457012572369555" MODIFIED="1457012572369" TEXT="This indicates whether rule checks are by default case sensitive unless specifically indicated. Defaults to enabled." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572369" ID="ID_1457012572369813" MODIFIED="1457012572369" TEXT="DEFAULT_RULE_STATUS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572370" ID="ID_1457012572370077" MODIFIED="1457012572370" TEXT="This determines the default status of rules unless explicitly defined in the rules configuration setup. This defaults to &quot;A&quot; (active) and individual rules can be suspended (status &quot;S&quot;) by using the ‑‑suspended flag in the drop-it rules configuration." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572370" ID="ID_1457012572370351" MODIFIED="1457012572370" TEXT="DEF_TRASH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572370" ID="ID_1457012572370611" MODIFIED="1457012572370" TEXT="The default &quot;trash&quot; directory to move &quot;deleted&quot; files to." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572370" ID="ID_1457012572370872" MODIFIED="1457012572370" TEXT="DELETE_DIRECTORY_IF_EMPTY_AFTER_RECURSE" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572371" ID="ID_1457012572371140" MODIFIED="1457012572371" TEXT="When using the ‑‑recurse action to process files, this config item indicates whether to attempt to remove the directory if it is empty after processing all files. Defaults to enabled. This is more for convenience as it may be annoying with left-over empty directories." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572371" ID="ID_1457012572371402" MODIFIED="1457012572371" TEXT="EXPLAIN_RULE_MATCH" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572371" ID="ID_1457012572371671" MODIFIED="1457012572371" TEXT="This is enabled by passing ‑e / ‑‑explain to drop-it. Enabling this in config is the same as always passing the ‑‑explain option to drop-it which enables additional output to explain why a file did or did not match a given rule." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572371" ID="ID_1457012572371934" MODIFIED="1457012572371" TEXT="EXPLAIN_ALL_RULES" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572372" ID="ID_1457012572372199" MODIFIED="1457012572372" TEXT="This is enabled by passing ‑a / ‑‑explain_all to drop-it. By enabling this additional explain output will be printed for all rules whether they are relevant to the given file or not." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572372" ID="ID_1457012572372461" MODIFIED="1457012572372" TEXT="EXPLAIN_CHECK_INDENT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572372" ID="ID_1457012572372723" MODIFIED="1457012572372" TEXT="Look and feel option which determines the indent used for the explain output, defaults to 3." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572372" ID="ID_1457012572372987" MODIFIED="1457012572372" TEXT="FILE_DATE_PREFIX_FORMAT" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572373" ID="ID_1457012572373255" MODIFIED="1457012572373" TEXT="If a rule specifies the ‑‑date action then the matching file will be prefixed with a date. This configuration item specifies the date format to use and it defaults to &quot;%Y.%m.%d.&quot; (clock format)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572373" ID="ID_1457012572373518" MODIFIED="1457012572373" TEXT="LOG_RULES" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572373" ID="ID_1457012572373789" MODIFIED="1457012572373" TEXT="If enabled then all rules in use will be logged when files are being processed. This might be useful if rules are changing often or an accurate record of what rules were in play when a certain match was made is needed. Enable via config, defaults to disabled. ‑‑rules parameter to drop-it." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572374" ID="ID_1457012572374057" MODIFIED="1457012572374" TEXT="NO_REPORT_ACTIONS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572374" ID="ID_1457012572374337" MODIFIED="1457012572374" TEXT="This lists actions which should not be reported as a match (i.e. process silently) unless verbose output or debug log level is enabled. This defaults to the &quot;stop&quot; action." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572374" ID="ID_1457012572374602" MODIFIED="1457012572374" TEXT="NOTIFY" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572374" ID="ID_1457012572374869" MODIFIED="1457012572374" TEXT="This enables desktop notification of the drop-it report. This is enabled with the ‑‑notify parameter and is currently limited to Linux operating systems." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572375" ID="ID_1457012572375133" MODIFIED="1457012572375" TEXT="PRINT_ARGUMENTS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572375" ID="ID_1457012572375405" MODIFIED="1457012572375" TEXT="This enables printing of all the parameters that are passed to drop-it. This can be useful if drop-it is being called by some background task such as a cronjob for example. This can also be enabled by passing in the ‑‑list_args parameter to drop-it." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572375" ID="ID_1457012572375671" MODIFIED="1457012572375" TEXT="PRINT_CONFIGURATION" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572375" ID="ID_1457012572375938" MODIFIED="1457012572375" TEXT="Enables listing of all configuration items on startup. This can be enabled by passing in the ‑‑list_conf parameter to drop-it." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572376" ID="ID_1457012572376200" MODIFIED="1457012572376" TEXT="PRINT_RULES" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572376" ID="ID_1457012572376467" MODIFIED="1457012572376" TEXT="Enables listing of all rules on startup. This can be enabled by passing in the ‑‑rules parameter to drop-it." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572376" ID="ID_1457012572376733" MODIFIED="1457012572376" TEXT="TIME" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572376" ID="ID_1457012572377003" MODIFIED="1457012572377" TEXT="Enables timing of rules for the current run. When enabled the list of rules will be printed along with information about how long time it took to check each rule against the given file(s)." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572377" ID="ID_1457012572377273" MODIFIED="1457012572377" TEXT="FUNC_STATS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572377" ID="ID_1457012572377552" MODIFIED="1457012572377" TEXT="Enables tracking of long time rule statistics. This includes rules performance (number of checks performed, number of matches and average time spent performing matches). If this is enabled then additional information is added to the ‑‑rules option output which indicates which rules are being used and which can be removed." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572377" ID="ID_1457012572377823" MODIFIED="1457012572377" TEXT="STAT_RUN_IN_DAYS" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572378" ID="ID_1457012572378123" MODIFIED="1457012572378" TEXT="Defines the period (number of days) we are interested in with regards to evaluating how relevant the drop-it rules are in the long run. Defaults to 90 days. In practice this means that it will take 90 days to gather statistics and a further 90 days will pass before negative rules are starting to display. Afterwards we should have a relatively good impression of what rules are actually being used and which ones can be removed. When listing rules one or more stars ( * ) indicates that the rule has matched a file within 72 days (40% of 180). One or more hyphens ( - ) means that no files have matched the given rule within 108 days (lower 40% of 180). If no stars or hyphens are displayed then that means that the last time the rule matched a file was in the middle 20% (i.e. between 72 and 108 days ago). Note that changing this config only affects the * column when listing rules, so is perfectly safe to play around with." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572378" ID="ID_1457012572378413" MODIFIED="1457012572378" TEXT="STATS_EXTENSION" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572378" ID="ID_1457012572378688" MODIFIED="1457012572378" TEXT="If FUNC_STATS is enabled then a stats file will be saved with the same name, but different file extension, as the rules configuration file. This config determines the extension to use. Defaults to &quot;.stat&quot;." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572378" ID="ID_1457012572378958" MODIFIED="1457012572378" TEXT="PARSE_RULES_ON_CHANGE_ONLY" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572379" ID="ID_1457012572379249" MODIFIED="1457012572379" TEXT="If drop-it is set up with a significant amount of rules then only parsing the rules when the rules file has changed may have performance benefits. If this is enabled then the parsed rules are dumped to a .db file that are loaded on the next run. The .db file will be replaced whenever the timestamp on the rules configuration file changes. The performance benefit of enabling this is unlikely to be noticeable with less than 300 rules. Also note that if the rules have OS specific variance then enabling this may have unforeseen consequences." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572379" ID="ID_1457012572379524" MODIFIED="1457012572379" TEXT="HELP_EXAMPLES" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572379" ID="ID_1457012572379797" MODIFIED="1457012572379" TEXT="Config item for internal use. Contains the information given when the ‑‑examples flag is passed in." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
<node CREATED="1457012572380" ID="ID_1457012572380067" MODIFIED="1457012572380" TEXT="LIMIT_TO_RULES" COLOR="#14666b" FOLDED="true">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
<node CREATED="1457012572380" ID="ID_1457012572380352" MODIFIED="1457012572380" TEXT="Config item for internal use. Specifies which of the rules in the rules configuration file that should be used. This config item is set when the ‑‑rule parameter is used." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
<node CREATED="1457012572380" ID="ID_1457012572380599" MODIFIED="1457012572380" TEXT="@see" FOLDED="true">
<node CREATED="1457012572380" ID="ID_1457012572380781" MODIFIED="1457012572380" TEXT="tapp_log for logging configuration options" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
</node>
</node>
</node>
<node CREATED="1457012571844" ID="ID_1457012571844262" MODIFIED="1457012571844" TEXT="drop_it_stats 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012571844" ID="ID_1457012571844515" MODIFIED="1457012571844" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571845" ID="ID_1457012571845201" MODIFIED="1457012571845" TEXT="stats.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/stats.tcl"/>
</node>
<node CREATED="1457012571845" ID="ID_1457012571845398" MODIFIED="1457012571845" TEXT="stats" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012571845" ID="ID_1457012571845742" MODIFIED="1457012571845" TEXT="stats::get" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571846" ID="ID_1457012571846015" MODIFIED="1457012571846" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571855" ID="ID_1457012571855784" MODIFIED="1457012571855">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> stats<font color="#990000">::</font>get <font color="#FF0000">{</font>key<font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists <font color="#990000">::</font>STATS<font color="#990000">(</font><font color="#009900">$key</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> $<font color="#990000">::</font>STATS<font color="#990000">(</font><font color="#009900">$key</font><font color="#990000">)</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900">#      cr_date num_checks avg_ms num_matches last_match last_update</font></i>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#993399">0</font> <font color="#993399">0</font> <font color="#993399">0</font> <font color="#FF0000">{}</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571856" ID="ID_1457012571856553" MODIFIED="1457012571856" TEXT="stats::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571856" ID="ID_1457012571856955" MODIFIED="1457012571856" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571866" ID="ID_1457012571866297" MODIFIED="1457012571866">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> stats<font color="#990000">::</font>init <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> DIRTY

        <b><font color="#0000FF">set</font></b> pname <font color="#990000">[</font>opt<font color="#990000">::</font><b><font color="#0000FF">proc</font></b> <font color="#990000">::</font>stats <font color="#990000">--</font>preset array<font color="#990000">]</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1457012571866" ID="ID_1457012571866976" MODIFIED="1457012571866" TEXT="drop_it_rule 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012571867" ID="ID_1457012571867277" MODIFIED="1457012571867" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571867" ID="ID_1457012571867471" MODIFIED="1457012571867" TEXT="tapp 1.0"/>
<node CREATED="1457012571867" ID="ID_1457012571867602" MODIFIED="1457012571867" TEXT="drop_it_check 1.0" LINK="#ID_1457012572396088"/>
<node CREATED="1457012571867" ID="ID_1457012571867729" MODIFIED="1457012571867" TEXT="drop_it_action 1.0" LINK="#ID_1457012572902157"/>
<node CREATED="1457012571867" ID="ID_1457012571867853" MODIFIED="1457012571867" TEXT="drop_it_destination 1.0" LINK="#ID_1457012572381190"/>
</node>
<node CREATED="1457012571868" ID="ID_1457012571868087" MODIFIED="1457012571868" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571868" ID="ID_1457012571868904" MODIFIED="1457012571868" TEXT="rule.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/rule.tcl"/>
</node>
<node CREATED="1457012571869" ID="ID_1457012571869109" MODIFIED="1457012571869" TEXT="rule" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012571869" ID="ID_1457012571869634" MODIFIED="1457012571869" TEXT="rule::action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571869" ID="ID_1457012571869915" MODIFIED="1457012571869" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571883" ID="ID_1457012571883213" MODIFIED="1457012571883">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>action <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE
        <b><font color="#0000FF">variable</font></b> current_rule_id

        <b><font color="#0000FF">set</font></b> rule_id <font color="#009900">$current_rule_id</font>

        <i><font color="#9A1900"># If rule_id is empty it means there were no rules that matched</font></i>
        <i><font color="#9A1900"># the input, thus no action to be executed.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule_id</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        touch RPT
        <b><font color="#0000FF">set</font></b> action_ids <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule action_id<font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> action_id <font color="#009900">$action_ids</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> action         <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#990000">[::</font>action get <font color="#009900">$action_id</font><font color="#990000">]]</font>
                <b><font color="#0000FF">set</font></b> action         <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$action</font> <font color="#FF0000">{</font><font color="#990000">-</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> destination_id <font color="#990000">[::</font>action destination_id <font color="#009900">$action_id</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> destination    <font color="#990000">[</font>destination <b><font color="#0000FF">subst</font></b> <font color="#009900">$destination_id</font> <font color="#009900">$input</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> flags          <font color="#FF0000">{}</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>action<font color="#990000">::</font>$<font color="#FF0000">{</font>action<font color="#FF0000">}</font>_action <font color="#009900">$input</font> <font color="#009900">$destination</font> <font color="#009900">$flags</font><font color="#990000">]</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        lassign <font color="#009900">$output</font> result_code dest
                        <b><font color="#0000FF">set</font></b> output <font color="#009900">$dest</font>
                        <b><font color="#0000FF">set</font></b> actioned <font color="#009900">$action</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$result_code</font> <font color="#990000">==</font> <font color="#FF0000">{</font>NOT_OK<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> actioned <font color="#FF0000">{</font> fail<font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>

                        <i><font color="#9A1900"># If the directory has changed, then only set the directory as the destination.</font></i>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dest</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$input</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$dest</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$dest</font><font color="#990000">]</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">[</font>cfg get NO_REPORT_ACTIONS <font color="#FF0000">{</font>stop<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#009900">$action</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font>
                                <font color="#990000">||</font> <font color="#990000">[</font>tapp_log<font color="#990000">::</font>log_level_enabled DEBUG<font color="#990000">]</font>
                                <font color="#990000">||</font> <font color="#990000">[</font>cfg enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> RPT <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$actioned</font> <font color="#009900">$input</font> <font color="#009900">$destination</font> <font color="#009900">$output</font> <font color="#009900">$rule_id</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>

                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$result_code</font> <font color="#990000">!=</font> <font color="#FF0000">"OK"</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">break</font></b>
                        <font color="#FF0000">}</font>

                        <i><font color="#9A1900"># If the input has changed as part of this action</font></i>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dest</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$dest</font> <font color="#990000">!=</font> <font color="#009900">$input</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                log DEBUG <font color="#FF0000">{</font>Overriding input <b><font color="#0000FF">file</font></b> <font color="#FF0000">"$input"</font> with <font color="#FF0000">"$dest"</font><font color="#FF0000">}</font>
                                <b><font color="#0000FF">set</font></b> input <font color="#009900">$dest</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> actioning <font color="#990000">[</font>text_utils<font color="#990000">::</font>ingify <font color="#009900">$action</font><font color="#990000">]</font>
                        log ERROR <font color="#FF0000">{</font>Error <font color="#009900">$actioning</font> <font color="#009900">$input</font> <font color="#990000">==&gt;</font> <font color="#009900">$destination</font> due to <font color="#009900">$msg</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">break</font></b>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$RPT</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571883" ID="ID_1457012571883841" MODIFIED="1457012571883" TEXT="rule::add" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571884" ID="ID_1457012571884478" MODIFIED="1457012571884" TEXT="Parses and adds rules based on the rule parameter definitions.&#xA;&#xA;See rule::init for details." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012571884" ID="ID_1457012571884837" MODIFIED="1457012571884" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571897" ID="ID_1457012571897956" MODIFIED="1457012571897">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>add args <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$args</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        log DEV <font color="#FF0000">{</font>rule<font color="#990000">::</font>add called with args<font color="#990000">:</font> <font color="#009900">$args</font><font color="#FF0000">}</font>
        touch check_flags action_flag types

        <b><font color="#0000FF">set</font></b> match_check <font color="#990000">[</font>cfg get DEFAULT_MATCH_CHECK name<font color="#990000">]</font>

        <b><font color="#0000FF">variable</font></b> PARAMS
        <b><font color="#0000FF">foreach</font></b> param <font color="#009900">$PARAMS</font><font color="#990000">(</font>parameters<font color="#990000">)</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> value <font color="#990000">[</font>_get_param <font color="#009900">$args</font> <font color="#009900">$param</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$param</font> <font color="#990000">!=</font> <font color="#FF0000">{</font><font color="#990000">--</font>rule<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$value</font> <font color="#990000">==</font> <font color="#FF0000">{</font>enabled<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> value <font color="#FF0000">{}</font>
                <font color="#FF0000">}</font>
                lassign <font color="#009900">$PARAMS</font><font color="#990000">(</font><font color="#009900">$param</font><font color="#990000">)</font> <font color="#990000">-</font> <font color="#990000">-</font> <font color="#990000">-</font> param_type
                <b><font color="#0000FF">set</font></b> check_type <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#990000">-]</font>

                <i><font color="#9A1900"># Certain parameter types needs to be dealt with in a specific manner</font></i>
                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$param_type</font> <font color="#FF0000">{</font>
                rule <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># Expected to be the first parameter type that is checked</font></i>
                        <b><font color="#0000FF">set</font></b> rule_id <font color="#009900">$value</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule_id</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">||</font> <font color="#990000">[</font>rule<font color="#990000">::</font>exists <font color="#009900">$rule_id</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rule_id <font color="#990000">[</font>rule<font color="#990000">::</font>_get_next_rule_id<font color="#990000">]</font>
                        <font color="#FF0000">}</font>

                        rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
                        rule<font color="#990000">::</font>set_rule status <font color="#990000">[</font>cfg get DEFAULT_RULE_STATUS A<font color="#990000">]</font>
                <font color="#FF0000">}</font>
                check_flag <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> check_flags <font color="#009900">$param</font>
                <font color="#FF0000">}</font>
                action_flag <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> action_flags <font color="#009900">$param</font>
                <font color="#FF0000">}</font>
                match_flag <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># A match flag of path indicates that we should do a path check when</font></i>
                        <i><font color="#9A1900"># adding regexp or glob matching, otherwise match on file name.</font></i>
                        <b><font color="#0000FF">set</font></b> match_check <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trimleft <font color="#009900">$param</font> <font color="#990000">-]</font>
                <font color="#FF0000">}</font>
                check <font color="#FF0000">{</font>
                        rule<font color="#990000">::</font>set_rule <font color="#009900">$param_type</font> <font color="#009900">$check_type</font> <font color="#009900">$value</font> <font color="#009900">$check_flags</font>
                <font color="#FF0000">}</font>
                action <font color="#FF0000">{</font>
                        rule<font color="#990000">::</font>set_rule <font color="#009900">$param_type</font> <font color="#009900">$check_type</font> <font color="#009900">$value</font> <font color="#FF0000">{</font><font color="#009900">$action_flag</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                type <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> types <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#009900">$value</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                match <font color="#FF0000">{</font>
                        rule<font color="#990000">::</font>set_rule check <font color="#009900">$match_check</font> <font color="#009900">$value</font> <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$check_flags</font> <font color="#990000">-</font><font color="#009900">$check_type</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        rule<font color="#990000">::</font>set_rule <font color="#009900">$param_type</font> <font color="#009900">$value</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Finally add the type check (if relevant)</font></i>
        <i><font color="#9A1900"># A sorted list prevents duplicates when adding type checks</font></i>
        <b><font color="#0000FF">set</font></b> types <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#990000">[</font><b><font color="#0000FF">lsort</font></b> <font color="#009900">$types</font><font color="#990000">]</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$types</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>set_rule check type <font color="#009900">$types</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">lappend</font></b> <font color="#990000">::</font>rule<font color="#990000">::</font>RULE<font color="#990000">(</font>rule_ids<font color="#990000">)</font> <font color="#009900">$rule_id</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571898" ID="ID_1457012571898612" MODIFIED="1457012571898" TEXT="rule::exists" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571898" ID="ID_1457012571898970" MODIFIED="1457012571898" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571908" ID="ID_1457012571908956" MODIFIED="1457012571908">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>exists <font color="#FF0000">{</font> rule_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists RULE<font color="#990000">(</font>rule_ids<font color="#990000">)]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$RULE</font><font color="#990000">(</font>rule_ids<font color="#990000">)</font> <font color="#009900">$rule_id</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">1</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571909" ID="ID_1457012571909543" MODIFIED="1457012571909" TEXT="rule::explain" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571909" ID="ID_1457012571909906" MODIFIED="1457012571909" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571923" ID="ID_1457012571923431" MODIFIED="1457012571923">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>explain <font color="#FF0000">{</font> input <font color="#FF0000">{</font>include_checks <font color="#993399">1</font><font color="#FF0000">}}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> PARAMS
        <b><font color="#0000FF">variable</font></b> current_rule_id

        <b><font color="#0000FF">set</font></b> rule_id <font color="#009900">$current_rule_id</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>rule<font color="#990000">::</font>exists <font color="#009900">$rule_id</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>Rule <font color="#990000">%</font>s does not exist<font color="#FF0000">}</font> <font color="#009900">$rule_id</font><font color="#990000">]]</font>
        <font color="#FF0000">}</font>

        touch output destination actions

        <b><font color="#0000FF">set</font></b> action_ids <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule action_id<font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$action_ids</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>Rule <font color="#990000">%</font>s<font color="#990000">:</font> <font color="#990000">%</font>s<font color="#FF0000">}</font> <font color="#009900">$rule_id</font> <font color="#009900">$input</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> action_id <font color="#009900">$action_ids</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> action <font color="#990000">[::</font>action get <font color="#009900">$action_id</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> destination_id <font color="#990000">[::</font>action destination_id <font color="#009900">$action_id</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> destination <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">[</font>destination <b><font color="#0000FF">subst</font></b> <font color="#009900">$destination_id</font> <font color="#009900">$input</font><font color="#990000">]]</font>

                <b><font color="#0000FF">set</font></b> num_args <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$PARAMS</font><font color="#990000">(--</font><font color="#009900">$action</font><font color="#990000">)</font> <font color="#993399">1</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> action_disp <font color="#990000">[</font><b><font color="#0000FF">string</font></b> totitle <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#990000">--</font> <font color="#FF0000">{}}</font> <font color="#009900">$action</font><font color="#990000">]]</font>
                <b><font color="#0000FF">set</font></b> action_disp <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font>Exec Execute<font color="#FF0000">}</font> <font color="#009900">$action_disp</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$destination</font> <font color="#990000">==</font> <font color="#FF0000">{{}}</font> <font color="#990000">||</font> <font color="#009900">$num_args</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>Rule <font color="#990000">%</font>s<font color="#990000">:</font> <font color="#990000">%</font>s <font color="#990000">%</font>s<font color="#FF0000">}</font> <font color="#009900">$rule_id</font> <font color="#009900">$action_disp</font> <font color="#009900">$input</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#FF0000">{</font><b><font color="#0000FF">exec</font></b><font color="#FF0000">}</font> <font color="#009900">$action</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>Rule <font color="#990000">%</font>s<font color="#990000">:</font> <font color="#990000">%</font>s <font color="#990000">%</font>s<font color="#FF0000">}</font> <font color="#009900">$rule_id</font> <font color="#009900">$action_disp</font> <font color="#009900">$destination</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font>Rule <font color="#990000">%</font>s<font color="#990000">:</font> <font color="#990000">%</font>s <font color="#990000">%</font>s to <font color="#990000">%</font>s <font color="#FF0000">}</font> <font color="#009900">$rule_id</font> <font color="#009900">$action_disp</font> <font color="#009900">$input</font> <font color="#009900">$destination</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$include_checks</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_indent <font color="#990000">[</font>cfg get EXPLAIN_CHECK_INDENT <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">foreach</font></b> check_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule check_ids<font color="#990000">]</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#009900">$check_indent</font><font color="#990000">][</font><b><font color="#0000FF">join</font></b> <font color="#990000">[</font>check<font color="#990000">::</font>explain <font color="#009900">$check_id</font> <font color="#009900">$input</font> <font color="#993399">1</font><font color="#990000">]</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571924" ID="ID_1457012571924066" MODIFIED="1457012571924" TEXT="rule::get_all_rules" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571924" ID="ID_1457012571924422" MODIFIED="1457012571924" TEXT="Returns all rules within the system" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012571924" ID="ID_1457012571924771" MODIFIED="1457012571924" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571934" ID="ID_1457012571934111" MODIFIED="1457012571934">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>get_all_rules <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$RULE</font><font color="#990000">(</font>rule_ids<font color="#990000">)</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571935" ID="ID_1457012571935026" MODIFIED="1457012571935" TEXT="rule::get_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571935" ID="ID_1457012571935735" MODIFIED="1457012571935" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571945" ID="ID_1457012571945775" MODIFIED="1457012571945">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>get_array <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>array get RULE<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571946" ID="ID_1457012571946428" MODIFIED="1457012571946" TEXT="rule::get_rule" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571946" ID="ID_1457012571946825" MODIFIED="1457012571946" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571959" ID="ID_1457012571959187" MODIFIED="1457012571959">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>get_rule <font color="#FF0000">{</font> option <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE
        <b><font color="#0000FF">variable</font></b> current_rule_id

        touch output

        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$option</font> <font color="#FF0000">{</font>
        action_id <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>action_id<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> output <font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>action_id<font color="#990000">)</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        action <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> action_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule action_id<font color="#990000">]</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">--[::</font>action get <font color="#009900">$action_id</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> dest <font color="#990000">[::</font>action destination <font color="#009900">$action_id</font><font color="#990000">]</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dest</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> output <font color="#009900">$dest</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        check_ids <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_ids <font color="#FF0000">{}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>check_ids<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> check_ids <font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>check_ids<font color="#990000">)</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>check<font color="#990000">::</font>get_optimal_check_order <font color="#009900">$check_ids</font><font color="#990000">]</font> 
        <font color="#FF0000">}</font>
        hash <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>hash<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> hashstr <font color="#990000">[</font>get_rule action<font color="#990000">]</font>
                        <b><font color="#0000FF">foreach</font></b> check_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule check_ids<font color="#990000">]</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> hashstr <font color="#990000">[</font>check<font color="#990000">::</font>to_string <font color="#009900">$check_id</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> hash <font color="#990000">[::</font>md5<font color="#990000">::</font>md5 <font color="#990000">-</font>hex <font color="#009900">$hashstr</font><font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>hash<font color="#990000">)</font> <font color="#009900">$hash</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> output <font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>hash<font color="#990000">)</font>
        <font color="#FF0000">}</font>
        status <font color="#990000">-</font>
        desc <font color="#990000">-</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font><font color="#009900">$option</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> output <font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font><font color="#009900">$option</font><font color="#990000">)</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571959" ID="ID_1457012571959812" MODIFIED="1457012571959" TEXT="rule::get_rules" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571960" ID="ID_1457012571960170" MODIFIED="1457012571960" TEXT="Returns currently relevant rules, may be limited to a specific set." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012571960" ID="ID_1457012571960508" MODIFIED="1457012571960" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571971" ID="ID_1457012571971509" MODIFIED="1457012571971">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>get_rules <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        touch order_list rule_list

        <b><font color="#0000FF">foreach</font></b> rule_id <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg exists LIMIT_TO_RULES<font color="#990000">]</font> <font color="#990000">?</font> <font color="#990000">[</font>cfg get LIMIT_TO_RULES<font color="#990000">]</font> <font color="#990000">:</font> <font color="#009900">$RULE</font><font color="#990000">(</font>rule_ids<font color="#990000">)</font><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">incr</font></b> idx
                <b><font color="#0000FF">lappend</font></b> order_list <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$rule_id</font> <font color="#990000">[</font>nvl <font color="#FF0000">{</font><font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">,</font>order<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#009900">$idx</font><font color="#990000">]]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> order <font color="#990000">[</font><b><font color="#0000FF">lsort</font></b> <font color="#990000">-</font>integer <font color="#990000">-</font>index <font color="#993399">1</font> <font color="#009900">$order_list</font><font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> rule_list <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$order</font> <font color="#993399">0</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$rule_list</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571972" ID="ID_1457012571972102" MODIFIED="1457012571972" TEXT="rule::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571972" ID="ID_1457012571972633" MODIFIED="1457012571972" TEXT="Notes on adding new checks:&#xA;  - add tests in check.test&#xA;  - add tests in drop_it.test&#xA;  - add parameter details to this proc&#xA;  - describe check the comment above&#xA;  - add check procs (named check::check_&lt;check&gt;) in check.tcl&#xA;  - if the new checks are of performance concern, consider changing&#xA;    the check order in check::init in check.tcl&#xA;  - add check to proc check::to_string in check.tcl&#xA;  - if adding a new parameter type check that it is captured in proc&#xA;    rule::add in rule.tcl&#xA;&#xA;Notes on adding new actions:&#xA;  - add tests in action.test&#xA;  - add tests in drop_it.test&#xA;  - add parameter details to this proc&#xA;  - describe action the comment above&#xA;  - add action procs in action.tcl" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012571972" ID="ID_1457012571972980" MODIFIED="1457012571972" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012571994" ID="ID_1457012571994530" MODIFIED="1457012571994">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>init <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> INIT
        <b><font color="#0000FF">variable</font></b> PARAMS

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists INIT<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$INIT</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Optional parameters:</font></i>
        <i><font color="#9A1900">#          name          regexp                  num_args  default  param_type</font></i>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>rule<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>rule$<font color="#FF0000">}</font>             <font color="#993399">1</font>         <font color="#FF0000">{}</font>       rule<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>status<font color="#990000">)</font>    <font color="#FF0000">{{</font><font color="#990000">^--?</font>status$<font color="#FF0000">}</font>           <font color="#993399">1</font>         A        status<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>suspended<font color="#990000">)</font> <font color="#FF0000">{{</font><font color="#990000">^--?</font>susp<font color="#990000">(</font>ended<font color="#990000">)</font><font color="#FF0000">}</font>       <font color="#993399">0</font>         S        status<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(</font><i><font color="#9A1900">#)           {{^(#|--)$}              0         S        status}</font></i>

        <i><font color="#9A1900"># Actions</font></i>
        <i><font color="#9A1900">#          name          regexp                  num_args  default  param_type</font></i>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>move<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>move$<font color="#FF0000">}</font>             <font color="#993399">1</font>         <font color="#FF0000">{}</font>       action<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>delete<font color="#990000">)</font>    <font color="#FF0000">{{</font><font color="#990000">^--?</font>del<font color="#990000">(</font>ete<font color="#990000">)?</font>$<font color="#FF0000">}</font>        <font color="#993399">1</font>         enabled  action<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font><b><font color="#0000FF">exec</font></b><font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font><b><font color="#0000FF">exec</font></b><font color="#990000">(</font>ute<font color="#990000">)?</font>$<font color="#FF0000">}</font>       <font color="#993399">1</font>         <font color="#FF0000">{}</font>       action<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>recurse<font color="#990000">)</font>   <font color="#FF0000">{{</font><font color="#990000">^--?</font>recurse$<font color="#FF0000">}</font>          <font color="#993399">1</font>         <font color="#FF0000">{</font>f d r<font color="#FF0000">}</font>  action<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font><b><font color="#0000FF">rename</font></b><font color="#990000">)</font>    <font color="#FF0000">{{</font><font color="#990000">^--?</font><b><font color="#0000FF">rename</font></b>$<font color="#FF0000">}</font>           <font color="#993399">1</font>         <font color="#FF0000">{}</font>       action<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>stop<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>stop$<font color="#FF0000">}</font>             <font color="#993399">0</font>         <font color="#FF0000">{}</font>       action<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(--</font>date<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>date$<font color="#FF0000">}</font>             <font color="#993399">0</font>         <font color="#FF0000">{}</font>       action<font color="#FF0000">}</font>

        <i><font color="#9A1900"># Check arguments:</font></i>
        <i><font color="#9A1900">#          name          regexp                  num_args  default  param_type</font></i>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font><b><font color="#0000FF">case</font></b><font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font><b><font color="#0000FF">case</font></b>$<font color="#FF0000">}</font>             <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check_flag<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>nocase<font color="#990000">)</font>     <font color="#FF0000">{{</font><font color="#990000">^--?</font>nocase$<font color="#FF0000">}</font>           <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check_flag<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>inverse<font color="#990000">)</font>    <font color="#FF0000">{{</font><font color="#990000">^--?</font>inverse$<font color="#FF0000">}</font>          <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check_flag<font color="#FF0000">}</font>

        <i><font color="#9A1900"># Checks:</font></i>
        <i><font color="#9A1900">#          name          regexp                  num_args  default  param_type</font></i>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>type<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font>type$<font color="#FF0000">}</font>             <font color="#993399">1</font>         <font color="#FF0000">{}</font>       type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>dir<font color="#990000">)</font>        <font color="#FF0000">{{</font><font color="#990000">^--?</font>dir<font color="#990000">(</font>ectory<font color="#990000">)?</font>$<font color="#FF0000">}</font>     <font color="#993399">0</font>         D        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font><b><font color="#0000FF">file</font></b><font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font><b><font color="#0000FF">file</font></b>$<font color="#FF0000">}</font>             <font color="#993399">0</font>         F        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font><b><font color="#0000FF">socket</font></b><font color="#990000">)</font>     <font color="#FF0000">{{</font><font color="#990000">^--?</font><b><font color="#0000FF">socket</font></b>$<font color="#FF0000">}</font>           <font color="#993399">0</font>         S        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>block<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>block$<font color="#FF0000">}</font>            <font color="#993399">0</font>         B        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>character<font color="#990000">)</font>  <font color="#FF0000">{{</font><font color="#990000">^--?</font>character$<font color="#FF0000">}</font>        <font color="#993399">0</font>         C        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>link<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?(</font>sym<font color="#990000">)?</font>link$<font color="#FF0000">}</font>       <font color="#993399">0</font>         L        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>text<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font>text$<font color="#FF0000">}</font>             <font color="#993399">0</font>         T        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>fifo<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?(</font>pipe<font color="#990000">|</font>fifo<font color="#990000">)</font>$<font color="#FF0000">}</font>      <font color="#993399">0</font>         P        type<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>writable<font color="#990000">)</font>   <font color="#FF0000">{{</font><font color="#990000">^--?</font>writable$<font color="#FF0000">}</font>         <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>readable<font color="#990000">)</font>   <font color="#FF0000">{{</font><font color="#990000">^--?</font>readable$<font color="#FF0000">}</font>         <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>executable<font color="#990000">)</font> <font color="#FF0000">{{</font><font color="#990000">^--?</font>executable$<font color="#FF0000">}</font>       <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>newer<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>newer$<font color="#FF0000">}</font>            <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>older<font color="#990000">)</font>      <font color="#FF0000">{{</font><font color="#990000">^--?</font>older$<font color="#FF0000">}</font>            <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>smaller<font color="#990000">)</font>    <font color="#FF0000">{{</font><font color="#990000">^--?</font>smaller$<font color="#FF0000">}</font>          <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>larger<font color="#990000">)</font>     <font color="#FF0000">{{</font><font color="#990000">^--?</font>larger$<font color="#FF0000">}</font>           <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>is_empty<font color="#990000">)</font>   <font color="#FF0000">{{</font><font color="#990000">^--?</font>is_<font color="#990000">?</font>empty$<font color="#FF0000">}</font>        <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>exists<font color="#990000">)</font>     <font color="#FF0000">{{</font><font color="#990000">^--?</font>exists$<font color="#FF0000">}</font>           <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>not_exists<font color="#990000">)</font> <font color="#FF0000">{{</font><font color="#990000">^--?</font>not_exists$<font color="#FF0000">}</font>       <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>windows<font color="#990000">)</font>    <font color="#FF0000">{{</font><font color="#990000">^--?</font>win<font color="#990000">(</font>dows<font color="#990000">)?</font>$<font color="#FF0000">}</font>       <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>unix<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?(</font>unix<font color="#990000">|</font>linux<font color="#990000">)</font>$<font color="#FF0000">}</font>     <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>mac<font color="#990000">)</font>        <font color="#FF0000">{{</font><font color="#990000">^--?</font>mac<font color="#990000">(</font>intosh<font color="#990000">)?</font>$<font color="#FF0000">}</font>     <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>java<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?(</font>java<font color="#990000">|</font>os400<font color="#990000">)</font>$<font color="#FF0000">}</font>     <font color="#993399">0</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>contains<font color="#990000">)</font>   <font color="#FF0000">{{</font><font color="#990000">^--?</font>contains$<font color="#FF0000">}</font>         <font color="#993399">1</font>         <font color="#FF0000">{}</font>       check<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>name<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font>name$<font color="#FF0000">}</font>             <font color="#993399">0</font>         <font color="#FF0000">{}</font>       match_flag<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>path<font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font>path$<font color="#FF0000">}</font>             <font color="#993399">0</font>         <font color="#FF0000">{}</font>       match_flag<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font>re<font color="#990000">)</font>         <font color="#FF0000">{{</font><font color="#990000">^--?</font>reg<font color="#990000">?(</font>exp<font color="#990000">?)?</font>$<font color="#FF0000">}</font>      <font color="#993399">1</font>         <font color="#FF0000">{}</font>       match<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(-</font><b><font color="#0000FF">glob</font></b><font color="#990000">)</font>       <font color="#FF0000">{{</font><font color="#990000">^--?</font><b><font color="#0000FF">glob</font></b>$<font color="#FF0000">}</font>             <font color="#993399">1</font>         <font color="#FF0000">{}</font>       match<font color="#FF0000">}</font>

        <i><font color="#9A1900"># Option to supply custom rule definitions, might be dangerous.</font></i>
        array <b><font color="#0000FF">set</font></b> PARAMS <font color="#990000">[</font>cfg get CUSTOM_RULE_PARAM_OVERRIDE <font color="#FF0000">{}</font><font color="#990000">]</font>

        <i><font color="#9A1900"># Define the order of the parameter types that need to be processed before other types.</font></i>
        <i><font color="#9A1900"># For example the check_flag arguments which indicates whether a check is supposed to</font></i>
        <i><font color="#9A1900"># be case insensitive or not have to take place before we process the checks that</font></i>
        <i><font color="#9A1900"># depend on these flags. The rule specifier needs to be the very first that we process</font></i>
        <i><font color="#9A1900"># when adding rules. Any undefined types will be appended to this list.</font></i>
        <b><font color="#0000FF">set</font></b> param_types <font color="#990000">[</font><b><font color="#0000FF">list</font></b> rule check_flag action_flag match_flag type match action<font color="#990000">]</font>
        touch <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$param_types</font>

        <i><font color="#9A1900"># Action types need specific sorting as the order determines in which order the actions</font></i>
        <i><font color="#9A1900"># are executed when a rule contains multiple actions.</font></i>
        <b><font color="#0000FF">foreach</font></b> action_type <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">--</font><b><font color="#0000FF">exec</font></b> <font color="#990000">--</font>date <font color="#990000">--</font><b><font color="#0000FF">rename</font></b> <font color="#990000">--</font>move <font color="#990000">--</font>stop <font color="#990000">--</font>recurse <font color="#990000">--</font>delete<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists PARAMS<font color="#990000">(</font><font color="#009900">$action_type</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> action <font color="#009900">$action_type</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Parse the rest of the parameters in a generic way.</font></i>
        <b><font color="#0000FF">foreach</font></b> name <font color="#990000">[</font>array names PARAMS<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> param_type <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#009900">$PARAMS</font><font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font> <font color="#993399">3</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists <font color="#009900">$param_type</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">[</font><b><font color="#0000FF">set</font></b> <font color="#009900">$param_type</font><font color="#990000">]</font> <font color="#009900">$name</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> <font color="#009900">$param_type</font> <font color="#009900">$name</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>exact <font color="#009900">$param_types</font> <font color="#009900">$param_type</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> param_types <font color="#009900">$param_type</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        log DEV <font color="#FF0000">{</font>rule<font color="#990000">::</font>init<font color="#990000">:</font> parameter types <font color="#990000">=</font> <font color="#009900">$param_types</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> PARAMS<font color="#990000">(</font>parameters<font color="#990000">)</font> <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> $<font color="#009900">$param_types</font> <font color="#FF0000">{</font> $<font color="#FF0000">}</font><font color="#990000">]]</font>
        log DEV <font color="#FF0000">{</font>rule<font color="#990000">::</font>init<font color="#990000">:</font> PARAMS<font color="#990000">(</font>parameters<font color="#990000">)</font> <font color="#990000">=</font> <font color="#009900">$PARAMS</font><font color="#990000">(</font>parameters<font color="#990000">)</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> INIT <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012571995" ID="ID_1457012571995311" MODIFIED="1457012571995" TEXT="rule::is_active" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012571995" ID="ID_1457012571995709" MODIFIED="1457012571995" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572006" ID="ID_1457012572006081" MODIFIED="1457012572006">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>is_active <font color="#FF0000">{</font> rule_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule_id</font> <font color="#990000">==</font> <font color="#FF0000">{}</font> <font color="#990000">||</font> <font color="#990000">![</font>rule<font color="#990000">::</font>exists <font color="#009900">$rule_id</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">,</font>status<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>A<font color="#FF0000">}}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572006" ID="ID_1457012572006715" MODIFIED="1457012572006" TEXT="rule::load" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572007" ID="ID_1457012572007098" MODIFIED="1457012572007" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572017" ID="ID_1457012572017053" MODIFIED="1457012572017">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font><b><font color="#0000FF">load</font></b> <font color="#FF0000">{</font> rule_file <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        store init <font color="#009900">$rule_file</font>
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>check rule<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                $<font color="#FF0000">{</font>type<font color="#FF0000">}</font><font color="#990000">::</font>set_array <font color="#990000">[</font>store get $<font color="#FF0000">{</font>type<font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>flag type destination action<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                $<font color="#FF0000">{</font>type<font color="#FF0000">}</font> <b><font color="#0000FF">load</font></b> <font color="#990000">[</font>store get $<font color="#FF0000">{</font>type<font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572017" ID="ID_1457012572017649" MODIFIED="1457012572017" TEXT="rule::lookup" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572018" ID="ID_1457012572018037" MODIFIED="1457012572018" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572027" ID="ID_1457012572027754" MODIFIED="1457012572027">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>lookup <font color="#FF0000">{</font> name <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists RULE<font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#009900">$RULE</font><font color="#990000">(</font><font color="#009900">$name</font><font color="#990000">)</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572028" ID="ID_1457012572028345" MODIFIED="1457012572028" TEXT="rule::parse_rules" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572028" ID="ID_1457012572028736" MODIFIED="1457012572028" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572038" ID="ID_1457012572038857" MODIFIED="1457012572038">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>parse_rules <font color="#FF0000">{</font> rules <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> rule <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$rules</font> <font color="#990000">[</font>cfg get RULE_SEPARATOR <font color="#990000">\</font>u00<font color="#990000">]]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> rule <font color="#990000">[</font><b><font color="#0000FF">string</font></b> trim <font color="#009900">$rule</font><font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> is <b><font color="#0000FF">list</font></b> <font color="#009900">$rule</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font>Unable to parse rule <font color="#FF0000">"$rule"</font> due to brace mismatch<font color="#990000">.</font><font color="#FF0000">}</font>
                        log STDOUT <font color="#FF0000">{</font>Unable to parse rule <font color="#FF0000">"$rule"</font> due to brace mismatch<font color="#990000">.</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        add <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$rule</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572039" ID="ID_1457012572039463" MODIFIED="1457012572039" TEXT="rule::print_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572039" ID="ID_1457012572039849" MODIFIED="1457012572039" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572049" ID="ID_1457012572049508" MODIFIED="1457012572049">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>print_array <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>parray RULE<font color="#FF0000">}</font> msg<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">error</font></b> writing <font color="#FF0000">"std(out|err)"</font><font color="#990000">:</font> broken pipe<font color="#FF0000">}</font> <font color="#009900">$msg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">puts</font></b> stderr <font color="#009900">$msg</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572050" ID="ID_1457012572050106" MODIFIED="1457012572050" TEXT="rule::reset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572050" ID="ID_1457012572050582" MODIFIED="1457012572050" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572059" ID="ID_1457012572059956" MODIFIED="1457012572059">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>reset <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        array unset RULE
        <b><font color="#0000FF">set</font></b> RULE<font color="#990000">(</font>rule_ids<font color="#990000">)</font> <font color="#FF0000">{}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572060" ID="ID_1457012572060542" MODIFIED="1457012572060" TEXT="rule::save" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572060" ID="ID_1457012572060945" MODIFIED="1457012572060" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572070" ID="ID_1457012572070762" MODIFIED="1457012572070">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>save <font color="#FF0000">{</font> rules_db_file <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        store init <font color="#009900">$rules_db_file</font>
        store reset
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>check rule<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                store <b><font color="#0000FF">set</font></b> <font color="#009900">$type</font> <font color="#990000">[</font>$<font color="#FF0000">{</font>type<font color="#FF0000">}</font><font color="#990000">::</font>get_array<font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>flag type destination action<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                store <b><font color="#0000FF">set</font></b> <font color="#009900">$type</font> <font color="#990000">[</font>$<font color="#FF0000">{</font>type<font color="#FF0000">}</font> unload<font color="#990000">]</font>
        <font color="#FF0000">}</font>
        store save
        log INFO <font color="#FF0000">{</font>Saved rules db <b><font color="#0000FF">file</font></b><font color="#990000">:</font> <font color="#009900">$rules_db_file</font><font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572071" ID="ID_1457012572071361" MODIFIED="1457012572071" TEXT="rule::select_rule" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572071" ID="ID_1457012572071763" MODIFIED="1457012572071" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572081" ID="ID_1457012572081763" MODIFIED="1457012572081">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>select_rule <font color="#FF0000">{</font> rule_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> current_rule_id

        <b><font color="#0000FF">set</font></b> current_rule_id <font color="#009900">$rule_id</font>
        log DEV <font color="#FF0000">{</font>Changed to rule_id <font color="#009900">$rule_id</font><font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572082" ID="ID_1457012572082453" MODIFIED="1457012572082" TEXT="rule::set_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572082" ID="ID_1457012572082890" MODIFIED="1457012572082" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572092" ID="ID_1457012572092207" MODIFIED="1457012572092">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>set_array <font color="#FF0000">{</font> rule_list <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE

        array <b><font color="#0000FF">set</font></b> RULE <font color="#009900">$rule_list</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572092" ID="ID_1457012572092818" MODIFIED="1457012572092" TEXT="rule::set_rule" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572093" ID="ID_1457012572093243" MODIFIED="1457012572093" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572105" ID="ID_1457012572105649" MODIFIED="1457012572105">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> rule<font color="#990000">::</font>set_rule <font color="#FF0000">{</font> option value <font color="#FF0000">{</font> param <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font> flags <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULE
        <b><font color="#0000FF">variable</font></b> current_rule_id

        log DEV <font color="#FF0000">{</font>Setting rule <font color="#009900">$current_rule_id</font> <font color="#009900">$option</font> <font color="#009900">$value</font> <font color="#009900">$param</font> <font color="#009900">$flags</font><font color="#FF0000">}</font>

        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$option</font> <font color="#FF0000">{</font>
        action_id <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>action_id<font color="#990000">)</font> <font color="#009900">$value</font>
        <font color="#FF0000">}</font>
        action <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>set_rule action_id <font color="#990000">[::</font>action add <font color="#009900">$value</font> <font color="#009900">$param</font> <font color="#009900">$flags</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        check_id <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>check_ids<font color="#990000">)</font> <font color="#009900">$value</font>
        <font color="#FF0000">}</font>
        check <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>set_rule check_id <font color="#990000">[</font>check<font color="#990000">::</font>add_check <font color="#009900">$value</font> <font color="#009900">$param</font> <font color="#009900">$flags</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        name <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font>name<font color="#990000">)</font> <font color="#009900">$value</font>
                <b><font color="#0000FF">set</font></b> RULE<font color="#990000">(</font><font color="#009900">$value</font><font color="#990000">)</font> <font color="#009900">$current_rule_id</font>
        <font color="#FF0000">}</font>
        status <font color="#990000">-</font>
        desc <font color="#990000">-</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> RULE<font color="#990000">(</font><font color="#009900">$current_rule_id</font><font color="#990000">,</font><font color="#009900">$option</font><font color="#990000">)</font> <font color="#009900">$value</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1457012572106" ID="ID_1457012572106354" MODIFIED="1457012572106" TEXT="drop_it_flag 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012572106" ID="ID_1457012572106663" MODIFIED="1457012572106" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572106" ID="ID_1457012572106854" MODIFIED="1457012572106" TEXT="tapp 1.0"/>
</node>
<node CREATED="1457012572107" ID="ID_1457012572107088" MODIFIED="1457012572107" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572107" ID="ID_1457012572107883" MODIFIED="1457012572107" TEXT="flag.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/flag.tcl"/>
</node>
<node CREATED="1457012572108" ID="ID_1457012572108090" MODIFIED="1457012572108" TEXT="flag" COLOR="#ff8300"/>
</node>
<node CREATED="1457012572108" ID="ID_1457012572108556" MODIFIED="1457012572108" TEXT="drop_it 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012572108" ID="ID_1457012572108826" MODIFIED="1457012572108" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572108" ID="ID_1457012572109002" MODIFIED="1457012572109" TEXT="struct::queue 1.4.2"/>
<node CREATED="1457012572109" ID="ID_1457012572109134" MODIFIED="1457012572109" TEXT="tapp 1.0"/>
<node CREATED="1457012572109" ID="ID_1457012572109265" MODIFIED="1457012572109" TEXT="notifications 1.0"/>
<node CREATED="1457012572109" ID="ID_1457012572109392" MODIFIED="1457012572109" TEXT="drop_it_rule 1.0" LINK="#ID_1457012571866976"/>
<node CREATED="1457012572109" ID="ID_1457012572109516" MODIFIED="1457012572109" TEXT="drop_it_action 1.0" LINK="#ID_1457012572902157"/>
<node CREATED="1457012572109" ID="ID_1457012572109643" MODIFIED="1457012572109" TEXT="drop_it_stats 1.0" LINK="#ID_1457012571844262"/>
</node>
<node CREATED="1457012572109" ID="ID_1457012572109876" MODIFIED="1457012572109" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572110" ID="ID_1457012572110611" MODIFIED="1457012572110" TEXT="drop_it_pkg.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/drop_it_pkg.tcl"/>
</node>
<node CREATED="1457012572110" ID="ID_1457012572110815" MODIFIED="1457012572110" TEXT="drop_it" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012572111" ID="ID_1457012572111200" MODIFIED="1457012572111" TEXT="drop_it::action_match" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572111" ID="ID_1457012572111497" MODIFIED="1457012572111" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572123" ID="ID_1457012572123238" MODIFIED="1457012572123">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>action_match <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RPT

        <b><font color="#0000FF">set</font></b> unmatched_files <font color="#FF0000">{}</font>

        <b><font color="#0000FF">while</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>match_queue size<font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                lassign <font color="#990000">[</font>match_queue get<font color="#990000">]</font> input rule_id
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule_id</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
                        <b><font color="#0000FF">set</font></b> RPT <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$RPT</font> <font color="#990000">[</font>rule<font color="#990000">::</font>action <font color="#009900">$input</font><font color="#990000">]]</font>
                        <b><font color="#0000FF">set</font></b> hash <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule hash<font color="#990000">]</font>
                        lassign <font color="#990000">[</font>stats get <font color="#009900">$hash</font><font color="#990000">]</font> cr_date num_checks avg_ms num_matches last_match last_update
                        <b><font color="#0000FF">set</font></b> last_match  <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> last_update <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
                        <b><font color="#0000FF">incr</font></b> num_matches <font color="#993399">1</font>
                        stats <b><font color="#0000FF">set</font></b> <font color="#009900">$hash</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$cr_date</font> <font color="#009900">$num_checks</font> <font color="#009900">$avg_ms</font> <font color="#009900">$num_matches</font> <font color="#009900">$last_match</font> <font color="#009900">$last_update</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> unmatched_files <font color="#009900">$input</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$unmatched_files</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572123" ID="ID_1457012572123836" MODIFIED="1457012572123" TEXT="drop_it::explain_all_rules" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572124" ID="ID_1457012572124193" MODIFIED="1457012572124" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572134" ID="ID_1457012572134576" MODIFIED="1457012572134">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>explain_all_rules <font color="#FF0000">{</font> <font color="#FF0000">{</font> input <font color="#FF0000">"&lt;input&gt;"</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        touch report
        <b><font color="#0000FF">foreach</font></b> rule_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rules<font color="#990000">]</font> <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
                <b><font color="#0000FF">lappend</font></b> report <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#990000">[</font>rule<font color="#990000">::</font>explain <font color="#009900">$input</font><font color="#990000">]</font> <font color="#FF0000">{</font><font color="#990000">\</font>n <font color="#FF0000">}</font><font color="#990000">]</font> <font color="#FF0000">{}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$report</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$report</font> <font color="#990000">\</font>n<font color="#990000">]</font>
                log INFO <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It explain<font color="#990000">:\</font>n<font color="#009900">$output</font><font color="#FF0000">}</font>
                log VERBOSE <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It explain<font color="#990000">:</font><font color="#FF0000">}</font>
                log STDOUT <font color="#FF0000">"$output"</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572135" ID="ID_1457012572135162" MODIFIED="1457012572135" TEXT="drop_it::generate_dropit_suffix" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572135" ID="ID_1457012572135528" MODIFIED="1457012572135" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572145" ID="ID_1457012572145431" MODIFIED="1457012572145">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>generate_dropit_suffix <font color="#FF0000">{</font> <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">{}</font> <font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$file</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font>cfg enabled CHECKSUM_DROPIT_SUFFIX <font color="#993399">1</font><font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> _drop_it_<font color="#990000">[</font>file_utils<font color="#990000">::</font>get_checksum <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> _dropit_<font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>int<font color="#990000">(</font>rand<font color="#990000">()*</font><font color="#993399">100000</font><font color="#990000">)</font><font color="#FF0000">}</font><font color="#990000">].</font>bak
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572146" ID="ID_1457012572146012" MODIFIED="1457012572146" TEXT="drop_it::generate_rule_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572146" ID="ID_1457012572146444" MODIFIED="1457012572146" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572157" ID="ID_1457012572157364" MODIFIED="1457012572157">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>generate_rule_file <font color="#FF0000">{</font> rule_file <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> msg <font color="#FF0000">{</font>Warning<font color="#990000">:</font> The rules configuration <b><font color="#0000FF">file</font></b> <font color="#009900">$rule_file</font> does not exist<font color="#990000">\</font>n<font color="#FF0000">}</font>
        log STDOUT <font color="#009900">$msg</font>
        log WARN <font color="#009900">$msg</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg AUTO_CREATE_RULES_FILE <font color="#993399">1</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>
        log STDOUT <font color="#FF0000">{</font>Generating empty rules <b><font color="#0000FF">file</font></b> <font color="#009900">$rule_file</font><font color="#990000">\</font>n<font color="#FF0000">}</font>
        <b><font color="#0000FF">lappend</font></b> rule_file_content $<font color="#FF0000">{</font><font color="#990000">::</font>rule<font color="#990000">::</font>rule_configuration_desc<font color="#FF0000">}</font><font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%-</font>8s<font color="#990000">%-</font>11s<font color="#990000">%-</font>16s<font color="#990000">%-</font>59s<font color="#990000">%-</font>29s<font color="#990000">%-</font>40s<font color="#FF0000">}</font> <i><font color="#9A1900"># Limit Action Destination Checks Match]</font></i>
        <b><font color="#0000FF">lappend</font></b> rule_file_content <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#FF0000">{</font>RULES <font color="#990000">=\\</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> rule_file_content <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%-</font>8s<font color="#990000">%-</font>11s<font color="#990000">%-</font>16s<font color="#990000">%-</font>59s<font color="#990000">%-</font>29s<font color="#990000">%-</font>40s<font color="#FF0000">}</font> <font color="#FF0000">{}</font> <font color="#FF0000">{}</font> <font color="#990000">--</font>stop <font color="#FF0000">{}</font> <font color="#990000">-</font><b><font color="#0000FF">file</font></b> <font color="#FF0000">"-glob {*.*}</font><font color="#CC33CC">\\</font><font color="#FF0000">"</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> rule_file_content <font color="#FF0000">{}</font> <font color="#FF0000">{}</font>
        file_utils<font color="#990000">::</font>write_file <font color="#009900">$rule_file</font> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$rule_file_content</font> <font color="#990000">\</font>n<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572157" ID="ID_1457012572157963" MODIFIED="1457012572157" TEXT="drop_it::get_rule_match" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572158" ID="ID_1457012572158356" MODIFIED="1457012572158" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572173" ID="ID_1457012572173449" MODIFIED="1457012572173">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>get_rule_match <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> TIME
        array <b><font color="#0000FF">set</font></b> CHECK_RES <font color="#FF0000">{}</font>

        <b><font color="#0000FF">set</font></b> explain_rule_match <font color="#993399">0</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled EXPLAIN_RULE_MATCH<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font>cfg exists LIMIT_TO_RULES<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> explain_rule_match <font color="#993399">1</font>
                <b><font color="#0000FF">set</font></b> indent <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#990000">[</font>cfg get EXPLAIN_CHECK_INDENT <font color="#993399">3</font><font color="#990000">]]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">foreach</font></b> rule_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rules<font color="#990000">]</font> <font color="#FF0000">{</font>

                <b><font color="#0000FF">set</font></b> start_time <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> milliseconds<font color="#990000">]</font>

                <b><font color="#0000FF">set</font></b> rule_matched <font color="#993399">1</font>
                rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$explain_rule_match</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> explain <font color="#990000">[</font>rule<font color="#990000">::</font>explain <font color="#009900">$input</font> <font color="#993399">0</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>rule<font color="#990000">::</font>is_active <font color="#009900">$rule_id</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>

                        <b><font color="#0000FF">set</font></b> check_ids <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule check_ids<font color="#990000">]</font>

                        <i><font color="#9A1900"># Running through rule checks are done here to keep track of which</font></i>
                        <i><font color="#9A1900"># checks have been verified already (as a performance optimisation).</font></i>
                        <i><font color="#9A1900"># It would be possible to move this to RULE, but that would lead to</font></i>
                        <i><font color="#9A1900"># it having the CHECK_RES thus needing to keep track of the files</font></i>
                        <i><font color="#9A1900"># that has been processed (actually just the previous file, assuming</font></i>
                        <i><font color="#9A1900"># the one file runs through all rules in order).</font></i>
                        clear running_through_check_ids
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> running_through_check_ids <font color="#FF0000">" Running through checks ($check_ids)"</font>
                        <font color="#FF0000">}</font>
                        log DEBUG <font color="#FF0000">{</font>Rule <font color="#009900">$rule_id</font><font color="#990000">:</font><font color="#009900">$running_through_check_ids</font><font color="#FF0000">}</font>

                        <b><font color="#0000FF">foreach</font></b> check_id <font color="#009900">$check_ids</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> check_key <font color="#009900">$input</font><font color="#990000">,</font>check_<font color="#009900">$check_id</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists CHECK_RES<font color="#990000">(</font><font color="#009900">$check_key</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">set</font></b> CHECK_RES<font color="#990000">(</font><font color="#009900">$check_key</font><font color="#990000">)</font> <font color="#990000">[</font>check<font color="#990000">::</font>check <font color="#009900">$check_id</font> <font color="#009900">$input</font><font color="#990000">]</font>
                                <font color="#FF0000">}</font>
                                
                                log DEBUG <font color="#FF0000">{</font>   <font color="#990000">[</font>check<font color="#990000">::</font>explain <font color="#009900">$check_id</font> <font color="#009900">$input</font> <font color="#009900">$CHECK_RES</font><font color="#990000">(</font><font color="#009900">$check_key</font><font color="#990000">)]</font><font color="#FF0000">}</font>

                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$explain_rule_match</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> explain <font color="#009900">$indent</font><font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#990000">[</font>check<font color="#990000">::</font>explain <font color="#009900">$check_id</font> <font color="#009900">$input</font> <font color="#009900">$CHECK_RES</font><font color="#990000">(</font><font color="#009900">$check_key</font><font color="#990000">)]</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
                                <font color="#FF0000">}</font>

                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$CHECK_RES</font><font color="#990000">(</font><font color="#009900">$check_key</font><font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">set</font></b> rule_matched <font color="#993399">0</font>
                                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$explain_rule_match</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                                log DEV <font color="#FF0000">{</font>Rule <font color="#009900">$rule_id</font><font color="#990000">:</font> <font color="#009900">$input</font> did not match <font color="#990000">(</font>skipping rule<font color="#990000">)</font><font color="#FF0000">}</font>
                                                <b><font color="#0000FF">break</font></b>
                                        <font color="#FF0000">}</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$explain_rule_match</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> explain <font color="#009900">$indent</font><font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#FF0000">{</font><font color="#990000">-</font> rule is suspended<font color="#FF0000">}</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> rule_matched <font color="#993399">0</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$explain_rule_match</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$explain</font> <font color="#990000">\</font>n<font color="#990000">]</font>
                        log INFO <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It explain<font color="#990000">:\</font>n<font color="#009900">$output</font><font color="#FF0000">}</font>
                        log VERBOSE <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It explain<font color="#990000">:</font><font color="#FF0000">}</font>
                        log STDOUT <font color="#FF0000">"$output</font><font color="#CC33CC">\n</font><font color="#FF0000">"</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">set</font></b> duration <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">clock</font></b> milliseconds<font color="#990000">]</font> <font color="#990000">-</font> <font color="#009900">$start_time</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <b><font color="#0000FF">incr</font></b> TIME<font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">)</font> <font color="#009900">$duration</font>
                <b><font color="#0000FF">incr</font></b> TIME<font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">,</font>count<font color="#990000">)</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule_matched</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>tapp_log<font color="#990000">::</font>log_level_enabled DEBUG<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$input</font><font color="#990000">]</font>
                                log DEBUG <font color="#FF0000">{</font>     <font color="#009900">$file_type</font> <font color="#009900">$input</font> matches rule <font color="#009900">$rule_id</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">return</font></b> <font color="#009900">$rule_id</font>
                        <b><font color="#0000FF">break</font></b>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        log DEBUG <font color="#FF0000">{</font>No rule match found <b><font color="#0000FF">for</font></b> <font color="#009900">$input</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572174" ID="ID_1457012572174118" MODIFIED="1457012572174" TEXT="drop_it::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572174" ID="ID_1457012572174520" MODIFIED="1457012572174" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572188" ID="ID_1457012572188882" MODIFIED="1457012572188">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>init <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        param register <font color="#990000">-</font>r <font color="#990000">--</font>rules                <font color="#990000">--</font>body <font color="#FF0000">{</font>
                        drop_it<font color="#990000">::</font>init_dependencies
                        drop_it<font color="#990000">::</font>load_rules
                        drop_it<font color="#990000">::</font>log_rules <font color="#990000">--</font>print
                <font color="#FF0000">}</font>                <font color="#990000">--</font>priority <font color="#993399">99</font>                <font color="#990000">--</font>comment <font color="#FF0000">{</font>shows a <b><font color="#0000FF">list</font></b> of the defined rules<font color="#990000">,</font> combine with <font color="#990000">--</font>verbose to also <b><font color="#0000FF">list</font></b> destinations<font color="#FF0000">}</font>
                
        param register <font color="#990000">-</font>n <font color="#990000">--</font>notify                <font color="#990000">--</font>config NOTIFY                <font color="#990000">--</font>comment <font color="#FF0000">{</font>enables system notification of the drop<font color="#990000">-</font>it report<font color="#FF0000">}</font>
                
        param register <font color="#FF0000">{}</font> <font color="#990000">--</font>examples                <font color="#990000">--</font>body <font color="#FF0000">{</font>
                        log STDOUT <font color="#990000">[</font>cfg get HELP_EXAMPLES<font color="#990000">]</font>
                <font color="#FF0000">}</font>                <font color="#990000">--</font>priority <font color="#993399">99</font>                <font color="#990000">--</font>comment <font color="#FF0000">{</font>prints a long <b><font color="#0000FF">list</font></b> of example usage<font color="#FF0000">}</font>
                
        param register <font color="#990000">-</font>a <font color="#990000">--</font>explain_all                <font color="#990000">--</font>body <font color="#FF0000">{</font>
                        drop_it<font color="#990000">::</font>init_dependencies
                        drop_it<font color="#990000">::</font>load_rules
                        drop_it<font color="#990000">::</font>explain_all_rules
                <font color="#FF0000">}</font>                <font color="#990000">--</font>comment <font color="#FF0000">{</font>shows explain output <b><font color="#0000FF">for</font></b> all rules<font color="#FF0000">}</font>

        param register <font color="#990000">-</font>e <font color="#990000">--</font>explain                <font color="#990000">--</font>config EXPLAIN_RULE_MATCH                <font color="#990000">--</font>comment <font color="#FF0000">{</font>explains how the given <b><font color="#0000FF">file</font></b> matched a rule<font color="#990000">,</font> <b><font color="#0000FF">if</font></b> used <b><font color="#0000FF">in</font></b> combination with <font color="#990000">--</font>rule <b><font color="#0000FF">then</font></b> non<font color="#990000">-</font>matches are explained as well<font color="#FF0000">}</font>
                
        param register <font color="#990000">-</font>t <font color="#990000">--</font><b><font color="#0000FF">time</font></b>                <font color="#990000">--</font>config TIME                <font color="#990000">--</font>comment <font color="#FF0000">{</font>enables performance debugging<font color="#990000">,</font> will print a <b><font color="#0000FF">time</font></b> spent report when the drop<font color="#990000">-</font>it job is finished<font color="#FF0000">}</font>

        param register <font color="#FF0000">{}</font> <font color="#990000">--</font>no_action                <font color="#990000">--</font>config ACTION_RULE_MATCH                <font color="#990000">--</font>config_value <font color="#993399">0</font>                <font color="#990000">--</font>morearg                <font color="#990000">--</font>comment <font color="#FF0000">{</font>do a real run<font color="#990000">,</font> but don<font color="#FF0000">'t perform actions (consider using dry-run instead)}</font>

<font color="#FF0000">        param register {} {--rule &lt;rule_id&gt;}                --body {</font>
<font color="#FF0000">                        cfg lappend LIMIT_TO_RULES {*}$values</font>
<font color="#FF0000">                        log DEBUG {Limiting operations to specific rule: $values}</font>
<font color="#FF0000">                }                --max_args 1                --min_args 1                --validation integer                --priority 40                --comment {limits match operations to specific rule_ids}</font>

<font color="#FF0000">        param register {} --list_args                --config PRINT_ARGUMENTS                --morearg                --comment {prints the arguments passed to drop-it}</font>
<font color="#FF0000">                </font>
<font color="#FF0000">        # Overrides the TApp default config option as this is handled</font>
<font color="#FF0000">        # in a special way in drop_it::load_rules.</font>
<font color="#FF0000">        param register -c --config                --override                --config CFG_FILE                --max_args 1                --min_args 1                --disporder 60                --priority 10                --morearg                --log {Using $value as configuration file}                --comment {specify the configuration file to use}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572189" ID="ID_1457012572189673" MODIFIED="1457012572189" TEXT="drop_it::init_dependencies" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572190" ID="ID_1457012572190166" MODIFIED="1457012572190" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572200" ID="ID_1457012572200390" MODIFIED="1457012572200">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>init_dependencies <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        check<font color="#990000">::</font>init
        rule<font color="#990000">::</font>init
        stats<font color="#990000">::</font>init
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572200" ID="ID_1457012572200974" MODIFIED="1457012572200" TEXT="drop_it::load_rules" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572201" ID="ID_1457012572201364" MODIFIED="1457012572201" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572216" ID="ID_1457012572216234" MODIFIED="1457012572216">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>load_rules <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RULES_LOADED 
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$RULES_LOADED</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>cfg exists CFG_FILE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Configuration item <font color="#FF0000">"CFG_FILE"</font> does not exist<font color="#990000">,</font> not loading rules<font color="#990000">.</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> load_rules_db_file <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> save_rules_db_file <font color="#993399">0</font>

        <b><font color="#0000FF">set</font></b> rule_file <font color="#990000">[</font>cfg get CFG_FILE<font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$rule_file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                generate_rule_file <font color="#009900">$rule_file</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> ext <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#990000">[</font><b><font color="#0000FF">file</font></b> extension <font color="#009900">$rule_file</font><font color="#990000">]]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$ext</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">.</font>db<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> load_rules_db_file <font color="#993399">1</font>
                <b><font color="#0000FF">set</font></b> rules_db_file <font color="#009900">$rule_file</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$ext</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">.</font>cfg<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># Loading the file</font></i>
                <b><font color="#0000FF">set</font></b> old_cfg_multi_line_separator <font color="#990000">[</font>cfg get CFG_MULTI_LINE_SEPARATOR <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
                cfg <b><font color="#0000FF">set</font></b> CFG_MULTI_LINE_SEPARATOR <font color="#990000">[</font>cfg get RULE_SEPARATOR <font color="#990000">\</font>u00<font color="#990000">]</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>cfg <b><font color="#0000FF">file</font></b> <font color="#009900">$rule_file</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log ERROR <font color="#FF0000">{</font>Error occurred reading rules configuration <b><font color="#0000FF">file</font></b><font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                        <b><font color="#0000FF">exit</font></b>
                <font color="#FF0000">}</font>
                cfg <b><font color="#0000FF">set</font></b> CFG_MULTI_LINE_SEPARATOR <font color="#009900">$old_cfg_multi_line_separator</font>
                log DEBUG <font color="#FF0000">{</font>Loaded rules configuration <b><font color="#0000FF">file</font></b> <font color="#009900">$rule_file</font><font color="#FF0000">}</font>
                
                <b><font color="#0000FF">set</font></b> rules_db_file <font color="#990000">[</font><b><font color="#0000FF">file</font></b> rootname <font color="#009900">$rule_file</font><font color="#990000">].</font>db
                
                <i><font color="#9A1900"># Option to save the parsed rules as a .db file and load that</font></i>
                <i><font color="#9A1900"># instead of parsing the rules for each run. We still need to</font></i>
                <i><font color="#9A1900"># load the configuration file above in case we have additional</font></i>
                <i><font color="#9A1900"># configuration items that the rules themselves rely on.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled PARSE_RULES_ON_CHANGE_ONLY<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$rules_db_file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> cfg_mtime <font color="#990000">[</font><b><font color="#0000FF">file</font></b> mtime <font color="#009900">$rule_file</font><font color="#990000">]</font>
                                <b><font color="#0000FF">set</font></b> dbf_mtime <font color="#990000">[</font><b><font color="#0000FF">file</font></b> mtime <font color="#009900">$rules_db_file</font><font color="#990000">]</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$dbf_mtime</font> <font color="#990000">&lt;</font> <font color="#009900">$cfg_mtime</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        log WARN <font color="#FF0000">{</font>Rules configuration <b><font color="#0000FF">file</font></b> has changed<font color="#990000">,</font> moving <font color="#990000">.</font>db <b><font color="#0000FF">file</font></b> to trash<font color="#FF0000">}</font>
                                        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>action<font color="#990000">::</font>move_action <font color="#009900">$rules_db_file</font> <font color="#990000">[</font>cfg get DEF_TRASH<font color="#990000">]</font> <font color="#990000">-</font>suffix<font color="#990000">]</font>
                                        <b><font color="#0000FF">set</font></b> save_rules_db_file <font color="#993399">1</font>
                                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                        log DEBUG <font color="#FF0000">{</font>Rules configuration <b><font color="#0000FF">file</font></b> has not changed<font color="#990000">,</font> using <font color="#990000">.</font>db <b><font color="#0000FF">file</font></b> instead<font color="#FF0000">}</font>
                                        <b><font color="#0000FF">set</font></b> load_rules_db_file <font color="#993399">1</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> save_rules_db_file <font color="#993399">1</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$load_rules_db_file</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                rule<font color="#990000">::</font><b><font color="#0000FF">load</font></b> <font color="#009900">$rules_db_file</font>
                log DEBUG <font color="#FF0000">{</font>Loaded rules <b><font color="#0000FF">file</font></b> <font color="#009900">$rules_db_file</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg exists RULES<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>parse_rules <font color="#990000">[</font>cfg get RULES<font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$save_rules_db_file</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log STDOUT <font color="#FF0000">{</font>Saving rules <b><font color="#0000FF">file</font></b> <font color="#009900">$rules_db_file</font><font color="#FF0000">}</font>
                rule<font color="#990000">::</font>save <font color="#009900">$rules_db_file</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled FUNC_STATS <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                stats <b><font color="#0000FF">load</font></b>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> RULES_LOADED <font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572216" ID="ID_1457012572216904" MODIFIED="1457012572216" TEXT="drop_it::log_match_report" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572217" ID="ID_1457012572217302" MODIFIED="1457012572217" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572231" ID="ID_1457012572231260" MODIFIED="1457012572231">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>log_match_report <font color="#FF0000">{</font> RPT <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>tcl_platform

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$RPT</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If we only have "recursed" matches, i.e. no actual file actions having taken</font></i>
        <i><font color="#9A1900"># place, then don't show the report unless we are debugging.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font>log_level_enabled DEBUG<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>index <font color="#993399">0</font> <font color="#990000">-</font>not <font color="#009900">$RPT</font> recurse<font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> prefix <font color="#990000">[</font><b><font color="#0000FF">string</font></b> repeat <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg get EXPLAIN_CHECK_INDENT <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">+</font> <font color="#993399">2</font><font color="#FF0000">}</font><font color="#990000">]]</font>
        touch report
        
        <b><font color="#0000FF">foreach</font></b> operation <font color="#009900">$RPT</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font> action <b><font color="#0000FF">file</font></b> dest output index <font color="#FF0000">}</font> <font color="#009900">$operation</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled EXPLAIN_RULE_MATCH<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font>cfg exists LIMIT_TO_RULES<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                rule<font color="#990000">::</font>select_rule <font color="#009900">$index</font>
                                <b><font color="#0000FF">set</font></b> report <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$report</font> <font color="#990000">[</font>rule<font color="#990000">::</font>explain <font color="#009900">$file</font><font color="#990000">]]</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> verb <font color="#990000">[</font>text_utils<font color="#990000">::</font>edify <font color="#009900">$action</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">-</font>nocase <font color="#990000">--</font>stop <font color="#009900">$action</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> destination <font color="#FF0000">{}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$output</font> <font color="#990000">!=</font> <font color="#FF0000">""</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> destination <font color="#FF0000">" ==&gt; [regsub -all {</font><font color="#CC33CC">\\\\</font><font color="#FF0000">} $output {/}]"</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$dest</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> destination <font color="#FF0000">" ==&gt; [regsub -all {</font><font color="#CC33CC">\\\\</font><font color="#FF0000">} $dest {/}]"</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> destination <font color="#FF0000">{}</font>
                        <font color="#FF0000">}</font>

                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> destination <font color="#FF0000">" (dry run: $index)"</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">append</font></b> destination <font color="#FF0000">" ($index)"</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> report <font color="#FF0000">"${prefix}$verb [regsub -all {</font><font color="#CC33CC">\\</font><font color="#FF0000">+} $file {/}]$destination"</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$report</font> <font color="#990000">\</font>n<font color="#990000">]</font>
        log INFO <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It report<font color="#990000">:\</font>n<font color="#009900">$output</font><font color="#FF0000">}</font>
        log VERBOSE <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It report<font color="#990000">:</font><font color="#FF0000">}</font>
        log STDOUT <font color="#FF0000">"$output</font><font color="#CC33CC">\n</font><font color="#FF0000">"</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled NOTIFY<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> rpt_count <font color="#FF0000">{}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$RPT</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> rpt_count <font color="#FF0000">" ([llength $RPT])"</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> toupper <font color="#009900">$tcl_platform</font><font color="#990000">(</font>platform<font color="#990000">)]</font> <font color="#FF0000">{</font>
                UNIX <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> icon <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#990000">[</font>this_dir<font color="#990000">]</font> icons drop_it<font color="#990000">.</font>svg<font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> icon <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#990000">[</font>this_dir<font color="#990000">]</font> icons drop_it<font color="#990000">.</font>png<font color="#990000">]</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                notifications<font color="#990000">::</font>notify <font color="#009900">$icon</font> <font color="#FF0000">"Drop-It Report${rpt_count}"</font> <font color="#009900">$output</font> <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572231" ID="ID_1457012572231915" MODIFIED="1457012572231" TEXT="drop_it::log_rules" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572232" ID="ID_1457012572232301" MODIFIED="1457012572232" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572250" ID="ID_1457012572250868" MODIFIED="1457012572250">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>log_rules args <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> TIME

        <b><font color="#0000FF">set</font></b> print <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> overall_duration <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> overall_count <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> max_count <font color="#993399">0</font>

        <b><font color="#0000FF">foreach</font></b> opt <font color="#FF0000">{</font>print <b><font color="#0000FF">time</font></b><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$args</font> <font color="#990000">--</font><font color="#009900">$opt</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> <font color="#009900">$opt</font> <font color="#993399">1</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$time</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font>cfg enabled TIME<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> action_width <font color="#993399">16</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> action_width <font color="#993399">70</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> report_format <font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#FF0000">{</font><font color="#990000">%</font>7s<font color="#990000">%-</font>4s<font color="#990000">%</font>7s <font color="#990000">%-</font>$<font color="#FF0000">{</font>action_width<font color="#FF0000">}</font>s<font color="#990000">%-</font>36s <font color="#990000">%</font>s<font color="#FF0000">}</font><font color="#990000">]</font>

        <b><font color="#0000FF">append</font></b> report <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$report_format</font> rule_id <font color="#FF0000">{</font> <font color="#990000">*</font><font color="#FF0000">}</font> avg<font color="#990000">/</font>ms action checks match <font color="#FF0000">{}</font> <font color="#FF0000">{}</font><font color="#990000">]\</font>n

        <b><font color="#0000FF">foreach</font></b> rule_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rules<font color="#990000">]</font> <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
                <b><font color="#0000FF">set</font></b> actions <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule action<font color="#990000">]</font>

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>rule<font color="#990000">::</font>get_rule status<font color="#990000">]</font> <font color="#990000">!=</font> <font color="#FF0000">{</font>A<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        prepend actions <font color="#FF0000">{</font><font color="#990000">--</font> <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>

                clear checks match contains avg_ms

                <b><font color="#0000FF">foreach</font></b> check_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule check_ids<font color="#990000">]</font> <font color="#FF0000">{</font>
                        lassign <font color="#990000">[</font>check<font color="#990000">::</font>to_string <font color="#009900">$check_id</font><font color="#990000">]</font> check_type check_checks check_match check_contains
                        <b><font color="#0000FF">foreach</font></b> check <font color="#009900">$check_checks</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$checks</font> <font color="#009900">$check</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> checks <font color="#009900">$check</font>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>

                <i><font color="#9A1900"># Splits normal check parameters and match parameters for display purposes</font></i>
                <b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><font color="#990000">^(.*?)</font> <font color="#990000">+(-(?:</font>re<font color="#990000">|</font><b><font color="#0000FF">glob</font></b><font color="#990000">|</font>contains<font color="#990000">).+)</font>$<font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$checks</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">-</font> checks match

                <b><font color="#0000FF">set</font></b> hash <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule hash<font color="#990000">]</font>
                lassign <font color="#990000">[</font>stats get <font color="#009900">$hash</font><font color="#990000">]</font> cr_date num_checks avg_ms num_matches last_match last_update

                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$last_update</font> <font color="#990000">!=</font> <font color="#FF0000">{}</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$cr_date</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$last_match</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> last_match <font color="#009900">$cr_date</font>
                        <font color="#FF0000">}</font>
                        <i><font color="#9A1900"># graceful run in time before reporting negative rules</font></i>
                        <b><font color="#0000FF">set</font></b> grace_time <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#993399">86400</font> <font color="#990000">*</font> <font color="#990000">[</font>cfg get STAT_RUN_IN_DAYS <font color="#993399">90</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
                
                        <b><font color="#0000FF">set</font></b> grace_pct <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$grace_time</font> <font color="#990000">*</font> <font color="#993399">100</font> <font color="#990000">/</font> double<font color="#990000">(</font><font color="#009900">$last_update</font> <font color="#990000">-</font> <font color="#009900">$cr_date</font><font color="#990000">)</font><font color="#FF0000">}</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$grace_pct</font> <font color="#990000">&gt;</font> <font color="#993399">50</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> grace_pct <font color="#993399">50</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> match_age_percent <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">(</font><font color="#009900">$last_match</font> <font color="#990000">-</font> <font color="#009900">$cr_date</font><font color="#990000">)</font> <font color="#990000">*</font> <font color="#993399">100</font> <font color="#990000">/</font> <font color="#990000">(</font><font color="#993399">1</font> <font color="#990000">+</font> <font color="#009900">$last_update</font> <font color="#990000">-</font> <font color="#009900">$cr_date</font><font color="#990000">)</font> <font color="#990000">+</font> <font color="#009900">$grace_pct</font><font color="#FF0000">}</font><font color="#990000">]</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$match_age_percent</font> <font color="#990000">&gt;=</font> <font color="#993399">80</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{</font> <font color="#990000">***</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$match_age_percent</font> <font color="#990000">&gt;=</font> <font color="#993399">70</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{</font> <font color="#990000">**</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$match_age_percent</font> <font color="#990000">&gt;=</font> <font color="#993399">60</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{</font> <font color="#990000">*</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$match_age_percent</font> <font color="#990000">&gt;=</font> <font color="#993399">40</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$match_age_percent</font> <font color="#990000">&gt;=</font> <font color="#993399">30</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{</font> <font color="#990000">-</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$match_age_percent</font> <font color="#990000">&gt;=</font> <font color="#993399">20</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{</font> <font color="#990000">--</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rating <font color="#FF0000">{</font> <font color="#990000">---</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$avg_ms</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> avg_ms <font color="#FF0000">{}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> avg_ms <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%.</font>2f<font color="#FF0000">}</font> <font color="#009900">$avg_ms</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$time</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists TIME<font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> duration <font color="#009900">$TIME</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">)</font>
                        <b><font color="#0000FF">incr</font></b> overall_duration <font color="#009900">$duration</font>
                        <b><font color="#0000FF">set</font></b> count <font color="#009900">$TIME</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">,</font>count<font color="#990000">)</font>
                        <b><font color="#0000FF">incr</font></b> overall_count <font color="#009900">$count</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$count</font> <font color="#990000">&gt;</font> <font color="#009900">$max_count</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> max_count <font color="#009900">$count</font>
                        <font color="#FF0000">}</font>
                
                        <b><font color="#0000FF">set</font></b> avg_ms <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#FF0000">{</font><font color="#990000">%.</font>2f<font color="#FF0000">}</font> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>double<font color="#990000">(</font><font color="#009900">$duration</font><font color="#990000">)/</font>double<font color="#990000">(</font><font color="#009900">$count</font><font color="#990000">)</font><font color="#FF0000">}</font><font color="#990000">]]</font>
                <font color="#FF0000">}</font>

                <b><font color="#0000FF">append</font></b> report <font color="#990000">[</font><b><font color="#0000FF">format</font></b> <font color="#009900">$report_format</font> <font color="#009900">$rule_id</font> <font color="#009900">$rating</font> <font color="#009900">$avg_ms</font> <font color="#009900">$actions</font> <font color="#009900">$checks</font> <font color="#009900">$match</font><font color="#990000">]\</font>n
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$time</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> file_desc  <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$max_count</font> <font color="#990000">==</font> <font color="#993399">1</font> <font color="#990000">?</font> <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b><font color="#FF0000">}</font> <font color="#990000">:</font> <font color="#FF0000">{</font>files<font color="#FF0000">}}</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> check_desc <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$overall_count</font> <font color="#990000">==</font> <font color="#993399">1</font> <font color="#990000">?</font> <font color="#FF0000">{</font>check<font color="#FF0000">}</font> <font color="#990000">:</font> <font color="#FF0000">{</font>checks<font color="#FF0000">}}</font><font color="#990000">]</font>
                <b><font color="#0000FF">append</font></b> report <font color="#990000">\</font>n<font color="#990000">[</font><b><font color="#0000FF">subst</font></b> <font color="#FF0000">{</font>Overall duration <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$overall_duration</font><font color="#990000">/</font><font color="#993399">1000.00</font><font color="#FF0000">}</font><font color="#990000">]</font>s <b><font color="#0000FF">for</font></b> <font color="#009900">$overall_count</font> rule <font color="#009900">$check_desc</font> across <font color="#009900">$max_count</font> <font color="#009900">$file_desc</font><font color="#990000">.</font><font color="#FF0000">}</font><font color="#990000">]\</font>n
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> header <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$time</font> <font color="#990000">?</font> <font color="#FF0000">{</font>Time report<font color="#990000">:</font><font color="#FF0000">}</font> <font color="#990000">:</font> <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It rules<font color="#990000">:</font><font color="#FF0000">}}</font><font color="#990000">]</font>
        log INFO   <font color="#FF0000">{</font><font color="#009900">$header</font><font color="#990000">\</font>n<font color="#009900">$report</font><font color="#FF0000">}</font>
        log STDOUT <font color="#FF0000">{</font><font color="#009900">$header</font><font color="#990000">\</font>n<font color="#009900">$report</font><font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572251" ID="ID_1457012572251586" MODIFIED="1457012572251" TEXT="drop_it::main" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572251" ID="ID_1457012572251973" MODIFIED="1457012572251" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572263" ID="ID_1457012572263396" MODIFIED="1457012572263">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>main <font color="#FF0000">{</font> argv <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$argv</font><font color="#990000">]</font> <font color="#990000">&lt;</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> argv <font color="#990000">--</font>help
        <font color="#FF0000">}</font>
        
        param parse <font color="#009900">$argv</font> remaining_args

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled PRINT_ARGUMENTS<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log STDOUT <font color="#FF0000">{</font>Application arguments were<font color="#990000">:</font> <font color="#009900">$argv</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Initialise based on config file</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>init_dependencies<font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log STDOUT <font color="#FF0000">{</font>There was a problem initialising due to<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                log ERROR  <font color="#FF0000">{</font>There was a problem initialising due to<font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">exit</font></b>
        <font color="#FF0000">}</font>
        
        param separate <font color="#009900">$remaining_args</font> app_args files dirs

        <b><font color="#0000FF">foreach</font></b> arg <font color="#009900">$app_args</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">--</font> <font color="#009900">$arg</font> <font color="#FF0000">{</font>
                <font color="#FF0000">{</font><font color="#990000">^[</font>A<font color="#990000">-</font>Za<font color="#990000">-</font>z<font color="#990000">]+://.*</font>$<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <i><font color="#9A1900"># Allow URL's to be passed in.</font></i>
                        log DEBUG <font color="#FF0000">{</font>Processing a non<font color="#990000">-</font><b><font color="#0000FF">file</font></b><font color="#FF0000">}</font>
                        <b><font color="#0000FF">lappend</font></b> files <font color="#009900">$arg</font>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                        log ERROR <font color="#FF0000">{</font>Cannot process non<font color="#990000">-</font>existing <b><font color="#0000FF">file</font></b> <font color="#009900">$arg</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If there are no application specific arguments or files to process</font></i>
        <i><font color="#9A1900"># then don't continue processing.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$files</font><font color="#990000">]</font> <font color="#990000">||</font> <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$dirs</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                process <font color="#009900">$files</font> <font color="#009900">$dirs</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg exists LIMIT_TO_RULES<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font>cfg enabled EXPLAIN_RULE_MATCH<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                drop_it<font color="#990000">::</font>load_rules
                drop_it<font color="#990000">::</font>explain_all_rules
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572264" ID="ID_1457012572264034" MODIFIED="1457012572264" TEXT="drop_it::print_debug_info" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572264" ID="ID_1457012572264426" MODIFIED="1457012572264" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572273" ID="ID_1457012572273969" MODIFIED="1457012572273">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>print_debug_info <font color="#FF0000">{}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">puts</font></b> <font color="#FF0000">{</font> <font color="#990000">***</font> Printing debug information <font color="#990000">***</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>check rule action destination<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">puts</font></b> <font color="#FF0000">" *** [string totitle $type] array: "</font>
                $<font color="#FF0000">{</font>type<font color="#FF0000">}</font><font color="#990000">::</font>print_array
        <font color="#FF0000">}</font>
        <i><font color="#9A1900"># flag print</font></i>
        <i><font color="#9A1900"># type print</font></i>
        <b><font color="#0000FF">puts</font></b> <font color="#FF0000">{</font> <font color="#990000">***</font> Configuration<font color="#990000">:</font> <font color="#FF0000">}</font>
        <b><font color="#0000FF">puts</font></b> <font color="#990000">[</font>cfg print<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572274" ID="ID_1457012572274594" MODIFIED="1457012572274" TEXT="drop_it::process" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572274" ID="ID_1457012572274983" MODIFIED="1457012572274" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572286" ID="ID_1457012572286865" MODIFIED="1457012572286">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>process <font color="#FF0000">{</font> files dirs <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RPT
        <b><font color="#0000FF">variable</font></b> RULES

        drop_it<font color="#990000">::</font>load_rules

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled LOG_RULES<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log_rules
        <font color="#FF0000">}</font>

        process_files <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$files</font> <font color="#009900">$dirs</font><font color="#990000">]</font>
        touch unmatched_files
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled ACTION_RULE_MATCH <font color="#993399">1</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> unmatched_files <font color="#990000">[</font>drop_it<font color="#990000">::</font>action_match<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled FUNC_STATS <font color="#993399">0</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                update_stats
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$RPT</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font> <font color="#990000">||</font> <font color="#990000">[</font>cfg enabled TIME<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                drop_it<font color="#990000">::</font>log_match_report <font color="#009900">$RPT</font>
                drop_it<font color="#990000">::</font>log_rules <font color="#990000">--</font><b><font color="#0000FF">time</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$unmatched_files</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> unmatched_files <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$unmatched_files</font> <font color="#990000">\</font>n<font color="#990000">]</font>
                log INFO   <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It reported no match on the <b><font color="#0000FF">file</font></b><font color="#990000">(</font>s<font color="#990000">)</font> provided<font color="#990000">:\</font>n<font color="#009900">$unmatched_files</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>tapp_log<font color="#990000">::</font>log_level_enabled DEBUG<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log STDOUT <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It reported no match on the <b><font color="#0000FF">file</font></b><font color="#990000">(</font>s<font color="#990000">)</font> provided<font color="#990000">:</font> <font color="#990000">\</font>n<font color="#009900">$unmatched_files</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>tapp_log<font color="#990000">::</font>log_level_enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log STDOUT <font color="#FF0000">{</font>Drop<font color="#990000">-</font>It reported no match on some of the files provided<font color="#990000">,</font> perhaps create a new rule<font color="#990000">?\</font>n<font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled FUNC_STATS <font color="#993399">0</font><font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">[</font>stats dirty<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                drop_it<font color="#990000">::</font>purge_void_stats
                stats save
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572287" ID="ID_1457012572287476" MODIFIED="1457012572287" TEXT="drop_it::process_file" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572287" ID="ID_1457012572287868" MODIFIED="1457012572287" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572297" ID="ID_1457012572297394" MODIFIED="1457012572297">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>process_file <font color="#FF0000">{</font> input <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <i><font color="#9A1900"># A blank / null rule_id means here that there were no match on the file.</font></i>
        <i><font color="#9A1900"># We still add this to the match_queue so that we can identify files that</font></i>
        <i><font color="#9A1900"># had no match.</font></i>
        log DEBUG <font color="#FF0000">{</font>Processing <font color="#009900">$input</font><font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> rule_id <font color="#990000">[</font>drop_it<font color="#990000">::</font>get_rule_match <font color="#009900">$input</font><font color="#990000">]</font>
        rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
        match_queue put <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$input</font> <font color="#009900">$rule_id</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572297" ID="ID_1457012572297993" MODIFIED="1457012572297" TEXT="drop_it::process_files" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572298" ID="ID_1457012572298472" MODIFIED="1457012572298" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572308" ID="ID_1457012572308182" MODIFIED="1457012572308">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>process_files <font color="#FF0000">{</font> files <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> <b><font color="#0000FF">file</font></b> <font color="#009900">$files</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>process_file <font color="#009900">$file</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$file</font><font color="#990000">]</font>
                        log ERROR  <font color="#FF0000">{</font>Error processing <font color="#009900">$file_type</font><font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                        log STDOUT <font color="#FF0000">{</font>Error processing <font color="#009900">$file_type</font><font color="#990000">:</font> <font color="#009900">$msg</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572308" ID="ID_1457012572308771" MODIFIED="1457012572308" TEXT="drop_it::purge_void_stats" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572309" ID="ID_1457012572309173" MODIFIED="1457012572309" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572318" ID="ID_1457012572318999" MODIFIED="1457012572319">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>purge_void_stats <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> hashes <font color="#990000">[</font>stats keys<font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> rule_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_all_rules<font color="#990000">]</font> <font color="#FF0000">{</font>
                rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
                <b><font color="#0000FF">set</font></b> hash <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule hash<font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> hashes <font color="#990000">[</font>lremove <font color="#009900">$hashes</font> <font color="#009900">$hash</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">foreach</font></b> hash <font color="#009900">$hashes</font> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font>Removing void hash <font color="#009900">$hash</font><font color="#FF0000">}</font>
                stats remove <font color="#009900">$hash</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572319" ID="ID_1457012572319596" MODIFIED="1457012572319" TEXT="drop_it::reset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572319" ID="ID_1457012572319990" MODIFIED="1457012572319" TEXT="Reset all drop-it arrays and configuration, used for testing purposes." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572320" ID="ID_1457012572320327" MODIFIED="1457012572320" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572329" ID="ID_1457012572329796" MODIFIED="1457012572329">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>reset <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> RPT
        <b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>unset RPT<font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> RPT <font color="#FF0000">{}</font>
        cfg reset
        tapp_log<font color="#990000">::</font>reset
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>check rule<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                $<font color="#FF0000">{</font>type<font color="#FF0000">}</font><font color="#990000">::</font>reset
        <font color="#FF0000">}</font>
        flag reset
        type reset
        destination reset
        action reset
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572330" ID="ID_1457012572330409" MODIFIED="1457012572330" TEXT="drop_it::update_stats" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572330" ID="ID_1457012572330818" MODIFIED="1457012572330" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572341" ID="ID_1457012572341553" MODIFIED="1457012572341">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>update_stats <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> TIME
        
        <b><font color="#0000FF">foreach</font></b> rule_id <font color="#990000">[</font>rule<font color="#990000">::</font>get_rules<font color="#990000">]</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">info</font></b> exists TIME<font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                rule<font color="#990000">::</font>select_rule <font color="#009900">$rule_id</font>
                
                <b><font color="#0000FF">set</font></b> hash <font color="#990000">[</font>rule<font color="#990000">::</font>get_rule hash<font color="#990000">]</font>
                lassign <font color="#990000">[</font>stats get <font color="#009900">$hash</font><font color="#990000">]</font> cr_date num_checks avg_ms num_matches last_match last_update

                <b><font color="#0000FF">set</font></b> avg_ms <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">(</font>double<font color="#990000">(</font><font color="#009900">$avg_ms</font><font color="#990000">)</font> <font color="#990000">*</font> <font color="#009900">$num_checks</font> <font color="#990000">+</font> double<font color="#990000">(</font><font color="#009900">$TIME</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">)))</font> <font color="#990000">/</font> double<font color="#990000">(</font><font color="#009900">$num_checks</font> <font color="#990000">+</font> <font color="#009900">$TIME</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">,</font>count<font color="#990000">))</font><font color="#FF0000">}</font><font color="#990000">]</font>
                <b><font color="#0000FF">incr</font></b> num_checks <font color="#009900">$TIME</font><font color="#990000">(</font><font color="#009900">$rule_id</font><font color="#990000">,</font>count<font color="#990000">)</font>
                <b><font color="#0000FF">set</font></b> last_update <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
                
                stats <b><font color="#0000FF">set</font></b> <font color="#009900">$hash</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$cr_date</font> <font color="#009900">$num_checks</font> <font color="#009900">$avg_ms</font> <font color="#009900">$num_matches</font> <font color="#009900">$last_match</font> <font color="#009900">$last_update</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572342" ID="ID_1457012572342167" MODIFIED="1457012572342" TEXT="drop_it::write_default_config" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572342" ID="ID_1457012572342610" MODIFIED="1457012572342" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572354" ID="ID_1457012572354338" MODIFIED="1457012572354">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> drop_it<font color="#990000">::</font>write_default_config <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        file_utils<font color="#990000">::</font>write_file <font color="#009900">$file</font> <font color="#FF0000">{</font>
<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># Drop-It configuration</font></i>
<i><font color="#9A1900">#</font></i>

<i><font color="#9A1900"># Log settings:</font></i>
<i><font color="#9A1900">#    LOG_DIR     - the log directory</font></i>
<i><font color="#9A1900">#    LOG_FILE    - the log file name (or stdout or stderr)</font></i>
<i><font color="#9A1900">#    LOG_LEVEL   - the log level to log at, limits how much is logged</font></i>
<i><font color="#9A1900"># LOG_FILE  = %Y-%m-%d_%H.drop_it.log</font></i>
LOG_FILE  <font color="#990000">=</font> stdout
LOG_LEVEL <font color="#990000">=</font> ERROR

DEF_TRASH <font color="#990000">=</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> $<font color="#990000">::</font>env<font color="#990000">(</font>HOME<font color="#990000">)</font> Trash<font color="#990000">]</font>

<i><font color="#9A1900"># Automatically create the rules file if it does not already exist</font></i>
AUTO_CREATE_RULES_FILE <font color="#990000">=</font> <font color="#993399">1</font>

<i><font color="#9A1900"># Defines which rules configuration file to use</font></i>
CFG_FILE <font color="#990000">=</font> <font color="#990000">[</font>this_dir<font color="#990000">]/</font>drop_it_rules<font color="#990000">.</font>cfg

<i><font color="#9A1900"># Enables stats keeping of rules performance across runs and adds additional</font></i>
<i><font color="#9A1900"># information to the --rules output to indicate which rules are actually being</font></i>
<i><font color="#9A1900"># used and which can be removed.</font></i>
FUNC_STATS <font color="#990000">=</font> <font color="#993399">0</font>

<i><font color="#9A1900"># Whether to use DBUS for desktop notifications or not. Requires dbus-tcl to be</font></i>
<i><font color="#9A1900"># installed. See http://dbus-tcl.sourceforge.net/</font></i>
DBUS_NOTIFICATION <font color="#990000">=</font> <font color="#993399">0</font>

<i><font color="#9A1900"># List of config items not to output in full when listing all configuration</font></i>
<i><font color="#9A1900"># items using the --show-config option</font></i>
MASKED_CFG <font color="#990000">=</font> HELP ARGS MOREARGS HELP_EXAMPLES

<i><font color="#9A1900"># Command to execute in order to generate a checksum (used for backup purposes)</font></i>
CHECKSUM_CMD <font color="#990000">=</font> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>$<font color="#990000">::</font>tcl_platform<font color="#990000">(</font>platform<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>unix<font color="#FF0000">}</font> <font color="#990000">?</font> <font color="#FF0000">{</font>md5sum <font color="#FF0000">"%file%"</font><font color="#FF0000">}</font> <font color="#990000">:</font> <font color="#FF0000">{</font>md5<font color="#990000">.</font>exe <font color="#FF0000">"%file%"</font><font color="#FF0000">}}</font><font color="#990000">]</font>

<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># This defines the executables to execute in relation to the -contains search.</font></i>
<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># The configuration items are on the form CONTAINS_EXEC_&lt;FILE EXTENSION&gt; where</font></i>
<i><font color="#9A1900"># the file extension is in uppercase and dots are replaced by underscores.</font></i>
<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># There are two placeholders:</font></i>
<i><font color="#9A1900">#   %file%     - will be replaced by the file being processed</font></i>
<i><font color="#9A1900">#   %contains% - will be replaced by the -contains expression for the rule</font></i>
<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># The default command (typically grep) is defined with CONTAINS_EXEC_DEFAULT.</font></i>
<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># For example for a .tar.gz file the configuration item under linux could be:</font></i>
<i><font color="#9A1900">#    CONTAINS_EXEC_TAR_GZ = tar -ztf %file% | grep -Ei "%contains%"</font></i>
<i><font color="#9A1900">#</font></i>
<i><font color="#9A1900"># This is all configurable for cross platform purposes and to allow for</font></i>
<i><font color="#9A1900"># customised searches through archive files.</font></i>
<i><font color="#9A1900">#</font></i>
CONTAINS_GREP <font color="#990000">=</font> grep <font color="#990000">-</font>Ei <font color="#FF0000">"%contains%"</font>
CONTAINS_EXEC_DEFAULT <font color="#990000">=</font> <font color="#009900">$CONTAINS_GREP</font> <font color="#990000">%</font><b><font color="#0000FF">file</font></b><font color="#990000">%</font>
<i><font color="#9A1900"># CONTAINS_EXEC_PDF     = pdftotext %file% - | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_TAR     = tar -tf %file% | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_TGZ     = tar -ztf %file% | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_TAR_GZ  = $CONTAINS_EXEC_TGZ</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_TAR_XZ  = tar -Jtf %file% | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_TAR_BZ2 = tar -jtf %file% | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_TAR_LZ  = tar --lzip -tf %file% | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_7Z      = 7z l %file% | $CONTAINS_GREP</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_ZIP     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_GZ      = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_BZ      = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_BZ2     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_XZ      = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_LZ      = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_ARJ     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_RAR     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_CAB     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_CPIO    = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_RPM     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_ISO     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_DEB     = $CONTAINS_EXEC_7Z</font></i>
<i><font color="#9A1900"># CONTAINS_EXEC_PPTM    = $CONTAINS_EXEC_7Z</font></i>
<font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1457012572381" ID="ID_1457012572381190" MODIFIED="1457012572381" TEXT="drop_it_destination 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012572381" ID="ID_1457012572381436" MODIFIED="1457012572381" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572381" ID="ID_1457012572381595" MODIFIED="1457012572381" TEXT="tapp 1.0"/>
</node>
<node CREATED="1457012572381" ID="ID_1457012572381802" MODIFIED="1457012572381" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572382" ID="ID_1457012572382528" MODIFIED="1457012572382" TEXT="destination.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/destination.tcl"/>
</node>
<node CREATED="1457012572382" ID="ID_1457012572382732" MODIFIED="1457012572382" TEXT="destination" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012572383" ID="ID_1457012572383128" MODIFIED="1457012572383" TEXT="destination::subst_destination" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572383" ID="ID_1457012572383408" MODIFIED="1457012572383" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572395" ID="ID_1457012572395168" MODIFIED="1457012572395">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> destination<font color="#990000">::</font>subst_destination <font color="#FF0000">{</font> destination_id input <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$input</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$file_type</font> <font color="#990000">==</font> <font color="#FF0000">{</font>text<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> file_absolutepath <font color="#009900">$input</font>
                <b><font color="#0000FF">set</font></b> file_name <font color="#009900">$input</font>
                <b><font color="#0000FF">set</font></b> file_time <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> file_absolutepath <font color="#990000">[</font>file_utils<font color="#990000">::</font>get_absolute_path <font color="#009900">$input</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> file_name <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$input</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> file_time <font color="#990000">[</font>file_utils<font color="#990000">::</font>determine_file_time <font color="#009900">$input</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font><b><font color="#0000FF">file</font></b><font color="#990000">%</font>     <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#990000">\\</font> <font color="#990000">/</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#990000">\</font> <font color="#FF0000">}}</font> <font color="#009900">$file_absolutepath</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font>filename<font color="#990000">%</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{{</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#990000">\</font> <font color="#FF0000">}}</font> <font color="#009900">$file_name</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font>rootname<font color="#990000">%</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{{</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#990000">\</font> <font color="#FF0000">}}</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> rootname <font color="#009900">$file_name</font><font color="#990000">]]</font>
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font>dirname<font color="#990000">%</font>  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#990000">\\</font> <font color="#990000">/</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#990000">\</font> <font color="#FF0000">}}</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$input</font><font color="#990000">]]</font>
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font>Y<font color="#990000">%</font>        <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$file_time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">%</font>Y<font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font>m<font color="#990000">%</font>        <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$file_time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">%</font>m<font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> replace_list <font color="#990000">%</font>d<font color="#990000">%</font>        <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$file_time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#990000">%</font>d<font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> destination <font color="#990000">[</font>destination get <font color="#009900">$destination_id</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> destination <font color="#990000">[</font>cfg <b><font color="#0000FF">subst</font></b> <font color="#009900">$destination</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> destination <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$replace_list</font> <font color="#009900">$destination</font><font color="#990000">]</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$destination</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1457012572396" ID="ID_1457012572396088" MODIFIED="1457012572396" TEXT="drop_it_check 1.0" POSITION="left" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012572396" ID="ID_1457012572396404" MODIFIED="1457012572396" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572396" ID="ID_1457012572396596" MODIFIED="1457012572396" TEXT="tapp 1.0"/>
<node CREATED="1457012572396" ID="ID_1457012572396724" MODIFIED="1457012572396" TEXT="drop_it_type 1.0" LINK="#ID_1457012571841757"/>
<node CREATED="1457012572396" ID="ID_1457012572396854" MODIFIED="1457012572396" TEXT="drop_it_flag 1.0" LINK="#ID_1457012572106354"/>
</node>
<node CREATED="1457012572397" ID="ID_1457012572397089" MODIFIED="1457012572397" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572397" ID="ID_1457012572397890" MODIFIED="1457012572397" TEXT="check.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/check.tcl"/>
</node>
<node CREATED="1457012572398" ID="ID_1457012572398092" MODIFIED="1457012572398" TEXT="check" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012572398" ID="ID_1457012572398581" MODIFIED="1457012572398" TEXT="check::add_check" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572398" ID="ID_1457012572398953" MODIFIED="1457012572398" TEXT="Loop through existing checks&#xA;If matching check found, then return check_id&#xA;Otherwise, add new check and return check id&#xA;&#xA;Array synompsis:&#xA;  CHECK(idx)                     - the last serial ID used&#xA;  CHECK(check_ids)               - list of all existing check_ids&#xA;  CHECK(&lt;check_id&gt;,match)        - regular expression, glob match, or empty&#xA;  CHECK(&lt;check_id&gt;,type_id)      - the type of check (see type.tcl)&#xA;  CHECK(&lt;check_id&gt;,flag_ids)     - list of type specific flags (see flag.tcl)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572399" ID="ID_1457012572399265" MODIFIED="1457012572399" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572409" ID="ID_1457012572409720" MODIFIED="1457012572409">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>add_check <font color="#FF0000">{</font> type match flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">set</font></b> flag_ids <font color="#FF0000">{}</font>
        <b><font color="#0000FF">foreach</font></b> flag <font color="#009900">$flags</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> flag_ids <font color="#990000">[</font>flag add <font color="#009900">$flag</font> <font color="#990000">-</font>unique<font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> type_id  <font color="#990000">[</font>type add <font color="#009900">$type</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> check_id <font color="#990000">[</font>check<font color="#990000">::</font>get_check_id <font color="#009900">$type_id</font> <font color="#009900">$match</font> <font color="#009900">$flag_ids</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># If no match then add the new check</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$check_id</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_id <font color="#990000">[</font>check<font color="#990000">::</font>get_next_check_id<font color="#990000">]</font>

                <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>match<font color="#990000">)</font>    <font color="#009900">$match</font>
                <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>type_id<font color="#990000">)</font>  <font color="#009900">$type_id</font>
                <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)</font> <font color="#009900">$flag_ids</font>

                <b><font color="#0000FF">lappend</font></b> CHECK<font color="#990000">(</font>check_ids<font color="#990000">)</font> <font color="#009900">$check_id</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$check_id</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572410" ID="ID_1457012572410334" MODIFIED="1457012572410" TEXT="check::check" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572410" ID="ID_1457012572410707" MODIFIED="1457012572410" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572421" ID="ID_1457012572421524" MODIFIED="1457012572421">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check <font color="#FF0000">{</font> check_id input <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        assert <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>match<font color="#990000">)]</font><font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> match <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>match<font color="#990000">)</font>
        <b><font color="#0000FF">set</font></b> match <font color="#990000">[</font>cfg <b><font color="#0000FF">subst</font></b> <font color="#009900">$match</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> flags <font color="#990000">[</font>flag all <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)]</font>
        <b><font color="#0000FF">set</font></b> type  <font color="#990000">[</font>type get <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>type_id<font color="#990000">)]</font>

        <i><font color="#9A1900"># Look for -inverse flag</font></i>
        <b><font color="#0000FF">set</font></b> inverse <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> f_idx <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#990000">-</font>inverse<font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$f_idx</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> inverse <font color="#993399">1</font>
                <b><font color="#0000FF">set</font></b> flags <font color="#990000">[</font><b><font color="#0000FF">lreplace</font></b> <font color="#009900">$flags</font> <font color="#009900">$f_idx</font> <font color="#009900">$f_idx</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> check_result <font color="#990000">[</font>check<font color="#990000">::</font>check_<font color="#009900">$type</font> <font color="#009900">$input</font> <font color="#009900">$match</font> <font color="#009900">$flags</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$inverse</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_result <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">!</font><font color="#009900">$check_result</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$check_result</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572422" ID="ID_1457012572422115" MODIFIED="1457012572422" TEXT="check::check_contains" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572422" ID="ID_1457012572422500" MODIFIED="1457012572422" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572434" ID="ID_1457012572434410" MODIFIED="1457012572434">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_contains <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$input</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> rule_matched <font color="#993399">0</font>

        <b><font color="#0000FF">switch</font></b> <font color="#990000">-</font>exact <font color="#009900">$file_type</font> <font color="#FF0000">{</font>
        directory <font color="#FF0000">{</font> 
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font> <font color="#990000">![</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font> <b><font color="#0000FF">set</font></b> subfiles <font color="#990000">[</font><b><font color="#0000FF">glob</font></b> <font color="#990000">-</font>types <font color="#FF0000">{</font>f d r<font color="#FF0000">}</font> <font color="#990000">-</font>path <font color="#009900">$input</font><font color="#990000">/</font> <font color="#990000">*]</font> <font color="#FF0000">}</font> msg<font color="#990000">]</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">foreach</font></b> subfile <font color="#009900">$subfiles</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">set</font></b> rule_matched <font color="#990000">[</font>check<font color="#990000">::</font>check_name <font color="#009900">$subfile</font> <font color="#009900">$match</font> <font color="#009900">$flags</font><font color="#990000">]</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$rule_matched</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">break</font></b>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        log INFO <font color="#FF0000">{</font>Directory <font color="#009900">$input</font> doesn<font color="#FF0000">'t contain any files or directories: $msg}</font>
<font color="#FF0000">                }</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        file {</font>
<font color="#FF0000">                touch contains_exec</font>
<font color="#FF0000">                </font>
<font color="#FF0000">                # Get the file extension preferring the presense of a double extension if</font>
<font color="#FF0000">                # it exists. For example prefer the .tar.gz extension over .gz</font>
<font color="#FF0000">                if {[regexp {</font><font color="#CC33CC">\.</font><font color="#FF0000">((?:[0-9A-Za-z]+</font><font color="#CC33CC">\.</font><font color="#FF0000">)?([0-9A-Za-z]+))$} $input --&gt; file_ext1 file_ext2]} {</font>
<font color="#FF0000">                </font>
<font color="#FF0000">                        # Retrieve the contains executable if configured for the found extension</font>
<font color="#FF0000">                        foreach ext [list $file_ext1 $file_ext2] {</font>
<font color="#FF0000">                                if {$ext == {}} {</font>
<font color="#FF0000">                                        continue</font>
<font color="#FF0000">                                }</font>
<font color="#FF0000">                                set ext [string toupper $ext]</font>
<font color="#FF0000">                                set ext [string map {. _} $ext]</font>
<font color="#FF0000">                                set contains_exec [cfg get CONTAINS_EXEC_$ext {}]</font>
<font color="#FF0000">                                if {$contains_exec != {}} {</font>
<font color="#FF0000">                                        break</font>
<font color="#FF0000">                                }</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                }</font>

<font color="#FF0000">                # If no configuration exists for the given extension then we need to use</font>
<font color="#FF0000">                # the default.</font>
<font color="#FF0000">                if {$contains_exec == {}} {</font>
<font color="#FF0000">                        set contains_exec [cfg get CONTAINS_EXEC_DEFAULT {}]</font>

<font color="#FF0000">                        # It is possible that no default contains executable has been defined</font>
<font color="#FF0000">                        # in which case we cannot confirm that the file actually contains the</font>
<font color="#FF0000">                        # expression we are searching for.</font>
<font color="#FF0000">                        if {$contains_exec == {}} {</font>
<font color="#FF0000">                                log WARN {No contains command found for extension $ext}</font>
<font color="#FF0000">                                return 0</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                }</font>

<font color="#FF0000">                set replace_map [list                        %file%     [string map {{ } {</font><font color="#CC33CC">\ </font><font color="#FF0000">}} $input]                        %contains% $match                ]</font>

<font color="#FF0000">                set contains_exec [string map $replace_map $contains_exec]</font>

<font color="#FF0000">                log DEBUG {Executing contains executable: $contains_exec}</font>

<font color="#FF0000">                if {[catch {set output [exec {*}$contains_exec]} msg]} {</font>
<font color="#FF0000">                        # child process exited abnormally is usually just the exit</font>
<font color="#FF0000">                        # signal indicating that there is not a match.</font>
<font color="#FF0000">                        if {$msg != {child process exited abnormally}} {</font>
<font color="#FF0000">                                log ERROR {check::check_contains - Error occurred while executing $contains_exec due to $msg}</font>
<font color="#FF0000">                        } else {</font>
<font color="#FF0000">                                log DEBUG {check::check_contains - No match found while executing $contains_exec}</font>
<font color="#FF0000">                        }</font>
<font color="#FF0000">                } elseif {$output != {}} {</font>
<font color="#FF0000">                        log DEBUG {check::check_contains - match found: $output}</font>
<font color="#FF0000">                        set rule_matched 1</font>
<font color="#FF0000">                }</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        text {</font>
<font color="#FF0000">                set rule_matched [check::check_match $input $match $flags]</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        default {</font>
<font color="#FF0000">                log ERROR {check::check_contains - Unknown file type ($file_type)}</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        return $rule_matched</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572435" ID="ID_1457012572435051" MODIFIED="1457012572435" TEXT="check::check_executable" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572435" ID="ID_1457012572435425" MODIFIED="1457012572435" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572444" ID="ID_1457012572444616" MODIFIED="1457012572444">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_executable <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> executable <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572445" ID="ID_1457012572445208" MODIFIED="1457012572445" TEXT="check::check_exists" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572445" ID="ID_1457012572445579" MODIFIED="1457012572445" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572454" ID="ID_1457012572454867" MODIFIED="1457012572454">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_exists <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$match</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572455" ID="ID_1457012572455441" MODIFIED="1457012572455" TEXT="check::check_is_empty" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572455" ID="ID_1457012572455814" MODIFIED="1457012572455" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572465" ID="ID_1457012572465163" MODIFIED="1457012572465">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_is_empty <font color="#FF0000">{</font> input <font color="#FF0000">{</font>match <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>file_utils<font color="#990000">::</font>is_empty <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572465" ID="ID_1457012572465740" MODIFIED="1457012572465" TEXT="check::check_java" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572466" ID="ID_1457012572466117" MODIFIED="1457012572466" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572475" ID="ID_1457012572475822" MODIFIED="1457012572475">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_java <font color="#FF0000">{</font> <font color="#FF0000">{</font>input <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>match <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>$<font color="#990000">::</font>tcl_platform<font color="#990000">(</font>platform<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>java<font color="#FF0000">}}</font><font color="#990000">];</font> <i><font color="#9A1900"># OS/400</font></i>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572476" ID="ID_1457012572476408" MODIFIED="1457012572476" TEXT="check::check_larger" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572476" ID="ID_1457012572476786" MODIFIED="1457012572476" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572486" ID="ID_1457012572486788" MODIFIED="1457012572486">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_larger <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> size <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_size <font color="#009900">$input</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> comp <font color="#990000">[</font>file_utils<font color="#990000">::</font>decode_human_readable_size <font color="#009900">$match</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$size</font> <font color="#990000">&gt;</font> <font color="#009900">$comp</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572487" ID="ID_1457012572487411" MODIFIED="1457012572487" TEXT="check::check_mac" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572487" ID="ID_1457012572487816" MODIFIED="1457012572487" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572497" ID="ID_1457012572497995" MODIFIED="1457012572498">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_mac <font color="#FF0000">{</font> <font color="#FF0000">{</font>input <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>match <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>$<font color="#990000">::</font>tcl_platform<font color="#990000">(</font>platform<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>macintosh<font color="#FF0000">}}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572498" ID="ID_1457012572498830" MODIFIED="1457012572498" TEXT="check::check_name" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572499" ID="ID_1457012572499244" MODIFIED="1457012572499" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572510" ID="ID_1457012572510092" MODIFIED="1457012572510">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_name <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$input</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> input <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$input</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>check<font color="#990000">::</font>_check_match <font color="#009900">$input</font> <font color="#009900">$match</font> <font color="#009900">$flags</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572510" ID="ID_1457012572510785" MODIFIED="1457012572510" TEXT="check::check_newer" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572511" ID="ID_1457012572511238" MODIFIED="1457012572511" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572521" ID="ID_1457012572521650" MODIFIED="1457012572521">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_newer <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$input</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><font color="#990000">-*</font><font color="#FF0000">}</font> <font color="#009900">$match</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                prepend match <font color="#990000">-</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> mtime <font color="#009900">$input</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> add <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$match</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572522" ID="ID_1457012572522309" MODIFIED="1457012572522" TEXT="check::check_not_exists" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572522" ID="ID_1457012572522730" MODIFIED="1457012572522" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572533" ID="ID_1457012572533613" MODIFIED="1457012572533">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_not_exists <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$match</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572534" ID="ID_1457012572534301" MODIFIED="1457012572534" TEXT="check::check_older" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572534" ID="ID_1457012572534739" MODIFIED="1457012572534" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572545" ID="ID_1457012572545437" MODIFIED="1457012572545">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_older <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$input</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#993399">0</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> match <font color="#FF0000">{</font><font color="#990000">-*</font><font color="#FF0000">}</font> <font color="#009900">$match</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                prepend match <font color="#990000">-</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> mtime <font color="#009900">$input</font><font color="#990000">]</font> <font color="#990000">&lt;</font> <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> add <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> seconds<font color="#990000">]</font> <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$match</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572546" ID="ID_1457012572546073" MODIFIED="1457012572546" TEXT="check::check_path" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572546" ID="ID_1457012572546526" MODIFIED="1457012572546" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572556" ID="ID_1457012572556730" MODIFIED="1457012572556">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_path <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$input</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> input <font color="#990000">[</font>file_utils<font color="#990000">::</font>get_absolute_path <font color="#009900">$input</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>check<font color="#990000">::</font>_check_match <font color="#009900">$input</font> <font color="#009900">$match</font> <font color="#009900">$flags</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572557" ID="ID_1457012572557324" MODIFIED="1457012572557" TEXT="check::check_readable" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572557" ID="ID_1457012572557751" MODIFIED="1457012572557" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572569" ID="ID_1457012572569244" MODIFIED="1457012572569">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_readable <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> readable <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572569" ID="ID_1457012572569856" MODIFIED="1457012572569" TEXT="check::check_smaller" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572570" ID="ID_1457012572570329" MODIFIED="1457012572570" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572580" ID="ID_1457012572580743" MODIFIED="1457012572580">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_smaller <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> size <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_size <font color="#009900">$input</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> comp <font color="#990000">[</font>file_utils<font color="#990000">::</font>decode_human_readable_size <font color="#009900">$match</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$size</font> <font color="#990000">&lt;</font> <font color="#009900">$comp</font><font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572581" ID="ID_1457012572581340" MODIFIED="1457012572581" TEXT="check::check_type" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572581" ID="ID_1457012572581764" MODIFIED="1457012572581" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572591" ID="ID_1457012572591882" MODIFIED="1457012572591">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_type <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">set</font></b> file_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$input</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> type <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#009900">$CHECK</font><font color="#990000">(</font>file_type_map<font color="#990000">)</font> <font color="#009900">$file_type</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> match <font color="#990000">*</font><font color="#009900">$type</font><font color="#990000">*</font> <font color="#009900">$match</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572592" ID="ID_1457012572592482" MODIFIED="1457012572592" TEXT="check::check_unix" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572592" ID="ID_1457012572592895" MODIFIED="1457012572592" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572605" ID="ID_1457012572605153" MODIFIED="1457012572605">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_unix <font color="#FF0000">{</font> <font color="#FF0000">{</font>input <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>match <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>$<font color="#990000">::</font>tcl_platform<font color="#990000">(</font>platform<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>unix<font color="#FF0000">}}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572605" ID="ID_1457012572605778" MODIFIED="1457012572605" TEXT="check::check_windows" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572606" ID="ID_1457012572606223" MODIFIED="1457012572606" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572617" ID="ID_1457012572617167" MODIFIED="1457012572617">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_windows <font color="#FF0000">{</font> <font color="#FF0000">{</font>input <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>match <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font>$<font color="#990000">::</font>tcl_platform<font color="#990000">(</font>platform<font color="#990000">)</font> <font color="#990000">==</font> <font color="#FF0000">{</font>windows<font color="#FF0000">}}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572617" ID="ID_1457012572617748" MODIFIED="1457012572617" TEXT="check::check_writable" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572618" ID="ID_1457012572618151" MODIFIED="1457012572618" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572627" ID="ID_1457012572627560" MODIFIED="1457012572627">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>check_writable <font color="#FF0000">{</font> input match <font color="#FF0000">{</font>flags <font color="#FF0000">{}}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> writable <font color="#009900">$input</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572628" ID="ID_1457012572628166" MODIFIED="1457012572628" TEXT="check::explain" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572628" ID="ID_1457012572628582" MODIFIED="1457012572628" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572641" ID="ID_1457012572641184" MODIFIED="1457012572641">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>explain <font color="#FF0000">{</font> check_id input <font color="#FF0000">{</font>matches <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        lassign <font color="#990000">[</font>check<font color="#990000">::</font>to_string <font color="#009900">$check_id</font><font color="#990000">]</font> check_type checks match 

        <b><font color="#0000FF">set</font></b> show_matches <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> show_does <font color="#993399">0</font>
        <b><font color="#0000FF">set</font></b> show_is <font color="#993399">0</font>

        <b><font color="#0000FF">set</font></b> checks <font color="#990000">[</font>lremove <font color="#009900">$checks</font> <font color="#990000">-</font><font color="#009900">$check_type</font><font color="#990000">]</font>
        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$check_type</font> <font color="#FF0000">{</font>
        is_empty <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$input</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> match <font color="#FF0000">{</font>empty<font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> show_is <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        contains <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_type <font color="#990000">[</font>file_utils<font color="#990000">::</font>file_type <font color="#009900">$input</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> checks <font color="#990000">[</font>linsert <font color="#009900">$checks</font> <font color="#993399">0</font> <font color="#FF0000">{</font>contain<font color="#FF0000">}</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> show_does <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        exists <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_type <font color="#FF0000">{}</font>
                <b><font color="#0000FF">set</font></b> match <font color="#FF0000">{</font>exist<font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> show_does <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        not_exists <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_type <font color="#FF0000">{}</font>
                <b><font color="#0000FF">set</font></b> match <font color="#FF0000">{</font>not exist<font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> show_does <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> show_matches <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        touch check_output
        <b><font color="#0000FF">lappend</font></b> check_output <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#FF0000">{</font><font color="#990000">-</font> <font color="#990000">+</font><font color="#FF0000">}</font> <font color="#009900">$matches</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> check_output <font color="#009900">$check_type</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled VERBOSE<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> check_output <font color="#FF0000">"check ($check_id)"</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$show_matches</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> check_output <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#FF0000">{{</font>does not match<font color="#FF0000">}</font> <font color="#FF0000">{</font>matches<font color="#FF0000">}}</font> <font color="#009900">$matches</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$show_does</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> check_output <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#FF0000">{{</font>does not<font color="#FF0000">}</font> does<font color="#FF0000">}</font> <font color="#009900">$matches</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$show_is</font> <font color="#990000">==</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> check_output <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#FF0000">{{</font>is not<font color="#FF0000">}</font> is<font color="#FF0000">}</font> <font color="#009900">$matches</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">lappend</font></b> check_output <font color="#009900">$checks</font>
        <b><font color="#0000FF">lappend</font></b> check_output <font color="#009900">$match</font>
        <b><font color="#0000FF">set</font></b> check_output <font color="#990000">[</font>lremove <font color="#009900">$check_output</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#009900">$check_output</font> <font color="#FF0000">{</font> <font color="#FF0000">}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572641" ID="ID_1457012572641833" MODIFIED="1457012572641" TEXT="check::get_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572642" ID="ID_1457012572642245" MODIFIED="1457012572642" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572651" ID="ID_1457012572651532" MODIFIED="1457012572651">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_array <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>array get CHECK<font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572652" ID="ID_1457012572652118" MODIFIED="1457012572652" TEXT="check::get_check_id" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572652" ID="ID_1457012572652532" MODIFIED="1457012572652" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572662" ID="ID_1457012572662706" MODIFIED="1457012572662">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_check_id <font color="#FF0000">{</font> type_id match flag_ids <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">set</font></b> search_string <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$type_id</font> <font color="#009900">$match</font> <font color="#009900">$flag_ids</font><font color="#990000">]</font> <font color="#FF0000">{</font><font color="#990000">|</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> check_id <font color="#009900">$CHECK</font><font color="#990000">(</font>check_ids<font color="#990000">)</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> check_string <font color="#990000">[</font><b><font color="#0000FF">join</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>type_id<font color="#990000">)</font> <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>match<font color="#990000">)</font> <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)]</font> <font color="#FF0000">{</font><font color="#990000">|</font><font color="#FF0000">}</font><font color="#990000">]</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$search_string</font> <font color="#990000">==</font> <font color="#009900">$check_string</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">return</font></b> <font color="#009900">$check_id</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">-</font><font color="#993399">1</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572663" ID="ID_1457012572663329" MODIFIED="1457012572663" TEXT="check::get_check_info" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572663" ID="ID_1457012572663759" MODIFIED="1457012572663" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572673" ID="ID_1457012572673835" MODIFIED="1457012572673">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_check_info <font color="#FF0000">{</font> check_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>check<font color="#990000">::</font>get_type  <font color="#009900">$check_id</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>check<font color="#990000">::</font>get_match <font color="#009900">$check_id</font><font color="#990000">]</font>
        <b><font color="#0000FF">lappend</font></b> output <font color="#990000">[</font>check<font color="#990000">::</font>get_flags <font color="#009900">$check_id</font><font color="#990000">]</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$output</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572674" ID="ID_1457012572674488" MODIFIED="1457012572674" TEXT="check::get_flags" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572674" ID="ID_1457012572674907" MODIFIED="1457012572674" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572684" ID="ID_1457012572684882" MODIFIED="1457012572684">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_flags <font color="#FF0000">{</font> check_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>flag all <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572685" ID="ID_1457012572685532" MODIFIED="1457012572685" TEXT="check::get_match" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572685" ID="ID_1457012572685996" MODIFIED="1457012572686" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572701" ID="ID_1457012572701425" MODIFIED="1457012572701">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_match <font color="#FF0000">{</font> check_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">return</font></b> <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>match<font color="#990000">)</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572702" ID="ID_1457012572702113" MODIFIED="1457012572702" TEXT="check::get_match_method" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572702" ID="ID_1457012572702685" MODIFIED="1457012572702" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572713" ID="ID_1457012572713833" MODIFIED="1457012572713">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_match_method <font color="#FF0000">{</font> flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> match_method <font color="#990000">[</font>cfg get DEFAULT_MATCH_METHOD <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">-</font><b><font color="#0000FF">glob</font></b><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> match_method <font color="#990000">-</font><b><font color="#0000FF">glob</font></b>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">^-</font>re<font color="#990000">?(</font>gexp<font color="#990000">?)?</font>$<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> match_method <font color="#990000">-</font><b><font color="#0000FF">regexp</font></b>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$match_method</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572714" ID="ID_1457012572714730" MODIFIED="1457012572714" TEXT="check::get_next_check_id" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572715" ID="ID_1457012572715381" MODIFIED="1457012572715" TEXT="Returns the next available check ID (serial)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572715" ID="ID_1457012572715889" MODIFIED="1457012572715" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572731" ID="ID_1457012572731824" MODIFIED="1457012572731">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_next_check_id <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">incr</font></b> CHECK<font color="#990000">(</font>idx<font color="#990000">)</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$CHECK</font><font color="#990000">(</font>check_ids<font color="#990000">)</font> <font color="#009900">$CHECK</font><font color="#990000">(</font>idx<font color="#990000">)]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>rule<font color="#990000">::</font>get_next_check_id<font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$CHECK</font><font color="#990000">(</font>idx<font color="#990000">)</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572732" ID="ID_1457012572732670" MODIFIED="1457012572732" TEXT="check::get_optimal_check_order" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572733" ID="ID_1457012572733138" MODIFIED="1457012572733" TEXT="Sort the check_ids based on type to get optimal performance" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572733" ID="ID_1457012572733521" MODIFIED="1457012572733" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572744" ID="ID_1457012572744177" MODIFIED="1457012572744">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_optimal_check_order <font color="#FF0000">{</font> check_ids <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">variable</font></b> CHECK

        touch check_order
        touch deprioritised

        <b><font color="#0000FF">foreach</font></b> check_id <font color="#009900">$check_ids</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$CHECK</font><font color="#990000">(</font>deprioritised_type_ids<font color="#990000">)</font> <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>type_id<font color="#990000">)]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> deprioritised <font color="#009900">$check_id</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <b><font color="#0000FF">lappend</font></b> check_order <font color="#009900">$check_id</font>
        <font color="#FF0000">}</font>
                
        <b><font color="#0000FF">lappend</font></b> check_order <font color="#FF0000">{</font><font color="#990000">*</font><font color="#FF0000">}</font><font color="#009900">$deprioritised</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$check_order</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572744" ID="ID_1457012572744811" MODIFIED="1457012572744" TEXT="check::get_type" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572745" ID="ID_1457012572745299" MODIFIED="1457012572745" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572755" ID="ID_1457012572755574" MODIFIED="1457012572755">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>get_type <font color="#FF0000">{</font> check_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>type get <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>type_id<font color="#990000">)]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572756" ID="ID_1457012572756233" MODIFIED="1457012572756" TEXT="check::init" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572756" ID="ID_1457012572756703" MODIFIED="1457012572756" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572767" ID="ID_1457012572767499" MODIFIED="1457012572767">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>init <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK
        
        <b><font color="#0000FF">foreach</font></b> type <font color="#FF0000">{</font>path name smaller larger contains<font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">lappend</font></b> type_ids <font color="#990000">[</font>type add <font color="#009900">$type</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font>deprioritised_type_ids<font color="#990000">)</font> <font color="#009900">$type_ids</font>

        <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font>file_type_map<font color="#990000">)</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">file</font></b>             F
                directory        D
                characterSpecial C
                blockSpecial     B
                fifo             P
                link             L
                <b><font color="#0000FF">socket</font></b>           S
                text             T
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572768" ID="ID_1457012572768144" MODIFIED="1457012572768" TEXT="check::is_case_sensitive" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572768" ID="ID_1457012572768605" MODIFIED="1457012572768" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572779" ID="ID_1457012572779243" MODIFIED="1457012572779">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>is_case_sensitive <font color="#FF0000">{</font> flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">set</font></b> case_sensitive <font color="#990000">[</font>cfg get DEFAULT_CASE_SENSITIVE <font color="#993399">1</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">-</font>nocase<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> case_sensitive <font color="#993399">0</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">-</font><b><font color="#0000FF">case</font></b><font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> case_sensitive <font color="#993399">1</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#009900">$case_sensitive</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572779" ID="ID_1457012572779876" MODIFIED="1457012572779" TEXT="check::print_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572780" ID="ID_1457012572780337" MODIFIED="1457012572780" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572792" ID="ID_1457012572792326" MODIFIED="1457012572792">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>print_array <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>parray CHECK<font color="#FF0000">}</font> msg<font color="#990000">]</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">error</font></b> writing <font color="#FF0000">"std(out|err)"</font><font color="#990000">:</font> broken pipe<font color="#FF0000">}</font> <font color="#009900">$msg</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">puts</font></b> stderr <font color="#009900">$msg</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572792" ID="ID_1457012572792940" MODIFIED="1457012572792" TEXT="check::remove_flag" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572793" ID="ID_1457012572793397" MODIFIED="1457012572793" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572804" ID="ID_1457012572804129" MODIFIED="1457012572804">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>remove_flag <font color="#FF0000">{</font> check_id flag <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK
        <b><font color="#0000FF">set</font></b> flag_id <font color="#990000">[</font>flag id <font color="#009900">$flag</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)</font> <font color="#990000">[</font>lremove <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)</font> <font color="#009900">$flag_id</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572804" ID="ID_1457012572804772" MODIFIED="1457012572804" TEXT="check::remove_flags" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572805" ID="ID_1457012572805232" MODIFIED="1457012572805" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572815" ID="ID_1457012572815555" MODIFIED="1457012572815">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>remove_flags <font color="#FF0000">{</font> check_id args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> flag <font color="#009900">$args</font> <font color="#FF0000">{</font>
                check<font color="#990000">::</font>remove_flag <font color="#009900">$check_id</font> <font color="#009900">$flag</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572816" ID="ID_1457012572816207" MODIFIED="1457012572816" TEXT="check::reset" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572816" ID="ID_1457012572816696" MODIFIED="1457012572816" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572826" ID="ID_1457012572826709" MODIFIED="1457012572826">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>reset <font color="#FF0000">{}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        array unset CHECK
        <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font>check_ids<font color="#990000">)</font> <font color="#FF0000">{}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572827" ID="ID_1457012572827381" MODIFIED="1457012572827" TEXT="check::set_array" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572827" ID="ID_1457012572827869" MODIFIED="1457012572827" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572840" ID="ID_1457012572840321" MODIFIED="1457012572840">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>set_array <font color="#FF0000">{</font> check_list <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        array <b><font color="#0000FF">set</font></b> CHECK <font color="#009900">$check_list</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572840" ID="ID_1457012572840957" MODIFIED="1457012572840" TEXT="check::set_flag" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572841" ID="ID_1457012572841444" MODIFIED="1457012572841" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572852" ID="ID_1457012572852212" MODIFIED="1457012572852">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>set_flag <font color="#FF0000">{</font> check_id flag <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK
        <b><font color="#0000FF">set</font></b> flag_ids <font color="#990000">[</font>flag add <font color="#009900">$flag</font> <font color="#990000">-</font>unique<font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">info</font></b> exists CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> flag_ids <font color="#990000">[</font><b><font color="#0000FF">lsort</font></b> <font color="#990000">-</font>unique <font color="#990000">[</font><b><font color="#0000FF">concat</font></b> <font color="#009900">$flag_ids</font> <font color="#009900">$CHECK</font><font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)]]</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>flag_ids<font color="#990000">)</font> <font color="#009900">$flag_ids</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572852" ID="ID_1457012572852850" MODIFIED="1457012572852" TEXT="check::set_flags" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572853" ID="ID_1457012572853353" MODIFIED="1457012572853" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572863" ID="ID_1457012572863034" MODIFIED="1457012572863">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>set_flags <font color="#FF0000">{</font> check_id args <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        <b><font color="#0000FF">foreach</font></b> flag <font color="#009900">$args</font> <font color="#FF0000">{</font>
                check<font color="#990000">::</font>set_flag <font color="#009900">$check_id</font> <font color="#009900">$flag</font>
        <font color="#FF0000">}</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572863" ID="ID_1457012572863667" MODIFIED="1457012572863" TEXT="check::set_match" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572864" ID="ID_1457012572864137" MODIFIED="1457012572864" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572874" ID="ID_1457012572874068" MODIFIED="1457012572874">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>set_match <font color="#FF0000">{</font> check_id match <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK

        <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>match<font color="#990000">)</font> <font color="#009900">$match</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572874" ID="ID_1457012572874728" MODIFIED="1457012572874" TEXT="check::set_type" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572875" ID="ID_1457012572875202" MODIFIED="1457012572875" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572885" ID="ID_1457012572885809" MODIFIED="1457012572885">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>set_type <font color="#FF0000">{</font> check_id type <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> CHECK
        <b><font color="#0000FF">set</font></b> type_id <font color="#990000">[</font>type add <font color="#009900">$type</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> CHECK<font color="#990000">(</font><font color="#009900">$check_id</font><font color="#990000">,</font>type_id<font color="#990000">)</font> <font color="#009900">$type_id</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572886" ID="ID_1457012572886558" MODIFIED="1457012572886" TEXT="check::to_string" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572887" ID="ID_1457012572887064" MODIFIED="1457012572887" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572901" ID="ID_1457012572901268" MODIFIED="1457012572901">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> check<font color="#990000">::</font>to_string <font color="#FF0000">{</font> check_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">variable</font></b> <font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS

        touch check match

        lassign <font color="#990000">[</font>check<font color="#990000">::</font>get_check_info <font color="#009900">$check_id</font><font color="#990000">]</font> check_type check_match check_flags

        <b><font color="#0000FF">foreach</font></b> check_flag <font color="#009900">$check_flags</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font>nvl <font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS<font color="#990000">(</font><font color="#009900">$check_flag</font><font color="#990000">)</font> <font color="#FF0000">{}</font><font color="#990000">]</font> <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#FF0000">{</font>check_flag<font color="#FF0000">}}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">lappend</font></b> check <font color="#009900">$check_flag</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">switch</font></b> <font color="#990000">--</font> <font color="#009900">$check_type</font> <font color="#FF0000">{</font>
        name <font color="#990000">-</font>
        path <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> param_pattern <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>exact <font color="#009900">$check_flags</font> <font color="#FF0000">{</font><font color="#990000">-</font>re<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">&gt;</font> <font color="#990000">-</font><font color="#993399">1</font> <font color="#990000">?</font> <font color="#FF0000">{</font><font color="#990000">-</font>re<font color="#FF0000">}</font> <font color="#990000">:</font> <font color="#FF0000">{</font><font color="#990000">-</font><b><font color="#0000FF">glob</font></b><font color="#FF0000">}}</font><font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> check <font color="#990000">-</font><font color="#009900">$check_type</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">default</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> param_pattern <font color="#990000">*-</font><font color="#009900">$check_type</font>
        <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">set</font></b> param_name <font color="#990000">[</font>array names <font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS <font color="#009900">$param_pattern</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">llength</font></b> <font color="#009900">$param_name</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font>check<font color="#990000">::</font>to_string<font color="#990000">:</font> Unexpected <b><font color="#0000FF">error</font></b><font color="#990000">,</font> parameter <b><font color="#0000FF">for</font></b> check_id <font color="#009900">$check_id</font> is <font color="#FF0000">{</font><font color="#009900">$param_name</font><font color="#FF0000">}}</font>
                log ERROR <font color="#FF0000">{</font>check<font color="#990000">::</font>to_string<font color="#990000">:</font> Check <b><font color="#0000FF">info</font></b> is <font color="#FF0000">{</font><font color="#009900">$check_type</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$check_match</font><font color="#FF0000">}</font> <font color="#FF0000">{</font><font color="#009900">$check_flags</font><font color="#FF0000">}}</font>
                <b><font color="#0000FF">return</font></b>
        <font color="#FF0000">}</font>

        lassign $<font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS<font color="#990000">(</font><font color="#009900">$param_name</font><font color="#990000">)</font> <font color="#990000">-</font> p_num_args <font color="#990000">-</font> p_param_type <font color="#990000">-</font>

        <b><font color="#0000FF">set</font></b> param_values <font color="#990000">[</font><b><font color="#0000FF">expr</font></b> <font color="#FF0000">{</font><font color="#009900">$param_name</font> <font color="#990000">==</font> <font color="#FF0000">{</font><font color="#990000">-</font>type<font color="#FF0000">}</font> <font color="#990000">?</font> <font color="#990000">[</font><b><font color="#0000FF">split</font></b> <font color="#009900">$check_match</font> <font color="#FF0000">{}</font><font color="#990000">]</font> <font color="#990000">:</font> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$check_match</font><font color="#990000">]</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> param_value <font color="#009900">$param_values</font> <font color="#FF0000">{</font>
                <i><font color="#9A1900"># If this is not one of the types that can have multiple parameters then</font></i>
                <i><font color="#9A1900"># use the parameter name as the check itself.</font></i>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#FF0000">{</font>type platform<font color="#FF0000">}</font> <font color="#009900">$p_param_type</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$p_num_args</font> <font color="#990000">==</font> <font color="#993399">0</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> check <font color="#009900">$param_name</font>
                        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">lappend</font></b> check <font color="#009900">$param_name</font> <font color="#990000">\</font><font color="#FF0000">{</font><font color="#009900">$param_value</font><font color="#990000">\</font><font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                        <b><font color="#0000FF">continue</font></b>
                <font color="#FF0000">}</font>
                <i><font color="#9A1900"># Apply a reverse lookup by looping through all possible parameters and for</font></i>
                <i><font color="#9A1900"># each parameter that has the same parameter type check if the default value</font></i>
                <i><font color="#9A1900"># matches or not. If it does then we have identified the check we are using.</font></i>
                <b><font color="#0000FF">foreach</font></b> p $<font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS<font color="#990000">(</font>parameters<font color="#990000">)</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> $<font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS<font color="#990000">(</font><font color="#009900">$p</font><font color="#990000">)</font> <font color="#993399">3</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#009900">$p_param_type</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> $<font color="#990000">::</font>rule<font color="#990000">::</font>PARAMS<font color="#990000">(</font><font color="#009900">$p</font><font color="#990000">)</font> <font color="#993399">2</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#009900">$param_value</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                                        <b><font color="#0000FF">lappend</font></b> check <font color="#009900">$p</font>
                                        <b><font color="#0000FF">break</font></b>
                                <font color="#FF0000">}</font>
                        <font color="#FF0000">}</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$check_type</font> <font color="#009900">$check</font> <font color="#009900">$match</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1457012572902" ID="ID_1457012572902157" MODIFIED="1457012572902" TEXT="drop_it_action 1.0" POSITION="right" COLOR="#a72db7" FOLDED="true">
<node CREATED="1457012572902" ID="ID_1457012572902496" MODIFIED="1457012572902" TEXT="requires" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572902" ID="ID_1457012572902695" MODIFIED="1457012572902" TEXT="tapp 1.0"/>
<node CREATED="1457012572902" ID="ID_1457012572902829" MODIFIED="1457012572902" TEXT="exec_utils 1.0"/>
<node CREATED="1457012572902" ID="ID_1457012572902960" MODIFIED="1457012572902" TEXT="drop_it_destination 1.0" LINK="#ID_1457012572381190"/>
<node CREATED="1457012572903" ID="ID_1457012572903086" MODIFIED="1457012572903" TEXT="md5"/>
</node>
<node CREATED="1457012572903" ID="ID_1457012572903322" MODIFIED="1457012572903" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572904" ID="ID_1457012572904125" MODIFIED="1457012572904" TEXT="action.tcl" LINK="https://bitbucket.org/bakkeby/drop-it/src/master/action.tcl"/>
</node>
<node CREATED="1457012572904" ID="ID_1457012572904326" MODIFIED="1457012572904" TEXT="action" COLOR="#ff8300" FOLDED="true">
<node CREATED="1457012572904" ID="ID_1457012572904716" MODIFIED="1457012572904" TEXT="action::add" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572905" ID="ID_1457012572905099" MODIFIED="1457012572905" TEXT="Loop through existing actions and return the associated action_id if found,&#xA;otherwise add the new action and return the action_id.&#xA;&#xA;Match actions:&#xA;  --exec           - executes a given script / command&#xA;  --date           - prefixes files with the file timestamp&#xA;  --rename         - renames files based on predefined regular expressions&#xA;  --move           - moves files to given directory&#xA;  --stop           - stops processing of the given file (no further rules checked)&#xA;  --recurse        - recurses a directory, re-processing files within&#xA;  --delete         - deletes a file (or moves to trash)" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572905" ID="ID_1457012572905410" MODIFIED="1457012572905" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572915" ID="ID_1457012572915336" MODIFIED="1457012572915">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>add <font color="#FF0000">{</font> action destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> destination_id <font color="#990000">[</font>destination add <font color="#009900">$destination</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> entry <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$action</font> <font color="#009900">$destination_id</font> <font color="#009900">$flags</font><font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> action_id <font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#990000">-</font>exact $<font color="#990000">::</font>ACTION <font color="#009900">$entry</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># If no match then add the new action</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$action_id</font> <font color="#990000">==</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> action_id <font color="#990000">[</font><b><font color="#0000FF">llength</font></b> $<font color="#990000">::</font>ACTION<font color="#990000">]</font>
                <b><font color="#0000FF">lappend</font></b> <font color="#990000">::</font>ACTION <font color="#009900">$entry</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#009900">$action_id</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572915" ID="ID_1457012572915927" MODIFIED="1457012572915" TEXT="action::date_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572916" ID="ID_1457012572916290" MODIFIED="1457012572916" TEXT="Pre-pends a file name with the file's modification time." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572916" ID="ID_1457012572916638" MODIFIED="1457012572916" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572927" ID="ID_1457012572927185" MODIFIED="1457012572927">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>date_action <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> file_name <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file</font><font color="#990000">]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$destination</font> <font color="#990000">==</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> destination <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># Do not add date if it already exists</font></i>
        <b><font color="#0000FF">set</font></b> date_format <font color="#990000">[</font>cfg get FILE_DATE_PREFIX_FORMAT <font color="#FF0000">{</font><font color="#990000">%</font>Y<font color="#990000">.%</font>m<font color="#990000">.%</font>d<font color="#990000">.</font><font color="#FF0000">}</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">regexp</font></b> <font color="#990000">--</font> <font color="#990000">[</font>text_utils<font color="#990000">::</font>convert_clock_format_to_regexp <font color="#009900">$date_format</font><font color="#990000">]</font> <font color="#009900">$file_name</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> <b><font color="#0000FF">time</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> mtime <font color="#009900">$file</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> date_prefix <font color="#990000">[</font><b><font color="#0000FF">clock</font></b> <b><font color="#0000FF">format</font></b> <font color="#009900">$time</font> <font color="#990000">-</font><b><font color="#0000FF">format</font></b> <font color="#009900">$date_format</font><font color="#990000">]</font>
                prepend file_name $<font color="#FF0000">{</font>date_prefix<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> dest <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$destination</font> <font color="#009900">$file_name</font><font color="#990000">]</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>move <font color="#009900">$file</font> <font color="#009900">$dest</font> <font color="#009900">$flags</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572927" ID="ID_1457012572927787" MODIFIED="1457012572927" TEXT="action::delete_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572928" ID="ID_1457012572928163" MODIFIED="1457012572928" TEXT="Physically removes a given file unless a destination is provided&#xA;in which case the delete is regarded as a &quot;move to trash / recycle bin&quot;." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572928" ID="ID_1457012572928498" MODIFIED="1457012572928" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572940" ID="ID_1457012572940352" MODIFIED="1457012572940">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>delete_action <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> Unable to delete non<font color="#990000">-</font>existing <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> NOT_OK <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#990000">--</font>if_empty<font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font> <font color="#990000">&amp;&amp;</font> <font color="#990000">![</font>file_utils<font color="#990000">::</font>is_empty <font color="#009900">$file</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log DEBUG <font color="#FF0000">{</font> <font color="#990000">-</font> Not deleting <font color="#009900">$file</font> as it is not empty<font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> NOT_OK <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <i><font color="#9A1900"># If we are deleting and we have a destination, presume that we are moving to trash</font></i>
        <i><font color="#9A1900"># rather than explicitly deleting the file.</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$destination</font> <font color="#990000">!=</font> <font color="#FF0000">{}}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>move_action <font color="#009900">$file</font> <font color="#009900">$destination</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> outcome <font color="#990000">[</font>file_utils<font color="#990000">::</font>delete_file <font color="#009900">$file</font><font color="#990000">]</font>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$outcome</font> <font color="#990000">!=</font> <font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font> <font color="#990000">-</font> File <font color="#009900">$file</font> not deleted<font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> map <font color="#FF0000">{</font><font color="#993399">1</font> OK <font color="#993399">0</font> NOT_OK<font color="#FF0000">}</font> <font color="#009900">$outcome</font><font color="#990000">]</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572940" ID="ID_1457012572940954" MODIFIED="1457012572940" TEXT="action::exec_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572941" ID="ID_1457012572941317" MODIFIED="1457012572941" TEXT="Attempts to execute whatever is in the destination variable." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572941" ID="ID_1457012572941654" MODIFIED="1457012572941" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572951" ID="ID_1457012572951670" MODIFIED="1457012572951">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>exec_action <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>exec_utils<font color="#990000">::</font>execute <font color="#009900">$destination</font><font color="#990000">]</font>
                <b><font color="#0000FF">set</font></b> code OK
        <font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> code NOT_OK
        <font color="#FF0000">}</font>
        <i><font color="#9A1900"># Currently we do not let the executable change the input file, so</font></i>
        <i><font color="#9A1900"># returning {} here. See rule::action for how the return value is</font></i>
        <i><font color="#9A1900"># used. It might be possible to allow the exec action to override</font></i>
        <i><font color="#9A1900"># the input by introducing a special flag, but it would depend on</font></i>
        <i><font color="#9A1900"># that the output of the executable is the new file name and the</font></i>
        <i><font color="#9A1900"># new file name alone.</font></i>
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> <font color="#009900">$code</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572952" ID="ID_1457012572952266" MODIFIED="1457012572952" TEXT="action::get_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572952" ID="ID_1457012572952647" MODIFIED="1457012572952" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572962" ID="ID_1457012572962686" MODIFIED="1457012572962">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>get_action <font color="#FF0000">{</font> action_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> $<font color="#990000">::</font>ACTION <font color="#009900">$action_id</font><font color="#990000">]</font> <font color="#993399">0</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572963" ID="ID_1457012572963285" MODIFIED="1457012572963" TEXT="action::get_destination" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572963" ID="ID_1457012572963685" MODIFIED="1457012572963" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572973" ID="ID_1457012572973427" MODIFIED="1457012572973">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>get_destination <font color="#FF0000">{</font> action_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>destination get <font color="#990000">[</font>action<font color="#990000">::</font>get_destination_id <font color="#009900">$action_id</font><font color="#990000">]]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572974" ID="ID_1457012572974012" MODIFIED="1457012572974" TEXT="action::get_destination_id" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572974" ID="ID_1457012572974455" MODIFIED="1457012572974" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572984" ID="ID_1457012572984593" MODIFIED="1457012572984">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>get_destination_id <font color="#FF0000">{</font> action_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> $<font color="#990000">::</font>ACTION <font color="#009900">$action_id</font><font color="#990000">]</font> <font color="#993399">1</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572985" ID="ID_1457012572985185" MODIFIED="1457012572985" TEXT="action::get_flag" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572985" ID="ID_1457012572985598" MODIFIED="1457012572985" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012572997" ID="ID_1457012572997782" MODIFIED="1457012572997">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>get_flag <font color="#FF0000">{</font> action_id <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> <font color="#990000">[</font><b><font color="#0000FF">lindex</font></b> $<font color="#990000">::</font>ACTION <font color="#009900">$action_id</font><font color="#990000">]</font> <font color="#993399">2</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012572998" ID="ID_1457012572998412" MODIFIED="1457012572998" TEXT="action::move" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012572998" ID="ID_1457012572998842" MODIFIED="1457012572998" TEXT="This proc does the actual moving of files.&#xA;&#xA;It is used by other procedures like move_action, date_action and rename_action." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012572999" ID="ID_1457012572999199" MODIFIED="1457012572999" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012573012" ID="ID_1457012573013001" MODIFIED="1457012573013">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>move <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> dest flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$dest</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">file</font></b> stat <font color="#009900">$file</font> FILE
                <b><font color="#0000FF">file</font></b> stat <font color="#009900">$dest</font> DEST

                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">-</font>suffix<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font> <font color="#990000">-</font> <font color="#009900">$file</font> exists at destination <font color="#990000">(</font><font color="#009900">$dest</font><font color="#990000">)</font> deleting instead<font color="#FF0000">}</font>
                        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>delete_action <font color="#009900">$file</font> <font color="#FF0000">{}</font> <font color="#FF0000">{}</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$FILE</font><font color="#990000">(</font>dev<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$DEST</font><font color="#990000">(</font>dev<font color="#990000">)</font> <font color="#990000">&amp;&amp;</font> <font color="#009900">$FILE</font><font color="#990000">(</font>ino<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$DEST</font><font color="#990000">(</font>ino<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font> <font color="#990000">-</font> Not moving <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font> to same location<font color="#FF0000">}</font>
                        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> NOT_OK <font color="#009900">$file</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$FILE</font><font color="#990000">(</font>mtime<font color="#990000">)</font> <font color="#990000">&gt;</font> <font color="#009900">$DEST</font><font color="#990000">(</font>mtime<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font>Older <b><font color="#0000FF">file</font></b> already exists at destination <font color="#990000">(</font><font color="#009900">$dest</font><font color="#990000">),</font> replacing<font color="#FF0000">}</font>
                        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>move_action <font color="#009900">$dest</font> <font color="#990000">[</font>cfg get DEF_TRASH<font color="#990000">]</font> <font color="#990000">-</font>suffix<font color="#990000">]</font>
                        log DEBUG <font color="#FF0000">{</font>Replace output was<font color="#990000">:</font> <font color="#009900">$output</font><font color="#FF0000">}</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#009900">$FILE</font><font color="#990000">(</font>size<font color="#990000">)</font> <font color="#990000">==</font> <font color="#009900">$DEST</font><font color="#990000">(</font>size<font color="#990000">)</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font> <font color="#990000">-</font> Duplicate exists at destination <font color="#990000">(</font><font color="#009900">$dest</font><font color="#990000">),</font> moving to trash instead<font color="#FF0000">}</font>
                        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>move_action <font color="#009900">$file</font> <font color="#990000">[</font>cfg get DEF_TRASH<font color="#990000">]</font> <font color="#990000">-</font>suffix<font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        log WARN <font color="#FF0000">{</font> <font color="#990000">-</font> Newer <b><font color="#0000FF">file</font></b> already exists at destination <font color="#990000">(</font><font color="#009900">$dest</font><font color="#990000">),</font> moving to trash instead<font color="#FF0000">}</font>
                        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>move_action <font color="#009900">$file</font> <font color="#990000">[</font>cfg get DEF_TRASH<font color="#990000">]</font> <font color="#990000">-</font>suffix<font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font>cfg enabled DRY_RUN<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> dest_dir <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$dest</font><font color="#990000">]</font>
                log DEBUG <font color="#FF0000">{</font> <font color="#990000">-</font> Pretended to move <font color="#009900">$file</font> to <font color="#009900">$dest_dir</font> <font color="#990000">(</font>dry run<font color="#990000">)</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">elseif</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">catch</font></b> <font color="#FF0000">{</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">rename</font></b> <font color="#009900">$file</font> <font color="#009900">$dest</font><font color="#FF0000">}</font> msg<font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log ERROR <font color="#FF0000">{</font> <font color="#990000">-</font> Failed to <b><font color="#0000FF">rename</font></b> <font color="#009900">$file</font> to <font color="#009900">$dest</font> due to <font color="#009900">$msg</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> NOT_OK <font color="#009900">$dest</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> dest_dir <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$dest</font><font color="#990000">]</font>
                log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> Moved <font color="#009900">$file</font> to <font color="#009900">$dest_dir</font><font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> OK <font color="#009900">$dest</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012573013" ID="ID_1457012573013702" MODIFIED="1457012573013" TEXT="action::move_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012573014" ID="ID_1457012573014178" MODIFIED="1457012573014" TEXT="Moves a file to a given destination.&#xA;&#xA;If the destination directory does not already exists the script&#xA;will attempt to create it automatically.&#xA;&#xA;This assumes that the destination is a directory." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012573014" ID="ID_1457012573014573" MODIFIED="1457012573014" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012573026" ID="ID_1457012573026865" MODIFIED="1457012573026">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>move_action <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        file_utils<font color="#990000">::</font>ensure_exists <font color="#009900">$destination</font>
        <b><font color="#0000FF">set</font></b> dest <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#009900">$destination</font> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file</font><font color="#990000">]]</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">-</font>suffix<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">append</font></b> dest <font color="#990000">[</font>drop_it<font color="#990000">::</font>generate_dropit_suffix <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font>move <font color="#009900">$file</font> <font color="#009900">$dest</font> <font color="#009900">$flags</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012573027" ID="ID_1457012573027489" MODIFIED="1457012573027" TEXT="action::recurse_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012573027" ID="ID_1457012573027891" MODIFIED="1457012573027" TEXT="Retrieves all readable files and directories within a given directory and processes them." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012573028" ID="ID_1457012573028240" MODIFIED="1457012573028" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012573039" ID="ID_1457012573039214" MODIFIED="1457012573039">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>recurse_action <font color="#FF0000">{</font> directory destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        touch types files

        <i><font color="#9A1900"># Glob Types:</font></i>
        <i><font color="#9A1900">#    b (block special file)</font></i>
        <i><font color="#9A1900">#    c (character special file)</font></i>
        <i><font color="#9A1900">#    d (directory)</font></i>
        <i><font color="#9A1900">#    f (plain file)</font></i>
        <i><font color="#9A1900">#    l (symbolic link)</font></i>
        <i><font color="#9A1900">#    p (named pipe)</font></i>
        <i><font color="#9A1900">#    s (socket)</font></i>
        <i><font color="#9A1900">#</font></i>
        <i><font color="#9A1900">#    r (readable)</font></i>
        <i><font color="#9A1900">#    w (writable)</font></i>
        <i><font color="#9A1900">#    x (special permissions)</font></i>
        <i><font color="#9A1900">#</font></i>
        <b><font color="#0000FF">regexp</font></b> <font color="#990000">--</font> <font color="#FF0000">{</font><font color="#990000">-</font>types <font color="#990000">[\</font><font color="#FF0000">"</font><font color="#CC33CC">\{\(\'\[</font><font color="#FF0000">]([bcdflpsfdrwx ]+)[</font><font color="#CC33CC">\"\}\)\'\]</font><font color="#FF0000">]} $flags match types]</font>
<font color="#FF0000">        if {$types == ""} {</font>
<font color="#FF0000">                set types {f d r}</font>
<font color="#FF0000">        }</font>
<font color="#FF0000">        # List all readable files and directories.</font>
<font color="#FF0000">        # Links are not included.</font>
<font color="#FF0000">        if {[catch {</font>
<font color="#FF0000">                set files [glob -types $types -path [file normalize $directory]/ *]</font>
<font color="#FF0000">        } msg]} {</font>
<font color="#FF0000">                log WARN {No files found while recursing directory $directory}</font>
<font color="#FF0000">        }</font>

<font color="#FF0000">        if {$files != {}} {</font>
<font color="#FF0000">                log INFO {Processing files and directories within $directory}</font>
<font color="#FF0000">                log INFO { - $files}</font>

<font color="#FF0000">                drop_it::process_file_args $files</font>
<font color="#FF0000">        }</font>

<font color="#FF0000">        if {[cfg enabled DELETE_DIRECTORY_IF_EMPTY_AFTER_RECURSE 1]} {</font>
<font color="#FF0000">                if {[file_utils::is_empty $directory]} {</font>
<font color="#FF0000">                        log INFO {Deleting empty directory $directory after recurse}</font>
<font color="#FF0000">                        file delete $directory</font>
<font color="#FF0000">                }</font>
<font color="#FF0000">        }</font>

<font color="#FF0000">        return [list OK]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012573039" ID="ID_1457012573039831" MODIFIED="1457012573039" TEXT="action::rename_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012573040" ID="ID_1457012573040257" MODIFIED="1457012573040" TEXT="Proc to rename file names making them follow a certain standard.&#xA;&#xA;The list needs to be of the following format:&#xA;   {-nocase?} {match} {replace with}" COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012573040" ID="ID_1457012573040607" MODIFIED="1457012573040" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012573054" ID="ID_1457012573054331" MODIFIED="1457012573054">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>rename_action <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        <b><font color="#0000FF">set</font></b> file_name <font color="#990000">[</font><b><font color="#0000FF">file</font></b> tail <font color="#009900">$file</font><font color="#990000">]</font>
        <b><font color="#0000FF">set</font></b> file_ext  <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#990000">[</font><b><font color="#0000FF">file</font></b> extension <font color="#009900">$file</font><font color="#990000">]]</font>
        
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">lsearch</font></b> <font color="#009900">$flags</font> <font color="#FF0000">{</font><font color="#990000">-</font>capitali<font color="#990000">*</font>e<font color="#FF0000">}</font><font color="#990000">]</font> <font color="#990000">!=</font> <font color="#990000">-</font><font color="#993399">1</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font>text_utils<font color="#990000">::</font>string_capitalize <font color="#009900">$file_name</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                <b><font color="#0000FF">set</font></b> output <font color="#009900">$file_name</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">![</font><b><font color="#0000FF">string</font></b> is <b><font color="#0000FF">list</font></b> <font color="#009900">$destination</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log WARN <font color="#FF0000">{</font>Rename replace <b><font color="#0000FF">list</font></b> is not a <b><font color="#0000FF">list</font></b> <font color="#990000">(</font><font color="#009900">$destination</font><font color="#990000">),</font> ignoring action <b><font color="#0000FF">for</font></b> <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font><font color="#FF0000">}</font>
                <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> NOT_OK <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font>

        <b><font color="#0000FF">set</font></b> case_sensitive <font color="#990000">[</font>cfg enabled CASE_SENSITIVE_RENAME_ACTION <font color="#993399">0</font><font color="#990000">]</font>
        <b><font color="#0000FF">foreach</font></b> <font color="#FF0000">{</font>match with<font color="#FF0000">}</font> <font color="#009900">$destination</font> <font color="#FF0000">{</font>
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#009900">$case_sensitive</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#990000">--</font> <font color="#009900">$match</font> <font color="#009900">$output</font> <font color="#009900">$with</font> output
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>nocase <font color="#990000">-</font>all <font color="#990000">--</font> <font color="#009900">$match</font> <font color="#009900">$output</font> <font color="#009900">$with</font> output
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <i><font color="#9A1900"># Changing file extension to be lower case</font></i>
        <b><font color="#0000FF">set</font></b> output <font color="#990000">[</font><b><font color="#0000FF">string</font></b> tolower <font color="#009900">$output</font> <font color="#990000">[</font><b><font color="#0000FF">string</font></b> last <font color="#990000">.</font> <font color="#009900">$output</font><font color="#990000">]</font> end<font color="#990000">]</font>

        <b><font color="#0000FF">set</font></b> dest <font color="#990000">[</font><b><font color="#0000FF">file</font></b> <b><font color="#0000FF">join</font></b> <font color="#990000">[</font><b><font color="#0000FF">file</font></b> dirname <font color="#009900">$file</font><font color="#990000">]</font> <font color="#009900">$output</font><font color="#990000">]</font>
        
        <i><font color="#9A1900"># If no rename necessary</font></i>
        <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#FF0000">{</font><font color="#990000">\\|/</font><font color="#FF0000">}</font> <font color="#009900">$file</font> <font color="#FF0000">{}</font><font color="#990000">]</font> <font color="#990000">==</font> <font color="#990000">[</font><b><font color="#0000FF">regsub</font></b> <font color="#990000">-</font>all <font color="#FF0000">{</font><font color="#990000">\\|/</font><font color="#FF0000">}</font> <font color="#009900">$dest</font> <font color="#FF0000">{}</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> File <font color="#009900">$file_name</font> is clean<font color="#990000">,</font> no <b><font color="#0000FF">rename</font></b> necessary<font color="#FF0000">}</font>
                <b><font color="#0000FF">set</font></b> ret <font color="#990000">[</font><b><font color="#0000FF">list</font></b> OK <font color="#009900">$file</font><font color="#990000">]</font>
        <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> Renaming <b><font color="#0000FF">file</font></b><font color="#990000">:\</font>n   <font color="#990000">&lt;--</font> <font color="#009900">$file_name</font><font color="#990000">\</font>n   <font color="#990000">--&gt;</font> <font color="#009900">$output</font><font color="#FF0000">}</font>
                
                <b><font color="#0000FF">if</font></b> <font color="#FF0000">{</font><font color="#990000">[</font><b><font color="#0000FF">file</font></b> exists <font color="#009900">$dest</font><font color="#990000">]</font><font color="#FF0000">}</font> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> backup_suffix <font color="#990000">[</font>drop_it<font color="#990000">::</font>generate_dropit_suffix<font color="#990000">]</font>
                        <b><font color="#0000FF">set</font></b> new_dest <font color="#009900">$dest$backup_suffix</font>
                        move <font color="#009900">$file</font> <font color="#009900">$new_dest</font> <font color="#009900">$flags</font>
                        <b><font color="#0000FF">set</font></b> ret <font color="#990000">[</font>move <font color="#009900">$new_dest</font> <font color="#009900">$dest</font> <font color="#009900">$flags</font><font color="#990000">]</font>
                <font color="#FF0000">}</font> <b><font color="#0000FF">else</font></b> <font color="#FF0000">{</font>
                        <b><font color="#0000FF">set</font></b> ret <font color="#990000">[</font>move <font color="#009900">$file</font> <font color="#009900">$dest</font> <font color="#009900">$flags</font><font color="#990000">]</font>
                <font color="#FF0000">}</font>
        <font color="#FF0000">}</font>
        
        <b><font color="#0000FF">return</font></b> <font color="#009900">$ret</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
<node CREATED="1457012573054" ID="ID_1457012573055002" MODIFIED="1457012573055" TEXT="action::stop_action" COLOR="#6d8421" FOLDED="true">
<font NAME="SansSerif" SIZE="12" BOLD="true"/>
<node CREATED="1457012573055" ID="ID_1457012573055440" MODIFIED="1457012573055" TEXT="Proc to stop further search and processing." COLOR="#14666b">
<font NAME="Courier New" SIZE="12" BOLD="true"/>
</node>
<node CREATED="1457012573055" ID="ID_1457012573055788" MODIFIED="1457012573055" TEXT="source" FOLDED="true">
<font NAME="SansSerif" SIZE="12" ITALIC="true"/>
<node CREATED="1457012573067" ID="ID_1457012573067782" MODIFIED="1457012573067">
<richcontent TYPE="NODE"><html><body><!-- Generator: GNU source-highlight 3.1.7
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt><b><font color="#0000FF">proc</font></b> action<font color="#990000">::</font>stop_action <font color="#FF0000">{</font> <b><font color="#0000FF">file</font></b> destination flags <font color="#FF0000">}</font> <font color="#FF0000">{</font>

        log INFO <font color="#FF0000">{</font> <font color="#990000">-</font> Stopping due to match on <b><font color="#0000FF">file</font></b> <font color="#009900">$file</font><font color="#FF0000">}</font>

        <b><font color="#0000FF">return</font></b> <font color="#990000">[</font><b><font color="#0000FF">list</font></b> OK <font color="#009900">$file</font><font color="#990000">]</font>
<font color="#FF0000">}</font>
</tt></pre></body></html></richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>

